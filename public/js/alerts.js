function Success() {
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Guardado exitosamente',
        showConfirmButton: false,
        timer: 3000
    })
}

function Solicitud() {
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Solicitud enviada exitosamente',
        showConfirmButton: false,
        timer: 3000
    })
}

function errorAceptarSolicitud() {
    Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Hubo un error',
        showConfirmButton: false,
        timer: 3000
    })
}

function error() {
    Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'No se pudo guardar',
        showConfirmButton: false,
        timer: 3000
    })
}

function ErrorEdit() {
    Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'No se pudo editar',
        showConfirmButton: false,
        timer: 3000
    })
}

function FillData() {
    Swal.fire({
        position: 'center',
        icon: 'warning',
        title: 'Por favor llene los campos ',
        showConfirmButton: false,
        timer: 1000
    })
}


function DoublePlacaEquipo() {
    Swal.fire({
        position: 'center',
        icon: 'warning',
        title: 'Ya existe un equipo con esa placa ',
        showConfirmButton: false,
        timer: 1000
    })
}

function Edit() {
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Actualizado exitosamente',
        showConfirmButton: false,
        timer: 3000
    })

}

function Delete() {
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Eliminado exitosamente',
        showConfirmButton: false,
        timer: 3000
    })
}

function ErrorDelete() {
    Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'No se pudo borrar',
        showConfirmButton: false,
        timer: 3000
    })
}

function Existe() {
    Swal.fire({
        position: 'center',
        icon: 'warning',
        title: 'EL DATO YA EXISTE',
        showConfirmButton: false,
        timer: 2000
    })
}

function IfDataExist() {
    Swal.fire({
        position: 'center',
        icon: 'warning',
        title: 'NO SE PUEDE ELIMINAR. YA EXISTEN DATOS',
        showConfirmButton: false,
        timer: 2000
    })
}