<?php 

	Class MaterialModel{
		private $db;

		public function __construct(){
			$this->db = new Base();
		}

		public function ObtenerMaterial($idMaterial){
			$this->db->query("SELECT * FROM tbl_materiales WHERE tbl_materiales_ID = '$idMaterial' AND tbl_materiales_ESTADO = 1");
            $resultado = $this->db->registro();
            return $resultado;
		}
		
		public function ObtenerMaterial2($idMaterial){
			$this->db->query("SELECT * FROM tbl_materiales WHERE tbl_materiales_ID = '$idMaterial'");
            $resultado = $this->db->registro();
            return $resultado;
		}
		
		public function MostrarMaterialesSolicitados($idPersona, $idSede){
		    $this->db->query("SELECT * FROM tbl_solicitudes WHERE id_sede = '$idSede' AND id_persona = '$idPersona' AND tipo = 'material' ORDER BY fecha DESC");
		    $result = $this->db->registros();
		    return $result;
		}

		public function ListarMaterial($idSede){
			$this->db->query("SELECT tbl_regional.tbl_regional_ID, tbl_regional.tbl_regional_NOMBRE, tbl_centro.tbl_centro_ID,  tbl_centro.tbl_centro_NOMBRE, tbl_sede.tbl_sede_ID, tbl_sede.tbl_sede_NOMBRE, tbl_materiales.* FROM tbl_materiales INNER JOIN tbl_sede ON tbl_sede.tbl_sede_ID = tbl_materiales.tbl_sede_tbl_sede_ID INNER JOIN tbl_centro ON tbl_centro.tbl_centro_ID = tbl_sede.tbl_centro_tbl_centro_ID INNER JOIN tbl_regional ON tbl_regional.tbl_regional_ID = tbl_centro.tbl_regional_tbl_regional_ID WHERE tbl_materiales_ESTADO  = 1 AND tbl_sede.tbl_sede_ID = '$idSede' ORDER BY tbl_materiales_FECHA DESC");
			return $resultado = $this->db->registros();
		}
		
		public function SolicitarMateriales($idSede){
		    $this->db->query("SELECT M.tbl_materiales_ID, tbl_materiales_RUBRO rubro, M.tbl_materiales_UNSPSC, M.tbl_materiales_DESCRIPCION, M.tbl_materiales_PROGRAMAFORMACION, M.tbl_materiales_CANTIDAD FROM tbl_materiales M INNER JOIN tbl_sede S ON M.tbl_sede_tbl_sede_ID = S.tbl_sede_ID WHERE S.tbl_sede_ID = '$idSede' AND M.tbl_materiales_CANTIDAD > 0 AND M.tbl_materiales_ESTADO = 1");
		    $result = $this->db->registros();
		    return $result;
		}
		
		public function CantidadMateriales($id){
		    $this->db->query("SELECT tbl_materiales_CANTIDAD FROM tbl_materiales WHERE tbl_materiales_ID = '$id' AND tbl_materiales_ESTADO = 1");
		    $result = $this->db->registro();
		    return $result;
		}
		

		public function RegistrarMaterial($datos){
			$this->db->query("INSERT INTO tbl_materiales(tbl_materiales_ID, tbl_materiales_CODIGOSENA, tbl_materiales_UNSPSC, tbl_materiales_DESCRIPCION, tbl_materiales_PROGRAMAFORMACION, tbl_materiales_CANTIDAD, cantidad_total, tbl_materiales_TIPOMATERIAL,  tbl_materiales_UNIDADMEDIDA, tbl_materiales_DESTINO, tbl_sede_tbl_sede_ID,  tbl_materiales_ESTADO, tbl_materiales_RUBRO) VALUES (NULL, :materialCodigo, :materialUnspsc, :materialDescripcion, :materialPrograma, :materialCantidad, :cantidad_total, :materialTipo, :materialUnidadMedida, :materialDestino, :materialSede, 1, :rubroPresupuestal)");
			//$this->db->bind(':materialFecha', $datos['materialFecha']);
			$this->db->bind(':materialSede', $datos['materialSede']);
			$this->db->bind(':materialCodigo', $datos['materialCodigo']);
			$this->db->bind(':materialUnspsc', $datos['materialUnspsc']);
			$this->db->bind(':materialDescripcion' ,$datos['materialDescripcion']);
			$this->db->bind(':materialPrograma', $datos['materialPrograma']);
			$this->db->bind(':materialCantidad', $datos['materialCantidad']);
			$this->db->bind(':cantidad_total' , $datos['materialCantidad']);
			$this->db->bind(':materialTipo' , $datos['materialTipo']);
			$this->db->bind(':materialUnidadMedida', $datos['materialUnidadMedida']);
			$this->db->bind(':materialDestino' , $datos['materialDestino']);
			$this->db->bind(':rubroPresupuestal', $datos['rubroPresupuestal']);
			if($this->db->execute()){ return true; }else{ return false;}
		}

		public function EditarMaterial($datos) {
			$this->db->query("UPDATE tbl_materiales SET tbl_materiales_UNSPSC = :materialUnspsc, tbl_materiales_DESCRIPCION = :materialDescripcion, tbl_materiales_PROGRAMAFORMACION = :materialPrograma, tbl_materiales_CANTIDAD = :materialCantidad, cantidad_total = :cantidad_total, tbl_materiales_TIPOMATERIAL = :materialTipo, tbl_materiales_UNIDADMEDIDA = :materialUnidadMedida, tbl_materiales_DESTINO = :materialDestino, tbl_materiales_FECHA = :materialFecha, tbl_materiales_RUBRO = :rubroPresupuestal WHERE tbl_materiales_ESTADO = 1 AND tbl_materiales_ID = :idMaterial AND tbl_sede_tbl_sede_ID = :idSede");
			$this->db->bind(':idMaterial', $datos['idMaterial']);
			$this->db->bind(':materialUnspsc', $datos['materialUnspsc']);
			$this->db->bind(':materialDescripcion' ,$datos['materialDescripcion']);
			$this->db->bind(':materialPrograma', $datos['materialPrograma']);
			$this->db->bind(':materialCantidad', $datos['materialCantidad']);
			$this->db->bind(':cantidad_total' , $datos['materialCantidad']);
			$this->db->bind(':materialTipo' , $datos['materialTipo']);
			$this->db->bind(':materialUnidadMedida', $datos['materialUnidadMedida']);
			$this->db->bind(':materialDestino' , $datos['materialDestino']);
			$this->db->bind(':materialFecha', $datos['materialFecha']);
			$this->db->bind(':idSede' , $datos['idSede']);
			$this->db->bind(':rubroPresupuestal', $datos['rubroPresupuestal']);
			if($this->db->execute()){ return true; }else{ return false;}
		}
		
		public function EditarMaterial2($datos) {
			$this->db->query("UPDATE tbl_materiales SET tbl_materiales_CODIGOSENA = :materialCodigo, tbl_sede_tbl_sede_ID = :materialSede WHERE tbl_materiales_ID = :idMaterial");
			$this->db->bind(':idMaterial', $datos['idMaterial']);
			$this->db->bind(':materialSede', $datos['materialSede']);
			$this->db->bind(':materialCodigo' , $datos['materialCodigo']);
			if($this->db->execute()){ return true; }else{ return false;}
		}

		public function EliminarMaterial($idMaterial, $idSede){
			$this->db->query("UPDATE tbl_materiales SET tbl_materiales_ESTADO = 0 WHERE tbl_materiales_ID = :idMaterial AND tbl_sede_tbl_sede_ID = :idSede AND tbl_materiales_ESTADO = 1");
            $this->db->bind(':idMaterial', $idMaterial);
            $this->db->bind(':idSede', $idSede);
            if($this->db->execute()){ return true; }else{ return false;} 
		}
		
		// Dentro del modelo se crea una función que extraiga los datos de la base de datos que nos sean necesarios mostrar
		public function LoadProgramas($idSede)
		{
		    $this->db->query("SELECT tbl_programa_ID, tbl_programa_NOMBRE FROM tbl_programa WHERE tbl_sede_tbl_sede_ID = '$idSede' AND tbl_programa_ESTADO = 1");
		    $result = $this->db->registros();
		    return $result;
		}
		
		public function ObtenerSede($idMaterial) {
		    $this->db->query("SELECT * FROM tbl_sede WHERE tbl_sede_ESTADO = 1 AND  tbl_centro_tbl_centro_ID = (SELECT tbl_centro.tbl_centro_ID FROM tbl_centro INNER JOIN tbl_sede ON tbl_sede.tbl_centro_tbl_centro_ID = tbl_centro.tbl_centro_ID INNER JOIN tbl_materiales ON tbl_materiales.tbl_sede_tbl_sede_ID = tbl_sede.tbl_sede_ID WHERE tbl_materiales.tbl_materiales_ID = '$idMaterial' AND tbl_materiales.tbl_materiales_ESTADO = 1)");
		    $resultado = $this->db->registros();
		    return $resultado;
		}
		
		public function CompararMaterial($materialSede){
		    $this->db->query("SELECT * FROM tbl_materiales WHERE tbl_sede_tbl_sede_ID ='$materialSede' AND tbl_materiales_ESTADO  = 1");
		    return $resultado = $this->db->registros();
		}
		
		public function CompararMateriales($materialCodigo){
		    $this->db->query("SELECT * FROM tbl_materiales WHERE tbl_materiales_CODIGOSENA ='$materialCodigo' AND tbl_materiales_ESTADO  = 1");
		    $result = $this->db->registros();
		    return $result;
		}
		
		
	}
?>