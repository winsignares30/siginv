<?php

	Class GavetaModel{
		private $db;
		public function __construct(){
			$this->db = new Base();
		}

		public function ObtenerGaveta($idGaveta){
            $this->db->query('SELECT * FROM tbl_gaveta WHERE tbl_gaveta_ID = :idGaveta');
            $this->db->bind(':idGaveta',$idGaveta);
            $row = $this->db->registro();
            return $row;
        }
        
		public function ListarGavetas($idSede){
			$this->db->query("SELECT tbl_gaveta.tbl_gaveta_ID, tbl_gaveta.tbl_gaveta_NUMERO, tbl_gaveta.tbl_gaveta_DESCRIPCION, tbl_gaveta.tbl_gaveta_ESTADO, tbl_gaveta.tbl_estante_tbl_estante_ID, tbl_estante.tbl_estante_NUMERO, tbl_estante.tbl_estante_DESCRIPCION, tbl_bodega.tbl_bodega_NOMBRE, tbl_sede.tbl_sede_NOMBRE, tbl_centro.tbl_centro_NOMBRE, tbl_regional.tbl_regional_NOMBRE FROM tbl_gaveta INNER JOIN tbl_estante ON tbl_estante.tbl_estante_ID  = tbl_gaveta.tbl_estante_tbl_estante_ID INNER JOIN tbl_bodega ON tbl_bodega.tbl_bodega_ID = tbl_estante.tbl_bodega_tbl_bodega_ID INNER JOIN tbl_sede ON tbl_sede.tbl_sede_ID = tbl_bodega.tbl_sede_tbl_sede_ID INNER JOIN tbl_centro ON tbl_centro.tbl_centro_ID = tbl_sede.tbl_centro_tbl_centro_ID INNER JOIN tbl_regional ON tbl_regional.tbl_regional_ID = tbl_centro.tbl_regional_tbl_regional_ID WHERE tbl_gaveta.tbl_gaveta_ESTADO = 1 AND tbl_sede.tbl_sede_ID = '$idSede'");
			$resultado = $this->db->registros();
			return $resultado;
		}

		public function RegistrarGaveta($datos){
			$this->db->query("INSERT INTO tbl_gaveta(tbl_gaveta_ID, tbl_gaveta_NUMERO, tbl_gaveta_DESCRIPCION, tbl_estante_tbl_estante_ID, tbl_gaveta_ESTADO) VALUES (NULL, :gavetaNumero, :gavetaDescripcion, :gavetaEstante, 1)");
			$this->db->bind(':gavetaEstante', $datos['gavetaEstante']);
			$this->db->bind(':gavetaNumero', $datos['gavetaNumero']);
			$this->db->bind(':gavetaDescripcion', $datos['gavetaDescripcion']);
			($this->db->execute()) ? true : false;
		}

		public function EditarGaveta($datos){
			$this->db->query("UPDATE tbl_gaveta SET tbl_gaveta_NUMERO=:gavetaNumero, tbl_gaveta_DESCRIPCION=:gavetaDescripcion, tbl_estante_tbl_estante_ID=:gavetaEstante WHERE tbl_gaveta_ID= :idGaveta");
			$this->db->bind(':idGaveta', $datos['idGaveta']);
			$this->db->bind(':gavetaEstante', $datos['gavetaEstante']);
			$this->db->bind(':gavetaNumero', $datos['gavetaNumero']);
			$this->db->bind(':gavetaDescripcion', $datos['gavetaDescripcion']);
			($this->db->execute()) ? true : false;
		}
		
		public function IfGavetaExiste($idGaveta){
		    $this->db->query("SELECT * FROM tbl_herramienta WHERE tbl_gaveta_tbl_gaveta_ID = '$idGaveta' AND tbl_herramienta_ESTADO = 1");
		    $result = $this->db->registros();
		    return $result;
		}

		public function LoadEstantes($idBodega) {
			$this->db->query("SELECT * FROM tbl_estante INNER JOIN tbl_bodega ON tbl_bodega.tbl_bodega_ID = tbl_estante.tbl_bodega_tbl_bodega_ID WHERE tbl_bodega.tbl_bodega_ID = '$idBodega' AND tbl_estante.tbl_estante_ESTADO = 1");
			$result= $this->db->registros();
			return $result;
		}
		public function DeleteGaveta($idGaveta, $idSede) {
			$this->db->query("UPDATE tbl_gaveta G INNER JOIN tbl_estante E ON E.tbl_estante_ID = G.tbl_estante_tbl_estante_ID INNER JOIN tbl_bodega B ON B.tbl_bodega_ID = E.tbl_bodega_tbl_bodega_ID INNER JOIN tbl_sede S ON S.tbl_sede_ID = B.tbl_sede_tbl_sede_ID SET G.tbl_gaveta_ESTADO = 0 WHERE G.tbl_gaveta_ID = '$idGaveta' AND G.tbl_gaveta_ESTADO = 1 AND S.tbl_sede_ID = '$idSede'");
			($this->db->execute())? true: false;
		}
		
		public function CompararGaveta($gavetaEstante){
		    $this->db->query("SELECT * FROM tbl_gaveta WHERE tbl_estante_tbl_estante_ID = '$gavetaEstante' AND tbl_gaveta_ESTADO = 1");
		    return $result = $this->db->registros();
		}
	}
?>
