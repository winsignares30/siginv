
<?php 

	Class HerramientaModel{
		private $db;

		public function __construct(){
			$this->db = new Base();
		}

        public function ListarHerramienta($idSede){
			$this->db->query("SELECT tbl_estante.tbl_estante_DESCRIPCION, tbl_gaveta_NUMERO, tbl_gaveta_DESCRIPCION, tbl_herramienta_FECHA, tbl_herramienta_NOMBRE, tbl_herramienta_CODIGO, tbl_herramienta_RUBRO, tbl_herramienta.tbl_herramienta_ID, tbl_herramienta.tbl_herramienta_DESCRIPCION, tbl_herramienta.tbl_herramienta_CANTIDAD, tbl_herramienta.tbl_herramienta_ESTADO, tbl_herramienta.tbl_gaveta_tbl_gaveta_ID, tbl_regional.tbl_regional_NOMBRE, tbl_centro.tbl_centro_NOMBRE, tbl_sede.tbl_sede_NOMBRE, tbl_bodega.tbl_bodega_NOMBRE,tbl_estante.tbl_estante_NUMERO FROM tbl_herramienta INNER JOIN tbl_gaveta ON tbl_gaveta.tbl_gaveta_ID  = tbl_herramienta.tbl_gaveta_tbl_gaveta_ID INNER JOIN tbl_estante ON tbl_estante.tbl_estante_ID  = tbl_gaveta.tbl_estante_tbl_estante_ID INNER JOIN tbl_bodega ON tbl_bodega.tbl_bodega_ID = tbl_estante.tbl_bodega_tbl_bodega_ID INNER JOIN tbl_sede ON tbl_sede.tbl_sede_ID = tbl_bodega.tbl_sede_tbl_sede_ID INNER JOIN tbl_centro ON tbl_centro.tbl_centro_ID = tbl_sede.tbl_centro_tbl_centro_ID INNER JOIN tbl_regional ON tbl_regional.tbl_regional_ID = tbl_centro.tbl_regional_tbl_regional_ID WHERE tbl_herramienta.tbl_herramienta_ESTADO = 1 AND tbl_sede.tbl_sede_ID = '$idSede' ORDER BY tbl_herramienta_FECHA DESC");
			return $resultado = $this->db->registros();
		}

		public function SolicitarHerramientas($idSede){
			$this->db->query("SELECT H.tbl_herramienta_ID, H.tbl_herramienta_RUBRO rubro, H.tbl_herramienta_NOMBRE, H.tbl_herramienta_DESCRIPCION, H.tbl_herramienta_CANTIDAD, H.tbl_herramienta_CODIGO FROM tbl_herramienta H INNER JOIN tbl_gaveta G ON G.tbl_gaveta_ID = H.tbl_gaveta_tbl_gaveta_ID 
            INNER JOIN tbl_estante E ON G.tbl_estante_tbl_estante_ID = E.tbl_estante_ID 
            INNER JOIN tbl_bodega B ON E.tbl_bodega_tbl_bodega_ID = B.tbl_bodega_ID
            INNER JOIN tbl_sede S ON B.tbl_sede_tbl_sede_ID = S.tbl_sede_ID WHERE S.tbl_sede_ID = '$idSede'
            AND H.tbl_herramienta_ESTADO = 1 AND H.tbl_herramienta_CANTIDAD > 0 ");
			$result = $this->db->registros();
			return $result;
		}
	
        
		public function ObtenerHerramienta($idHerramienta){
			$this->db->query('SELECT * FROM tbl_herramienta WHERE tbl_herramienta_ID = :idHerramienta');
            $this->db->bind(':idHerramienta' , $idHerramienta);
            $row = $this->db->registro();
            return $row;
		}
		
		public function ObtenerGaveta($idHerramienta){
		    $this->db->query("SELECT * FROM tbl_gaveta WHERE tbl_gaveta_ESTADO = 1 AND tbl_estante_tbl_estante_ID = (SELECT tbl_estante.tbl_estante_ID FROM tbl_estante INNER JOIN tbl_gaveta ON tbl_estante.tbl_estante_ID = tbl_gaveta.tbl_estante_tbl_estante_ID INNER JOIN tbl_herramienta ON tbl_gaveta.tbl_gaveta_ID = tbl_herramienta.tbl_gaveta_tbl_gaveta_ID WHERE tbl_herramienta.tbl_herramienta_ID = :idHerramienta AND tbl_herramienta.tbl_herramienta_ESTADO = 1)");
		    $this->db->bind(':idHerramienta', $idHerramienta);
		    $result = $this->db->registros();
		    return $result;
		}

		public function RegistrarHerramienta($datos){
			$this->db->query("INSERT INTO tbl_herramienta(tbl_herramienta_ID, tbl_herramienta_CODIGO, tbl_herramienta_NOMBRE, tbl_herramienta_DESCRIPCION, tbl_herramienta_CANTIDAD, cantidad_total , tbl_gaveta_tbl_gaveta_ID, tbl_herramienta_RUBRO, tbl_herramienta_ESTADO) VALUES (NULL, :herramientaCodigo, :herramientaNombre, :herramientaDescripcion, :herramientaCantidad, :cantidad_total, :herramientaGaveta, :rubroPresupuestal, 1)");
			$this->db->bind(':herramientaDescripcion', $datos['herramientaDescripcion']);
			$this->db->bind(':herramientaCodigo', $datos['herramientaCodigo']);
			$this->db->bind(':herramientaNombre', $datos['herramientaNombre']);
			$this->db->bind(':herramientaGaveta', $datos['herramientaGaveta']);
			$this->db->bind(':herramientaCantidad', $datos['herramientaCantidad']);
		    $this->db->bind(':cantidad_total', $datos['herramientaCantidad']);
			$this->db->bind(':rubroPresupuestal', $datos['rubroPresupuestal']);
			($this->db->execute()) ? true : false;
		}
		
		public function EditarHerramienta($datos){
		    $this->db->query("UPDATE tbl_herramienta SET tbl_herramienta_CANTIDAD= :herramientaCantidad, cantidad_total = :cantidad_total, tbl_herramienta_FECHA = :herramientaFechadeAdquision, tbl_herramienta_RUBRO = :rubroPresupuestal, tbl_herramienta_CODIGO = :herramientaCodigo WHERE tbl_herramienta_ID = :idHerramienta AND tbl_herramienta_ESTADO=1");
		    $this->db->bind(':idHerramienta', $datos['idHerramienta']);
		    $this->db->bind(':herramientaCantidad', $datos['herramientaCantidad']);
		    $this->db->bind(':cantidad_total', $datos['herramientaCantidad']);
		    $this->db->bind(':herramientaFechadeAdquision', $datos['herramientaFechadeAdquision']);
		    $this->db->bind(':rubroPresupuestal', $datos['rubroPresupuestal']);
		    $this->db->bind(':herramientaCodigo', $datos['herramientaCodigo']);
		    ($this->db->execute()) ?true : false;
		}
		
		public function EditarHerramienta2($datos){
		    $this->db->query("UPDATE tbl_herramienta SET tbl_herramienta_DESCRIPCION = :herraminetaDescripcion, tbl_gaveta_tbl_gaveta_ID = :herramientaGaveta WHERE tbl_herramienta_ID = :idHerramienta AND tbl_herramienta_ESTADO=1");
		    $this->db->bind(':idHerramienta', $datos['idHerramienta']);
		    $this->db->bind(':herraminetaDescripcion', $datos['herraminetaDescripcion']);
		    $this->db->bind(':herramientaGaveta', $datos['herramientaGaveta']);
		    ($this->db->execute()) ?true : false;
		}
		
		public function CantidadHerramienta($id, $idSede){
		    $this->db->query("SELECT H.tbl_herramienta_CANTIDAD FROM tbl_herramienta H
            INNER JOIN tbl_gaveta G ON G.tbl_gaveta_ID = H.tbl_gaveta_tbl_gaveta_ID 
            INNER JOIN tbl_estante E ON G.tbl_estante_tbl_estante_ID = E.tbl_estante_ID 
            INNER JOIN tbl_bodega B ON E.tbl_bodega_tbl_bodega_ID = B.tbl_bodega_ID
            INNER JOIN tbl_sede S ON B.tbl_sede_tbl_sede_ID = S.tbl_sede_ID WHERE S.tbl_sede_ID ='$idSede'  AND tbl_herramienta_ESTADO = 1 AND H.tbl_herramienta_ID = '$id'");
		    $result = $this->db->registro();
		    return $result;
		}
		
		public function totalSolicitudesHerramientas($idSede){
		    $this->db->query("SELECT SL.id_solicitud FROM tbl_solicitudes SL WHERE id_sede = '$idSede' AND tipo = 'herramienta' GROUP BY SL.id_solicitud");
		    $result = $this->db->registros();
		    return $result;
		}
		
		public function totalSolicitudesMateriales($idSede){
		    $this->db->query("SELECT SL.id_solicitud FROM tbl_solicitudes SL WHERE id_sede = '$idSede' AND tipo = 'material' GROUP BY SL.id_solicitud");
		    $result = $this->db->registros();
		    return $result;
		}
		
		public function totalSolicitudesEquipos($idSede){
		    $this->db->query("SELECT SL.id_solicitud FROM tbl_solicitudes SL WHERE id_sede = '$idSede' AND tipo = 'equipo' GROUP BY SL.id_solicitud");
		    $result = $this->db->registros();
		    return $result;
		}

		public function totalHerramientas($idSede){
			$this->db->query("SELECT SUM(H.cantidad_total) total FROM tbl_herramienta H 
            INNER JOIN tbl_gaveta G ON G.tbl_gaveta_ID = H.tbl_gaveta_tbl_gaveta_ID 
            INNER JOIN tbl_estante E ON E.tbl_estante_ID = G.tbl_estante_tbl_estante_ID 
            INNER JOIN tbl_bodega B ON B.tbl_bodega_ID = E.tbl_bodega_tbl_bodega_ID 
            INNER JOIN tbl_sede S ON S.tbl_sede_ID = B.tbl_sede_tbl_sede_ID 
            WHERE H.tbl_herramienta_ESTADO = 1 AND S.tbl_sede_ID = '$idSede'");
			$result = $this->db->registros();
			return $result;
		}

		public function totalEquipos($idSede){
		    $this->db->query("SELECT SUM(E.cantidad_total) total FROM tbl_equipo E WHERE E.tbl_sede_tbl_sede_ID = '$idSede' AND E.tbl_equipo_ESTADO = 1");
		    $result = $this->db->registros();
		    return $result;
		}
		
		public function totalMateriales($idSede){
		    $this->db->query("SELECT SUM(M.cantidad_total) total FROM tbl_materiales M WHERE M.tbl_sede_tbl_sede_ID = '$idSede' AND M.tbl_materiales_ESTADO = 1");
		    $result = $this->db->registros();
		    return $result;
		}
        
		public function HerramientasDisponibles($idSede){
			$this->db->query("SELECT SUM(H.tbl_herramienta_CANTIDAD) as cantidad FROM tbl_herramienta H
            INNER JOIN tbl_gaveta G ON G.tbl_gaveta_ID = H.tbl_gaveta_tbl_gaveta_ID 
            INNER JOIN tbl_estante E ON E.tbl_estante_ID = G.tbl_estante_tbl_estante_ID 
            INNER JOIN tbl_bodega B ON B.tbl_bodega_ID = E.tbl_bodega_tbl_bodega_ID 
            INNER JOIN tbl_sede S ON S.tbl_sede_ID = B.tbl_sede_tbl_sede_ID 
            WHERE H.tbl_herramienta_ESTADO = 1 AND S.tbl_sede_ID = '$idSede'");
			$result = $this->db->registros();
			return $result;
		}
		
		public function MaterialesDisponibles($idSede){
		    $this->db->query("SELECT SUM(M.tbl_materiales_CANTIDAD) cantidad FROM tbl_materiales M WHERE M.tbl_sede_tbl_sede_ID = '$idSede' AND M.tbl_materiales_ESTADO = 1");
		    $result = $this->db->registros();
		    return $result;
		}
		
		public function EquiposDisponibles($idSede){
		    $this->db->query("SELECT SUM(E.tbl_equipo_CANTIDAD) cantidad FROM tbl_equipo E WHERE E.tbl_sede_tbl_sede_ID = '$idSede' AND E.tbl_equipo_ESTADO = 1");
		    $result = $this->db->registros();
		    return $result;
		}
		
		
		public function MostrarHerramientasSolicitadas($idPersona,$idSede){
		    $this->db->query("SELECT * FROM tbl_solicitudes WHERE id_sede = '$idSede' AND id_persona = '$idPersona' AND tipo = 'herramienta' ORDER BY fecha DESC");
		    $result = $this->db->registros();
		    return $result;
		}
		
		//Load Fichas
		public function LoadFichas($idSede)
		{
		    $this->db->query("SELECT * FROM tbl_ficha WHERE tbl_ficha_SEDE = '$idSede' AND tbl_ficha_ESTADO = 1");
		    $result = $this->db->registros();
		    return $result;
		}
		
		
		// Load gavetas
		public function LoadGavetas($idEstante){
		    $this->db->query("SELECT * FROM tbl_gaveta INNER JOIN tbl_estante ON tbl_estante.tbl_estante_ID = tbl_gaveta.tbl_estante_tbl_estante_ID WHERE tbl_estante.tbl_estante_ID = '$idEstante' AND tbl_gaveta.tbl_gaveta_ESTADO = 1");
		    $result= $this->db->registros();
			return $result;
		}
		
		public function CompararHerramienta($herramientaGaveta){
		    $this->db->query("SELECT * FROM tbl_herramienta WHERE tbl_gaveta_tbl_gaveta_ID = '$herramientaGaveta' AND tbl_herramienta_ESTADO = 1");
		    $result = $this->db->registros();
		    return $result;
		}
		
		public function CompararHerramienta2($herramientaCodigo){
		    $this->db->query("SELECT * FROM tbl_herramienta WHERE tbl_herramienta_CODIGO = '$herramientaCodigo' AND tbl_herramienta_ESTADO = 1");
		    $result = $this->db->registros();
		    return $result;
		}

		public function DeleteHerramienta($idHerramienta, $idSede){
			$this->db->query("UPDATE tbl_herramienta H INNER JOIN tbl_gaveta G ON H.tbl_gaveta_tbl_gaveta_ID = G.tbl_gaveta_ID INNER JOIN tbl_estante E ON G.tbl_estante_tbl_estante_ID = E.tbl_estante_ID INNER JOIN tbl_bodega B ON E.tbl_bodega_tbl_bodega_ID = B.tbl_bodega_ID INNER JOIN tbl_sede S ON B.tbl_sede_tbl_sede_ID = S.tbl_sede_ID SET H.tbl_herramienta_ESTADO = 0 WHERE H.tbl_herramienta_ID = :idHerramienta AND S.tbl_sede_ID = '$idSede' ");
			$this->db->bind(':idHerramienta', $idHerramienta);
			($this->db->execute())? true: false;
		}
		
		public function LoadEstantes($idBodega) {
			$this->db->query("SELECT * FROM tbl_estante INNER JOIN tbl_bodega ON tbl_bodega.tbl_bodega_ID = tbl_estante.tbl_bodega_tbl_bodega_ID WHERE tbl_bodega.tbl_bodega_ID = '$idBodega' AND tbl_estante.tbl_estante_ESTADO = 1");
			$result= $this->db->registros();
			return $result;
		}
		
		// function para el carrito
		public function AgregarHerramienta($idHerramienta){
		    $this->db->query("SELECT * FROM tbl_herramienta WHERE tbl_herramienta_ID = '$idHerramienta' AND tbl_herramienta_ESTADO = 1");
		    return $result = $this->db->registros();
		}
		
		//function para las estadisticas TOP 4 Herramientas
		public function TopHerramientas()
		{
			$this->db->query("SELECT nombre, SUM(cantidad) AS cantidad FROM tbl_solicitudes WHERE identificacion = 1 AND tipo = 'herramienta' UNION SELECT nombre, SUM(cantidad) AS cantidad FROM tbl_solicitudes WHERE identificacion = 2 AND tipo = 'herramienta' UNION SELECT nombre, SUM(cantidad) AS cantidad FROM tbl_solicitudes WHERE identificacion = 3 AND tipo = 'herramienta' UNION SELECT nombre, SUM(cantidad) AS cantidad FROM tbl_solicitudes WHERE identificacion = 4 AND tipo = 'herramienta'");
			// $this->db->query("SELECT s.nombre, s.cantidad, s.fecha, s.codigo FROM tbl_solicitudes s WHERE tipo = 'herramienta' AND DATE_FORMAT(s.fecha, '%Y-%m-%d') = CURDATE()");
			$result = $this->db->registros();
			return $result;
		}
		
		public function imprimirReporteSolicitudes(){
		    $fechaInicio = '2022-02-28';
		    $fechaFin = '2022-02-28';
		    $this->db->query("");
		    $result = $this->db->registros();
		    return $result;
		}
		
		// DEVOLUCIONES HERRAMIENTAS
		
		public function devolverHerramienta($datos,$idSede){
		  
		    $this->db->query("SELECT * FROM tbl_solicitudes WHERE id_sede = '$idSede' AND estado = 3 AND id_herramentero = :idPersona AND tipo = :tipo "); //id_persona = :idPersona AND tipo = :tipo 
		    $this->db->bind(':idPersona', $datos['idPersona']);
		    $this->db->bind(':tipo', $datos['tipo']);
		    //echo "su id persona ". $datos['idPersona'];
		    $result = $this->db->registros();
		    return $result;
		}
		
		public function DevolucionHerramientas($datos){
		    $cantidad = $datos['cantidad'];
		    $cantidad2 = $datos['cantidad2'];
		    $cantidadTotal = $cantidad + $cantidad2;
		    $id = $datos['id'];
		    $tipo = $datos['tipo'];
		    
		    if($tipo == 'herramienta'){
		        $this->db->query("UPDATE tbl_herramienta SET tbl_herramienta_CANTIDAD ='$cantidadTotal' WHERE tbl_herramienta_ID = '$id' AND tbl_herramienta_ESTADO = 1");
		        ($this->db->execute())? true: false;
		    }elseif($tipo == 'equipo'){
		        $this->db->query("UPDATE tbl_equipo SET tbl_equipo_CANTIDAD ='$cantidadTotal' WHERE tbl_equipo_ID = '$id' AND tbl_equipo_ESTADO = 1");
		        ($this->db->execute())? true: false;
		    }
		}
		
		public function DevoHerramientas($datos){
		    $idPersona = $datos['idPersona'];
		    $id = $datos['id'];
		    $id2 = $datos['id2'];
		    $tipo = $datos['tipo'];
		     $this->db->query("UPDATE tbl_solicitudes SET estado = 4 WHERE identificacion = '$id' AND id='$id2' AND tipo = '$tipo' AND id_herramentero = '$idPersona'");
		    ($this->db->execute())?true: false;
		}
		
		//APARTE
		public function DevHerramientaUser($id_solicitud){
		    $this->db->query("SELECT * FROM tbl_solicitudes WHERE estado = 3 AND id_solicitud = '$id_solicitud' ");
		    $this->db->bind(':id_solicitud', $datos['id_solicitud']);
		    $result = $this->db->registro();
		    return $result;
		}
		
		public function EditarDevUsuario($datos){
		    $this->db->query("UPDATE tbl_solicitudes SET codigo = :devolucionCodigo, nombre = :devolucionNombre, descripcion = :devolucionDescripcion, cantidad = :devolucionCantidad, persona = :devolucionPersona, estado = 4 WHERE codigo = :devolucionCodigo AND tipo = 'herramienta' ");
		    $this->db->bind(':devolucionCodigo', $datos['devolucionCodigo']);
		    $this->db->bind(':devolucionNombre', $datos['devolucionNombre']);
		    $this->db->bind(':devolucionDescripcion', $datos['devolucionDescripcion']);
		    $this->db->bind(':devolucionCantidad', $datos['devolucionCantidad']);
		    $this->db->bind(':devolucionPersona', $datos['devolucionPersona']);
		    ($this->db->execute()) ?true : false;
		}
		
		public function getRequest($idRequest, $idSede){
            $this->db->query("SELECT * FROM tbl_solicitudes WHERE id_solicitud = '$idRequest' AND id_sede = '$idSede'  ORDER BY fecha DESC");
            $result = $this->db->registros();
            return $result;
        }
	}
?>