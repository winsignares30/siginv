<?php 
    class AmbienteModel{
        private $db;

        public function __construct(){
            $this->db = new Base();
        }
        // List ambiente
        public function ListarAmbiente($idSede){
            $this->db->query("SELECT tbl_regional.tbl_regional_NOMBRE, tbl_centro.tbl_centro_NOMBRE, tbl_sede.tbl_sede_NOMBRE,  A.tbl_amb_ID, A.tbl_amb_NOMBRE, P.tbl_persona_ID, P.tbl_persona_NOMBRES, P.tbl_persona_PRIMERAPELLIDO, P.tbl_persona_SEGUNDOAPELLIDO
            FROM tbl_persona P 
            INNER JOIN tbl_ambiente A ON P.tbl_persona_ID = A.tbl_persona_ID
            INNER JOIN tbl_sede ON A.tbl_sede_tbl_sede_ID = tbl_sede.tbl_sede_ID
            INNER JOIN tbl_centro ON tbl_centro.tbl_centro_ID=tbl_sede.tbl_centro_tbl_centro_ID 
            INNER JOIN tbl_regional ON tbl_centro.tbl_regional_tbl_regional_ID = tbl_regional.tbl_regional_ID
            WHERE tbl_sede.tbl_sede_ID = '$idSede' AND A.tbl_amb_ESTADO = 1 AND P.tbl_persona_ESTADO = 1");
            return $result = $this->db->registros();
        }
        /* function to load centros */
        public function LoadCentros($idRegional) {
            $this->db->query("SELECT * FROM tbl_centro WHERE tbl_regional_tbl_regional_ID = '$idRegional' AND tbl_centro_ESTADO = 1");
            return $result = $this->db->registros();
        }
        
        public function LoadInstructores($idSede){
            $this->db->query("SELECT P.tbl_persona_NOMBRES, P.tbl_persona_PRIMERAPELLIDO, P.tbl_persona_SEGUNDOAPELLIDO, P.tbl_persona_ID FROM tbl_persona P 
            INNER JOIN tbl_usuario U ON P.tbl_persona_ID = U.tbl_persona_tbl_persona_ID
            WHERE P.tbl_persona_ESTADO = 1 AND P.tbl_cargo_tbl_cargo_ID = 3 AND U.tbl_sede = '$idSede'");
            $result = $this->db->registros();
            return $result;
        }

        /* function to load sedes */
        public function LoadSedes($idCentro) {
            $this->db->query("SELECT * FROM tbl_sede WHERE tbl_centro_tbl_centro_ID = '$idCentro' AND tbl_sede_ESTADO = 1");
            return $result= $this->db->registros();
        }

        /*Function to register centros */
        public function RegistrarAmbiente($datos){
            $this->db->query("INSERT INTO tbl_ambiente (tbl_amb_ID, tbl_amb_NOMBRE, tbl_persona_ID,  tbl_sede_tbl_sede_ID, tbl_amb_ESTADO) VALUES (NULL, :ambienteNombre, :ambienteInstructor, :ambienteSede, 1)");
            $this->db->bind(':ambienteNombre', $datos['ambienteNombre']);
            $this->db->bind(':ambienteSede', $datos['ambienteSede']);
            $this->db->bind(':ambienteInstructor', $datos['ambienteInstructor']);
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        /*get ambiente */
        public function ObtenerAmbiente($idAmbiente, $idSede){
            $this->db->query("SELECT A.* FROM tbl_ambiente A
            INNER JOIN tbl_sede S ON S.tbl_sede_ID = A.tbl_sede_tbl_sede_ID WHERE A.tbl_amb_ID = '$idAmbiente' AND A.tbl_amb_ESTADO = 1 AND S.tbl_sede_ID = '$idSede'");
            $result = $this->db->registro();
            return $result;
        }
        public function ObtenerSede($idAmbiente){
            $this->db->query("SELECT * FROM tbl_sede WHERE tbl_sede.tbl_centro_tbl_centro_ID = (SELECT tbl_centro.tbl_centro_ID FROM tbl_ambiente INNER JOIN tbl_sede ON tbl_sede.tbl_sede_ID = tbl_ambiente.tbl_sede_tbl_sede_ID INNER JOIN tbl_centro ON tbl_centro.tbl_centro_ID = tbl_sede.tbl_centro_tbl_centro_ID  WHERE tbl_ambiente.tbl_amb_ID = '$idAmbiente')");
            $result = $this->db->registros();
            return $result;
        }
        
        /*Function to edit */
        public function EditarAmbiente($datos) {
            $this->db->query("UPDATE tbl_ambiente SET tbl_amb_NOMBRE=:ambienteNombre, tbl_persona_ID=:ambienteInstructor WHERE tbl_amb_ID=:idAmbiente AND tbl_sede_tbl_sede_ID = :idSede AND tbl_amb_ESTADO =1");
            $this->db->bind(':idAmbiente', $datos['idAmbiente']);
            $this->db->bind(':idSede', $datos['idSede']);
            $this->db->bind(':ambienteNombre', $datos['ambienteNombre']);
            $this->db->bind(':ambienteInstructor', $datos['ambienteInstructor']);
            ($this->db->execute())? true : false;
			
        }

        // Delete ambiente
        public function DeleteAmbiente($idAmbiente, $idsede) {
            $this->db->query("UPDATE tbl_ambiente SET tbl_amb_ESTADO = 0 WHERE tbl_amb_ID = '$idAmbiente' AND tbl_ambiente.tbl_sede_tbl_sede_ID = '$idsede' AND tbl_ambiente.tbl_amb_ESTADO = 1");
            ($this->db->execute()) ? true : false;
        }
        public function Listarsede($idCentro) {
            $this->db->query("SELECT * tbl_sede WHERE tbl_centro_tbl_centro_ID = '$idCentro' AND tbl_sede_ESTADO = 1");
            $ListarSede = $this->db->registros();
            return $ListarSede;
        }
      
    } 

?>