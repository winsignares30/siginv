<?php
    class PersonaModel{
        private $db;

        public function __construct(){
            $this->db = new Base ();
        }
        // Controlador Listar Administradores
        public function ListarAdministrador($idSede){
            $this->db->query("SELECT tbl_regional.tbl_regional_NOMBRE, tbl_centro.tbl_centro_NOMBRE, tbl_sede.tbl_sede_NOMBRE, tbl_persona.tbl_persona_ID, tbl_persona.tbl_persona_NOMBRES, tbl_persona.tbl_persona_PRIMERAPELLIDO, tbl_persona.tbl_persona_SEGUNDOAPELLIDO, tbl_persona.tbl_persona_FECHANAC, tbl_persona.tbl_tipodocumento_tbl_tipodocumento_ID, tbl_persona.tbl_persona_NUMDOCUMENTO, tbl_persona.tbl_persona_TELEFONO, tbl_persona.tbl_persona_CORREO, tbl_cargo.tbl_cargo_TIPO, tbl_persona.tbl_persona_DIRECCION, tbl_persona.tipo_contrato FROM tbl_persona INNER JOIN tbl_usuario ON tbl_usuario.tbl_persona_tbl_persona_ID = tbl_persona.tbl_persona_ID INNER JOIN tbl_sede ON tbl_usuario.tbl_sede = tbl_sede.tbl_sede_ID INNER JOIN tbl_cargo ON tbl_persona.tbl_cargo_tbl_cargo_ID = tbl_cargo.tbl_cargo_ID INNER JOIN tbl_centro ON tbl_centro.tbl_centro_ID = tbl_sede.tbl_centro_tbl_centro_ID INNER JOIN tbl_regional ON tbl_regional.tbl_regional_ID = tbl_centro.tbl_regional_tbl_regional_ID  WHERE tbl_persona.tbl_persona_ESTADO = 1 AND tbl_usuario.tbl_sede = '$idSede' AND tbl_persona.tbl_cargo_tbl_cargo_ID = 1 AND tbl_usuario.tbl_usuario_USERNAME = tbl_persona.tbl_persona_CORREO");
            $resultados = $this->db->registros();
            return $resultados;
        }
    
        //SELECT P.tbl_persona_ID FROM tbl_persona P ORDER BY P.tbl_persona_ID DESC LIMIT 1
        public function contadorRegistros(){
            $this->db->query("SELECT P.tbl_persona_ID FROM tbl_persona P ORDER BY P.tbl_persona_ID DESC LIMIT 1"); // SELECT COUNT(*) FROM tbl_persona
            $result = $this->db->registros();
            return $result;
        }
        
        // Controlador Registrar Personal (Administradores, Herramentero, Instructores)
        public function RegistrarAdministradorPersona($datos){
           
            $this->db->query("INSERT INTO tbl_persona (tbl_persona_ID, tbl_persona_NUMDOCUMENTO, tbl_persona_NOMBRES, tbl_persona_PRIMERAPELLIDO, tbl_persona_SEGUNDOAPELLIDO, tbl_persona_FECHANAC, tbl_persona_TELEFONO, tbl_persona_CORREO, tbl_cargo_tbl_cargo_ID, tbl_persona_DIRECCION, tbl_tipodocumento_tbl_tipodocumento_ID, tipo_contrato, tbl_persona_ESTADO) VALUES (NULL, :administradorNumeroDocumento, :administradorNombre, :administradorApellido1, :administradorApellido2, :administradorFechadeNacimiento, :administradorNumeroTelefono, :administradorDireccionCorreo, :administradorCargo, :administradorDireccion, :administradorTipoDocumento, :administradorTipoContrato, 1)");
            
            $this->db->bind(':administradorNumeroDocumento', $datos['administradorNumeroDocumento']);
            $this->db->bind(':administradorNombre', $datos['administradorNombre']);
            $this->db->bind(':administradorApellido1', $datos['administradorApellido1']);
            $this->db->bind(':administradorApellido2', $datos['administradorApellido2']);
            $this->db->bind(':administradorFechadeNacimiento', $datos['administradorFechadeNacimiento']);
            $this->db->bind(':administradorTipoDocumento', $datos['administradorTipoDocumento']);
            $this->db->bind(':administradorNumeroTelefono', $datos['administradorNumeroTelefono']);
            $this->db->bind(':administradorDireccionCorreo', $datos['administradorDireccionCorreo']);
            $this->db->bind(':administradorCargo', $datos['administradorCargo']);
            $this->db->bind(':administradorDireccion', $datos['administradorDireccion']);
            $this->db->bind(':administradorTipoContrato', $datos['administradorTipoContrato']);
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }
        
        public function RegistrarAdministradorUsuario($datos){
            $this->db->query("INSERT INTO tbl_usuario (tbl_usuario_ID, tbl_usuario_USERNAME, tbl_usuario_PASSWORD, tbl_usuario_ESTADO, tbl_sede, tbl_persona_tbl_persona_ID) VALUES (NULL, :administradorDireccionCorreo, :administradorContrasenia, 1, :administradorSedeID, :cantidadRegistros)");
            $this->db->bind(':administradorDireccionCorreo', $datos['administradorDireccionCorreo']);
            $this->db->bind(':administradorContrasenia', $datos['administradorContrasenia']);
            $this->db->bind(':administradorSedeID', $datos['administradorSedeID']);
            $this->db->bind(':cantidadRegistros', $datos['cantidadRegistros']);
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }
        
        // Controlador Obtener Administradores
        public function ObtenerAdministradorId($idAdministrador) {
          $this->db->query("SELECT * FROM tbl_persona  WHERE tbl_persona_ID = :idAdministrador");
          $this->db->bind(':idAdministrador',$idAdministrador);
          $result = $this->db->registros();
          return $result;
        }
        
        
        // Controlador Editar Administradores
        public function EditarAdministrador($datos) {
            $this->db->query("UPDATE tbl_persona P SET P.tbl_persona_NUMDOCUMENTO = :administradorNumeroDocumento, P.tbl_persona_NOMBRES = :administradorNombre, P.tbl_persona_PRIMERAPELLIDO = :administradorApellido1, P.tbl_persona_SEGUNDOAPELLIDO = :administradorApellido2, P.tbl_persona_FECHANAC = :administradorFechadeNacimiento, P.tbl_persona_TELEFONO = :administradorNumeroTelefono, P.tbl_persona_CORREO = :administradorDireccionCorreo, P.tbl_cargo_tbl_cargo_ID = :administradorCargo, P.tbl_persona_DIRECCION = :administradorDireccion, P.tipo_contrato = :administradorTipoContrato, P.tbl_tipodocumento_tbl_tipodocumento_ID = :administradorTipoDocumento WHERE P.tbl_persona_ID = :idAdministrador AND P.tbl_persona_ESTADO = 1");
            $this->db->bind(':idAdministrador', $datos['idAdministrador']);
            $this->db->bind(':administradorNumeroDocumento', $datos['administradorNumeroDocumento']);
            $this->db->bind(':administradorNombre', $datos['administradorNombre']);
            $this->db->bind(':administradorApellido1', $datos['administradorApellido1']);
            $this->db->bind(':administradorApellido2', $datos['administradorApellido2']);
            $this->db->bind(':administradorFechadeNacimiento', $datos['administradorFechadeNacimiento']);
            $this->db->bind(':administradorNumeroTelefono', $datos['administradorNumeroTelefono']);
            $this->db->bind(':administradorDireccionCorreo', $datos['administradorDireccionCorreo']);
            $this->db->bind(':administradorCargo', $datos['administradorCargo']);
            $this->db->bind(':administradorDireccion', $datos['administradorDireccion']);
            $this->db->bind(':administradorTipoContrato', $datos['administradorTipoContrato']);
            $this->db->bind(':administradorTipoDocumento', $datos['administradorTipoDocumento']);
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }
        
        public function EditarAdministradorUsuario($datos) {
            $this->db->query("UPDATE tbl_usuario U INNER JOIN tbl_persona P ON P.tbl_persona_ID = U.tbl_persona_tbl_persona_ID SET U.tbl_usuario_USERNAME = :administradorDireccionCorreo WHERE U.tbl_usuario_ID = :idAdministrador AND P.tbl_persona_ESTADO = 1 ");
            $this->db->bind(':idAdministrador', $datos['idAdministrador']);
            $this->db->bind(':administradorDireccionCorreo', $datos['administradorDireccionCorreo']);
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }
        
        public function EditarAdministrador2($datos) {
            $this->db->query("UPDATE tbl_instructor I INNER JOIN tbl_sede S ON S.tbl_sede_ID = I.tbl_sede_tbl_sede_ID SET I.tbl_instructor_NUMDECUMENTO = :instructorNumeroDocumento , I.tbl_sede_tbl_sede_ID = :instructorSedeID WHERE I.tbl_instructor_ID = :idInstructor");
            $this->db->bind(':idInstructor', $datos['idInstructor']);
            $this->db->bind(':instructorSedeID', $datos['instructorSedeID']);
            $this->db->bind(':instructorNumeroDocumento', $datos['instructorNumeroDocumento']);
            ($this->db->execute())? true : false;
        }

        public function EliminarAdministrador($idAdministrador, $idSede){
            $this->db->query("UPDATE tbl_persona P INNER JOIN tbl_usuario U ON P.tbl_persona_ID = U.tbl_persona_tbl_persona_ID INNER JOIN tbl_sede S ON U.tbl_sede = S.tbl_sede_ID SET P.tbl_persona_ESTADO = 0, U.tbl_usuario_ESTADO = 0 WHERE P.tbl_persona_ID = :idAdministrador AND S.tbl_sede_ID = '$idSede'");//, U.tbl_usuario_ESTADO = 0
            $this->db->bind(':idAdministrador', $idAdministrador);
            //$this->db->bind(':idUsuario', $idUsuario);
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }
        
        public function CompararAdministrador($administradorNombre){
            $this->db->query("SELECT * FROM tbl_persona WHERE tbl_persona_NOMBRES = '$administradorNombre' AND tbl_persona_ESTADO = 1");
            $result = $this->db->registro();
            return $result ;
        }
        
        public function ListarSede($idAdministrador) {
            $this->db->query("SELECT * FROM tbl_sede WHERE tbl_sede.tbl_centro_tbl_centro_ID = (SELECT tbl_centro.tbl_centro_ID FROM tbl_centro INNER JOIN tbl_sede ON tbl_sede.tbl_centro_tbl_centro_ID = tbl_centro.tbl_centro_ID INNER JOIN tbl_usuario ON tbl_usuario.tbl_sede = tbl_sede.tbl_sede_ID INNER JOIN tbl_persona ON tbl_persona.tbl_persona_ID = tbl_usuario.tbl_persona_tbl_persona_ID WHERE tbl_persona.tbl_persona_ID = '$idAdministrador' AND tbl_persona_ESTADO = 1)");
            $result = $this->db->registros();
            return $result;
        }
        
        public function ComprobarAdministrador($idInstructor) {
            $this->db->query("SELECT * FROM tbl_instructor WHERE tbl_instructor_ID = '$idInstructor' AND tbl_instructor_ESTADO = 1");
            $result = $this->db->registros();
            return $result;
        }
        
        /* function to load sedes */
        public function LoadSedes($idCentro) {
            $this->db->query("SELECT * FROM tbl_sede WHERE tbl_centro_tbl_centro_ID = '$idCentro' AND tbl_sede_ESTADO = 1");
            return $result= $this->db->registros();
        }
        
        /* function to load centros */
        public function LoadCentros($idRegional) {
            $this->db->query("SELECT * FROM tbl_centro WHERE tbl_regional_tbl_regional_ID = '$idRegional' AND tbl_centro_ESTADO = 1");
            return $result = $this->db->registros();
        }
        
        //---------- INSTRUCTORES ----------
        
        // Controlador Listar Instructores
        public function ListarInstructor($idSede){
            $this->db->query("SELECT P.*, R.tbl_regional_ID, R.tbl_regional_NOMBRE, C.tbl_centro_ID, C.tbl_centro_NOMBRE, S.tbl_sede_ID, S.tbl_sede_NOMBRE, Cg.tbl_cargo_TIPO FROM tbl_persona P INNER JOIN tbl_cargo Cg ON Cg.tbl_cargo_ID = P.tbl_cargo_tbl_cargo_ID 
            INNER JOIN tbl_usuario U ON U.tbl_persona_tbl_persona_ID = P.tbl_persona_ID INNER JOIN tbl_sede S ON S.tbl_sede_ID = U.tbl_sede INNER JOIN tbl_centro C ON C.tbl_centro_ID = S.tbl_centro_tbl_centro_ID 
            INNER JOIN tbl_regional R ON R.tbl_regional_ID = C.tbl_regional_tbl_regional_ID WHERE P.tbl_persona_ESTADO = 1 AND P.tbl_cargo_tbl_cargo_ID = 3 AND S.tbl_sede_ID = '$idSede' ");
            $result = $this->db->registros();
            return $result;
        }
        
        // Controlador Obtener Instructores
        public function ObtenerInstructor($instructorId){
            $this->db->query("SELECT * FROM tbl_persona WHERE tbl_persona_ID = '$instructorId' AND tbl_persona_ESTADO = 1 AND tbl_cargo_tbl_cargo_ID = 3");
            $result = $this->db->registros();
            return $result;
        }
        
        //Controlador Editar Instructores
        public function EditarInstructor($datos) {
            $this->db->query("UPDATE tbl_persona P SET P.tbl_persona_NUMDOCUMENTO = :instructorDocumento, P.tbl_persona_NOMBRES = :instructorNombre, P.tbl_persona_PRIMERAPELLIDO = :instructorApellido1, P.tbl_persona_SEGUNDOAPELLIDO = :instructorApellido2, P.tbl_persona_FECHANAC = :instructorFechaNacimiento, P.tbl_persona_TELEFONO = :instructorCelular, P.tbl_persona_CORREO = :instructorCorreo, P.tbl_cargo_tbl_cargo_ID = :instructorCargo, P.tbl_persona_DIRECCION = :instructorDireccion, P.tipo_contrato = :instructorTipoContrato, P.tbl_tipodocumento_tbl_tipodocumento_ID = :instructorTipoDocumento WHERE P.tbl_persona_ID = :instructorId AND P.tbl_persona_ESTADO = 1 AND P.tbl_cargo_tbl_cargo_ID = 3");
            $this->db->bind(':instructorId', $datos['instructorId']);
            $this->db->bind(':instructorDocumento', $datos['instructorDocumento']);
            $this->db->bind(':instructorNombre', $datos['instructorNombre']);
            $this->db->bind(':instructorApellido1', $datos['instructorApellido1']);
            $this->db->bind(':instructorApellido2', $datos['instructorApellido2']);
            $this->db->bind(':instructorFechaNacimiento', $datos['instructorFechaNacimiento']);
            $this->db->bind(':instructorCelular', $datos['instructorCelular']);
            $this->db->bind(':instructorCorreo', $datos['instructorCorreo']);
            $this->db->bind(':instructorTipoContrato', $datos['instructorTipoContrato']);
            $this->db->bind(':instructorCargo', $datos['instructorCargo']);
            $this->db->bind(':instructorDireccion', $datos['instructorDireccion']);
            $this->db->bind(':instructorTipoDocumento', $datos['instructorTipoDocumento']);
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }
        //Editar Instructor Usuario
        public function EditarInstructorUsuario($datos) {
            $this->db->query("UPDATE tbl_usuario U INNER JOIN tbl_persona P ON P.tbl_persona_ID = U.tbl_persona_tbl_persona_ID SET U.tbl_usuario_USERNAME = :instructorCorreo WHERE U.tbl_usuario_ID = :instructorId AND P.tbl_persona_ESTADO = 1 ");
            $this->db->bind(':instructorId', $datos['instructorId']);
            $this->db->bind(':instructorCorreo', $datos['instructorCorreo']);
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }
        
        //Eliminar Instructor
        public function EliminarInstructor($instructorId, $idSede){
            $this->db->query("UPDATE tbl_persona P INNER JOIN tbl_usuario U ON P.tbl_persona_ID = U.tbl_persona_tbl_persona_ID INNER JOIN tbl_sede S ON U.tbl_sede = S.tbl_sede_ID SET P.tbl_persona_ESTADO = 0, U.tbl_usuario_ESTADO = 0 WHERE P.tbl_persona_ID = :instructorId AND S.tbl_sede_ID = '$idSede' AND P.tbl_cargo_tbl_cargo_ID = 3");
            $this->db->bind(':instructorId', $instructorId);
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }
        
        
        //---------- HERRAMENTERO ----------
        
        // Controlador Listar Instructores
        public function ListarHerramentero($idSede){
            $this->db->query("SELECT P.*, R.tbl_regional_ID, R.tbl_regional_NOMBRE, C.tbl_centro_ID, C.tbl_centro_NOMBRE, S.tbl_sede_ID, S.tbl_sede_NOMBRE, Cg.tbl_cargo_TIPO FROM tbl_persona P INNER JOIN tbl_cargo Cg ON Cg.tbl_cargo_ID = P.tbl_cargo_tbl_cargo_ID 
            INNER JOIN tbl_usuario U ON U.tbl_persona_tbl_persona_ID = P.tbl_persona_ID INNER JOIN tbl_sede S ON S.tbl_sede_ID = U.tbl_sede INNER JOIN tbl_centro C ON C.tbl_centro_ID = S.tbl_centro_tbl_centro_ID 
            INNER JOIN tbl_regional R ON R.tbl_regional_ID = C.tbl_regional_tbl_regional_ID WHERE P.tbl_persona_ESTADO = 1 AND P.tbl_cargo_tbl_cargo_ID = 2 AND S.tbl_sede_ID = '$idSede' ");
            $result = $this->db->registros();
            return $result;
        }
        
        // Controlador Obtener Instructores
        public function ObtenerHerramentero($herramenteroId){
            $this->db->query("SELECT * FROM tbl_persona WHERE tbl_persona_ID = '$herramenteroId' AND tbl_persona_ESTADO = 1 AND tbl_cargo_tbl_cargo_ID = 2");
            $result = $this->db->registros();
            return $result;
        }
        
        //Controlador Editar Instructores
        public function EditarHerramentero($datos) {
            $this->db->query("UPDATE tbl_persona P SET P.tbl_persona_NUMDOCUMENTO = :herramenteroDocumento, P.tbl_persona_NOMBRES = :herramenteroNombre, P.tbl_persona_PRIMERAPELLIDO = :herramenteroApellido1, P.tbl_persona_SEGUNDOAPELLIDO = :herramenteroApellido2, P.tbl_persona_FECHANAC = :herramenteroFechaNacimiento, P.tbl_persona_TELEFONO = :herramenteroCelular, P.tbl_persona_CORREO = :herramenteroCorreo, P.tbl_cargo_tbl_cargo_ID = :herramenteroCargo, P.tbl_persona_DIRECCION = :herramenteroDireccion, P.tipo_contrato = :herramenteroTipoContrato, P.tbl_tipodocumento_tbl_tipodocumento_ID = :herramenteroTipoDocumento WHERE P.tbl_persona_ID = :herramenteroId AND P.tbl_persona_ESTADO = 1 AND P.tbl_cargo_tbl_cargo_ID = 2");
            $this->db->bind(':herramenteroId', $datos['herramenteroId']);
            $this->db->bind(':herramenteroDocumento', $datos['herramenteroDocumento']);
            $this->db->bind(':herramenteroNombre', $datos['herramenteroNombre']);
            $this->db->bind(':herramenteroApellido1', $datos['herramenteroApellido1']);
            $this->db->bind(':herramenteroApellido2', $datos['herramenteroApellido2']);
            $this->db->bind(':herramenteroFechaNacimiento', $datos['herramenteroFechaNacimiento']);
            $this->db->bind(':herramenteroCelular', $datos['herramenteroCelular']);
            $this->db->bind(':herramenteroCorreo', $datos['herramenteroCorreo']);
            $this->db->bind(':herramenteroTipoContrato', $datos['herramenteroTipoContrato']);
            $this->db->bind(':herramenteroCargo', $datos['herramenteroCargo']);
            $this->db->bind(':herramenteroDireccion', $datos['herramenteroDireccion']);
            $this->db->bind(':herramenteroTipoDocumento', $datos['herramenteroTipoDocumento']);
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }
        //Editar Instructor Usuario
        public function EditarHerramenteroUsuario($datos) {
            $this->db->query("UPDATE tbl_usuario U INNER JOIN tbl_persona P ON P.tbl_persona_ID = U.tbl_persona_tbl_persona_ID SET U.tbl_usuario_USERNAME = :herramenteroCorreo WHERE U.tbl_usuario_ID = :herramenteroId AND P.tbl_persona_ESTADO = 1 ");
            $this->db->bind(':herramenteroId', $datos['herramenteroId']);
            $this->db->bind(':herramenteroCorreo', $datos['herramenteroCorreo']);
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }
        
        //Eliminar Instructor
        public function EliminarHerramentero($herramenteroId, $idSede){
            $this->db->query("UPDATE tbl_persona P INNER JOIN tbl_usuario U ON P.tbl_persona_ID = U.tbl_persona_tbl_persona_ID INNER JOIN tbl_sede S ON U.tbl_sede = S.tbl_sede_ID SET P.tbl_persona_ESTADO = 0, U.tbl_usuario_ESTADO = 0 WHERE P.tbl_persona_ID = :herramenteroId AND S.tbl_sede_ID = '$idSede' AND P.tbl_cargo_tbl_cargo_ID = 2");
            $this->db->bind(':herramenteroId', $herramenteroId);
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }
        
    }
?>