<?php
    class LoginModel {
      
        private $db;

        function __construct(){
           
          $this->db = new Base;
        }

        public function Autenticacion($username = ''){
          $sql = "SELECT T1.tbl_sede AS idSede, T1.Tbl_usuario_ID, T1.Tbl_usuario_USERNAME, T1.Tbl_usuario_PASSWORD AS PASSWORD, T1.tbl_persona_tbl_persona_ID, T2.Tbl_persona_ID AS IDPERSONA,
          T2.Tbl_tipodocumento_Tbl_tipodocumento_ID AS TIPO_DOCUMENTO, T2.Tbl_persona_NUMDOCUMENTO, T2.Tbl_persona_NOMBRES, T2.Tbl_persona_PRIMERAPELLIDO,
          T2.Tbl_persona_SEGUNDOAPELLIDO, T2.Tbl_persona_FECHANAC, T2.Tbl_persona_TELEFONO, T2.Tbl_persona_CORREO, T3.Tbl_cargo_TIPO AS CARGO, 
          T1.Tbl_usuario_ESTADO FROM tbl_usuario T1 INNER JOIN tbl_sede ON T1.tbl_sede = tbl_sede.tbl_sede_ID INNER JOIN tbl_persona T2 ON T1.tbl_persona_tbl_persona_ID = T2.Tbl_persona_Id LEFT JOIN tbl_cargo T3 ON T2.Tbl_cargo_Tbl_cargo_ID = T3.Tbl_cargo_ID
          WHERE T1.Tbl_usuario_USERNAME = :username";
          $this->db->query($sql);
          $this->db->bind(':username', $username);
          return $this->db->registro();
        }
        
        public function getRequestApproved($idSede){
            $this->db->query("SELECT * FROM tbl_solicitudes WHERE id_sede = '$idSede' AND estado = 2 ORDER BY fecha DESC");
            $result = $this->db->registros();
            return $result;
        }
        
        public function ShowRequest($idSede){
            $this->db->query("SELECT * FROM tbl_solicitudes WHERE estado = 1 AND id_sede = '$idSede' ORDER BY fecha DESC");
            $result = $this->db->registros();
            return $result;
        }
        
        public function getToolsRequested($idSede){
            $this->db->query("SELECT * FROM tbl_solicitudes WHERE id_sede = '$idSede' AND tipo = 'herramienta' ORDER BY fecha DESC");
            $result = $this->db->registros();
            return $result;
        }
        
        public function getMaterialsRequested($idSede){
            $this->db->query("SELECT * FROM tbl_solicitudes WHERE id_sede = '$idSede' AND tipo = 'material' ORDER BY fecha DESC");
            $result = $this->db->registros();
            return $result;
        }
        
         public function getEquipmentsRequested($idSede){
            $this->db->query("SELECT * FROM tbl_solicitudes WHERE id_sede = '$idSede' AND tipo = 'equipo' ORDER BY fecha DESC");
            $result = $this->db->registros();
            return $result;
        }
        
        public function consultarCorreo($email){
            $this->db->query("SELECT * FROM tbl_usuario WHERE tbl_usuario_ESTADO = 1 AND tbl_usuario_USERNAME = '$email'");
            $result = $this->db->registros();
            return $result;
        }
        
        public function CambioContrasenia($datos){
            $this->db->query("UPDATE tbl_usuario SET tbl_usuario_PASSWORD = :contra WHERE tbl_usuario_USERNAME = :email");
            $this->db->bind(':contra', $datos['password1']);
            $this->db->bind(':email', $datos['email']);
            ($this->db->execute()) ? true : false;
        }
        
        public function CambioContraseniaActual($datos){
            $this->db->query("UPDATE tbl_usuario SET tbl_usuario_PASSWORD = :password WHERE tbl_usuario_ESTADO = 1 AND tbl_persona_tbl_persona_ID = :idPersona");
            $this->db->bind(':password', $datos['password']);
            $this->db->bind(':idPersona', $datos['idPersona']);
            ($this->db->execute()) ? true : false;
        }
        
        public function Rechazarsolicitud($datos){
            $this->db->query("UPDATE tbl_solicitudes SET estado = :estado, motivoRechazo = :rejectModal  WHERE id_solicitud = :id_solicitud ");
            $this->db->bind(':id_solicitud', $datos['id_solicitud']);
            $this->db->bind(':rejectModal', $datos['rejectModal']);
            $this->db->bind(':estado', $datos['estado']);
            ($this->db->execute()) ? true : false;
        }
        //Obtener productos disponibles para listar en el modal de total de productos disponibles
        
        /*public function getToolsAvailable($idSede){
            $this->db->query("SELECT H.*, SUM(H.tbl_herramienta_CANTIDAD) cantidad FROM tbl_herramienta H
            INNER JOIN tbl_gaveta G ON G.tbl_gaveta_ID = H.tbl_gaveta_tbl_gaveta_ID 
            INNER JOIN tbl_estante E ON E.tbl_estante_ID = G.tbl_estante_tbl_estante_ID 
            INNER JOIN tbl_bodega B ON B.tbl_bodega_ID = E.tbl_bodega_tbl_bodega_ID 
            INNER JOIN tbl_sede S ON S.tbl_sede_ID = B.tbl_sede_tbl_sede_ID 
            WHERE S.tbl_sede_ID = '$idSede' AND H.tbl_herramienta_ESTADO = 1
            GROUP BY H.tbl_herramienta_CODIGO
            ORDER BY H.tbl_herramienta_FECHA DESC");
            $result = $this->db->registros();
            return $result;
        }
        
        public function getMaterialsAvailable($idSede){
            $this->db->query("SELECT M.*, SUM(M.tbl_materiales_CANTIDAD) cantidad FROM tbl_materiales M
            WHERE M.tbl_sede_tbl_sede_ID = '$idSede' AND M.tbl_materiales_ESTADO = 1
            GROUP BY M.tbl_materiales_UNSPSC
            ORDER BY M.tbl_materiales_FECHA DESC");
            $result = $this->db->registros();
            return $result;
        }
        
        public function getEquipmentsAvailable($idSede){
            $this->db->query("SELECT E.*, SUM(E.tbl_equipo_CANTIDAD) cantidad FROM tbl_equipo E
            WHERE E.tbl_sede_tbl_sede_ID = '$idSede' AND E.tbl_equipo_ESTADO = 1
            GROUP BY E.tbl_equipo_PLACA
            ORDER BY E.tbl_equipo_FECHA_ADQUISICION DESC");
            $result = $this->db->registros();
            return $result ;
        }*/
        
        //Los cambios del stock se visualizan en la vista del home->index.php con los siguientes controladores:
        public function getToolsAvailable($idSede){
            $this->db->query("SELECT H.* FROM tbl_herramienta H
            INNER JOIN tbl_gaveta G ON G.tbl_gaveta_ID = H.tbl_gaveta_tbl_gaveta_ID 
            INNER JOIN tbl_estante E ON E.tbl_estante_ID = G.tbl_estante_tbl_estante_ID 
            INNER JOIN tbl_bodega B ON B.tbl_bodega_ID = E.tbl_bodega_tbl_bodega_ID 
            INNER JOIN tbl_sede S ON S.tbl_sede_ID = B.tbl_sede_tbl_sede_ID 
            WHERE S.tbl_sede_ID = '$idSede' AND H.tbl_herramienta_ESTADO = 1
            ORDER BY H.tbl_herramienta_FECHA DESC");
            $result = $this->db->registros();
            return $result;
        }
        
        public function getMaterialsAvailable($idSede){
            $this->db->query("SELECT M.* FROM tbl_materiales M
            WHERE M.tbl_sede_tbl_sede_ID = '$idSede' AND M.tbl_materiales_ESTADO = 1
            ORDER BY M.tbl_materiales_FECHA DESC");
            $result = $this->db->registros();
            return $result;
        }
        
        public function getEquipmentsAvailable($idSede){
            $this->db->query("SELECT E.* FROM tbl_equipo E
            WHERE E.tbl_sede_tbl_sede_ID = '$idSede' AND E.tbl_equipo_ESTADO = 1
            ORDER BY E.tbl_equipo_FECHA_ADQUISICION DESC");
            $result = $this->db->registros();
            return $result ;
        }
        
        //Final de prueba
        
        public function getMaterials($idSede){
            $this->db->query("SELECT M.tbl_materiales_CODIGOSENA codigo, M.tbl_materiales_DESCRIPCION descripcion, 
            SUM(M.tbl_materiales_CANTIDAD) AS cantidad_total, 
            SUM(SL.cantidad) AS cantidad_solicitudes, 
            SUM(M.tbl_materiales_CANTIDAD)- SUM(SL.cantidad) AS stock
            FROM tbl_materiales M 
            LEFT JOIN tbl_solicitudes SL ON SL.identificacion = M.tbl_materiales_ID AND SL.tipo = 'material' AND SL.estado != 1
            INNER JOIN tbl_sede S ON S.tbl_sede_ID = M.tbl_sede_tbl_sede_ID 
            WHERE S.tbl_sede_ID = '$idSede' AND M.tbl_materiales_ESTADO = 1
            GROUP BY M.tbl_materiales_CODIGOSENA, M.tbl_materiales_DESCRIPCION");
            $materials = $this->db->registros();
            return $materials;
        }
        
        public function getEquipments($idSede){
            $this->db->query("SELECT  E.tbl_equipo_ID, E.tbl_equipo_DESCRIPCION_ACTUAL, E.tbl_equipo_VALOR_INGRESO, SUM(E.tbl_equipo_CANTIDAD) cantidad_total, SUM(SL.cantidad) cantidad_solicitudes, SUM(E.tbl_equipo_CANTIDAD) - SUM(SL.cantidad) stock, E.tbl_equipo_CONSECUTIVO consecutivo, E.tbl_equipo_PLACA placa, E.tbl_equipo_DESCRIPCION descripcion
            FROM tbl_equipo E
            LEFT JOIN tbl_solicitudes SL ON SL.identificacion = E.tbl_equipo_ID AND SL.tipo = 'equipo' AND SL.estado != 1
            INNER JOIN tbl_sede S ON S.tbl_sede_ID = E.tbl_sede_tbl_sede_ID
            WHERE S.tbl_sede_ID = '$idSede' AND E.tbl_equipo_ESTADO = 1
            GROUP BY E.tbl_equipo_CONSECUTIVO");
            $equipments = $this->db->registros();
            return $equipments;
        }
        
        // funciones de instructores
        
        public function getToolsRequestedInstructor($idPersona, $idSede){
            $this->db->query("SELECT S.* FROM tbl_solicitudes S WHERE id_persona = '$idPersona' AND id_sede = '$idSede' AND S.tipo = 'herramienta' ORDER BY S.fecha DESC ");
            $result = $this->db->registros();
            return $result;
        }
        
        public function getAmountToolsInstructor($idPersona, $idSede){
            $this->db->query("SELECT S.id_solicitud FROM tbl_solicitudes S WHERE S.id_persona = '$idPersona' AND S.id_sede = '$idSede' AND S.tipo = 'herramienta' GROUP BY S.id_solicitud");
            $result = $this->db->registros();
            return $result;
        }
        
        public function getMaterialsRequestedInstructor($idPersona, $idSede){
            $this->db->query("SELECT S.* FROM tbl_solicitudes S WHERE id_persona = '$idPersona' AND id_sede = '$idSede' AND S.tipo = 'material' ORDER BY S.fecha DESC");
            $result = $this->db->registros();
            return $result;
        }
        
        public function getAmountMaterialsInstructor($idPersona, $idSede){
            $this->db->query("SELECT S.id_solicitud FROM tbl_solicitudes S WHERE S.id_persona = '$idPersona' AND S.id_sede = '$idSede' AND S.tipo = 'material' GROUP BY S.id_solicitud");
            $result = $this->db->registros();
            return $result;
        }
        
        public function getEquipmentsRequestedInstructor($idPersona, $idSede){
            $this->db->query("SELECT S.* FROM tbl_solicitudes S WHERE id_persona = '$idPersona' AND id_sede = '$idSede' AND S.tipo = 'equipo' ORDER BY S.fecha DESC");
            $result = $this->db->registros();
            return $result;
        }
        
        public function getAmountEquipmentsInstructor($idPersona, $idSede){
            $this->db->query("SELECT S.id_solicitud FROM tbl_solicitudes S WHERE S.id_persona = '$idPersona' AND S.id_sede = '$idSede' AND S.tipo = 'equipo' GROUP BY S.id_solicitud");
            $result = $this->db->registros();
            return $result;
        }
        
        // funcion para que el administrador apruebe
        public function sendRequest($datos){
            $id = $datos['identificacion'];
            // stock = a lo que tengo en la tabla herramientas y cantidad es lo que solicito
            
            /*$stockTotal =  $datos['stock'] - $datos['cantidad'];
            
            if($datos['tipo'] == 'material'){
                $this->db->query("UPDATE tbl_materiales SET tbl_materiales_CANTIDAD = '$stockTotal' WHERE tbl_materiales_ID = '$id' AND tbl_materiales_ESTADO = 1");
                ($this->db->execute())? true: false;
            }*/
            
            $this->db->query("UPDATE tbl_solicitudes SET estado = 2 WHERE id_solicitud = :idRequest AND codigo = :codigo  AND estado = 1 AND id_sede = :idSede");
            $this->db->bind(':idRequest', $datos['idRequest']);
            $this->db->bind(':codigo', $datos['codigo']);
            $this->db->bind(':idSede', $datos['idSede']);
            ($this->db->execute()) ? true : false;
        }
        
        // funcion para que el herramentero entregue
        public function deliverRequestHerramentero($datos){
            $id = $datos['identificacion'];
            // stock = a lo que tengo en la tabla herramientas y cantidad es lo que solicito
            
            $stockTotal =  $datos['stock'] - $datos['cantidad']; // esto es lo que voy a solicitar y se debe restar
            //$stockTotal =  $datos['stock'] - $datos['cantidad']; materiales
            if($datos['tipo'] == 'herramienta'){
                $this->db->query("UPDATE tbl_herramienta SET tbl_herramienta_CANTIDAD = '$stockTotal' WHERE tbl_herramienta_ID = '$id' AND tbl_herramienta_ESTADO = 1");
                ($this->db->execute())? true: false;
            } elseif($datos['tipo'] == 'equipo'){
                $this->db->query("UPDATE tbl_equipo SET tbl_equipo_CANTIDAD = '$stockTotal' WHERE tbl_equipo_ID = '$id' AND tbl_equipo_ESTADO = 1");
                ($this->db->execute())? true: false;
            } elseif($datos['tipo'] == 'material'){
                $this->db->query("UPDATE tbl_materiales SET tbl_materiales_CANTIDAD = '$stockTotal' WHERE tbl_materiales_ID = '$id' AND tbl_materiales_ESTADO = 1");
                ($this->db->execute())? true: false;
            }
            
            
            
            $this->db->query("UPDATE tbl_solicitudes SET estado = 3 WHERE id_solicitud = :idRequest AND codigo = :codigo  AND id_sede = :idSede");
            $this->db->bind(':idRequest', $datos['idRequest']);
            $this->db->bind(':codigo', $datos['codigo']);
            $this->db->bind(':idSede', $datos['idSede']);
            ($this->db->execute()) ? true : false;
        }
        
        // funcion para traer las cantidades de herramientas, equipos o materilas
		public function cantidades($datos){
		    if($datos['tipo'] == 'herramienta'){
		        $this->db->query("SELECT H.tbl_herramienta_CANTIDAD cantidad FROM tbl_herramienta H 
                INNER JOIN tbl_gaveta G ON G.tbl_gaveta_ID = H.tbl_gaveta_tbl_gaveta_ID 
                INNER JOIN tbl_estante E ON E.tbl_estante_ID = G.tbl_estante_tbl_estante_ID 
                INNER JOIN tbl_bodega B ON B.tbl_bodega_ID = E.tbl_bodega_tbl_bodega_ID 
                INNER JOIN tbl_sede S ON S.tbl_sede_ID = B.tbl_sede_tbl_sede_ID 
                WHERE H.tbl_herramienta_ESTADO = 1 AND S.tbl_sede_ID =:idSede AND H.tbl_herramienta_ID = :id");
                $this->db->bind(':idSede', $datos['idSede']);
                $this->db->bind(':id', $datos['id']);
                $result = $this->db->registros();
                return $result;
		    }elseif($datos['tipo'] == 'material'){
		        $this->db->query("SELECT M.tbl_materiales_CANTIDAD cantidad FROM tbl_materiales M WHERE M.tbl_sede_tbl_sede_ID = :idSede AND M.tbl_materiales_ESTADO = 1 AND M.tbl_materiales_ID = :id");
		        $this->db->bind(':idSede', $datos['idSede']);
                $this->db->bind(':id', $datos['id']);
		        $result = $this->db->registros();
		        return $result;
		    }elseif($datos['tipo'] == 'equipo'){
		        $this->db->query("SELECT E.tbl_equipo_CANTIDAD cantidad FROM tbl_equipo E WHERE E.tbl_equipo_ID = :id AND E.tbl_equipo_ESTADO = 1 AND E.tbl_sede_tbl_sede_ID = :idSede");
		        $this->db->bind(':idSede', $datos['idSede']);
                $this->db->bind(':id', $datos['id']);
                $result = $this->db->registros();
                return $result;
		    }
		}
        
        public function deliverRequest($idRequest, $codigo, $idSede){
            $this->db->query("UPDATE tbl_solicitudes SET estado = 3 WHERE id_solicitud = '$idRequest' AND codigo = '$codigo' AND estado = 2 AND id_sede = '$idSede'");
            ($this->db->execute()) ? true : false;
        }
        
        
        
        public function getRequest($idRequest, $idSede){
            $this->db->query("SELECT * FROM tbl_solicitudes WHERE id_solicitud = '$idRequest' AND id_sede = '$idSede'  ORDER BY fecha DESC");
            $result = $this->db->registros();
            return $result;
        }
    }

    