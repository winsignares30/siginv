<?php
    class AdministradorModel{
        private $db;

        public function __construct(){
            $this->db = new Base ();
        }
        
        public function ListarAdministrador($idSede){
            $this->db->query("SELECT tbl_sede.tbl_sede_NOMBRE, tbl_instructor.tbl_instructor_ID, tbl_instructor.tbl_instructor_NOMBRES, tbl_instructor.tbl_instructor_APELLIDOS, tbl_instructor.tbl_instructor_TIPODOCUMENTO, tbl_instructor.tbl_instructor_NUMDECUMENTO, tbl_instructor.tbl_instructor_TELEFONO, tbl_instructor.tbl_instructor_CORREO, tbl_instructor.tbl_instructor_DIRECION, tbl_instructor.tbl_sede_tbl_sede_ID FROM tbl_instructor INNER JOIN tbl_sede ON tbl_instructor.tbl_sede_tbl_sede_ID = tbl_sede.tbl_sede_ID WHERE tbl_instructor.tbl_instructor_ESTADO = 1 AND tbl_sede.tbl_sede_ID = '$idSede' ");
            return $resultados = $this->db->registros();
        }
        
        public function RegistrarAdministrador($datos){
            $this->db->query("INSERT INTO tbl_instructor (tbl_instructor_ID, tbl_instructor_NOMBRES, tbl_instructor_APELLIDOS, tbl_instructor_TIPODOCUMENTO, tbl_instructor_NUMDECUMENTO, tbl_instructor_TELEFONO, tbl_instructor_CORREO, tbl_instructor_DIRECION, tbl_sede_tbl_sede_ID, tbl_instructor_ESTADO) VALUES (NULL, :instructorNombre, :instructorApellido, :instructorTipoDocumento, :instructorNumeroDocumento, :instructorNumeroTelefono, :instructorDirecionCorreo, :instructorDirecion, :instructorSedeID, 1)");
            $this->db->bind(':instructorSedeID', $datos['instructorSedeID']);
            $this->db->bind(':instructorNombre', $datos['instructorNombre']);
            $this->db->bind(':instructorApellido', $datos['instructorApellido']);
            $this->db->bind(':instructorTipoDocumento', $datos['instructorTipoDocumento']);
            $this->db->bind(':instructorNumeroDocumento', $datos['instructorNumeroDocumento']);
            $this->db->bind(':instructorNumeroTelefono', $datos['instructorNumeroTelefono']);
            $this->db->bind(':instructorDirecionCorreo', $datos['instructorDirecionCorreo']);
            $this->db->bind(':instructorDirecion', $datos['instructorDirecion']);
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        public function ObtenerAdministradorId($idAdministrador) {
          $this->db->query("SELECT * FROM tbl_instructor  WHERE tbl_persona_ID = :idAdministrador");
          $this->db->bind(':idAdministrador',$idAdministrador);
          $row = $this->db->registro();
          return $row;
        }

        public function EditarAdministrador($datos) {
            $this->db->query("UPDATE tbl_instructor SET tbl_instructor_NOMBRES = :instructorNombre, tbl_instructor_APELLIDOS = :instructorApellido, tbl_instructor_TIPODOCUMENTO = :instructorTipoDocumento, tbl_instructor_NUMDECUMENTO = :instructorNumeroDocumento, tbl_instructor_TELEFONO = :instructorNumeroTelefono, tbl_instructor_CORREO = :instructorDirecionCorreo, tbl_instructor_DIRECION =  :instructorDirecion WHERE tbl_instructor_ID = :idInstructor");
            $this->db->bind(':idInstructor', $datos['idInstructor']);
            $this->db->bind(':instructorNombre', $datos['instructorNombre']);
            $this->db->bind(':instructorApellido', $datos['instructorApellido']);
            $this->db->bind(':instructorTipoDocumento', $datos['instructorTipoDocumento']);
            $this->db->bind(':instructorNumeroDocumento', $datos['instructorNumeroDocumento']);
            $this->db->bind(':instructorNumeroTelefono', $datos['instructorNumeroTelefono']);
            $this->db->bind(':instructorDirecionCorreo', $datos['instructorDirecionCorreo']);
            $this->db->bind(':instructorDirecion', $datos['instructorDirecion']);
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }
        
          public function EditarAdministrador2($datos) {
            $this->db->query("UPDATE tbl_instructor I INNER JOIN tbl_sede S ON S.tbl_sede_ID = I.tbl_sede_tbl_sede_ID SET I.tbl_instructor_NUMDECUMENTO = :instructorNumeroDocumento , I.tbl_sede_tbl_sede_ID = :instructorSedeID WHERE I.tbl_instructor_ID = :idInstructor");
            $this->db->bind(':idInstructor', $datos['idInstructor']);
            $this->db->bind(':instructorSedeID', $datos['instructorSedeID']);
            $this->db->bind(':instructorNumeroDocumento', $datos['instructorNumeroDocumento']);
            ($this->db->execute())? true : false;
        }

        public function EliminarAdministrador($idInstructor, $idSede){
            $this->db->query("UPDATE tbl_instructor I INNER JOIN tbl_sede S ON I.tbl_sede_tbl_sede_ID = S.tbl_sede_ID SET tbl_instructor_ESTADO = 0 WHERE I.tbl_instructor_ID = :idInstructor AND S.tbl_sede_ID = '$idSede' ");
            $this->db->bind(':idInstructor', $idInstructor);
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }
        
        public function CompararAdministrador($instructorNombre){
            $this->db->query("SELECT * FROM tbl_instructor WHERE tbl_instructor_NOMBRES = '$instructorNombre' AND tbl_instructor_ESTADO = 1");
            $result = $this->db->registro();
            return $result ;
        }
        
        public function ListarSede($idInstructor) {
            $this->db->query("SELECT * FROM tbl_sede WHERE tbl_sede.tbl_centro_tbl_centro_ID = (SELECT tbl_centro.tbl_centro_ID FROM tbl_centro INNER JOIN tbl_sede ON tbl_sede.tbl_centro_tbl_centro_ID = tbl_centro.tbl_centro_ID INNER JOIN tbl_instructor ON tbl_instructor.tbl_sede_tbl_sede_ID = tbl_sede.tbl_sede_ID WHERE tbl_instructor.tbl_instructor_ID = '$idInstructor' AND tbl_instructor_ESTADO = 1)");
            $result = $this->db->registros();
            return $result;
        }
        
        public function ComprobarAdministrador($idInstructor) {
            $this->db->query("SELECT * FROM tbl_instructor WHERE tbl_instructor_ID = '$idInstructor' AND tbl_instructor_ESTADO = 1");
            $result = $this->db->registros();
            return $result;
        }
    }
?>