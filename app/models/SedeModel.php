<?php

	class SedeModel{
		private $db;

		public function __construct(){
			$this->db = new Base();
		}
		public function ListarSede(){
			$this->db->query("SELECT tbl_regional.tbl_regional_ID,tbl_regional.tbl_regional_NOMBRE, tbl_centro.tbl_centro_ID ,tbl_centro.tbl_centro_NOMBRE, tbl_sede.tbl_sede_ID, tbl_sede.tbl_sede_NOMBRE, tbl_sede.tbl_sede_RESPONSABLE, tbl_sede.tbl_sede_TELEFONO FROM tbl_centro INNER JOIN tbl_regional ON tbl_centro.tbl_regional_tbl_regional_ID = tbl_regional.tbl_regional_ID INNER JOIN tbl_sede ON tbl_centro.tbl_centro_ID=tbl_sede.tbl_centro_tbl_centro_ID WHERE tbl_sede.tbl_sede_ESTADO = 1");
			$resultados = $this->db->registros();
			return $resultados;
		}
		// register sede
		public function RegistrarSede($datos){
			$this->db->query("INSERT INTO tbl_sede (tbl_sede_NOMBRE, tbl_sede_RESPONSABLE, tbl_sede_TELEFONO, tbl_centro_tbl_centro_ID) VALUES ( :sedeNombre, :sedeResponsable, :sedeTelefono, :sedeCentro)");
			$this->db->bind(':sedeNombre', $datos['sedeNombre']);
			$this->db->bind(':sedeResponsable', $datos['sedeResponsable']);
			$this->db->bind(':sedeTelefono', $datos['sedeTelefono']);
			$this->db->bind(':sedeCentro', $datos['sedeCentro']);
			($this->db->execute())? true : false;
			
		}
		// get sede
		public function ObtenerSedeId($idSede) {
			$this->db->query("SELECT * FROM tbl_sede WHERE tbl_sede_ID = :idSede" );
			$this->db->bind(':idSede', $idSede);
			$row = $this->db->registro();
			return $row;
		}
		// edit sede
		public function EditarSede($datos){
			$this->db->query("UPDATE tbl_sede SET  tbl_sede_NOMBRE = :sedeNombre, tbl_sede_RESPONSABLE = :sedeResponsable, tbl_sede_TELEFONO = :sedeTelefono WHERE tbl_sede_ID = :idSede");
			$this->db->bind(':idSede', $datos['idSede']);
			$this->db->bind(':sedeNombre', $datos['sedeNombre']);
			$this->db->bind(':sedeResponsable', $datos['sedeResponsable']);
			$this->db->bind(':sedeTelefono', $datos['sedeTelefono']);
			($this->db->execute())?true : false;
		}
		public function EditarSede2($datos){
			$this->db->query("UPDATE tbl_sede SET  tbl_sede_NOMBRE = :sedeNombre, tbl_centro_tbl_centro_ID = :sedeCentro WHERE tbl_sede_ID = :idSede");
			$this->db->bind(':idSede', $datos['idSede']);
			$this->db->bind(':sedeNombre', $datos['sedeNombre']);
			$this->db->bind(':sedeCentro', $datos['sedeCentro']);
			($this->db->execute())?true : false;
		}
		// delete sede
		public function EliminarSede($idSede){
            $this->db->query("UPDATE tbl_sede SET tbl_sede_ESTADO = 0 WHERE tbl_sede_ID = :idSede");
			$this->db->bind(':idSede', $idSede);
			if($this->db->execute()){
				return true;
			}else{
				return false;
			}
        }
        
        public function ListarCentro($idSede){
            $this->db->query("SELECT * FROM tbl_centro WHERE tbl_centro.tbl_regional_tbl_regional_ID = ( SELECT tbl_regional.tbl_regional_ID FROM tbl_regional INNER JOIN tbl_centro ON tbl_centro.tbl_regional_tbl_regional_ID = tbl_regional.tbl_regional_ID INNER JOIN tbl_sede ON tbl_sede.tbl_centro_tbl_centro_ID = tbl_centro.tbl_centro_ID WHERE tbl_sede.tbl_sede_ID = '$idSede' AND tbl_sede.tbl_sede_ESTADO = 1) ");
            $result = $this->db->registros();
            return $result;
        }
        
		// compare sede
		public function CompararSede($sedeCentro){
			$this->db->query("SELECT * FROM tbl_sede WHERE tbl_centro_tbl_centro_ID = :sedeCentro AND tbl_sede_ESTADO = 1");
			$this->db->bind(':sedeCentro', $sedeCentro);
			$result = $this ->db->registros();
			return $result ;
		}
		
		public function IfDataExist($idSede) {
			$this->db->query("SELECT * FROM tbl_ambiente WHERE tbl_sede_tbl_sede_ID = '$idSede' AND tbl_amb_ESTADO = 1");
			$result = $this->db->registros();
			return $result;
		}
		
		public function IfDataExist2($idSede) {
			$this->db->query("SELECT * FROM tbl_bodega WHERE tbl_sede_tbl_sede_ID = '$idSede' AND tbl_sede_ESTADO = 1");
			$result = $this->db->registros();
			return $result;
		}
    }
?>
