<?php 

	Class EquipoModel{
		private $db;

		public function __construct(){
			$this->db = new Base();
		}
		public function ObtenerEquipo($idEquipo){
			$this->db->query('SELECT * FROM tbl_equipo WHERE tbl_equipo_ID = :idEquipo');
            $this->db->bind(':idEquipo', $idEquipo);
            $resultado = $this->db->registro();
            return $resultado;
		}
		
		public function ObtenerEquipo2($idEquipo){
			$this->db->query("SELECT * FROM tbl_equipo WHERE tbl_equipo_ID = '$idEquipo'");
            $resultado = $this->db->registro();
            return $resultado;
		}
		
		public function SolicitarEquipos($idSede){
		    $this->db->query("SELECT E.tbl_equipo_ID,E.tbl_equipo_CANTIDAD, tbl_equipo_RUBRO rubro, E.tbl_equipo_CONSECUTIVO, E.tbl_equipo_DESCRIPCION, E.tbl_equipo_PLACA, E.tbl_equipo_TIPO FROM tbl_equipo E INNER JOIN tbl_sede S ON E.tbl_sede_tbl_sede_ID = S.tbl_sede_ID WHERE S.tbl_sede_ID = '$idSede' AND E.tbl_equipo_CANTIDAD > 0 AND E.tbl_equipo_ESTADO = 1");
		    $result = $this->db->registros();
		    return $result;
		}

		public function CantidadEquipos($id, $idSede){
			$this->db->query("SELECT E.tbl_equipo_CANTIDAD FROM tbl_equipo E WHERE E.tbl_equipo_ID = '$id' AND E.tbl_equipo_ESTADO = 1 AND E.tbl_sede_tbl_sede_ID = '$idSede'");
			$result = $this->db->registro();
			return $result;
		}

		public function MostrarEquiposSolicitados($idSede, $idPersona){
		    $this->db->query("SELECT * FROM tbl_solicitudes WHERE id_sede = '$idSede' AND id_persona = '$idPersona' AND tipo = 'equipo' ORDER BY fecha DESC");
		    $result = $this->db->registros();
		    return $result;
		}

		public function ListarEquipos($idSede){
			$this->db->query("SELECT tbl_regional.tbl_regional_ID, tbl_regional.tbl_regional_NOMBRE, tbl_centro.tbl_centro_ID, tbl_centro.tbl_centro_NOMBRE, tbl_sede.tbl_sede_ID, tbl_sede.tbl_sede_NOMBRE, tbl_equipo.* FROM tbl_equipo INNER JOIN tbl_sede ON tbl_sede.tbl_sede_ID = tbl_equipo.tbl_sede_tbl_sede_ID INNER JOIN tbl_centro ON tbl_centro.tbl_centro_ID = tbl_sede.tbl_centro_tbl_centro_ID INNER JOIN tbl_regional ON tbl_regional.tbl_regional_ID = tbl_centro.tbl_regional_tbl_regional_ID WHERE tbl_equipo_ESTADO = 1 AND tbl_sede.tbl_sede_ID = '$idSede' ORDER BY tbl_equipo_FECHA_ADQUISICION DESC");
        	return $resultado = $this->db->registros();
		}

		
		public function EnviarEquipos($datos){
		    $estado = $datos['estado'];
		    if($datos['tipo'] == 'material'){
		        $estado = 1;
		    }
		    
            $this->db->query("INSERT INTO tbl_solicitudes(id, identificacion, id_solicitud, codigo, nombre, descripcion, cantidad, tipo, id_persona, persona, id_sede, ficha, rubro, estado, id_herramentero) VALUES (NULL, :identificacion, :id_solicitud, :codigo, :nombre, :descripcion, :cantidad, :tipo, :idPersona, :nombrePersona, :id_sede, :ficha, :rubro, '$estado', 2)");
            $this->db->bind(':identificacion', $datos['id']);
            $this->db->bind(':id_solicitud', $datos['id_solicitud']);
            $this->db->bind(':codigo', $datos['codigo']);
            $this->db->bind(':nombre', $datos['nombre']);
            $this->db->bind(':descripcion', $datos['descripcion']);
            $this->db->bind(':cantidad', $datos['cantidad']);
            $this->db->bind(':tipo', $datos['tipo']);
            $this->db->bind(':id_sede', $datos['idSede']);
            $this->db->bind(':idPersona', $datos['idPersona']);
            $this->db->bind(':nombrePersona', $datos['nombrePersona']);
            $this->db->bind(':ficha', $datos['ficha']);
            $this->db->bind(':rubro', $datos['rubro']);
            ($this->db->execute())?true: false;
		}
		
        public function RegistrarEquipo($datos){
			$this->db->query("INSERT INTO tbl_equipo (tbl_equipo_ID, tbl_equipo_MODELO, tbl_equipo_CANTIDAD, cantidad_total, tbl_equipo_CONSECUTIVO, tbl_equipo_DESCRIPCION, tbl_equipo_DESCRIPCION_ACTUAL, tbl_equipo_RUBRO, tbl_equipo_TIPO, tbl_equipo_PLACA, tbl_equipo_VALOR_INGRESO, tbl_equipo_ESTADO, tbl_sede_tbl_sede_ID) VALUES (null, :equipoModelo, :equipoCantidad, :cantidad_total,  :equipoConsecutivo, :equipoDescripcion, :equipoDescripcionActual, :rubroPresupuestal, :equipoTipo,  :equipoPlaca, :equipoValordeingreso, 1, :equipoSede)");
			$this->db->bind(':equipoSede', $datos['equipoSede']);
			$this->db->bind(':equipoModelo', $datos['equipoModelo']);
			$this->db->bind(':equipoCantidad', $datos['equipoCantidad']);
			$this->db->bind(':cantidad_total', $datos['equipoCantidad']);
			$this->db->bind(':equipoConsecutivo', $datos['equipoConsecutivo']);
			$this->db->bind(':equipoDescripcion', $datos['equipoDescripcion']);
			$this->db->bind(':equipoDescripcionActual', $datos['equipoDescripcionActual']);
			$this->db->bind(':rubroPresupuestal', $datos['rubroPresupuestal']);
			$this->db->bind(':equipoTipo', $datos['equipoTipo']);
			$this->db->bind(':equipoPlaca', $datos['equipoPlaca']);
			$this->db->bind(':rubroPresupuestal', $datos['rubroPresupuestal']);
			//$this->db->bind(':equipoFechadeAdquision', $datos['equipoFechadeAdquision']);
			$this->db->bind(':equipoValordeingreso', $datos['equipoValordeingreso']);
			
			if($this->db->execute()){ return true; }else{ return false;}
		}

		public function EditarEquipo($datos){
			$this->db->query("UPDATE tbl_equipo SET tbl_equipo_MODELO = :equipoModelo, tbl_equipo_CANTIDAD = :equipoCantidad, cantidad_total = :cantidad_total, tbl_equipo_CONSECUTIVO = :equipoConsecutivo, tbl_equipo_DESCRIPCION = :equipoDescripcion, tbl_equipo_DESCRIPCION_ACTUAL = :equipoDescripcionActual, tbl_equipo_RUBRO = :rubroPresupuestal, tbl_equipo_TIPO = :equipoTipo, tbl_equipo_VALOR_INGRESO = :equipoValordeingreso, tbl_equipo_FECHA_ADQUISICION = :equipoFechadeAdquision WHERE tbl_equipo_ID = :idEquipo ");
			$this->db->bind(':idEquipo', $datos['idEquipo']);
			$this->db->bind(':equipoModelo', $datos['equipoModelo']);
			$this->db->bind(':equipoCantidad', $datos['equipoCantidad']);
			$this->db->bind(':cantidad_total', $datos['equipoCantidad']);
			$this->db->bind(':equipoConsecutivo' ,$datos['equipoConsecutivo']);
			$this->db->bind(':equipoTipo' , $datos['equipoTipo']);
			$this->db->bind(':equipoDescripcion' ,$datos['equipoDescripcion']);
			$this->db->bind(':equipoDescripcionActual' , $datos['equipoDescripcionActual']);
			$this->db->bind(':rubroPresupuestal', $datos['rubroPresupuestal']);
			$this->db->bind(':equipoValordeingreso' , $datos['equipoValordeingreso']);
			$this->db->bind(':equipoFechadeAdquision' , $datos['equipoFechadeAdquision']);
			if($this->db->execute()){ return true; }else{ return false;}
		}
		
		public function EditarEquipo2($datos){
			$this->db->query("UPDATE tbl_equipo SET tbl_equipo_PLACA =:equipoPlaca, tbl_sede_tbl_sede_ID=:equipoSede WHERE tbl_equipo_ID = :idEquipo");
			$this->db->bind(':idEquipo', $datos['idEquipo']);
			$this->db->bind(':equipoSede', $datos['equipoSede']);
			$this->db->bind(':equipoPlaca', $datos['equipoPlaca']);
			if($this->db->execute()){ return true; }else{ return false;}
		}
		
		public function EditarEquipo3($datos){
			$this->db->query("UPDATE tbl_equipo SET  tbl_sede_tbl_sede_ID=:equipoSede WHERE tbl_equipo_ID = :idEquipo");
			$this->db->bind(':idEquipo', $datos['idEquipo']);
			$this->db->bind(':equipoSede', $datos['equipoSede']);
			//$this->db->bind(':equipoSerial', $datos['equipoSerial']);
			if($this->db->execute()){ return true; }else{ return false;}
		}

		public function EliminarEquipo($idEquipo){
			$this->db->query("UPDATE tbl_equipo SET tbl_equipo_ESTADO = 0 WHERE tbl_equipo_ID = :idEquipo");
            $this->db->bind(':idEquipo', $idEquipo);
            if($this->db->execute()){ return true; }else{ return false;} 
		}
		
		public function ObtenerSede($idEquipo) {
		    $this->db->query("SELECT * FROM tbl_sede WHERE tbl_sede_ESTADO = 1 AND  tbl_centro_tbl_centro_ID = (SELECT tbl_centro.tbl_centro_ID FROM tbl_centro INNER JOIN tbl_sede ON tbl_sede.tbl_centro_tbl_centro_ID = tbl_centro.tbl_centro_ID INNER JOIN tbl_equipo ON tbl_equipo.tbl_sede_tbl_sede_ID = tbl_sede.tbl_sede_ID WHERE tbl_equipo.tbl_equipo_ID = '$idEquipo' AND tbl_equipo.tbl_equipo_ESTADO = 1)");
		    $resultado = $this->db->registros();
		    return $resultado;
		}
		
		public function CompararEquipo2($equipoPlaca){
		    $this->db->query("SELECT tbl_equipo_PLACA FROM tbl_equipo  WHERE tbl_equipo_PLACA = $equipoPlaca AND tbl_equipo_ESTADO = 1");
		    return $result = $this->db->registros();
		}
		
		public function CompararEquipo($idEquipo){
		    $this->db->query("SELECT * FROM tbl_equipo WHERE tbl_equipo_ID ='$idEquipo' AND tbl_equipo_ESTADO  = 1");
		    return $resultado = $this->db->registros();
		}
		
		
	}
?>