<?php

class EstanteModel
{
    private $db;

    public function __construct()
    {
        $this->db = new Base();
    }

    public function ListarEstante($idSede)
    { 
        $this->db->query("SELECT tbl_estante.*, S.tbl_sede_NOMBRE, B.tbl_bodega_ID, B.tbl_bodega_NOMBRE,  B.tbl_sede_tbl_sede_ID, C.tbl_centro_NOMBRE, R.tbl_regional_NOMBRE FROM tbl_estante INNER JOIN tbl_bodega B ON B.tbl_bodega_ID = tbl_estante.tbl_bodega_tbl_bodega_ID INNER JOIN tbl_sede S ON S.tbl_sede_ID = B.tbl_sede_tbl_sede_ID INNER JOIN tbl_centro C ON C.tbl_centro_ID = S.tbl_centro_tbl_centro_ID INNER JOIN tbl_regional R ON R.tbl_regional_ID = C.tbl_regional_tbl_regional_ID WHERE tbl_estante.tbl_estante_ESTADO = 1 AND S.tbl_sede_ID = '$idSede'");
        $resualtados = $this->db->registros();
        return $resualtados;
    }

    public function RegistrarEstante($datos)
    {
        $this->db->query("INSERT INTO tbl_estante(tbl_estante_ID, tbl_estante_NUMERO, tbl_estante_DESCRIPCION, tbl_bodega_tbl_bodega_ID, tbl_estante_ESTADO) VALUES (NULL, :estanteNumero, :estanteDescripcion, :estanteBodega, 1)");
        $this->db->bind(':estanteNumero', $datos['estanteNumero']);
        $this->db->bind(':estanteDescripcion', $datos['estanteDescripcion']);
        $this->db->bind(':estanteBodega', $datos['estanteBodega']);
        ($this->db->execute()) ? true : false;
    }

    public function ObtenerEstante($idEstante)
    {
        $this->db->query("SELECT * FROM tbl_estante WHERE tbl_estante_ID = :idEstante");
        $this->db->bind(':idEstante', $idEstante);
        $row = $this->db->registro();
        return $row;
    }

    public function EditarEstante($datos)
    {
        $this->db->query("UPDATE tbl_estante SET tbl_estante_NUMERO = :estanteNumero, tbl_estante_DESCRIPCION = :estanteDescripcion, tbl_bodega_tbl_bodega_ID = :estanteBodega WHERE tbl_estante_ID = :idEstante");
        $this->db->bind(':idEstante', $datos['idEstante']);
        $this->db->bind(':estanteNumero', $datos['estanteNumero']);
        $this->db->bind(':estanteDescripcion', $datos['estanteDescripcion']);
        $this->db->bind(':estanteBodega', $datos['estanteBodega']);
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function DeleteEstante($idEstante, $idSede)
    {
        $this->db->query("UPDATE tbl_estante E INNER JOIN tbl_bodega B ON B.tbl_bodega_ID = E.tbl_bodega_tbl_bodega_ID INNER JOIN tbl_sede S ON S.tbl_sede_ID = B.tbl_sede_tbl_sede_ID SET E.tbl_estante_ESTADO = 0 WHERE E.tbl_estante_ID = '$idEstante' AND S.tbl_sede_ID = '$idSede' ");
        $this->db->bind(':idEstante', $idEstante);
        ($this->db->execute())? true : false;
    }

    public function LoadBodegas($idSede)
    {
        $this->db->query("SELECT * FROM tbl_bodega WHERE tbl_sede_tbl_sede_ID = '$idSede' AND tbl_bodega_ESTADO = 1");
        $result = $this->db->registros();
        return $result;
    }
    public function CompararEstante($estanteBodega) {
        $this->db->query("SELECT * FROM tbl_estante WHERE tbl_bodega_tbl_bodega_ID = '$estanteBodega' AND tbl_estante_ESTADO = 1");
        $result = $this->db->registros();
        return $result;
    }
    
    public function IfDataExist($idEstante){
		    $this->db->query("SELECT * FROM tbl_gaveta WHERE tbl_estante_tbl_estante_ID = '$idEstante' AND tbl_gaveta_ESTADO = 1");
		    $result = $this->db->registros();
		    return $result;
	}
}
