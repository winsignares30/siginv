<?php
    class ReporteModel {
        private $db;
        
        public function __construct(){
            $this->db = new Base();
        }
        
        public function reporteSolicitudHerramientaInstructor($fechaInicial, $fechaFinal, $idSede, $idInstructor){
            $this->db->query("SELECT s.* FROM tbl_solicitudes s WHERE s.tipo = 'herramienta' 
            AND s.fecha BETWEEN '$fechaInicial 00:00:00' AND '$fechaFinal 23:59:59' and id_sede = '$idSede' AND id_persona = '$idInstructor'");
            $result = $this->db->registros();
            return $result;
        }
        
        public function reporteSolicitudMaterialesInstructor($fechaInicial, $fechaFinal, $idSede, $idInstructor){
            $this->db->query("SELECT s.* FROM tbl_solicitudes s WHERE s.tipo = 'material' 
            AND s.fecha BETWEEN '$fechaInicial 00:00:00' AND '$fechaFinal 23:59:59' and id_sede = '$idSede' AND id_persona = '$idInstructor'");
            $result = $this->db->registros();
            return $result;
        }
        
        public function reporteSolicitudEquiposInstructor($fechaInicial, $fechaFinal, $idSede, $idInstructor){
            $this->db->query("SELECT s.* FROM tbl_solicitudes s WHERE s.tipo = 'equipo' 
            AND s.fecha BETWEEN '$fechaInicial 00:00:00' AND '$fechaFinal 23:59:59' and id_sede = '$idSede' AND id_persona = '$idInstructor'");
            $result = $this->db->registros();
            return $result;
        }
        
        public function imprimirReporteHerramientasSolicitadasEntreDias ($fechaInicial, $fechaFinal, $idSede){
            $this->db->query("SELECT s.* FROM tbl_solicitudes s WHERE s.tipo = 'herramienta' 
            AND s.fecha BETWEEN '$fechaInicial 00:00:00' AND '$fechaFinal 23:59:59' AND id_sede = '$idSede'");
            $result = $this->db->registros();
            return $result;
        }
        
        public function imprimirReporteMaterialesSolicitadosEntreDias ($fechaInicial, $fechaFinal, $idSede){
            $this->db->query("SELECT s.* FROM tbl_solicitudes s WHERE s.tipo = 'material' 
            AND s.fecha BETWEEN '$fechaInicial 00:00:00' AND '$fechaFinal 23:59:59' AND id_sede = '$idSede'");
            $result = $this->db->registros();
            return $result;
        }
        
        public function imprimirReporteEquiposSolicitadosEntreDias ($fechaInicial, $fechaFinal, $idSede){
            $this->db->query("SELECT s.* FROM tbl_solicitudes s WHERE s.tipo = 'equipo' 
            AND s.fecha BETWEEN '$fechaInicial 00:00:00' AND '$fechaFinal 23:59:59' AND id_sede = '$idSede'");
            $result = $this->db->registros();
            return $result;
        }
        
        public function getInstructors($idSede){
            $this->db->query("SELECT P.tbl_persona_ID id, P.tbl_persona_NOMBRES nombres, P.tbl_persona_PRIMERAPELLIDO apellido1, P.tbl_persona_SEGUNDOAPELLIDO apellido2, U.tbl_sede FROM tbl_persona P 
            INNER JOIN tbl_usuario U ON U.tbl_persona_tbl_persona_ID = P.tbl_persona_ID
            WHERE P.tbl_cargo_tbl_cargo_ID = 3 AND P.tbl_persona_ESTADO = 1 AND U.tbl_sede = '$idSede'");
            $result = $this->db->registros();
            return $result;
        }
        
        
    }
?>