<?php 
	class BodegaModel
	{
		private $db;	
		public function __construct()
		{
			$this->db = new Base();
		}

		public function ListarBodega($idSede){
			$this->db->query("SELECT S.tbl_sede_NOMBRE, B.tbl_bodega_ID, B.tbl_bodega_NOMBRE, B.tbl_bodega_ESTADO, B.tbl_sede_tbl_sede_ID, C.tbl_centro_NOMBRE, R.tbl_regional_NOMBRE FROM tbl_bodega B INNER JOIN tbl_sede S ON S.tbl_sede_ID = B.tbl_sede_tbl_sede_ID INNER JOIN tbl_centro C ON C.tbl_centro_ID = S.tbl_centro_tbl_centro_ID INNER JOIN tbl_regional R ON R.tbl_regional_ID = C.tbl_regional_tbl_regional_ID WHERE B.tbl_bodega_ESTADO = 1 AND B.tbl_sede_tbl_sede_ID = '$idSede'");
			return $resultado = $this->db->registros();
		}

		public function RegistrarBodega($datos){
			$this->db->query("INSERT INTO tbl_bodega(tbl_bodega_ID, tbl_bodega_NOMBRE, tbl_sede_tbl_sede_ID, tbl_bodega_ESTADO) VALUES (NULL, :bodegaNombre, :bodegaSede, 1)");
			$this->db->bind(':bodegaNombre', $datos['bodegaNombre']);
			$this->db->bind(':bodegaSede', $datos['bodegaSede']);
			if($this->db->execute()){ return true; }else{ return false;}  
		}
        public function ObtenerBodega($idBodega){
        	$this->db->query("SELECT * FROM tbl_bodega WHERE  tbl_bodega_ID = :idBodega AND tbl_bodega_ESTADO = 1");
  			$this->db->bind(':idBodega', $idBodega);
			$row = $this->db->registro();
			return $row;
        }
		public function DeleteBodega($idBodega, $idSede) {
			$this->db->query("UPDATE tbl_bodega SET tbl_bodega.tbl_bodega_ESTADO = 0 WHERE tbl_bodega.tbl_bodega_ID = '$idBodega' AND tbl_bodega.tbl_sede_tbl_sede_ID = '$idSede' AND tbl_bodega.tbl_bodega_ESTADO = 1");
			($this->db->execute())? true : false;
		}
		public function EditarBodega($datos) {
			$this->db->query("UPDATE tbl_bodega SET tbl_bodega_NOMBRE= :bodegaNombre, tbl_sede_tbl_sede_ID= :bodegaSede WHERE tbl_bodega_ID = :idBodega AND tbl_bodega_ESTADO = 1"); //
			$this->db->bind(':bodegaNombre', $datos['bodegaNombre']);
			$this->db->bind(':bodegaSede', $datos['bodegaSede']);
			$this->db->bind(':idBodega', $datos['idBodega']);
			($this->db->execute()) ? true : false;
		}
		public function CompararBodega($bodegaSede) {
			$this->db->query("SELECT * FROM tbl_bodega WHERE tbl_sede_tbl_sede_ID = '$bodegaSede' AND tbl_bodega.tbl_bodega_ESTADO = 1");
			return $resultado = $this->db->registros();
		}
			
		public function IfDataExist($idBodega){
		    $this->db->query("SELECT * FROM tbl_estante WHERE tbl_bodega_tbl_bodega_ID = '$idBodega' AND tbl_estante_ESTADO = 1");
		    $result = $this->db->registros();
		    return $result;
		}
	}
?>