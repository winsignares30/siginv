<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="CARGAR IMAGEN AQUI"></i>
    </div>
    <div class="sidebar-brand-text mx-3">SIGINV</div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
    <a class="nav-link" href="<?php echo URL_SISINV ?>Home/index">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>INICIO</span></a>
  </li>
  <!-- Divider -->
  <hr class="sidebar-divider">
  <!-- Heading -->
  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
      <i class="fas fa-fw fa-cog"></i>
      <span><strong>INVENTARIO</strong></span>
    </a>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Que desea realizar!</h6>
        <a class="collapse-item" href="<?php echo URL_SISINV; ?>Herramienta/ListarHerramienta"><strong>HERRAMIENTAS</strong></a>
        <a class="collapse-item" href="<?php echo URL_SISINV; ?>Material/ListarMaterial"><strong>MATERIALES</strong></a>
        <a class="collapse-item" href="<?php echo URL_SISINV; ?>Equipo/ListarEquipo"><strong>EQUIPOS</strong></a>
      </div>
    </div>
  </li>
  <!-- Nav Item - Utilities Collapse Menu -->
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseConfig" aria-expanded="true" aria-controls="collapseConfig">
      <i class="fas fa-fw fa-wrench"></i>
      <span><strong>CONFIGURACION</strong></span>
    </a>
    <div id="collapseConfig" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">¡Informacion del Sistema!</h6>
        <a class="collapse-item" href="<?php echo URL_SISINV; ?>Regional/ListarRegional"><strong>REGIONAL</strong></a>
        <a class="collapse-item" href="<?php echo URL_SISINV; ?>Centro/ListarCentro"><strong>CENTRO</strong></a>
        <a class="collapse-item" href="<?php echo URL_SISINV; ?>Sede/ListarSede"><strong>SEDE</strong></a>
        <a class="collapse-item" href="<?php echo URL_SISINV; ?>Ambiente/ListarAmbiente"><strong>AMBIENTE</strong></a>
        <a class="collapse-item" href="<?php echo URL_SISINV; ?>Bodega/ListarBodega"><strong>BODEGAS</strong></a>
        <a class="collapse-item" href="<?php echo URL_SISINV; ?>Estante/ListarEstante"><strong>ESTANTES</strong></a>
        <a class="collapse-item" href="<?php echo URL_SISINV; ?>Gaveta/ListarGaveta"><strong>GAVETAS</strong></a>
        <!-- <a class="collapse-item" href="#">Animations</a>
            <a class="collapse-item" href="#">Other</a> -->
      </div>
    </div>
  </li>

  <!-- Nav Item - Utilities Collapse Menu -->
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePers" aria-expanded="true" aria-controls="collapsePers">
      <i class="fas fa-users"></i>
      <span><strong>PERSONAL</strong></span>
    </a>
    <div id="collapsePers" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">¡Informacion del Personal!</h6>
        <a class="collapse-item" href="<?php echo URL_SISINV; ?>Persona/ListarAdministrador"><strong>ADMINISTRADOR</strong></a>
        <a class="collapse-item" href="<?php echo URL_SISINV; ?>Persona/ListarInstructor"><strong>INSTRUCTOR</strong></a>
         <a class="collapse-item" href="<?php echo URL_SISINV; ?>Persona/ListarHerramentero"><strong>HERRAMENTERO</strong></a>
        <!-- <a class="collapse-item" href="#">Animations</a>
            <a class="collapse-item" href="#">Other</a> -->
      </div>
    </div>
  </li>
    <li class="nav-item ">
        <a class="nav-link" href="<?php echo URL_SISINV?>Reporte/ImprimirReportes">
            <!--<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="icon-w3org bi bi-printer" viewBox="0 0 16 16" >
              <path d="M2.5 8a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z"/>
              <path d="M5 1a2 2 0 0 0-2 2v2H2a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h1v1a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-1h1a2 2 0 0 0 2-2V7a2 2 0 0 0-2-2h-1V3a2 2 0 0 0-2-2H5zM4 3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1v2H4V3zm1 5a2 2 0 0 0-2 2v1H2a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-1v-1a2 2 0 0 0-2-2H5zm7 2v3a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1z"/>
            </svg>-->
            <i class="fa-solid fa-print"></i>
            <span class=""><strong>REPORTE</strong></span>
        </a>
    </li>

  <!-- Divider -->
  <hr class="sidebar-divider">


  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>