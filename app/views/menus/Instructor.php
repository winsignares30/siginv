<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="CARGAR IMAGEN AQUI"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SIGINV</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="<?php echo URL_SISINV ?>Home/index">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>INICIO</span></a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">
    <!-- Heading -->
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSoli" aria-expanded="true" aria-controls="collapseSoli">
            <i class="fas fa-fw fa-wrench"></i>
            <span><strong>SOLICITUDES</strong></span>
        </a>
        <div id="collapseSoli" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">¡Recursos!</h6>
                <a class="collapse-item" href="<?php echo URL_SISINV; ?>SolicitudHerramientas/SolicitarHerramientas"><strong>HERRAMIENTAS</strong></a>
                <a class="collapse-item" href="<?php echo URL_SISINV; ?>SolicitudMateriales/SolicitarMateriales"><strong>MATERIALES</strong></a>
                <a class="collapse-item" href="<?php echo URL_SISINV; ?>SolicitudEquipos/SolicitarEquipos"><strong>EQUIPOS</strong></a>
                <!-- <a class="collapse-item" href="#">Animations</a>
                    <a class="collapse-item" href="#">Other</a> -->
            </div>
        </div>
    </li>
</ul>