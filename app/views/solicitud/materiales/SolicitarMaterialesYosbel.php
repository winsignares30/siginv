<?php require_once "../app/views/template.php"; ?>
<!--<div id="contenedor_carga">
  <div id="carga"></div>
</div>-->

<div class="tab-content">
    <div class="tab-pane tabs-animation fade active show" id="tab-content-0" role="tabpanel">
        <div class=" main-card mb-4 card">
            <div class="row">
                
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 p-3 row justify-content-center"> <!-- justify-content-center -->
                    <img src="<?php echo URL_SISINV ?>images/logosena.png" style="width:30%;">
                    <h5 class="align-self-end card-title text-center font-weight-bold text-primary">SENA: METALMECANICA
                        DE MALAMBO</h5>
                </div>
                
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 p-3 row align-items-center">
                    <div class="col">
                        <div>
                            <p class="card-title text-center font-weight-bold text-primary">REGISTRO</p>
                            <h6 class="card-title text-center font-weight-bold text-primary">SOLICITUD DE MATERIALES
                            </h6>
                            <p class="card-title text-center font-weight-bold text-primary">AMBIENTE: SOFTWARE </p>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 p-3 row align-items-center text-center">
                    <div class="col">
                        <div>
                             <p class="card-title  font-weight-bold text-primary">FECHA DE SOLICITUD:
                                <script type="text/javascript">
                                        var d = new Date();
                                        document.write(String(d.getDate()).padStart(2, '0') + '-' + String(d.getMonth() + 1).padStart(2, '0') + '-' + d.getFullYear(), '-' + d.getHours(), ':' + d.getMinutes(), '-' + d.getSeconds());
                                </script>
                            </p>

                            <p class="card-title  font-weight-bold text-primary">PERSONA QUE SOLICITA:
                                <?php echo $_SESSION['sesion_active']['nombre'] . " " .  $_SESSION['sesion_active']['p_apellido'] . " " .  $_SESSION['sesion_active']['s_apellido']; ?>
                            </p>
                            <input id="nombrePersona" type="hidden" value="<?php echo $_SESSION['sesion_active']['nombre'] . " " .  $_SESSION['sesion_active']['p_apellido'] . " " .  $_SESSION['sesion_active']['s_apellido']; ?>">
                            <h6 class="card-title  font-weight-bold text-primary">JEFE INMEDIATO: ALDO SILVERA</h6>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <br>
    <div class="card shadow mb-4">
        <div class="card-header py-3 px-5">
            <div class="row">
                <div class="col-lg-5 col-12">
                    <br>
                    <h6 class="m-0 font-weight-bold text-primary pt-4">LISTADO DE MATERIALES</h6>
                </div>
                <div class="col-lg-5 col-12 pt-4">
                    <div class="col-xs-5">
                        <div class="row">
                            <div class="col-lg-2">
                                <label class="" style="font-weight: bold;">
                                    <h5>Ficha:*</h5>
                                </label>
                            </div>
                        
                            <!--<input class="form-control" id="ficha" placeholder="Ficha" type="text" required>-->
                            <!--Fichas para seleccionar-->
                            <div class="col-lg-10">
                                <select class="form-control" id="ficha" required>
                                    <option>--SELECCIONAR--</option>
                                    <?php foreach ($datos['LoadFichas'] as $LoadFichas) : ?> <!-- La var $datos llama la llave y a su vez la asigna como la var $ListarPrograma ya declara en el controlador-->
                                        <?php if ($LoadFichas) : ?> <!-- Se realiza una condici贸n donde si es verdadera traer谩 en primer lugar el ID del programa y luego traemos el Nombre para luego ser seleccionado con el tag Option -->
                                            <option value="<?php echo $LoadFichas->tbl_ficha_GRUPO; ?>"><?php echo $LoadFichas->tbl_ficha_GRUPO; ?></option>
                                        <?php else : ?>
                                            <option value="0">-- NO SE ENCONTRARON DATOS --</option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select><br>
                                <!--Fin-->
                                <p id="id_request"></p>
                            </div>
                            
                        </div>
                     </div>
                </div>
                <div class="col-lg-2 col-12">
                    <div class="row justify-content-center">
                        <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR' || $_SESSION['sesion_active']['tipo_usuario'] == 'INSTRUCTOR') : ?>
                            <div class="col-6 p-4">
                                <cite title="Agregar" id='BtnMateriales'>
                                    <center><a class="btn btn-none btn-icon-split" id="btnMateriales" data-toggle="modal" data-target="#Agregarmateriales">
                                        <!--<span class="icon text-white-50">
                                            <i class="fas fa-plus"></i>
                                        </span>-->
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-cart-plus" viewBox="0 0 16 16">
                                          <path d="M9 5.5a.5.5 0 0 0-1 0V7H6.5a.5.5 0 0 0 0 1H8v1.5a.5.5 0 0 0 1 0V8h1.5a.5.5 0 0 0 0-1H9V5.5z"/>
                                          <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
                                        </svg>
                                    </a></center>
                                </cite>
                                <cite title="A" id='BottonMateriales'>
                                    
                                </cite>
                            </div>
                            <div class="col-6 p-4">
                                <cite title="Ver" id='VerHerramientas'>
                                    <center><a class="btn btn-none btn-icon-split" id="" data-toggle="modal" data-target="#AgregarHerramienta2">
                                        <!--<span class="icon text-white-50">
                                            <i class="fas fa-plus"></i>
                                        </span>-->
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-clipboard2-data" viewBox="0 0 16 16">
                                          <path d="M9.5 0a.5.5 0 0 1 .5.5.5.5 0 0 0 .5.5.5.5 0 0 1 .5.5V2a.5.5 0 0 1-.5.5h-5A.5.5 0 0 1 5 2v-.5a.5.5 0 0 1 .5-.5.5.5 0 0 0 .5-.5.5.5 0 0 1 .5-.5h3Z"/>
                                          <path d="M3 2.5a.5.5 0 0 1 .5-.5H4a.5.5 0 0 0 0-1h-.5A1.5 1.5 0 0 0 2 2.5v12A1.5 1.5 0 0 0 3.5 16h9a1.5 1.5 0 0 0 1.5-1.5v-12A1.5 1.5 0 0 0 12.5 1H12a.5.5 0 0 0 0 1h.5a.5.5 0 0 1 .5.5v12a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5v-12Z"/>
                                          <path d="M10 7a1 1 0 1 1 2 0v5a1 1 0 1 1-2 0V7Zm-6 4a1 1 0 1 1 2 0v1a1 1 0 1 1-2 0v-1Zm4-3a1 1 0 0 0-1 1v3a1 1 0 1 0 2 0V9a1 1 0 0 0-1-1Z"/>
                                        </svg>
                                    </a></center>
                                </cite>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <!-- tabla solicitud final -->
                <table class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Código</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">Stock</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Solicitud De Medidas</th>
                            <th scope="col">Acción</th>
                        </tr>
                    </thead>
                    <tbody id="items">
                    </tbody>

                </table>
                <tfoot>
                    <div id="footer">
                        <br>
                        <th scope="row" colspan="5">carrito de solicitudes vacío - comience hacer una solicitud!</th>
                    </div>
                </tfoot>

                <template id="template-footer">

                    <th scope="row" class="pr-2" colspan="2">Total materialess : </th>
                    <td>10</td>

                    <td class="d-flex justify-content-center">
                        <button class="btn btn-danger btn-sm mr-5" id="vaciar-carrito2">
                            Vaciar
                            <i class="far fa-trash-alt"></i>
                        </button>
                       <!--<button id="enviar" class="btn btn-success">Enviar<i class="fas fa-location-arrow pl-2"></i></button>-->
                       <button id="enviar" class="btn btn-success" type="submit" value="">Enviar<i class="fas fa-location-arrow pl-2"></i></button>
                    </td>

                </template>

                <template id="template-carrito2">
                    <tr>
                        <th scope="row">id</th>
                        <td>Código</td>
                        <td>nombre</td>
                        <td>descripcion</td>
                        <td>Stock</td>
                        <td>cantidad</td>
                        <td><input type="text" id="input_cantidad" value=""></td>
                        
                        <td>
                            <center>
                                <button class="btn btn-info m-2">
                                    +
                                    <!--<i class="fas fa-plus"></i>-->
                                </button>
                                <button class="btn btn-danger m-2">
                                    -
                                    <!--<i class="fas fa-minus"></i>-->
                                </button>
    
                                <button class="btn btn-secondary m-2">
    
                                    <i class="far fa-trash-alt"></i>
                                </button>
                            </center>
                        </td>
                        
                        
                        
                    </tr>
                </template>

            </div>
        </div>
    </div>
</div>

<!-- /.container-fluid  -->
<!-- MODAL INSERTAR MATERIALES -->
<div class="modal fade" id="Agregarmateriales" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">AGREGAR MATERIALES</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" style="overflow: auto; width: width:'100%'; height: 600px; background: #ff6b00">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <div class="row">
                            <div class="col-md-11">
                                <br>
                                <h6 class="m-0 font-weight-bold text-primary">LISTADO DE MATERIALES</h6>
                            </div>
                        </div>
                          <input type="text" id="card-header_search-input" style="float: right" placeholder="Search">
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Código</th>
                                        <th>Descripción</th>
                                        <th>Programa</th>
                                        <th>Stock</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody id="cards">
    
                                </tbody>
    
                            </table>
                        </div>
                        <template id="template-card">
                            <tr class="tr">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <!--<button class="btn btn-success"><i class="fas fa-plus"></i></button>-->
                                    <button class="btn btn-success" type="submit" > 
                                        Agregar
                                    </button>
                                </td>
                            </tr>
                        </template>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>

<!-- MODAL VER HERRAMIENTAS SOLICITADAS-->
<div class="modal fade" id="AgregarHerramienta2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">MATERIALES</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <div class="row">
                            <div class="col-md-11">
                                <br>
                                <h6 class="m-0 font-weight-bold text-primary">LISTADO DE MATERIALES SOLICITADOS</h6>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th>Cantidad</th>
                                    <th>Fecha</th>
                                    <th>Estado</th>
                                    <th>Rechazo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $contador = 1;
                                foreach ($datos['MaterialesSolicitados'] as $MaterialesSolicitados) : ?>
                                    <tr>
                                        <th scope="row"><?php echo $contador++; ?></th>
                                        <td><?php echo $MaterialesSolicitados->codigo; ?></td>
                                        <td><?php echo $MaterialesSolicitados->nombre; ?></td>
                                        <td><?php echo $MaterialesSolicitados->descripcion; ?></td>
                                        <td><?php echo $MaterialesSolicitados->cantidad; ?></td>
                                        <td><?php echo $MaterialesSolicitados->fecha; ?></td>
                                        <?php if ($MaterialesSolicitados->estado == 1) : ?>
                                            <td><?php echo "Ingreso" ?></td>
                                        <?php elseif($MaterialesSolicitados->estado == 2): ?>
                                            <td><?php echo "Aprobado";?></td>
                                        <?php elseif($MaterialesSolicitados->estado == 3): ?>
                                            <td><?php echo "Entregado"?></td>
                                        <?php else : ?>
                                            <td><?php echo "Devuelta" ?></td>
                                        <?php endif ?>
                                        <td><?php echo $MaterialesSolicitados->motivoRechazo; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        
         $('#id_request').hide()
        // funciones para crear el id de la solicitud
        // Función "añadir cero".
        function addZero(x, n) {
          while (x.toString().length < n) {
            x = "0" + x;
          }
          return x;
        }
        
        // Añadir control al elemento "p" principal de la página.
        addControl()
        function addControl() {
          var d = new Date();
          var x = document.getElementById("id_request");
          var y = addZero(d.getFullYear(), 4)
          var month = addZero(d.getMonth() + 1, 2);
          var day = addZero(d.getDate(), 2)
          var h = addZero(d.getHours(), 2);
          var m = addZero(d.getMinutes(), 2);
          var s = addZero(d.getSeconds(), 2);
          var ms = addZero(d.getMilliseconds(), 7);

           x.innerHTML += "<p id='"+ y + month+ day + h + m + s + ms + "'>SM" + y + month + day + h  + m + s + ms + "</p>";

        }
        
        let carrito2 = {}
        let toolList = [];

        const tr = document.getElementById('tr')
        const cards = document.getElementById('cards')
        const items = document.getElementById('items')
        const footer = document.getElementById('footer')
        const filtro = document.getElementById('filtro')
        const templateFiltro = document.getElementById('template-filtrar')
        const templateCard = document.getElementById('template-card').content
        const templatecarrito2 = document.getElementById('template-carrito2').content
        const templateFooter = document.getElementById('template-footer').content
        const fragment = document.createDocumentFragment()
        const searchCardsInput = document.querySelector("#card-header_search-input")
        
        function rePintarCards(tools){
            cards.innerHTML = ""
            pintarCards2(tools)
        }
        
        searchCardsInput.addEventListener('input', (event) =>{
            let inputValue = $("#card-header_search-input").val() // obtener el valor 
            
            toolsResults = []
            
            toolList.forEach(tool =>{
                let toolFound = false
                let toolProps = Object.entries(tool); // arreglo que contiene las propiedades de una sola herramienta
                toolProps.forEach(prop =>{
                    if(toolFound) return // indica que una propiedad fue encontrada en el input y no es necesario buscarla de nuevo
                    
                    let propValue = prop[1]
                    let propIdentifier = prop[0]
                    
                    if(propIdentifier.includes("FECHA") || propIdentifier.includes('ARTICULACION')){
                        // ocurre cuando la propiedad es la fecha, en este caso no nos interesa
                        return
                        
                    }else if(typeof(propValue) === typeof("string")){
                        // ocurre cuando la propiedad es string
                        
                        let propValueLowerCased = propValue.toLowerCase()
                        let propValueUpperCased = propValue.toUpperCase()
                        
                        if(propValueLowerCased.includes(inputValue)){
                            toolsResults.push(tool)
                            toolFound = true
                            
                        }else if(propValueUpperCased.includes(inputValue)){
                            toolsResults.push(tool)
                            toolFound = true
                        }
                    }else{
                        // ocurre cuando la propiedad es un número
                        let propValueTransformed = propValue.toString()
                        if(propValueTransformed.includes(inputValue)){
                            toolsResults.push(tool)
                            toolFound = true
                        }
                    }
                })
            })
            if(inputValue === ""){
                rePintarCards(toolList)
            }else{
                rePintarCards(toolsResults)
            }
            
            
            
        })
        
        cards.addEventListener('click', e => {
            addcarrito2(e)
        })
        items.addEventListener('click', e => {
            btnAccion(e)
        })

        $('#BottonMateriales').hide();
        
        for(var h = 0; h < 1; h++){
            var AgreagarBtn = "";
            AgreagarBtn += '<center><a class="btn btn-none btn-icon-split" id="bottonMateriales" data-toggle="modal" data-target="#Agregarmateriales"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-cart-check" viewBox="0 0 16 16"><path d="M11.354 6.354a.5.5 0 0 0-.708-.708L8 8.293 6.854 7.146a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z"/><path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/></svg></a></center>';
            $("#BottonMateriales").html(AgreagarBtn);
        }
        

        $('#BtnMateriales').click(function() {

            $('#BtnMateriales').hide();
            $('#BottonMateriales').show();

            $.ajax({
                url: '<?php echo URL_SISINV ?>SolicitudMateriales/SolicitarMateriales2',
                type: 'GET',
                datatype: 'json'
            }).done(response => {
                toolList = JSON.parse(response);
                pintarCards(toolList)
                
            });
        });

        //Pintar materialess

        const pintarCards = toolList => {
            let contador = 0
            toolList.forEach(materiales => {
                contador = contador + 1
                templateCard.querySelectorAll('td')[0].textContent = contador
                templateCard.querySelectorAll('td')[1].textContent = materiales.tbl_materiales_UNSPSC
                templateCard.querySelectorAll('td')[2].textContent = materiales.tbl_materiales_DESCRIPCION
                templateCard.querySelectorAll('td')[3].textContent = materiales.tbl_materiales_PROGRAMAFORMACION
                templateCard.querySelectorAll('td')[4].textContent = materiales.tbl_materiales_CANTIDAD
                /*templateCard.querySelectorAll('td')[5].textContent = materiales.tbl_materiales_Medidas*/
                templateCard.querySelector('.btn-success').dataset.id = materiales.tbl_materiales_ID


                const clone = templateCard.cloneNode(true)
                fragment.appendChild(clone)
            })
            cards.appendChild(fragment)
        }
        
        const pintarCards2 = toolList => {
            let contador = 0
            toolList.forEach(materiales => {
                contador = contador + 1
                templateCard.querySelectorAll('td')[0].textContent = contador
                templateCard.querySelectorAll('td')[1].textContent = materiales.tbl_materiales_UNSPSC
                templateCard.querySelectorAll('td')[2].textContent = materiales.tbl_materiales_DESCRIPCION
                templateCard.querySelectorAll('td')[3].textContent = materiales.tbl_materiales_PROGRAMAFORMACION
                templateCard.querySelectorAll('td')[4].textContent = materiales.tbl_materiales_CANTIDAD
                /*templateCard.querySelectorAll('td')[5].textContent = materiales.tbl_materiales_Medidas*/
                templateCard.querySelector('.btn-success').dataset.id = materiales.tbl_materiales_ID


                const clone = templateCard.cloneNode(true)
                fragment.appendChild(clone)
            })
            cards.appendChild(fragment)
        }
        
        // agregar al arrito
        const addcarrito2 = e => {

            if (e.target.classList.contains('btn-success')) {
                let id = e.target.dataset.id

                let objeto = toolList.find(objeto => objeto.tbl_materiales_ID === id)
                setcarrito2(objeto)
            }
            e.stopPropagation()
        }
        
        // se establece el carrito
        const setcarrito2 = objeto => {
            const materiales = {
                id: objeto.tbl_materiales_ID,
                codigo: objeto.tbl_materiales_UNSPSC,
                nombre: objeto.tbl_materiales_DESCRIPCION,
                descripcion: objeto.tbl_materiales_DESCRIPCION,
                stock: objeto.tbl_materiales_CANTIDAD,
                rubro: objeto.rubro,
                cantidad: 1,
               /* solicitud de medidas : objeto.tbl_materiales_Medidas, */
                tipo: 'material'
            }
            carrito2[materiales.id] = {
                ...materiales
            } // copia de carrito2
            pintarcarrito2()
        }

        // mostrar carrito2
        const pintarcarrito2 = () => {

            items.innerHTML = "" // limpiar carrito2. es decir que parta en 0
            let contador = 0
            Object.values(carrito2).forEach(materiales => {
                contador++

                templatecarrito2.querySelector('th').textContent = contador
                templatecarrito2.querySelectorAll('td')[0].textContent = materiales.codigo
                templatecarrito2.querySelectorAll('td')[1].textContent = materiales.nombre
                templatecarrito2.querySelectorAll('td')[2].textContent = materiales.descripcion
                templatecarrito2.querySelectorAll('td')[3].textContent = materiales.stock
                templatecarrito2.querySelectorAll('td')[4].textContent = materiales.cantidad
               /* templatecarrito2.querySelectorAll('td')[5].textContent = materiales.medidas*/
                templatecarrito2.querySelector('.btn-info').dataset.id = materiales.id
                templatecarrito2.querySelector('.btn-danger').dataset.id = materiales.id
                templatecarrito2.querySelector('.btn-secondary').dataset.id = materiales.id

                const clone = templatecarrito2.cloneNode(true)
                fragment.appendChild(clone)
            })
            items.appendChild(fragment)
            pintarFooter()
            // local storage
            sessionStorage.setItem('carrito2', JSON.stringify(carrito2))

        }


        const pintarFooter = () => {
            footer.innerHTML = "" // limpiar 
            if (Object.keys(carrito2).length === 0) {
                footer.innerHTML = `
                    <th scope="row" colspan="5">carrito2 de devoluciones vacío - comience hacer una solicitud!</th>
                    `
                return
            }
            const cantidad = Object.values(carrito2).reduce((acumulador, {
                cantidad
            }) => acumulador + cantidad, 0)


            templateFooter.querySelectorAll('td')[0].textContent = cantidad


            const clone = templateFooter.cloneNode(true)
            fragment.appendChild(clone)
            footer.appendChild(fragment)

            // vaciar carrito2 
            const btnVaciar = document.getElementById('vaciar-carrito2')
            btnVaciar.addEventListener('click', () => {
                Swal.fire({
                    title: '¿Quieres vaciar todo?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, vaciar !'
                }).then((result) => {
                    if (result.isConfirmed) {
                        carrito2 = {}
                        pintarcarrito2() // se ejecutanuevamente
                    }
                })

            })

            document.getElementById("enviar").addEventListener("click", function() {
                let ficha = $('#ficha').val()
                let id_solicitud = document.getElementById('id_request').textContent;
                var wasSent = true
                if(ficha === "" || ficha === "--SELECCIONAR--"){
                    FillData()
                }else{
                    for (const property in carrito2) {
                        var id = `${carrito2[property]['id']}`
                        var codigo = `${carrito2[property]['codigo']}`
                        var nombre = `${carrito2[property]['nombre']}`
                        var stock = `${carrito2[property]['stock']}`
                        var cantidad = `${carrito2[property]['cantidad']}`
                        var descripcion = `${carrito2[property]['descripcion']}`
                        var rubro = `${carrito2[property]['rubro']}`
                        var tipo = `${carrito2[property]['tipo']}`
                        var nombrePersona = $('#nombrePersona').val()
                        $.ajax({
                            url: '<?php echo URL_SISINV ?>SolicitudEquipos/EnviarEquipos',
                            type: 'POST',
                            data: {
                                id: id,
                                id_solicitud: id_solicitud,
                                codigo: codigo,
                                nombre: nombre,
                                cantidad: cantidad,
                                descripcion: descripcion,
                                stock: stock,
                                rubro: rubro,
                                ficha: ficha,
                                tipo: tipo,
                                nombrePersona: nombrePersona
                            }
                        }).done(function() {
                            sessionStorage.removeItem('carrito2')
                            wasSent = true;
                        }).fail(function() {
                            sessionStorage.removeItem('carrito2')
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'No se pudo enviar la solicitud',
                                showConfirmButton: false,
                                timer: 3000
                            })
                            setTimeout(function() {
                                window.location.href = '<?php echo URL_SISINV ?>SolicitudMateriales/SolicitarMateriales';
                            }, 3000);
                        })
                    }
                    if(wasSent === true){
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Solicitud enviada exitosamente',
                            showConfirmButton: false,
                            timer: 3000
                        })
                        setTimeout(function() {
                            window.location.href = '<?php echo URL_SISINV ?>SolicitudMateriales/SolicitarMateriales';
                        }, 3000);
                    }
                }
            })

        }

        // accciones de los botones
        const btnAccion = e => {
            // aumentar
            if (e.target.classList.contains('btn-info')) {
                const materiales = carrito2[e.target.dataset.id]
                let stockValue = carrito2[e.target.dataset.id].stock
                let cantidadValue = carrito2[e.target.dataset.id].cantidad
                if (stockValue > cantidadValue) {
                    materiales.cantidad = carrito2[e.target.dataset.id].cantidad + 1
                    carrito2[e.target.dataset.id] = {
                        ...materiales
                    } // copia
                    pintarcarrito2()
                }

            }

            //acccion de disminuir
            if (e.target.classList.contains('btn-danger')) {
                const materiales = carrito2[e.target.dataset.id]

                materiales.cantidad-- // restar
                if (materiales.cantidad === 0) {
                    delete carrito2[e.target.dataset.id]
                }
                pintarcarrito2()
            }


            if (e.target.classList.contains('btn-secondary')) {

                delete carrito2[e.target.dataset.id]

                pintarcarrito2()
            }

            e.stopPropagation()
        }

        // obtener datos del localSotage
        if (sessionStorage.getItem('carrito2')) {
            carrito2 = JSON.parse(sessionStorage.getItem('carrito2'));
            pintarcarrito2()
        }
        /*window.onload = function() {
        var contenedor = document.getElementById('contenedor_carga');
        contenedor.style.visibility = 'hidden';
        contenedor.style.opacity = '0';
        }*/

    })
</script>