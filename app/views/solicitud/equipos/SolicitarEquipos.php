<?php require_once "../app/views/template.php"; ?>
<!--<div id="contenedor_carga">
  <div id="carga"></div>
</div>-->

<div class="tab-content">
    <div class="tab-pane tabs-animation fade active show" id="tab-content-0" role="tabpanel">
        <div class=" main-card mb-4 p-3 card">
            <div class="row">
                
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 p-3 justify-content-center row">
                    <img src="<?php echo URL_SISINV ?>images/logosena.png" style="width:30%;">
                    <h5 class="align-self-end card-title text-center font-weight-bold text-primary">SENA: METALMECANICA
                        DE MALAMBO</h5>
                </div>
                
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 p-1 pt-3 row align-items-center">
                    <div class="col">
                        <div>
                            <p class="card-title text-center font-weight-bold text-primary">REGISTRO</p>
                            <h6 class="card-title text-center font-weight-bold text-primary">SOLICITUD DE EQUIPOS
                            </h6>
                            <p class="card-title text-center font-weight-bold text-primary">AMBIENTE: SOFTWARE </p>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 p-3 row align-items-center text-center">
                    <div class="col">
                        <div>
                            <p class="card-title  font-weight-bold text-primary">FECHA DE SOLICITUD:
                                <script type="text/javascript">
                                        var d = new Date();
                                        document.write(String(d.getDate()).padStart(2, '0') + '-' + String(d.getMonth() + 1).padStart(2, '0') + '-' + d.getFullYear(), '-' + d.getHours(), ':' + d.getMinutes(), '-' + d.getSeconds());
                                </script>
                            </p>

                            <p class="card-title  font-weight-bold text-primary">PERSONA QUE SOLICITA:
                                <?php echo $_SESSION['sesion_active']['nombre'] . " " .  $_SESSION['sesion_active']['p_apellido'] . " " .  $_SESSION['sesion_active']['s_apellido']; ?>
                            </p>
                            <input id="idPersona" type="hidden" value="<?php echo $_SESSION['sesion_active']['cod'] ?>">
                            <input id="nombrePersona" type="hidden" value="<?php echo $_SESSION['sesion_active']['nombre'] . " " .  $_SESSION['sesion_active']['p_apellido'] . " " .  $_SESSION['sesion_active']['s_apellido']; ?>">
                            <h6 class="card-title  font-weight-bold text-primary">JEFE INMEDIATO: ALDO SILVERA</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="card shadow mb-4">
        <div class="card-header py-3 px-5">
            <div class="row">
                 <div class="col-lg-5 col-12">
                    <br>
                    <h6 class="m-0 font-weight-bold text-primary pb-4">LISTADO DE EQUIPOS</h6>
                </div>
                <div class="col-lg-5 col-12 pt-4">
                    <div class="col-xs-5">
                        <div class="row">
                            <div class="col-lg-2">
                                <label class="" style="font-weight: bold;">
                                    <h5 class="">Ficha:*</h5>
                                </label>
                            </div>
                            <!--<input class="form-control" id="ficha" placeholder="Ficha" type="text" required>-->
                            <!--Fichas para seleccionar-->
                            <div class="col-lg-10">
                                <select class="form-control" id="ficha" required>
                                    <option>--SELECCIONAR--</option>
                                    <?php foreach ($datos['LoadFichas'] as $LoadFichas) : ?> <!-- La var $datos llama la llave y a su vez la asigna como la var $ListarPrograma ya declara en el controlador-->
                                        <?php if ($LoadFichas) : ?> <!-- Se realiza una condici贸n donde si es verdadera traer谩 en primer lugar el ID del programa y luego traemos el Nombre para luego ser seleccionado con el tag Option -->
                                            <option value="<?php echo $LoadFichas->tbl_ficha_GRUPO; ?>"><?php echo $LoadFichas->tbl_ficha_GRUPO; ?></option>
                                        <?php else : ?>
                                            <option value="0">-- NO SE ENCONTRARON DATOS --</option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select><br>
                                <!--Fin-->
                                <p id="id_request"></p>
                            </div>
                        </div>
                     </div>
                </div>
                    
                <div class="col-lg-2 col-12">
                    <div class="row justify-content-center">
                        <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR' || $_SESSION['sesion_active']['tipo_usuario'] == 'INSTRUCTOR') : ?>
                            <div class="col-6 p-4">
                                <cite title="Agregar" id="bottonEquipos">
                                    <center><a class="btn btn-none btn-icon-split" data-toggle="modal" data-target="#Agregarequipo">
                                        <!--<span class="icon text-white-50">
                                            <i class="fas fa-plus"></i>
                                        </span>-->
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-cart-plus" viewBox="0 0 16 16">
                                          <path d="M9 5.5a.5.5 0 0 0-1 0V7H6.5a.5.5 0 0 0 0 1H8v1.5a.5.5 0 0 0 1 0V8h1.5a.5.5 0 0 0 0-1H9V5.5z"/>
                                          <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
                                        </svg>
                                    </a></center>
                                </cite>
        
                                <cite title="Agregar" id="btnEquipos">
                                    
                                </cite>
                            </div>
                            <div class="col-6 p-4">
                                <cite title="Ver" id='VerHerramientas'>
                                    <center><a class="btn btn-none btn-icon-split" id="" data-toggle="modal" data-target="#AgregarHerramienta2">
                                        <!--<span class="icon text-white-50">
                                            <i class="fas fa-plus"></i>
                                        </span>-->
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-clipboard2-data" viewBox="0 0 16 16">
                                          <path d="M9.5 0a.5.5 0 0 1 .5.5.5.5 0 0 0 .5.5.5.5 0 0 1 .5.5V2a.5.5 0 0 1-.5.5h-5A.5.5 0 0 1 5 2v-.5a.5.5 0 0 1 .5-.5.5.5 0 0 0 .5-.5.5.5 0 0 1 .5-.5h3Z"/>
                                          <path d="M3 2.5a.5.5 0 0 1 .5-.5H4a.5.5 0 0 0 0-1h-.5A1.5 1.5 0 0 0 2 2.5v12A1.5 1.5 0 0 0 3.5 16h9a1.5 1.5 0 0 0 1.5-1.5v-12A1.5 1.5 0 0 0 12.5 1H12a.5.5 0 0 0 0 1h.5a.5.5 0 0 1 .5.5v12a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5v-12Z"/>
                                          <path d="M10 7a1 1 0 1 1 2 0v5a1 1 0 1 1-2 0V7Zm-6 4a1 1 0 1 1 2 0v1a1 1 0 1 1-2 0v-1Zm4-3a1 1 0 0 0-1 1v3a1 1 0 1 0 2 0V9a1 1 0 0 0-1-1Z"/>
                                        </svg>
                                    </a></center>
                                </cite>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <!-- tabla solicitud final -->
                <table class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Código</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">Stock</th>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Acción</th>
                        </tr>
                    </thead>
                    <tbody id="items">
                    </tbody>

                </table>
                <tfoot>
                    <div id="footer">
                        <br>
                        <th scope="row" colspan="5">carrito de solicitudes vacío - comience hacer una solicitud!</th>
                    </div>
                </tfoot>

                <template id="template-footer">

                    <th scope="row" class="pr-2" colspan="2">Total equipos : </th>
                    <td>10</td>

                    <td class="d-flex justify-content-center">
                        <button class="btn btn-danger btn-sm mr-5" id="vaciar-carrito3">
                            Vaciar
                            <i class="far fa-trash-alt"></i>
                        </button>
                       <!-- <button id="enviar" class="btn btn-success">Enviar<i class="fas fa-location-arrow pl-2"></i></button>-->
                       <button id="enviar" class="btn btn-success" type="submit" value="">Enviar<i class="fas fa-location-arrow pl-2"></i></button>
                    </td>

                </template>


                <template id="template-carrito3">
                    <tr>
                        <th scope="row">id</th>
                        <td>Código</td>
                        <td>nombre</td>
                        <td>descripcion</td>
                        <td>Stock
                        <td>cantidad</td>
                        <td>
                            <center>
                                <button class="btn btn-info m-2">
                                    +
                                    <!--<i class="fas fa-plus"></i>-->
                                </button>
                                <button class="btn btn-danger m-2">
                                    -
                                    <!--<i class="fas fa-minus"></i>-->
                                </button>
    
                                <button class="btn btn-secondary m-2">
    
                                    <i class="far fa-trash-alt"></i>
                                </button>
                            </center>
                        </td>


                    </tr>
                </template>

            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid  -->
<!-- MODAL INSERTAR EQUIPOS -->
<div class="modal fade" id="Agregarequipo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">AGREGAR EQUIPOS</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" style="overflow: auto; width: width:'100%'; height: 600px; background: #ff6b00">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <div class="row">
                            <div class="col-md-11">
                                <br>
                                <h6 class="m-0 font-weight-bold text-primary">LISTADO DE EQUIPOS</h6>
                            </div>

                        </div>
                        <input type="text" id="card-header_search-input" style="float: right" placeholder="Search" >
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Código</th>
                                        <th>Nombre</th>
                                        <th>Descripción</th>
                                        <th>stock</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody id="cards">
    
                                </tbody>
    
                            </table>
                        </div>
                        <template id="template-card">
                            <tr class="tr">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><!--<button class="btn btn-success"><i class="fas fa-plus"></i></button>-->
                                    <button class="btn btn-success" type="submit" > 
                                        Agregar
                                    </button>
                                </td>
                            </tr>
                        </template>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>

<!-- MODAL VER HERRAMIENTAS SOLICITADAS-->
<div class="modal fade" id="AgregarHerramienta2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">MATERIALES</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <div class="row">
                            <div class="col-md-11">
                                <br>
                                <h6 class="m-0 font-weight-bold text-primary">LISTADO DE EQUIPOS SOLICITADOS</h6>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th>Cantidad</th>
                                    <th>Fecha</th>
                                    <th>Estado</th>
                                    <th>Rechazo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $contador = 1;
                                foreach ($datos['EquiposSolicitados'] as $EquiposSolicitados) : ?>
                                    <tr>
                                        <th scope="row"><?php echo $contador++; ?></th>
                                        <td><?php echo $EquiposSolicitados->codigo; ?></td>
                                        <td><?php echo $EquiposSolicitados->nombre; ?></td>
                                        <td><?php echo $EquiposSolicitados->descripcion; ?></td>
                                        <td><?php echo $EquiposSolicitados->cantidad; ?></td>
                                        <td><?php echo $EquiposSolicitados->fecha; ?></td>
                                        <?php if ($EquiposSolicitados->estado == 1) : ?>
                                            <td><?php echo "Ingreso" ?></td>
                                        <?php elseif($EquiposSolicitados->estado == 2): ?>
                                            <td><?php echo "Aprobado";?></td>
                                        <?php elseif($EquiposSolicitados->estado == 3): ?>
                                            <td><?php echo "Entregado"?></td>
                                        <?php else : ?>
                                            <td><?php echo "Devuelta" ?></td>
                                        <?php endif ?>
                                        <td><?php echo $EquiposSolicitados->motivoRechazo; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
         $('#id_request').hide()
        // funciones para crear el id de la solicitud
        // Función "añadir cero".
        function addZero(x, n) {
          while (x.toString().length < n) {
            x = "0" + x;
          }
          return x;
        }
        
        // Añadir control al elemento "p" principal de la página.
        addControl()
        function addControl() {
          var d = new Date();
          var x = document.getElementById("id_request");
          var y = addZero(d.getFullYear(), 4)
          var month = addZero(d.getMonth() + 1, 2);
          var day = addZero(d.getDate(), 2)
          var h = addZero(d.getHours(), 2);
          var m = addZero(d.getMinutes(), 2);
          var s = addZero(d.getSeconds(), 2);
          var ms = addZero(d.getMilliseconds(), 7);

           x.innerHTML += "<p id='"+ y + month+ day + h + m + s + ms + "'>SE" + y + month + day + h  + m + s + ms + "</p>";

        }
        
        
        let carrito3 = {}
        let toolList = []

        const tr = document.getElementById('tr')
        const cards = document.getElementById('cards')
        const items = document.getElementById('items')
        const footer = document.getElementById('footer')
        const templatecarrito3 = document.getElementById('template-carrito3').content
        const templateCard = document.getElementById('template-card').content
        const templateFooter = document.getElementById('template-footer').content
        const fragment = document.createDocumentFragment()
        const cardsInputSearch = document.querySelector("#card-header_search-input")
        
        function rePintarCards(tool){
            cards.innerHTML = ""
            pintarCards2(tool)
        }
        
        cardsInputSearch.addEventListener("input", (event)=>{
            let inputValue = $("#card-header_search-input").val()
            
            toolsResults = []
            
            toolList.forEach(tool=>{
                let toolFound = false
                
                let toolProps = Object.entries(tool)
                
                toolProps.forEach(prop =>{
                    
                    if(toolFound) return
                    
                    let propValue = prop[1]
                    let propIdentifier = prop[0]
                    
                    if(typeof(propValue) === typeof("string")){
                        let propValueLowerCased = propValue.toLowerCase()
                        let propValueUpperCased = propValue.toUpperCase()
                        
                        if(propValueLowerCased.includes(inputValue)){
                            toolsResults.push(tool)
                            toolFound = true
                        }else if(propValueUpperCased.includes(inputValue)){
                            toolsResults.push(tool)
                            toolFound = true
                        }
                    }else{
                        let propValueTransformed = propValue.toString()
                        if(propValueTransformed.includes(inputValue)){
                            toolsResults.push(tool)
                            toolFound = true
                        }
                    }
                })
            })
            
            if(inputValue === ""){
                rePintarCards(toolList)
            }else{
                rePintarCards(toolsResults)
            }
        })
        

        cards.addEventListener('click', e => {
            addCarrito(e)
        })
        items.addEventListener('click', e => {
            btnAccion(e)
        })

        $('#btnEquipos').hide();
        for(var h = 0; h < 1; h++){
            var AgreagarBtn = "";
            AgreagarBtn += '<center><a class="btn btn-none btn-icon-split" data-toggle="modal" data-target="#Agregarequipo"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-cart-check" viewBox="0 0 16 16"><path d="M11.354 6.354a.5.5 0 0 0-.708-.708L8 8.293 6.854 7.146a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z"/><path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/></svg></a></center>';
            $("#btnEquipos").html(AgreagarBtn);
        }
        
        $('#bottonEquipos').on('click', function() {
            $('#bottonEquipos').hide()
            $('#btnEquipos').show()

            $.ajax({
                url: '<?php echo URL_SISINV ?>SolicitudEquipos/SolicitarEquipos2',
                type: 'GET',
                datatype: 'json'
            }).done(response => {
                toolList = JSON.parse(response)
                pintarCards(toolList)
            })
        })

        // Pintar Equipos
        const pintarCards = toolList => {
            let contador = 0

            toolList.forEach(equipo => {
                contador = contador + 1
                templateCard.querySelectorAll('td')[0].textContent = contador
                templateCard.querySelectorAll('td')[1].textContent = equipo.tbl_equipo_PLACA
                templateCard.querySelectorAll('td')[2].textContent = equipo.tbl_equipo_DESCRIPCION
                templateCard.querySelectorAll('td')[3].textContent = equipo.tbl_equipo_TIPO
                templateCard.querySelectorAll('td')[4].textContent = equipo.tbl_equipo_CANTIDAD
                templateCard.querySelector('.btn-success').dataset.id = equipo.tbl_equipo_ID

                const clone = templateCard.cloneNode(true)
                fragment.appendChild(clone)
            })
            cards.appendChild(fragment)
        }
        
        const pintarCards2 = toolList => {
            let contador = 0

            toolList.forEach(equipo => {
                contador = contador + 1
                templateCard.querySelectorAll('td')[0].textContent = contador
                templateCard.querySelectorAll('td')[1].textContent = equipo.tbl_equipo_PLACA
                templateCard.querySelectorAll('td')[2].textContent = equipo.tbl_equipo_DESCRIPCION
                templateCard.querySelectorAll('td')[3].textContent = equipo.tbl_equipo_TIPO
                templateCard.querySelectorAll('td')[4].textContent = equipo.tbl_equipo_CANTIDAD
                templateCard.querySelector('.btn-success').dataset.id = equipo.tbl_equipo_ID

                const clone = templateCard.cloneNode(true)
                fragment.appendChild(clone)
            })
            cards.appendChild(fragment)
        }
        
        // agregar al carrito

        const addCarrito = e => {
            if (e.target.classList.contains('btn-success')) {
                let id = e.target.dataset.id

                let objeto = toolList.find(objeto => objeto.tbl_equipo_ID === id)
                setCarrito(objeto)
            }
            e.stopPropagation()
        }

        const setCarrito = objeto => {
            const equipo = {
                id: objeto.tbl_equipo_ID,
                codigo: objeto.tbl_equipo_PLACA,
                nombre: objeto.tbl_equipo_TIPO,
                descripcion: objeto.tbl_equipo_DESCRIPCION,
                stock: objeto.tbl_equipo_CANTIDAD,
                rubro: objeto.rubro,
                tipo: 'equipo',
                cantidad: 1
            }
            carrito3[equipo.id] = {
                ...equipo
            } // copia de carrito3
            pintarCarrito()
        }

        // mostrar carrito3
        const pintarCarrito = () => {

            items.innerHTML = "" // limpiar carrito3. es decir que parta en 0
            let contador = 0
            Object.values(carrito3).forEach(equipo => {
                contador++

                templatecarrito3.querySelector('th').textContent = contador
                templatecarrito3.querySelectorAll('td')[0].textContent = equipo.codigo
                templatecarrito3.querySelectorAll('td')[1].textContent = equipo.nombre
                templatecarrito3.querySelectorAll('td')[2].textContent = equipo.descripcion
                templatecarrito3.querySelectorAll('td')[3].textContent = equipo.stock
                templatecarrito3.querySelectorAll('td')[4].textContent = equipo.cantidad
                templatecarrito3.querySelector('.btn-info').dataset.id = equipo.id
                templatecarrito3.querySelector('.btn-danger').dataset.id = equipo.id
                templatecarrito3.querySelector('.btn-secondary').dataset.id = equipo.id

                const clone = templatecarrito3.cloneNode(true)
                fragment.appendChild(clone)
            })
            items.appendChild(fragment)
            pintarFooter()
            // local storage



            sessionStorage.setItem('carrito3', JSON.stringify(carrito3))

        }


        const pintarFooter = () => {
            footer.innerHTML = "" // limpiar 
            if (Object.keys(carrito3).length === 0) {
                footer.innerHTML = `
                    <th scope="row" colspan="5">carrito3 de devoluciones vacío - comience hacer una solicitud!</th>
                    `
                return
            }
            const cantidad = Object.values(carrito3).reduce((acumulador, {
                cantidad
            }) => acumulador + cantidad, 0)


            templateFooter.querySelectorAll('td')[0].textContent = cantidad


            const clone = templateFooter.cloneNode(true)
            fragment.appendChild(clone)
            footer.appendChild(fragment)

            // vaciar carrito3 
            const btnVaciar = document.getElementById('vaciar-carrito3')
            btnVaciar.addEventListener('click', () => {
                Swal.fire({
                    title: '¿Quieres vaciar todo?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, vaciar !'
                }).then((result) => {
                    if (result.isConfirmed) {
                        carrito3 = {}
                        pintarCarrito() // se ejecutanuevamente
                    }
                })

            })

            document.getElementById("enviar").addEventListener("click", function() {
                let ficha = $('#ficha').val()
                let id_solicitud = document.getElementById('id_request').textContent;
                var wasSent = true
                 if(ficha === "" || ficha === "--SELECCIONAR--"){
                    FillData()
                }else{
                     for (const property in carrito3) {

                        var id = `${carrito3[property]['id']}`
                        var codigo = `${carrito3[property]['codigo']}`
                        var nombre = `${carrito3[property]['nombre']}`
                        var cantidad = `${carrito3[property]['cantidad']}`
                        var stock = `${carrito3[property]['stock']}`
                        var descripcion = `${carrito3[property]['descripcion']}`
                        var tipo = `${carrito3[property]['tipo']}`
                        var rubro = `${carrito3[property]['rubro']}`
                        var nombrePersona = $('#nombrePersona').val()
                        
                        $.ajax({
                            url: '<?php echo URL_SISINV ?>SolicitudEquipos/EnviarEquipos',
                            type: 'POST',
                            data: {
                                id: id,
                                id_solicitud: id_solicitud,
                                codigo: codigo,
                                nombre: nombre,
                                cantidad: cantidad,
                                descripcion: descripcion,
                                stock: stock,
                                ficha: ficha,
                                rubro: rubro,
                                tipo: tipo,
                                nombrePersona: nombrePersona
                            }
                        }).done(function() {
                            sessionStorage.removeItem('carrito3')
                            wasSent = true
                        }).fail(function() {
                            Swal.fire({
                                position: 'center',
                                icon: 'error',
                                title: 'No se pudo enviar la solicitud',
                                showConfirmButton: false,
                                timer: 3000
                            })
                            setTimeout(function() {
                                window.location.href = '<?php echo URL_SISINV ?>SolicitudEquipos/SolicitarEquipos';
                            }, 3000);
    
                        })
                    }
                    if(wasSent === true){
                         Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Solicitud enviada exitosamente',
                            showConfirmButton: false,
                            timer: 3000
                        })
    
                        setTimeout(function() {
                            window.location.href = '<?php echo URL_SISINV ?>SolicitudEquipos/SolicitarEquipos';
                        }, 3000);
                    }
                
                }
                
            })
        }

        // accciones de los botones
        const btnAccion = e => {
            // aumentar
            if (e.target.classList.contains('btn-info')) {
                const equipo = carrito3[e.target.dataset.id]
                let stockValue = carrito3[e.target.dataset.id].stock
                let cantidadValue = carrito3[e.target.dataset.id].cantidad
                if (stockValue > cantidadValue) {
                    equipo.cantidad = carrito3[e.target.dataset.id].cantidad + 1
                    carrito3[e.target.dataset.id] = {
                        ...equipo
                    } // copia
                    pintarCarrito() 
                }

            }

            //acccion de disminuir
            if (e.target.classList.contains('btn-danger')) {
                const equipo = carrito3[e.target.dataset.id]

                equipo.cantidad-- // restar
                if (equipo.cantidad === 0) {
                    delete carrito3[e.target.dataset.id]
                }
                pintarCarrito()
            }


            if (e.target.classList.contains('btn-secondary')) {

                delete carrito3[e.target.dataset.id]

                pintarCarrito()
            }

            e.stopPropagation()
        }

        // obtener datos del localSotage
        if (sessionStorage.getItem('carrito3')) {
            carrito3 = JSON.parse(sessionStorage.getItem('carrito3'));
            pintarCarrito()
        }
     
    })
</script>