<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
        <meta content="" name="description">
        <link rel="icon" type="image/png" href="<?php echo URL_SISINV?>logo/favicon-32x32.png" sizes="32x32">
        <meta content="SERVICO NACIONAL APRENDIZAJE          - SENA" name="author">
        <title><?php echo NAME_SISINV; ?></title>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<?php echo URL_SISINV;?>/css/styles.css" />
        <!-- Custom fonts for this template-->
        <link href="<?php echo URL_SISINV;?>/MATERIAL_THEME/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo URL_SISINV;?>/css/styles.css" rel="stylesheet" type="text/css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@700&display=swap" rel="stylesheet">
    </head>
    <body id="page-top" style="width: 100%; height: 100%; background-size: cover; padding-top: 80px;" class="bg-gradient-primary">
        <div class="row">
            <div class="col-12 col-sm-12 p-0">
                <nav class="navbar navbar-expand-lg bg-light text-uppercase sticky-top shadow-lg" style="position: fixed; width: 100%; left: 0; top: 0; height: 80px;" id="mainNav">
                    <div class="container pl-4">
                        <a class="navbar-brand" href="#">
                            <img src="<?php echo URL_SISINV?>logo/planning.svg" alt="Logo" style="width:50px;">
                          </a>
                        <a class="navbar-brand js-scroll-trigger">SIGINV</a>
                        <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                            Menu
                            <i class="fas fa-bars"></i>
                        </button>
                        <div class="collapse navbar-collapse p-0" id="navbarResponsive">
                            <ul class="navbar-nav ml-auto">
                                <!--<button class="btn btn-primary nav-item mx-0 mx-lg-1 ">Ingresar como <svg xmlns="http://www.w3.org/2000/svg" style="color:black" width="30" height="30"  fill="currentColor" class="bi bi-box-arrow-in-right" viewBox="0 0 16 16">
                                  <path fill-rule="evenodd" d="M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0v-2z"/>
                                  <path fill-rule="evenodd" d="M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/></svg>                  
                                </button>-->
                                <button data-toggle="modal" data-target="#adminModal" class="btn btn-secondary nav-item mx-0 mx-lg-1">Administrador</button>
                                <button data-toggle="modal" data-target="#herramenteroModal" class="btn btn-primary nav-item mx-0 mx-lg-1">Herramentero</button>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            
        
            <div class="col-12 col-sm-12 p-0 mb-xl-5 pb-xl-5">
                <div class="container mb-lg-5 pb-lg-5 pb-sm-5 pb-5 pl-5" > <!-- class="container my-lg-5 py-lg-5 py-sm-5 py-5" style="border: 5px solid black;" -->
                    <!-- Outer Row -->
                    <div class="row justify-content-center" > <!-- style="border: 5px solid pink;" -->
                        
                        <!--<div class="col-xl-10 col-lg-12 col-md-9 mb-lg-5 pb-lg-5" style="border: 5px solid black;"> <!-- class="col-xl-10 col-lg-12 col-md-9 my-lg-5 pb-lg-5" -->
                        
                            <!--<div class="card o-hidden border-0 shadow-lg mb-lg-5 mb-lg-3" style="border: 5px solid red;"> <!-- mb-lg-5 mb-lg-3 -->
                            
                                <div class="card-body pt-4">
                                    <!-- Nested Row within Card Body -->
                                    <div class="row mt-4" > <!-- style="border: 5px solid red;" -->
                                        
                                        <!-- Caja Carousel -->
                                        <div class="col-xl-7 col-lg-7 d-none d-lg-block " >
                                            <!--<img src="<?php echo URL_SISINV?>logo/395.jpg" style="width:400px;" class="text-center" >-->
                                            <div class="shadow-lg flex p-3 mb-5 bg-white rounded" style="width: 90%;"><!--p-lg-4 p-xl-3 px-3 mb-xl-5 mb-lg-3 shadow-lg p-3 mb-5 bg-white rounded-->
                                                <center>
                                                    <div style="width: 26rem;" id="carouselExampleIndicators" class="carousel slide text-center" data-ride="carousel">
                                                      <ol class="carousel-indicators">
                                                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                                      </ol>
                                                      <div class="carousel-inner">
                                                        <div class="carousel-item active">
                                                          <img class="d-block w-100" src="<?php echo URL_SISINV?>logo/395.jpg" alt="First slide">
                                                        </div>
                                                        <div class="carousel-item">
                                                          <img class="d-block w-100 p-4" src="<?php echo URL_SISINV?>images/undraw4.png" alt="Second slide">
                                                        </div>
                                                        <div class="carousel-item">
                                                          <img class="d-block w-100 p-4" src="<?php echo URL_SISINV?>images/LogoSenaNaranja.png" alt="Third slide">
                                                        </div>
                                                      </div>
                                                      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                        <span class="sr-only">Previous</span>
                                                      </a>
                                                      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                        <span class="sr-only">Next</span>
                                                      </a>
                                                    </div>
                                                </center>
                                            </div>
                                        </div>
                                        
                                        <!-- Caja Inicio Sesion -->
                                        <div class="col-xl-5 col-lg-5 "> <!--  mt-xl-5 mt-lg-3 -->
                                            <div class=" shadow-lg p-3 mb-5 bg-white rounded"> <!-- p-lg-4 p-xl-5 px-3 mb-xl-5 mb-lg-5 -->
                                                <div class="text-center">
                                                   <?php !empty($datos['message']) ? print "<div class='alert alert-danger fade show' role='alert'>{$datos['message']}</div>": $datos = ['message' => ''];
                                                  ?>  
                                                    <h1 class="h3 text-gray-900 mb-2 p-3">
                                                        Inicio Sesión
                                                    </h1>
                                                </div>
                                                <form class="user px-2" action="<?php echo URL_SISINV;?>Login/SignIn" method="POST">
                                                    
                                                    <div class="form-group">
                                                        <label class="label" >
                                                            Usuario
                                                        </label>
                                                        <input required aria-describedby="username" class="form-control form-control-user" id="USERNAME" placeholder="usuario@gmail.com" type="text" name="USERNAME">
                                                        </input>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="label" >
                                                            Contraseña
                                                        </label>
                                                        <input required class="form-control form-control-user" id="PASSWORD" placeholder="password" type="password" name="PASSWORD">
                                                        <input required class="form-control form-control-user" value="3" type="hidden" name="numeroComprobacion">
                                                        </input>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="custom-control custom-checkbox small">
                                                            <input class="custom-control-input" id="customCheck" type="checkbox">
                                                                <label class="custom-control-label" for="customCheck">
                                                                    Recordarme
                                                                </label>
                                                            </input>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <button class="btn btn-primary btn-user btn-block" type="submit">
                                                        Login
                                                    </button>
                                                </form>
                                                <hr>
                                                    
                                                    <div class="text-center">
                                                        <a class="small" style="color: black;" data-toggle="modal" data-target="#CambioContrasenia">
                                                            ¿Olvide mi contraseña?
                                                        </a>
                                                    </div>
                                                    
                                                
                                            
                                                    <div class="text-center">
                                                        <a class="small hidden" style="color: black;" href="#">
                                                            
                                                        </a>
                                                    </div>
                                                </hr>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!--</div>-->
                        <!--</div>-->
                    </div>
                    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)-->
                    <div class="scroll-to-top d-lg-none position-fixed">
                        <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"><i class="fa fa-chevron-up"></i></a>
                    </div>
                </div>
            </div>
            <br>
            
            <div class="col-12 col-sm-12 m-0 p-0">
                <!-- Footer-->
                <center><a class="ir-arriba"  javascript:void(0) title="Volver arriba">
                  <span class="fa-stack " width:"70px" >
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-arrow-up fa-stack-1x fa-inverse"></i>
                  </span>
                </a></center>
                <footer class="footer text-center mt-xl-5 mt-lg-5">
                    <div class="container my-lg-2 "> <!-- Se agrega my-2 -->
                        <div class="row">
                            <!-- Footer Location-->
                            <div class="col-lg-4 mb-5 mb-lg-0">
                                <h4 class="text-uppercase mb-4">Ubicacion</h4>
                                <p class="lead mb-0">
                                    #16- a 16-123, Dg. 18 
                                    #111, Malambo, Atlántico
                                    <br />
                                    Clark, MO 65243
                                </p>
                            </div>
                            <!-- Footer Social Icons-->
                            <div class="col-lg-4 mb-5 mb-lg-0">
                                <h4 class="text-uppercase mb-4">nuestras redes sociales</h4>
                                <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-facebook-f"></i></a>
                                <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-twitter"></i></a>
                                <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-linkedin-in"></i></a>
                                <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-dribbble"></i></a>
                            </div>
                            <!-- Footer About Text-->
                            <div class="col-lg-4">
                                <h4 class="text-uppercase mb-4">Pagina Principal SENA</h4>
                                <p class="lead mb-0">
                                    <a href="https://www.sena.edu.co/" target="_blank">Pagina del SENA Oficial</a><br>
                                    <a href="http://oferta.senasofiaplus.edu.co/sofia-oferta" target="_blank">Portal Sena Sofia Plus</a>
                                    
                                </p>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- Copyright Section-->
                <div class="copyright py-4 text-center text-white">
                    <div class="container"><small>Copyright © Sena Malambo 2021  |  By Orion-Group</small></div>
                </div>
            </div>
            
        
        </div>
        
         <!-- MODAL INSERTAR CONTRASEÑA NUEVA -->
         <div class="modal fade" id="CambioContrasenia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">CAMBIAR CONTRASEÑA</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" type="reset">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="row">
                                <div class="position-relative form-group col-md-12">
                                    <div id="div-correo" class="form-group">
                                        <label>Consultar Correo</label>
                                        <input class="form-control input-lg" placeholder="Ingrese su correo" type="email" id="email"  required>
                                        <center>
                                            <button class="btn btn-primary mt-5" id="BtnConsultarCorreo">Consultar</button>
                                        </center>
                                    </div>
                                    
                                    <div id="div-password">
                                        <div class="form-group">
                                           <label>Nueva contraseña</label>
                                           <input class="form-control input-lg" placeholder="Ingresa la nueva contraseña" type="password" id = "password1" required>
                                        </div>
        
                                        <div class="form-group">
                                          <label>Repetir nueva contraseña</label>
                                          <input class="form-control input-lg" placeholder="Repite la nueva contraseña" type="password"  id = "password2" required>
                                        </div>
            
                                        <p class="mb-4" style="color: #FF0000" id = "campo"></p>
                                        <button class="btn btn-primary mt-2" id="BtnCambioContrasenia" type="button">Cambiar contraseña</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Admin Modal -->
        <div class="modal fade" id="adminModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Iniciar como Administrador</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="text-center">
                                <?php !empty($datos['messageAdmin']) ? print "<div class='alert alert-danger fade show' role='alert'>{$datos['messageAdmin']}</div>" : $datos = ['messageAdmin' => '']; ?>
                            </div>
                            <h2 class="text-center color-danger">Iniciar Sesión</h2>
                            <form method="POST" action="<?php echo URL_SISINV;?>Login/SignIn" class="login-form">
                                <div class="form-group px-3">
                                    <label class="text" for="exampleInputEmail1">Usuario</label>
                                    <input class="form-control" name="USERNAME" placeholder="miusuario@email.com" type="text" required="El campo no puede ir vacío"></input>
                                </div>
                                <div class="form-group px-3">
                                    <label class="text" for="exampleInputPassword1">Password</label>
                                    <input class="form-control" id="" name="PASSWORD" placeholder="*********" type="password" required="Por favor ingrese una contraseña"></input>
                                    <input required class="form-control form-control-user" value="1" type="hidden" name="numeroComprobacion">
                                </div>
                                <center class="mt-5">
                                    <button class="btn btn-primary px-3 " type="submit">Ingresar</button>
                                </center>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
         <!-- herramentero Modal -->
        <div class="modal fade" id="herramenteroModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Iniciar como Herramentero</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="text-center">
                                <?php !empty($datos['messageAdmin']) ? print "<div class='alert alert-danger fade show' role='alert'>{$datos['messageAdmin']}</div>" : $datos = ['messageAdmin' => '']; ?>
                            </div>
                            <h2 class="text-center color-danger">Iniciar Sesión</h2>
                            <form method="POST" action="<?php echo URL_SISINV;?>Login/SignIn" class="login-form">
                                <div class="form-group">
                                    <label class="text" for="exampleInputEmail1">Usuario</label>
                                    <input class="form-control" name="USERNAME" placeholder="miusuario@email.com" type="text" required="El campo no puede ir vacío"></input>
                                </div>
                                <div class="form-group">
                                    <label class="text" for="exampleInputPassword1">Password</label>
                                    <input class="form-control" id="" name="PASSWORD" placeholder="*********" type="password" required="Por favor ingrese una contraseña"></input>
                                    <input required class="form-control form-control-user" value="2" type="hidden" name="numeroComprobacion">
                                </div>
                                <center class="mt-5">
                                    <button class="btn btn-primary px-3" type="submit">Ingresar</button><!-- float-right -->
                                </center>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
    </body>
    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo URL_SISINV;?>/MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo URL_SISINV;?>/MATERIAL_THEME/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="<?php echo URL_SISINV;?>/MATERIAL_THEME/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="<?php echo URL_SISINV;?>/MATERIAL_THEME/js/sb-admin-2.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
    <script  type="text/javascript">
        $(document).ready(function(){ //Hacia arriba
          irArriba();
        });
        
        function irArriba(){
          $('.ir-arriba').click(function(){ $('body,html').animate({ scrollTop:'0px' },1000); });
          $(window).scroll(function(){
            if($(this).scrollTop() > 0){ $('.ir-arriba').slideDown(600); }else{ $('.ir-arriba').slideUp(600); }
          });
          $('.ir-abajo').click(function(){ $('body,html').animate({ scrollTop:'1000px' },1000); });
        }
    </script>
    <script type="text/javaScript">
        $(document).ready(function(){
            irArriba();
            
            function irArriba(){
              $('.ir-arriba').click(function(){ $('body,html').animate({ scrollTop:'0px' },1000); });
              $(window).scroll(function(){
                if($(this).scrollTop() > 0){ $('.ir-arriba').slideDown(600); }else{ $('.ir-arriba').slideUp(600); }
              });
              $('.ir-abajo').click(function(){ $('body,html').animate({ scrollTop:'1000px' },1000); });
            }
            
            $('#div-password').hide();
            
            /* Cambio contraseña*/
            $('#div-correo').show();
            
            document.getElementById("BtnConsultarCorreo").addEventListener('click', function() {
                ConsultarCorreo()
            })
            
            /* Registro consulta correo */
            function ConsultarCorreo() {
                var email = $('#email').val().trim();
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Login/ConsultarCorreo',
                    type: 'POST',
                    data: {
                        email : email
                    } 
                }).done(function(response){
                    var data = JSON.parse(response)
                    if(data.length < 1){
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Hubo un error',
                            ShowConfirmButton: false,
                            timer: 2000
                        })   
                    }else{
                        $('#div-password').show()
                        $('#div-correo').hide()
                    }
                });
            }
            
            document.getElementById("BtnCambioContrasenia").addEventListener('click', function() {
                CambioContrasenia()
            });
            
            function CambioContrasenia(){
                var password1 = $('#password1').val().trim();
                var password2 = $('#password2').val().trim();
                var email = $('#email').val().trim();
                
                if (password1 == password2){
                    $.ajax({
                        url: '<?php echo URL_SISINV ?>Login/CambioContrasenia',
                        type: 'POST',
                        data: {
                            password1 : password1,
                            email : email
                        }
                    }).done(function(){
                       Swal.fire({
                           position: 'center',
                           icon: 'success',
                           title: 'Guardado exitosamente',
                           showConfirmButton: false,
                           timer: 3000
                       })
                        Success();
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Login/senasedemetalmecanica.com/Login/SignIn';
                    }, 2000);
                    });
                    
                }else{
                   Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        title: 'Las contraseñas deben ser iguales',
                       showConfirmButton: false,
                        timer: 2000
                    })
                
                } 
            }
        });
        
       

          
    </script>
    
</html>































