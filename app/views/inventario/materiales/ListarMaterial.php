<?php require_once "../app/views/template.php"; ?>
<!--<div id="contenedor_carga">
  <div id="carga"></div>
</div>-->

<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">SISTEMA DE INVENTARIO</h1>
    <p class="mb-4">En esta seccion puede consultar los materiales existentes.</p>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-md-11">
                    <br>
                    <h6 class="m-0 font-weight-bold text-primary">LISTADO DE MATERIALES</h6>
                    <!--<input type="text" id="card-header_search-input" style="float: right" placeholder="Search">-->
                </div>
                <div class="col-md-1">
                    <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR') : ?>
                        <cite title="Agregar">
                            <a class="btn btn-success btn-icon-split" data-toggle="modal" data-target="#AgregarMateriales">
                                <span class="icon text-white-50">
                                    <i class="fas fa-plus"></i>
                                </span>
                            </a>
                        </cite>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>REGIONAL</th>
                            <th>CENTRO</th>
                            <th>SEDE</th>
                            <th>FECHA INGRESO</th>
                            <th>CODIGO SENA</th>
                            <th>UNSPSC</th>
                            <th>DESCRIPCION</th>
                            <th>PROGRAMA DE FORMACION</th>
                            <th>CANTIDAD</th>
                            <th>TIPO DE MATERIAL</th>
                            <th>UNIDAD DE MEDIDA</th>
                            <th>RUBRO PRESUPUESTAL</th>
                            <th>DESTINO</th>
                            <th>IDENTIFICADOR</th>
                             <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR') : ?>
                                <th>ACCIONES</th>
                             <?php endif;?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $contador = 1;
                        foreach ($datos['ListarMateriales'] as $ListarMateriales) : ?>
                            <tr>
                                <td scope="row"><?php echo $contador++; ?></td>
                                <td><?php echo $ListarMateriales->tbl_regional_NOMBRE; ?></td>
                                <td><?php echo $ListarMateriales->tbl_centro_NOMBRE; ?></td>
                                <td><?php echo $ListarMateriales->tbl_sede_NOMBRE; ?></td>
                                <td><?php echo $ListarMateriales->tbl_materiales_FECHA; ?></td>
                                <td><a href="<?php echo URL_SISINV; ?>Material/EditarMaterialCodigo/<?php echo $ListarMateriales->tbl_materiales_ID ?>"><?php echo $ListarMateriales->tbl_materiales_CODIGOSENA; ?></a></td>
                                <td><?php echo $ListarMateriales->tbl_materiales_UNSPSC; ?></td>
                                <td><?php echo $ListarMateriales->tbl_materiales_DESCRIPCION; ?></td>
                                <td><?php echo $ListarMateriales->tbl_materiales_PROGRAMAFORMACION; ?></td>
                                <td><?php echo $ListarMateriales->tbl_materiales_CANTIDAD; ?></td>
                                <td><?php echo $ListarMateriales->tbl_materiales_TIPOMATERIAL; ?></td>
                                <td><?php echo $ListarMateriales->tbl_materiales_UNIDADMEDIDA; ?></td>
                                <td><?php echo $ListarMateriales->tbl_materiales_RUBRO; ?></td>
                                <td><?php echo $ListarMateriales->tbl_materiales_DESTINO; ?></td>
                                <td><?php echo $ListarMateriales->tbl_materiales_IDENTIFICADOR; ?></td> 
                                <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR') : ?>
                                    <td>
                                        <cite title="Editar">
                                            <a class="btn btn-info btn-icon-split" href="<?php echo URL_SISINV; ?>Material/ObtenerMaterial/<?php echo $ListarMateriales->tbl_materiales_ID; ?>">
                                                <span class="icon text-white-50">
                                                    <i class="fas fa-edit"></i>
                                                </span>
                                            </a>
                                        </cite>
                                        <cite title="Borrar">
                                            <a class="btn btn-danger btn-icon-split" href="<?php echo URL_SISINV; ?>Material/EliminarMaterial/<?php echo $ListarMateriales->tbl_materiales_ID; ?>">
                                                <span class="icon text-white-50">
                                                    <i class="fas fa-trash"></i>
                                                </span>
                                            </a>
                                        </cite>
                                    </td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid MODAL REGISTRAR MATERIALES-->
<div class="modal fade" id="AgregarMateriales" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">AGREGAR MATERIALES</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="position-relative form-group col-md-12">
                            
                            <label class="" style="font-weight: bold;">
                                SELECCIONA LA REGIONAL:*
                            </label>
                            <select class="form-control" id="materialRegional" name="materialRegional">
                                <option>--SELECCIONAR--</option>
                                <?php foreach ($datos['ListarReginal'] as $ListarReginal) : ?>
                                    <?php if ($ListarReginal) : ?>
                                        <option value="<?php echo $ListarReginal->tbl_regional_ID; ?>"><?php echo $ListarReginal->tbl_regional_NOMBRE; ?></option>
                                    <?php else : ?>
                                        <option value="0">-- SELECCIONA LA REGIONAL--</option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select><br>
                            
                            <label class="" style="font-weight: bold;">CENTRO:*</label>
                            <select class="form-control" id="materialCentro" name="materialCentro">
                            </select><br>
                            <label class="" style="font-weight: bold;">SEDE:*</label>
                            <select class="form-control" id="materialSede" name="materialSede">
                            </select><br>
                            <label class="" style="font-weight: bold;">CODIGO SENA:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="materialCodigo" name="materialCodigo" placeholder="" type="number" required><br>
                            <label class="" style="font-weight: bold;">UNSPSC:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="materialUnspsc" name="materialUnspsc" placeholder="" type="number" required><br>
                            <label class="" style="font-weight: bold;">DESCRIPCION:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="materialDescripcion" name="materialDescripcion" placeholder="" type="text" required><br>
                            <label class="" style="font-weight: bold;">PROGRAMA DE FORMACION:*</label>
                        
                            <!-- Campo de Selección para los programas -->
                            <select class="form-control" id="materialPrograma" name="materialPrograma">
                                <option>--SELECCIONAR--</option>
                                <?php foreach ($datos['ListarProgramas'] as $ListarProgramas) : ?> <!-- La var $datos llama la llave y a su vez la asigna como la var $ListarPrograma ya declara en el controlador-->
                                    <?php if ($ListarProgramas) : ?> <!-- Se realiza una condición donde si es verdadera traerá en primer lugar el ID del programa y luego traemos el Nombre para luego ser seleccionado con el tag Option -->
                                        <option value="<?php echo $ListarProgramas->tbl_programa_NOMBRE; ?>"><?php echo $ListarProgramas->tbl_programa_NOMBRE; ?></option>
                                    <?php else : ?>
                                        <option value="0">-- NO SE ENCONTRARON DATOS --</option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select><br>
                            
                            <label class="" style="font-weight: bold;">CANTIDAD DE MATERIALES:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="materialCantidad" name="materialCantidad" placeholder="" type="number" required><br>
                            <label class="" style="font-weight: bold;">TIPO DE MATERIALES:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="materialTipo" name="materialTipo" placeholder="" type="text" required><br>
                            </select><br>
                            
                            <label class="" style="font-weight: bold;">RUBRO PRESUPUESTAL:*</label>
                            <select class="form-control" id="rubroPresupuestal" name="rubroPresupuestal">
                                <option value="REGULAR">REGULAR</option>
                                <option value="ARTICULACION">ARTICULACION</option>
                                <option value="FIC">FIC</option>
                                <option value="DESPLAZADO">DESPLAZADO</option>
                            </select><br>
                            
                            <label class="" style="font-weight: bold;">IDENTIFICADOR:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="materialIdUnico" name="materialIdentificador" placeholder="" type="text" required><br>
                            <label class="" style="font-weight: bold;">UNIDAD DE MEDIDA:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="materialUnidadMedida" name="materialUnidadMedida" placeholder="" type="text" required><br>
                            <label class="" style="font-weight: bold;">DESTINO:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="materialDestino" name="materialDestino" placeholder="" type="text" required><br>
                            <button class="mb-2 mr-2 btn btn-primary col-md-12" type="button" id="RegistrarMaterial">REGISTRAR</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#dataTable').DataTable()
        document.getElementById("RegistrarMaterial").addEventListener('click', function() {
            RegistrarMaterial();
        });
        /*Cambiar regional */
        $('#materialRegional').change(function() {
            var regionalID = $('#materialRegional').val();
            LoadCentros(regionalID)
        });

        function LoadCentros(regionalID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadCentros',
                type: 'POST',
                data: {
                    regionalID: regionalID
                }
            }).done(function(response) {
                var data = JSON.parse(response);
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='materialCentro' value='" + data[i].tbl_centro_ID + "'>" + data[i].tbl_centro_NOMBRE + "</option>"
                    }
                    $('#materialCentro').html(cadena);
                    var centroID = $('#materialCentro').val();
                    LoadSedes(centroID);
                } else {
                    cadena = "<option value='0'>NO SE ENCONTRARON DATOS </option>";
                    $('#materialCentro').html(cadena);
                    var centroID = $('#materialCentro').val();
                    LoadSedes(centroID);
                }
            })
        }
        $('#materialCentro').change(function() {
            var centroID = $('#materialCentro').val();
            LoadSedes(centroID);
        })
        // Load sedes
        function LoadSedes(centroID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadSedes',
                type: 'POST',
                data: {
                    centroID: centroID
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='materialSede' value='" + data[i].tbl_sede_ID + "'>" + data[i].tbl_sede_NOMBRE + "</option>";
                    }
                    var centroID = $('#materialCentro').val()
                    if (centroID == 0) {
                        $('#materialSede').html('');
                    }
                    $('#materialSede').html(cadena);
                } else {
                    cadena = "<option id='materialSede' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#materialSede').html(cadena); // <- significa poner los datos dentro del input
                }
            })
        }

        function RegistrarMaterial() {
            
            var materialRegional = $('#materialRegional').val();
            var materialCentro = $('#materialCentro').val();
            var materialSede = $('#materialSede').val();
            var materialCodigo = $('#materialCodigo').val().trim();
            var materialUnspsc = $('#materialUnspsc').val().trim();
            var materialDescripcion = $('#materialDescripcion').val().trim();
            var materialPrograma = $('#materialPrograma').val().trim();
            var materialCantidad = $('#materialCantidad').val().trim();
            var materialTipo = $('#materialTipo').val().trim();
            var materialUnidadMedida = $('#materialUnidadMedida').val().trim();
            var materialDestino = $('#materialDestino').val().trim();
            var rubroPresupuestal = $('#rubroPresupuestal').val();
            var materialExists = false;
            if (materialRegional == "--SELECCIONAR--" || materialCentro == "0" || materialCentro == null || materialSede == "0" || materialSede == null || materialUnspsc == "" || materialDescripcion == "" || materialPrograma == "--SELECCIONAR--" || materialCantidad == "" || materialDestino == "" || rubroPresupuestal == "") {
                FillData()
            } else {
                /*Pasamos a comparar el codigo de la herramienta.*/
               $.ajax({
                url: '<?php echo URL_SISINV ?>Material/RegistrarMaterial',
                type: 'POST',
                data: {
                    materialSede: materialSede,
                    materialCodigo: materialCodigo,
                    materialUnspsc: materialUnspsc,
                    materialDescripcion: materialDescripcion,
                    materialPrograma: materialPrograma,
                    materialCantidad: materialCantidad,
                    materialTipo: materialTipo,
                    materialUnidadMedida: materialUnidadMedida,
                    materialDestino: materialDestino,
                    rubroPresupuestal: rubroPresupuestal
                },
                }).done(function() {
                    Success();
                    setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Material/ListarMaterial';
                }, 2000);
                }).fail(function() {
                    error();
                    setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Material/ListarMaterial';
                }, 2000);
                })
            }
        }
        /*window.onload = function() {
        var contenedor = document.getElementById('contenedor_carga');
        contenedor.style.visibility = 'hidden';
        contenedor.style.opacity = '0';
        }*/

    });
</script>