<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR REGIONAL-->
<div class="modal fade" id="Modelmaterial" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDITAR MATERIAL</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-md-12 pr-1">
                            <label class="" style="font-weight: bold;">
                                SELECCIONA LA SEDE:*
                            </label>
                            <select class="form-control" id="materialSede">
                                <option>--SELECCIONAR--</option>
                                <?php foreach ($datos['ListarSede'] as $ListarSede) : ?>
                                    <option value="<?php echo $ListarSede->tbl_sede_ID ?>"><?php echo $ListarSede->tbl_sede_NOMBRE ?></option>
                                <?php endforeach; ?>
                            </select> <br>

                            <div class="form-group">
                                <input type="hidden" id="idMaterial" value="<?php echo $datos['idMaterial'] ?>">
                                <label>Material Codigo:</label>
                                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" type="text" class="form-control" value="<?php echo $datos['materialCodigo'] ?>" id="materialCodigo"><br>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-info btn-round col-md-12" id="EditarMaterial">ACTUALIZAR</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#Modelmaterial").modal("show");
        $("#Modelmaterial").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Material/ListarMaterial');
        });
        document.getElementById('EditarMaterial').addEventListener('click', function() {
            EditarMaterial()
        });

        function EditarMaterial() {
            var materialSede = $('#materialSede').val()
            var idMaterial = $('#idMaterial').val();
            var materialCodigo = $('#materialCodigo').val().trim();
            var materialCodigoExists = false;
            if (materialCodigo == "" || materialSede == "--SELECCIONAR--") {
                FillData();
            } else {
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Material/CompararMaterial', 
                    type: 'POST',
                    data: {
                        materialSede
                    },
                }).done(function(response){
                    var data = JSON.parse(response);
                    if (data.length > 0){
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].tbl_sede_tbl_sede_ID == materialSede && data[i].tbl_materiales_CODIGOSENA == materialCodigo) {
                                Existe();
                                materialCodigoExists = true;
                                break;
                            }
                        }
                    }
                    if (!materialCodigoExists) {
                        $.ajax({
                            url: '<?php echo URL_SISINV ?>Material/EditarMaterial2',
                            type: 'POST',
                            data: {
                                idMaterial : idMaterial,
                                materialSede: materialSede,
                                materialCodigo: materialCodigo
                            }
                        }).done(function() {
                            Edit();
                            // function de tiempo
                            setTimeout(function() {
                                window.location.href = '<?php echo URL_SISINV ?>Material/ListarMaterial';
                            }, 2000);
                        }).fail(function() {
                            ErrorEdit()
                            // function de tiempo
                            setTimeout(function() {
                                window.location.href = '<?php echo URL_SISINV ?>Material/ListarMaterial';
                            }, 2000);
                        })
                    }
                })
            }
        }
    })
</script>