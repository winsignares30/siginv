<?php require_once "../app/views/template.php"; ?>
<!-- /.container-fluid MODAL ELIMINAR MATERIALES-->
<div class="modal fade" id="ModalMaterial" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">ELIMINAR MATERIALES</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">X</span>
          </button>
        </div>
        <div class="modal-body">
          <form>
            <div class="row">
              <div class="position-relative form-group col-md-12">
                <input onkeyup="mayus(this);" type="hidden" class="form-control" id="idMaterial" value="<?php echo $datos['idMaterial'];?>"><br>
                <label class="" style="font-weight: bold;">CODIGO SENA:*</label>
                <input onkeyup="mayus(this);" class="form-control" id="materialCodigo"  value="<?php echo $datos['materialCodigo'];?>" type="text" disabled><br>
                <label class="" style="font-weight: bold;">DESCRIPCION:*</label>
                <input onkeyup="mayus(this);" class="form-control" id="materialDescripcion"  value="<?php echo $datos['materialDescripcion'];?>" type="text" disabled><br>
                    <button class="mb-2 mr-2 btn btn-danger col-md-12" type="button" id="Evento">ELIMINAR</button>
              </div>
            </div>
          </form>            
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
        </div>
      </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#ModalMaterial").modal("show");
        $("#ModalMaterial").on('hidden.bs.modal', function(){
            window.location.replace('<?php echo URL_SISINV;?>Material/ListarMaterial');
        });
         
        document.getElementById('Evento').addEventListener('click', function() {
            EliminarMaterial()
        });
        
        function EliminarMaterial(){
            var idMaterial = $('#idMaterial').val();
            $.ajax({
                url: '<?php echo URL_SISINV;?>Material/DeleteMaterial',
                type: 'POST',
                data: {
                    idMaterial: idMaterial
                }
            }).done(function() {
                Delete();
            // function de tiempo
                setTimeout(function() {
                     window.location.href = '<?php echo URL_SISINV ?>Material/ListarMaterial';
                }, 2000);

                }).fail(function() {
                    errorDelete();
                // function de tiempo
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Material/ListarMaterial';
                }, 2000);
            })
        }
        
    });
</script>