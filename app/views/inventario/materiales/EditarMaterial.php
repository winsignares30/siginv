<?php require_once "../app/views/template.php"; ?>
<!-- /.container-fluid MODAL EDITAR MATERIALES-->
<div class="modal fade" id="ModelMaterial" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">EDITAR MATERIALES</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">X</span>
          </button>
        </div>
        <div class="modal-body">
          <form>
            <div class="row">
              <div class="position-relative form-group col-md-12">
                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="idMaterial" name="idMaterial" value="<?php echo $datos['idMaterial'];?>" type="hidden" required><br>
                <label class="" style="font-weight: bold;">CODIGO SENA:*</label>
                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" name="materialCodigo" value="<?php echo $datos['materialCodigo'];?>" type="text" readonly="readonly"><br> 
                <label class="" style="font-weight: bold;">UNSPSC:*</label>
                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="materialUnspsc" name="materialUnspsc" value="<?php echo $datos['materialUnspsc'];?>" type="number" required><br>
                <label class="" style="font-weight: bold;">DESCRIPCION:*</label>
                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="materialDescripcion" name="materialDescripcion" value="<?php echo $datos['materialDescripcion'];?>" type="text" required><br>
                <label class="" style="font-weight: bold;">PROGRAMA DE FORMACION:*</label>
                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="materialPrograma" name="materialPrograma" value="<?php echo $datos['materialPrograma'];?>" type="text" required><br>
                <label class="" style="font-weight: bold;">CANTIDAD DE MATERIALES:*</label>
                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="materialCantidad" name="materialCantidad" value="<?php echo $datos['materialCantidad'];?>" type="number" required><br>
                <label class="" style="font-weight: bold;">TIPO DE MATERIALES:*</label>
                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="materialTipo" name="materialTipo" value="<?php echo $datos['materialTipo'];?>" type="text" required><br>
                <label class="" style="font-weight: bold;">UNIDAD DE MEDIDA:*</label>   
                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="materialUnidadMedida" name="materialUnidadMedida" value="<?php echo $datos['materialUnidadMedida'];?>" type="text" required><br>
                <label class="" style="font-weight: bold;">DESTINO:*</label>
                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="materialDestino" name="materialDestino" value="<?php echo $datos['materialDestino'];?>" type="text" required><br>
                <label class="" style="font-weight: bold;">RUBRO PRESUPUESTAL:*</label>
                            <select class="form-control" id="rubroPresupuestal" name="rubroPresupuestal">
                                <option value="REGULAR">REGULAR</option>
                                <option value="ARTICULACION">ARTICULACION</option>
                                <option value="FIC">FIC</option>
                                <option value="DESPLAZADO">DESPLAZADO</option>
                            </select><br>
                <label class="" style="font-weight: bold;">FECHA DE ADQUISICION:*</label>
                    <div class="form-group row">
                        <div class="col-10">
                            <input class="form-control" type="datetime" value="<?php echo $datos['materialFecha']; ?>" name="FECHA_INGRESO" id="materialFecha"  id="example-date-input">
                            <i class="fa fas-calendar "></i>
                        </div>
                    </div>
                    
                <button class="mb-2 mr-2 btn btn-info col-md-12" type="button" id="EditarMaterial" >ACTUALIZAR</button>
              </div>
            </div>
          </form>            
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
        </div>
      </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#ModelMaterial").modal("show");
    $("#ModelMaterial").on('hidden.bs.modal', function(){
      window.location.replace('<?php echo URL_SISINV;?>Material/ListarMaterial');
    });
    
    
    document.getElementById('EditarMaterial').addEventListener('click', function() {
        EditarMaterial()
    });
    
    function EditarMaterial(){
        var idMaterial = $('#idMaterial').val().trim();
        var materialUnspsc = $('#materialUnspsc').val().trim();
        var materialDescripcion = $('#materialDescripcion').val().trim();
        var materialPrograma = $('#materialPrograma').val().trim();
        var materialCantidad = $('#materialCantidad').val().trim();
        var materialTipo = $('#materialTipo').val().trim();
        var materialUnidadMedida = $('#materialUnidadMedida').val().trim();
        var materialDestino = $('#materialDestino').val().trim();
        var materialFecha = $('#materialFecha').val();
        var rubroPresupuestal = $('#rubroPresupuestal').val();
        if(materialUnspsc ==="" || materialDescripcion ==="" || materialPrograma==="" || materialCantidad==="" || materialDestino ==="" || materialFecha === "" || rubroPresupuestal == "" ){
            FillData()
        }else{
            $.ajax({
                url: '<?php echo URL_SISINV;?>Material/EditarMaterial',
                type: 'POST',
                data: {
                    idMaterial : idMaterial,
                    materialUnspsc : materialUnspsc,
                    materialDescripcion: materialDescripcion,
                    materialPrograma: materialPrograma,
                    materialCantidad: materialCantidad,
                    materialTipo: materialTipo,
                    materialUnidadMedida: materialUnidadMedida,
                    materialDestino : materialDestino,
                    materialFecha : materialFecha,
                    rubroPresupuestal : rubroPresupuestal
                },
            }).done(function() {
                Edit();
                setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Material/ListarMaterial';
                }, 2000); 
            }).fail(function() {
                ErrorEdit()
                setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Material/ListarMaterial';
                }, 2000);
            })
        }
        
    }
  });
</script>















