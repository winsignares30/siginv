<?php require_once "../app/views/template.php"; ?>
<!--MODAL EDITAR HERRAMIENTA-->
<div class="modal fade" id="ModalHerramienta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDITAR HERRAMIENTA</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="FormularioAjax" data-form="save" action="<?php echo URL_SISINV; ?>Herramienta/RegistrarHerramienta" method="POST">
                    <div class="row">
                        <div class="position-relative form-group col-md-12">
                            <label class="" style="font-weight: bold;">RUBRO PRESUPUESTAL:*</label>
                            <select class="form-control" id="rubroPresupuestal" name="rubroPresupuestal">
                                <option value="REGULAR">REGULAR</option>
                                <option value="ARTICULACION">ARTICULACION</option>
                                <option value="FIC">FIC</option>
                                <option value="DESPLAZADO">DESPLAZADO</option>
                            </select><br>
                             <input class="form-control" id="idHerramienta"  type="hidden" value="<?php echo $datos['idHerramienta']?>"><br>
                            <label class="" style="font-weight: bold;">DESCRIPCIÓN DE LA HERRAMIENTA:*</label>
                            <input onkeyup="mayus(this);" class="form-control" id="herraminetaDescripcion" value="<?php echo $datos['herraminetaDescripcion']?>" type="text" readonly="readonly"><br>
                            <label class="" style="font-weight: bold;">CANTIDAD DE HERRAMIENTA:*</label>
                            <input class="form-control" id="herramientaCantidad"  placeholder="" type="number" required><br>
                             <label class="" style="font-weight: bold;">CÓDIGO:*</label>
                            <input class="form-control" id="herramientaCodigo" value="<?php echo $datos['herramientaCodigo']?>"  placeholder="Código" type="number" required><br>
                            <label class="" style="font-weight: bold;">FECHA DE ADQUISICION:*</label>
                            <div class="form-group row">
                                <div class="col-10">
                                    <input class="form-control" type="datetime" value="<?php echo $datos['herramientaFechadeAdquision']; ?>" name="FECHA_INGRESO" id="herramientaFechadeAdquision" id="example-date-input">
                                    <i class="fa fas-calendar "></i>
                                </div>
                            </div>
                            <button class="mb-2 mr-2 btn btn-primary col-md-12" type="button" id="EditarHerramienta">EDITAR</button>
                        </div>
                    </div>
                </form>
            </div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
        </div>
      </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script>
  $(document).ready(function(){
    $("#ModalHerramienta").modal("show");
    $("#ModalHerramienta").on('hidden.bs.modal', function(){
        window.location.replace('<?php echo URL_SISINV;?>Herramienta/ListarHerramienta');
    });
    
    document.getElementById('EditarHerramienta').addEventListener('click', function() {
        EditarHerramienta()
    });
    
    function EditarHerramienta(){
        var rubroPresupuestal = $('#rubroPresupuestal').val().trim();
        var idHerramienta = $('#idHerramienta').val().trim();
        var herramientaCantidad = $('#herramientaCantidad').val().trim();
        var herramientaFechadeAdquision = $('#herramientaFechadeAdquision').val();
        let herramientaCodigo = $('#herramientaCodigo').val()
        //console.log(herramientaCantidad)
        if (herramientaCantidad == "" || herramientaFechadeAdquision == "" || rubroPresupuestal == ""){
            FillData()
        }else{
            $.ajax({
                url: '<?php echo URL_SISINV;?>Herramienta/EditarHerramienta',
                type: 'POST',
                data: {
                    idHerramienta : idHerramienta,
                    herramientaCantidad : herramientaCantidad,
                    herramientaFechadeAdquision: herramientaFechadeAdquision,
                    rubroPresupuestal : rubroPresupuestal,
                    herramientaCodigo: herramientaCodigo
                },
            }).done(function() {
                Edit();
                setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Herramienta/ListarHerramienta';
                }, 2000); 
            }).fail(function() {
                ErrorEdit()
                setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Herramienta/ListarHerramienta';
                }, 2000);
            })
        }
    }
  });
</script>

































