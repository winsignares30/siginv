<?php require_once "../app/views/template.php"; ?>
<!--MODAL ELIMINAR HERRAMIENTA-->
<div class="modal fade" id="ModelHerramienta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ELIMINAR HERRAMIENTA</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">
            <div class="position-relative form-group col-md-12">
              <input onkeyup="mayus(this);" class="form-control" type="hidden" id="idHerramienta" name="idHerramienta" value=" <?php echo $datos['idHerramienta'] ?>"><br>
              <label class="" style="font-weight: bold;">DESCRIPCION:*</label>
              <input onkeyup="mayus(this);" class="form-control" name="DESCRIPCION_HERRAMIENTA" value=" <?php echo $datos['herraminetaDescripcion'] ?> " type="text" disabled><br>
              <label class="" style="font-weight: bold;">CANTIDAD DE HERRAMIENTA:*</label>
              <input class="form-control" name="CANTIDAD_HERRAMIENTA" value="<?php echo $datos['herramientaCantidad']; ?>" type="text" disabled><br>
              <button class="mb-2 mr-2 btn btn-danger col-md-12" type="button" id="EliminarHerramienta">ELIMINAR</button>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script>
  $(document).ready(function() {
    $("#ModelHerramienta").modal("show");
    $("#ModelHerramienta").on('hidden.bs.modal', function() {
      window.location.replace('<?php echo URL_SISINV; ?>Herramienta/ListarHerramienta');
    });
    document.getElementById("EliminarHerramienta").addEventListener('click', function() {
      EliminarHerramienta()
    })

    function EliminarHerramienta() {
      var idHerramienta = $('#idHerramienta').val();
      $.ajax({
        url: '<?php echo URL_SISINV ?>Herramienta/DeleteHerramienta',
        type: 'POST',
        data: {
          idHerramienta: idHerramienta
        }
      }).done(function() {
        Delete()
        // function de tiempo
        setTimeout(function() {
          window.location.href = '<?php echo URL_SISINV ?>Herramienta/ListarHerramienta';
        }, 2000);
      }).fail(function() {
        ErrorDelete()
        // function de tiempo
        setTimeout(function() {
          window.location.href = '<?php echo URL_SISINV ?>Herramienta/ListarHerramienta';
        }, 2000);
      })
    }
  });
</script>