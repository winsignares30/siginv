<?php require_once "../app/views/template.php"; ?>
<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">SISTEMA DE INVENTARIO</h1>
    <p class="mb-4">En esta seccion puede consultar las Herramientas existentes.</p>
    <!-- DataTales Example -->
    <div class="">
        <div class="container col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="row">
                        <div class="col-md-10 col-sm-7">
                            <br>
                            <h6 class="m-0 font-weight-bold text-primary">LISTADO DE HERRAMIENTAS</h6>
                        </div>
                        <div class="col-md-2 col-sm-5">
                            <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR') : ?>
                                <cite title="Agregar">
                                    <a class="btn btn-success btn-icon-split" data-toggle="modal" data-target="#AgregarHerramienta">
                                        <span class="icon text-white-50">
                                            <i class="fas fa-plus"></i>
                                        </span>
                                    </a>
                                </cite>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>REGIONAL</th>
                                    <th>CENTRO</th>
                                    <th>SEDE</th>
                                    <th>BODEGA</th>
                                    <th>ESTANTE</th>
                                    <th>GAVETA DESCRIPCIÓN</th>
                                    <th>RUBRO PRESUPUESTAL</th>
                                    <th>IDENTIFICADOR</th>
                                    <th>FECHA</th>
                                    <th>CANTIDAD</th>
                                    <th>CÓDIGO</th>
                                    <th>NOMBRE</th>
                                    <th>DESCRIPCIÓN</th>
                                    <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR') : ?>
                                        <th>ACCIONES</th>
                                    <?php endif;?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $contador = 1;
                                foreach ($datos['ListarHerramienta'] as $ListarHerramienta) : ?>
                                    <tr>
                                        <td><?php echo $contador++ ?></td>
                                        <td><?php echo $ListarHerramienta->tbl_regional_NOMBRE; ?></td>
                                        <td><?php echo $ListarHerramienta->tbl_centro_NOMBRE; ?></td>
                                        <td><?php echo $ListarHerramienta->tbl_sede_NOMBRE; ?></td>
                                        <td><?php echo $ListarHerramienta->tbl_bodega_NOMBRE; ?></td>
                                        <td><?php echo $ListarHerramienta->tbl_estante_DESCRIPCION; ?></td>
                                        <td><?php echo $ListarHerramienta->tbl_gaveta_DESCRIPCION; ?></td>
                                        <td><?php echo $ListarHerramienta->tbl_herramienta_RUBRO; ?></td>
                                        <td><?php echo $ListarHerramienta->tbl_herramienta_IDENTIFICADOR; ?></td>
                                        <td><?php echo $ListarHerramienta->tbl_herramienta_FECHA; ?></td>
                                        <td><?php echo $ListarHerramienta->tbl_herramienta_CANTIDAD; ?></td>
                                        <td><?php echo $ListarHerramienta->tbl_herramienta_CODIGO; ?></td>
                                        <td><?php echo $ListarHerramienta->tbl_herramienta_NOMBRE; ?></td>
                                        <td><a href="<?php echo URL_SISINV; ?>Herramienta/EditarHerramientaNombre/<?php echo $ListarHerramienta->tbl_herramienta_ID ?>"><?php echo $ListarHerramienta->tbl_herramienta_DESCRIPCION; ?></a></td>
                                        <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR') : ?>
                                            <td>
                                                <cite title="Editar">
                                                    <a class="btn btn-info btn-icon-split" href="<?php echo URL_SISINV; ?>Herramienta/ObtenerHerramienta/<?php echo $ListarHerramienta->tbl_herramienta_ID; ?>">
                                                        <span class="icon text-white-50">
                                                            <i class="fas fa-edit"></i>
                                                        </span>
                                                    </a>
                                                </cite>
                                                <cite title="Borrar">
                                                    <a class="btn btn-danger btn-icon-split" href="<?php echo URL_SISINV; ?>Herramienta/EliminarHerramienta/<?php echo $ListarHerramienta->tbl_herramienta_ID; ?>">
                                                        <span class="icon text-white-50">
                                                            <i class="fas fa-trash"></i>
                                                        </span>
                                                    </a>
                                                </cite>
                                            </td>
                                        <?php endif; ?>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> <!-- FINAL PUNTO DE INTERRUPCIÓN -->
        </div>
    </div>
</div>
<!-- /.container-fluid  -->
<!-- MODAL INSERTAR HERRAMIENTA -->
<div class="modal fade" id="AgregarHerramienta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">AGREGAR HERRAMIENTAS</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="position-relative form-group col-md-12">
                            <label class="" style="font-weight: bold;">
                                SELECCIONA LA REGIONAL:*
                            </label>
                            <select class="form-control" id="herramientaRegional" name="herramientaRegional">
                                <option>--SELECCIONAR--</option>
                                <?php foreach ($datos['ListarReginal'] as $ListarRegional) : ?>
                                    <?php if ($ListarRegional) : ?>
                                        <option value="<?php echo $ListarRegional->tbl_regional_ID; ?>"><?php echo $ListarRegional->tbl_regional_NOMBRE; ?></option>
                                    <?php else : ?>
                                        <option value="0">-- SELECCIONA LA REGIONAL--</option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select><br>
                            <label class="" style="font-weight: bold;">CENTRO:*</label>
                            <select class="form-control" id="herramientaCentro" name="herramientaCentro">
                            </select><br>
                            <label class="" style="font-weight: bold;">SEDE:*</label>
                            <select class="form-control" id="herramientaSede" name="herramientaSede">
                            </select><br>
                            <label class="" style="font-weight: bold;">BODEGA:*</label>
                            <select class="form-control" id="herramientaBodega" name="herramientaBodega">
                            </select><br>
                            <label class="" id="estante" style="font-weight: bold;">NUMERO DE ESTANTE:*</label>
                            <select class="form-control" id="herramientaEstante" name="herramientaEstante">
                            </select><br>
                            
                            <label class="" id="gaveta" style="font-weight: bold;">NUMERO DE GAVETA:*</label>
                            <select class="form-control" id="herramientaGaveta" name="herramientaGaveta">
                            </select><br>
                            
                            <label class="" style="font-weight: bold;">RUBRO PRESUPUESTAL:*</label>
                            <select class="form-control" id="rubroPresupuestal" name="rubroPresupuestal">
                                <option value="REGULAR">REGULAR</option>
                                <option value="ARTICULACION">ARTICULACION</option>
                                <option value="FIC">FIC</option>
                                <option value="DESPLAZADO">DESPLAZADO</option>
                            </select><br>
                           
                            <label class="" style="font-weight: bold;">IDENTIFICADOR:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="herramientaIdUnico" name="herramientaIdentificador" placeholder="" type="text" required><br>
                            <label class="" style="font-weight: bold;">CÓDIGO:*</label>
                            <input class="form-control" id="herramientaCodigo" name="herramientaCodigo" placeholder="Código Herramienta" type="number" required><br>
                            <label class="" style="font-weight: bold;">NOMBRE DE LA HERRAMIENTA:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="herramientaNombre" name="herramientaNombre" placeholder="Nombre Herramienta" type="text" required><br>
                            <label class="" style="font-weight: bold;">DESCRIPCIÓN DE LA HERRAMIENTA:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="herramientaDescripcion" name="herramientaDescripcion" placeholder="Descripción Herramienta" type="text" required><br>
                            <label class="" style="font-weight: bold;">CANTIDAD DE HERRAMIENTA:*</label>
                            <input class="form-control" id="herramientaCantidad" name="CANTIDAD_HERRAMIENTA" placeholder="" type="number" required><br>
                            <button class="mb-2 mr-2 btn btn-primary col-md-12" id="RegistrarHerramienta" type="button" value="REGISTRAR">REGISTRAR</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        
        $('#dataTable').DataTable()
        document.getElementById("RegistrarHerramienta").addEventListener('click', function() {
            RegistrarHerramienta()
        })
        
        /*Cambiar regional */
        $('#herramientaRegional').change(function() {
            var regionalID = $('#herramientaRegional').val();
            LoadCentros(regionalID)
        });
    
        function LoadCentros(regionalID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadCentros',
                type: 'POST',
                data: {
                    regionalID: regionalID
                }
            }).done(function(response) {
                var data = JSON.parse(response);
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='herramientaCentro' value='" + data[i].tbl_centro_ID + "'>" + data[i].tbl_centro_NOMBRE + "</option>"
                    }
                    $('#herramientaCentro').html(cadena);
                    var centroID = $('#herramientaCentro').val();
                    LoadSedes(centroID);
                } else {
                    cadena = "<option value='0'>NO SE ENCONTRARON DATOS </option>";
                    $('#herramientaCentro').html(cadena);
                    var centroID = $('#herramientaCentro').val();
                    LoadSedes(centroID);
                }
            })
        }
        $('#herramientaCentro').change(function() {
            var centroID = $('#herramientaCentro').val();
            LoadSedes(centroID);
        })
        // Load sedes
        function LoadSedes(centroID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadSedes',
                type: 'POST',
                data: {
                    centroID: centroID
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='herramientaSede' value='" + data[i].tbl_sede_ID + "'>" + data[i].tbl_sede_NOMBRE + "</option>";
                    }
                    var centroID = $('#herramientaCentro').val()
                    if (centroID == 0) {
                        $('#herramientaSede').html('');
                    }
                    $('#herramientaSede').html(cadena);
                    var sedeId = $('#herramientaSede').val();
                    LoadBodegas(sedeId);
                } else {
                    cadena = "<option id='herramientaSede' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#herramientaSede').html(cadena); // <- significa poner los datos dentro del input
                    var sedeId = $('#herramientaSede').val();
                    LoadBodegas(sedeId);
                }
            })
        }
        $('#herramientaSede').change(function() {
            var sedeId = $('#herramientaSede').val();
            LoadBodegas(sedeId);
        })
    
        function LoadBodegas(sedeId) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Estante/LoadBodegas',
                type: 'POST',
                data: {
                    sedeId: sedeId
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='herramientaBodega' value='" + data[i].tbl_bodega_ID + "'>" + data[i].tbl_bodega_NOMBRE + "</option>";
                    }
                    var sedeId = $('#herramientaSede').val()
                    if (sedeId == 0) {
                        $('#herramientaBodega').html('');
                    }
                    $('#herramientaBodega').html(cadena);
                    var bodegaID = $('#herramientaBodega').val();
                    LoadEstantes(bodegaID)
                } else {
                    cadena = "<option id='herramientaBodega' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#herramientaBodega').html(cadena); // <- significa poner los datos dentro del input
                    var bodegaID = $('#herramientaBodega').val();
                    LoadEstantes(bodegaID)
                }
            })
        }
        $('#herramientaBodega').change(function() {
            var bodegaID = $('#herramientaBodega').val();
            LoadEstantes(bodegaID)
        })
    
        // Load Estantes
        function LoadEstantes(bodegaID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Gaveta/LoadEstantes',
                type: 'POST',
                data: {
                    bodegaID: bodegaID
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='herramientaEstante' value='" + data[i].tbl_estante_ID + "'>" + data[i].tbl_estante_DESCRIPCION + "</option>";
                    }
                    var bodegaID = $('#herramientaBodega').val()
                    if (bodegaID == 0) {
                        $('#herramientaEstante').html('');
                    }
                    $('#herramientaEstante').html(cadena);
                    var estanteID = $('#herramientaEstante').val()
                    LoadGavetas(estanteID)
    
                } else {
                    cadena = "<option id='herramientaEstante' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#herramientaEstante').html(cadena); // <- significa poner los datos dentro del input
                    var estanteID = $('#herramientaEstante').val()
                    LoadGavetas(estanteID)
                }
            })
        }
        
        $('#herramientaEstante').change(function(){
            var estanteID = $('#herramientaEstante').val()
            LoadGavetas(estanteID)
        })
        
       // Load Gavetas
        function LoadGavetas(estanteID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Herramienta/LoadGavetas',
                type: 'POST',
                data: {
                    estanteID: estanteID
                }
            }).done(function(respuesta) {
                var data = JSON.parse(respuesta);
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='herramientaGaveta' value='" + data[i].tbl_gaveta_ID + "'>" + data[i].tbl_gaveta_DESCRIPCION + "</option>";
                    }
                    var estanteID = $('#herramientaEstante').val();
                    if (estanteID == 0) {
                        $('#herramientaGaveta').html('');
                    }
                    $('#herramientaGaveta').html(cadena);
                } else{
                    cadena = "<option id='herramientaGaveta' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#herramientaGaveta').html(cadena); 
                }
            })
        }
        // Registrar las herramientas
        function RegistrarHerramienta() {
            var herramientaRegional = $('#herramientaRegional').val();
            var herramientaCentro = $('#herramientaCentro').val();
            var herramientaSede = $('#herramientaSede').val();
            var herramientaBodega = $('#herramientaBodega').val();
            var herramientaEstante = $('#herramientaEstante').val();
            var herramientaGaveta = $('#herramientaGaveta').val();
            var herramientaDescripcion = $('#herramientaDescripcion').val();
            var rubroPresupuestal = $('#rubroPresupuestal').val();
            var herramientaCantidad = $('#herramientaCantidad').val();
            var herramientaCodigo = $('#herramientaCodigo').val();
            var herramientaNombre = $('#herramientaNombre').val();
            var herramientaExists = false;
            console.log('herramientaGaveta '+ herramientaGaveta +' herramientaDescripcion '+ herramientaDescripcion + ' rubroPresupuestal '+ rubroPresupuestal+ ' herramientaCantidad '+ herramientaCantidad + " herramientaCodigo "+ herramientaCodigo + " herramientaNombre "+ herramientaNombre)
            if(herramientaRegional == "--SELECCIONAR--"|| herramientaCentro == "0" || herramientaCentro == null || herramientaSede == "0" || herramientaSede == null ||herramientaBodega == "0" ||herramientaBodega == null ||  herramientaEstante == "0" || herramientaEstante == null || herramientaGaveta == "0" || herramientaGaveta == null || herramientaDescripcion == "" || rubroPresupuestal == "" || herramientaCantidad == "" ){
                FillData()
            } else{
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Herramienta/RegistrarHerramienta',
                    type: 'POST',
                    data: {
                        herramientaGaveta : herramientaGaveta,
                        herramientaDescripcion: herramientaDescripcion,
                        rubroPresupuestal: rubroPresupuestal,
                        herramientaCantidad : herramientaCantidad,
                        herramientaCodigo : herramientaCodigo,
                        herramientaNombre : herramientaNombre
                    }
                }).done(function(){
                    Success();
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Herramienta/ListarHerramienta';
                    }, 2000); 
                        
                }).fail(function(){
                        error();
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Herramienta/ListarHerramienta';
                    }, 2000);
                    
                })
            }
        }
        window.onload = function() {
        var contenedor = document.getElementById('contenedor_carga');
        contenedor.style.visibility = 'hidden';
        contenedor.style.opacity = '0';
        }
        
    });
</script>