<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR REGIONAL-->
<div class="modal fade" id="ModelHerramienta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDITAR HERRAMIENTA</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-md-12 pr-1">
                            <label class="" style="font-weight: bold;">
                                SELECCIONA LA GAVETA:*
                            </label>
                            <select class="form-control" id="herramientaGaveta">
                                <option>--SELECCIONAR--</option>
                                <?php foreach ($datos['ListarGaveta'] as $ListarGaveta) : ?>
                                    <option value="<?php echo $ListarGaveta->tbl_gaveta_ID ?>"><?php echo $ListarGaveta->tbl_gaveta_DESCRIPCION ?></option>
                                <?php endforeach; ?>
                            </select> <br>

                            <div class="form-group">
                                <input type="hidden" id="idHerramienta" value="<?php echo $datos['idHerramienta'] ?>">
                                <label>Nombre Sede:</label>
                                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" type="text" class="form-control" value="<?php echo $datos['herraminetaDescripcion'] ?>" id="herraminetaDescripcion"><br>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-info btn-round col-md-12" id="EditarHerramienta">ACTUALIZAR</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#ModelHerramienta").modal("show");
        $("#ModelHerramienta").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Herramienta/ListarHerramienta');
        });
        document.getElementById('EditarHerramienta').addEventListener('click', function(event) {
            EditarHerramienta()
        });

        function EditarHerramienta() {
            var herramientaGaveta = $('#herramientaGaveta').val()
            var herraminetaDescripcion = $('#herraminetaDescripcion').val();
            var idHerramienta = $('#idHerramienta').val();
            var HerramientaExistes = false;
            if (herraminetaDescripcion == "" || herramientaGaveta == "--SELECCIONAR--") {
                FillData();
            } else {
                var idHerramienta = $('#idHerramienta').val();
                $.ajax({
                    data: {
                        herramientaGaveta
                    }, //datos que se envian a traves de ajax
                    url: '<?php echo URL_SISINV ?>Herramienta/CompararHerramienta', //archivo que recibe la peticion
                    type: 'POST', //método de envio
                    success: function(response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                        var data = JSON.parse(response)
                        if (data.length > 0){
                            for (i = 0; i < data.length; i++) {
                                if (data[i].tbl_gaveta_tbl_gaveta_ID == herramientaGaveta && data[i].tbl_herramienta_DESCRIPCION == herraminetaDescripcion) {
                                    Existe();
                                    HerramientaExistes = true;
                                    break;
                                }
                            }
                        }
                        if (!HerramientaExistes) {
                            $.ajax({
                                url: '<?php echo URL_SISINV ?>Herramienta/EditarHerramienta2',
                                type: 'POST',
                                data: {
                                    idHerramienta : idHerramienta,
                                    herramientaGaveta: herramientaGaveta,
                                    herraminetaDescripcion: herraminetaDescripcion
                                }
                            }).done(function() {
                                Edit();
                                // function de tiempo
                                setTimeout(function() {
                                    window.location.href = '<?php echo URL_SISINV ?>Herramienta/ListarHerramienta';
                                }, 2000);
                            }).fail(function() {
                                ErrorEdit()
                                // function de tiempo
                                setTimeout(function() {
                                    window.location.href = '<?php echo URL_SISINV ?>Herramienta/ListarHerramienta';
                                }, 2000);
                            })
                        }
                    }

                });


            }
        }
    })
</script>