<?php require_once "../app/views/template.php"; ?>
<!--<div id="contenedor_carga">
  <div id="carga"></div>
</div>-->
<!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">SISTEMA DE INVENTARIO</h1>
          <p class="mb-4">En esta seccion puede consultar los equipos existentes.</p>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <div class="row">
                <div class="col-md-11" >
                <br>
                <h6 class="m-0 font-weight-bold text-primary">LISTADO DE EQUIPOS</h6>
                </div>
              <div class="col-md-1">
                    <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR') : ?>
                        <cite title="Agregar">
                            <a class="btn btn-success btn-icon-split" data-toggle="modal" data-target="#AgregarEquipos">
                                <span class="icon text-white-50">
                                    <i class="fas fa-plus"></i>
                                </span>
                            </a>
                        </cite>
                    <?php endif; ?>
              </div>
            </div>
          </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>REGIONAL</th>
                      <th>CENTRO</th>
                      <th>SEDE</th>
                      <th>MODELO</th>
                      <th>CANTIDAD</th>
                      <th>CONSECUTIVO</th>
                      <th>DESCRIPCION</th>
                      <th>DESCRIPCION ACTUAL</th>
                      <th>TIPO</th>
                      <th>PLACA</th>
                      <th>FECHA DE ADQUISICION</th> 
                      <th>VALOR DE INGRESO</th>
                      <th>RUBRO PRESUPUESTAL</th>
                      <th>IDENTIFICADOR</th>
                       <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR') : ?>
                            <th>ACCIONES</th>
                        <?php endif;?>
                    </tr>
                  </thead>
                  <tbody>
                      <?php $contador = 1; foreach ($datos['ListarEquipos'] as $ListarEquipos): ?>
                    <tr>
                      <td><?php echo $contador++ ?></td>
                      <td><?php echo $ListarEquipos->tbl_regional_NOMBRE; ?></td>
                      <td><?php echo $ListarEquipos->tbl_centro_NOMBRE; ?></td>
                      <td><?php echo $ListarEquipos->tbl_sede_NOMBRE; ?></td>
                      <td><?php echo $ListarEquipos->tbl_equipo_MODELO?></td>
                      <td><?php echo $ListarEquipos->tbl_equipo_CANTIDAD?></td>
                      <td><?php echo $ListarEquipos->tbl_equipo_CONSECUTIVO?></td>
                      <td><?php echo $ListarEquipos->tbl_equipo_DESCRIPCION?></td>
                      <td><?php echo $ListarEquipos->tbl_equipo_DESCRIPCION_ACTUAL?></td>
                      <td><?php echo $ListarEquipos->tbl_equipo_TIPO?></td>
                      <td><a href="<?php echo URL_SISINV; ?>Equipo/EditarEquipoPlaca/<?php echo $ListarEquipos->tbl_equipo_ID ?>"><?php echo $ListarEquipos->tbl_equipo_PLACA;?></a></td>
                      <td><?php echo $ListarEquipos->tbl_equipo_FECHA_ADQUISICION?></td>
                      <td><?php echo $ListarEquipos->tbl_equipo_VALOR_INGRESO?></td>
                      <td><?php echo $ListarEquipos->tbl_equipo_RUBRO ?></td>
                      <td><?php echo $ListarEquipos->tbl_equipo_IDENTIFICADOR ?></td>
                      <?php  if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR') : ?>
                          <td>
                          <cite title="Editar">
                               <a  class="btn btn-info btn-icon-split" href="<?php echo URL_SISINV;?>Equipo/ObtenerEquipo/<?php echo $ListarEquipos->tbl_equipo_ID;?>">
                                <span class="icon text-white-50">
                                  <i class="fas fa-edit"></i>
                                </span>
                              </a>
                            </cite>
                            <cite title="Borrar">
                              <a  class="btn btn-danger btn-icon-split" href="<?php echo URL_SISINV;?>Equipo/EliminarEquipo/<?php echo $ListarEquipos->tbl_equipo_ID;?>">
                                  <span class="icon text-white-50">
                                  <i class="fas fa-trash"></i>
                                </span>
                              </a>
                            </cite>
                          </td>
                      <?php endif; ?>
                    </tr>
                  <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
<!-- /.container-fluid -->
<div class="modal fade" id="AgregarEquipos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">AGREGAR EQUIPOS</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <form>
                <div class="row">
                    <div class="position-relative form-group col-md-12">
                        <label class="" style="font-weight: bold;">
                            SELECCIONA LA REGIONAL:*
                        </label>
                        <select class="form-control" id="equipoRegional" name="equipoRegional">
                            <option>--SELECCIONAR--</option>
                            <?php foreach ($datos['ListarRegional'] as $ListarRegional) : ?>
                                <?php if ($ListarRegional) : ?>
                                    <option value="<?php echo $ListarRegional->tbl_regional_ID; ?>"><?php echo $ListarRegional->tbl_regional_NOMBRE; ?></option>
                                <?php else : ?>
                                    <option value="0">-- SELECCIONA LA REGIONAL--</option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select><br>
                        <label class="" style="font-weight: bold;">CENTRO:*</label>
                        <select class="form-control" id="equipoCentro" >
                        </select><br>
                        <label class="" style="font-weight: bold;">SEDE:*</label>
                        <select class="form-control" id="equipoSede">
                        </select><br>
                        <label class="" style="font-weight: bold;" >MODELO:*</label>
                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" type="text" required id="equipoModelo"><br>
                        <label class="" style="font-weight: bold;">CANTIDAD:*</label>
                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" type="number" required id="equipoCantidad" ><br>
                        <label class="" style="font-weight: bold;">CONSECUTIVO:*</label>
                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" type="number" required id="equipoConsecutivo" ><br>
                        <label class="" style="font-weight: bold;">DESCRIPCION:*</label>
                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" type="text" required id="equipoDescripcion" ><br>
                        <label class="" style="font-weight: bold;">DESCRIPCION ACTUAL:*</label>
                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" type="text" required id="equipoDescripcionActual"><br>
                        
                        <label class="" style="font-weight: bold;">RUBRO PRESUPUESTAL:*</label>
                        <select class="form-control" id="rubroPresupuestal" name="rubroPresupuestal">
                            <option value="REGULAR">REGULAR</option>
                            <option value="ARTICULACION">ARTICULACION</option>
                            <option value="FIC">FIC</option>
                            <option value="DESPLAZADO">DESPLAZADO</option>
                        </select><br>
                        
                        <label class="" style="font-weight: bold;">IDENTIFICADOR:*</label>
                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" type="text" required id="equipoIdUnico"><br>
                        <label class="" style="font-weight: bold;">TIPO:*</label>
                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" type="text" required id="equipoTipo"><br>
                        <label class="" style="font-weight: bold;">PLACA:*</label>
                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" type="number" required id="equipoPlaca"><br>
                       
                        
                        <label class="" style="font-weight: bold;">VALOR DE INGRESO:*</label>
                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()"  class="form-control" name="VALOR_INGRESO" placeholder="" type="number" required id="equipoValordeingreso"><br>
                        <button class="mb-2 mr-2 btn btn-primary col-md-12" type="button" id="RegistrarEquipo">REGISTRAR</button>
                    </div>
                </div>
            </form>            
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
        </div>
      </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script  type="text/javascript"> 
    $(document).ready(function(){
        $('#dataTable').DataTable()
        document.getElementById("RegistrarEquipo").addEventListener('click', function() {
            RegistrarEquipo();
        });
        
        /*Cambiar regional */
        $('#equipoRegional').change(function(){
            var regionalID = $('#equipoRegional').val();
            LoadCentros(regionalID);
        });
    
        function LoadCentros(regionalID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadCentros',
                type: 'POST',
                data: {
                    regionalID: regionalID
                }
            }).done(function(response) {
                var data = JSON.parse(response);
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='equipoCentro' value='" + data[i].tbl_centro_ID + "'>" + data[i].tbl_centro_NOMBRE + "</option>"
                    }
                    $('#equipoCentro').html(cadena);
                    var centroID = $('#equipoCentro').val();
                    LoadSedes(centroID);
                } else {
                    cadena = "<option value='0'>NO SE ENCONTRARON DATOS </option>";
                    $('#equipoCentro').html(cadena);
                    var centroID = $('#equipoCentro').val();
                    LoadSedes(centroID);
                }
            });
        }
        $('#equipoCentro').change(function() {
            var centroID = $('#equipoCentro').val();
            LoadSedes(centroID);
        })
        // Load sedes
        function LoadSedes(centroID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadSedes',
                type: 'POST',
                data: {
                    centroID: centroID
                }
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='equipoSede' value='" + data[i].tbl_sede_ID + "'>" + data[i].tbl_sede_NOMBRE + "</option>";
                    }
                    var centroID = $('#equipoCentro').val()
                    if (centroID == 0) {
                        $('#equipoSede').html('');
                    }
                    $('#equipoSede').html(cadena);
                    var sedeId = $('#equipoSede').val();
             
                } else {
                    cadena = "<option id='equipoSede' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#equipoSede').html(cadena); // <- significa poner los datos dentro del input
                    var sedeId = $('#equipoSede').val();
              
                }
            });
        }
        function RegistrarEquipo(){
            var equipoSede = $('#equipoSede').val();
            var equipoModelo = $('#equipoModelo').val().trim();
            var equipoCantidad = $('#equipoCantidad').val().trim();
            var equipoConsecutivo = $('#equipoConsecutivo').val().trim();
            var equipoDescripcion = $('#equipoDescripcion').val().trim();
            var equipoDescripcionActual = $('#equipoDescripcionActual').val().trim();
            var rubroPresupuestal = $('#rubroPresupuestal').val();
            var equipoTipo = $('#equipoTipo').val().trim();
            var equipoPlaca = $('#equipoPlaca').val().trim();
            var equipoValordeingreso = $('#equipoValordeingreso').val().trim();
            
            console.log(rubroPresupuestal)
            
            if(equipoSede === '0' || equipoRegional == "--SELECCIONAR--" ||  equipoModelo == "" || equipoCantidad == "" || equipoConsecutivo == "" || equipoDescripcion == "" || equipoDescripcionActual == "" || rubroPresupuestal == "" || equipoTipo == "" || equipoPlaca == "" ||  equipoValordeingreso == "" ) {
                FillData();
            } else {
                // MirarEquipoPlaca
                
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Equipo/RegistrarEquipo',
                    type: 'POST',
                    data: {
                        
                        equipoSede: equipoSede,
                        equipoModelo: equipoModelo,
                        equipoCantidad: equipoCantidad,
                        equipoConsecutivo: equipoConsecutivo,
                        equipoDescripcion: equipoDescripcion,
                        equipoDescripcionActual: equipoDescripcionActual,
                        rubroPresupuestal: rubroPresupuestal,
                        equipoTipo: equipoTipo,
                        equipoPlaca: equipoPlaca,
                        equipoValordeingreso: equipoValordeingreso,
                        
                    }
                }).done(function(){
                        Success();
                        setTimeout(function() {
                            window.location.href = '<?php echo URL_SISINV ?>Equipo/ListarEquipo';
                        }, 2000);
                }).fail(function(){
                    error();
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Equipo/ListarEquipo';
                    }, 2000);
                });
                
            }
        }
        window.onload = function() {
        var contenedor = document.getElementById('contenedor_carga');
        contenedor.style.visibility = 'hidden';
        contenedor.style.opacity = '0';
        }
    });    
</script>