<?php require_once "../app/views/template.php"; ?>
<!--MODAL ELIMINAR EQUIPO-->
<div class="modal fade" id="ModelEquipo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">ELIMINAR EQUIPOS</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <form class="" action="<?php echo URL_SISINV;?>Equipo/EliminarEquipo/<?php echo $datos['idEquipo'];?>" method="POST">
            <div class="row">
                <div class="position-relative form-group col-md-12">
                      
                     <input class="form-control" id="idEquipo"  type="hidden" value="<?php echo $datos['idEquipo']?>"><br>
                      
                      <label class="" style="font-weight: bold;">MODELO:*</label>
                      <input onkeyup="mayus(this);" class="form-control" name="MODELO" value=" <?php echo $datos['equipoModelo'] ?> " type="text" disabled><br>

                      <label class="" style="font-weight: bold;">CONSECUTIVO:*</label>
                      <input onkeyup="mayus(this);" class="form-control" name="CONSECUTIVO" value=" <?php echo $datos['equipoConsecutivo'] ?> " type="text" disabled><br>

                      <label class="" style="font-weight: bold;">DESCRIPCION:*</label>
                      <input onkeyup="mayus(this);" class="form-control" name="DESCRIPCION" value=" <?php echo $datos['equipoDescripcion'] ?> " type="text" disabled><br>

                      <label class="" style="font-weight: bold;">DESCRIPCION ACTUAL:*</label>
                      <input onkeyup="mayus(this);" class="form-control" name="DESCRIPCION_ACTUAL" value=" <?php echo $datos['equipoDescripcionActual'] ?> " type="text" disabled><br>

                      <label class="" style="font-weight: bold;">TIPO:*</label>
                      <input onkeyup="mayus(this);" class="form-control" name="TIPO" value=" <?php echo $datos['equipoTipo'] ?> " type="text" disabled><br>

                      <label class="" style="font-weight: bold;">PLACA:*</label>
                      <input onkeyup="mayus(this);" class="form-control" name="PLACA" value=" <?php echo $datos['equipoPlaca'] ?> " type="text" disabled><br>

                      <label class="" style="font-weight: bold;">SERIAL:*</label>
                      <input onkeyup="mayus(this);" class="form-control" name="SERIAL" value=" <?php echo $datos['equipoSerial'] ?> " type="text" disabled><br>

                      <label class="" style="font-weight: bold;">FECHA DE ADQUISICION:*</label>
                      <div class="form-group row">
                        <div class="col-10">
                          <input class="form-control" type="text" value="<?php echo $datos['equipoFechadeAdquision'];?> " name="FECHA_INGRESO" id="example-date-input" disabled>
                          <i class="fa fas-calendar "></i>
                        </div>
                      </div>
                      <label class="" style="font-weight: bold;">VALOR DE INGRESO:*</label>
                      <input onkeyup="mayus(this);" class="form-control" name="VALOR_INGRESO" value=" <?php echo $datos['equipoValordeingreso'] ?> " type="text" disabled><br>
                      <button class="mb-2 mr-2 btn btn-danger col-md-12" type="button" id="EliminarEquipo" >ELIMINAR</button>
                </div>
            </div>
          </form>            
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
        </div>
      </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $("#ModelEquipo").modal("show");
    $("#ModelEquipo").on('hidden.bs.modal', function() {
      window.location.replace('<?php echo URL_SISINV; ?>Equipo/ListarEquipo');
    });
    document.getElementById("EliminarEquipo").addEventListener('click', function() {
      EliminarEquipo()
    })

    function EliminarEquipo() {
      var idEquipo = $('#idEquipo').val();
       $.ajax({
            url: '<?php echo URL_SISINV ?>Equipo/DeleteEquipo',
            type: 'POST',
            data: {
              idEquipo: idEquipo
            }
        }).done(function(){
            Delete();
            setTimeout(function() {
                window.location.href = '<?php echo URL_SISINV ?>Equipo/ListarEquipo';
            }, 2000);
        }).fail(function(){
            errorDelete();
            setTimeout(function() {
                window.location.href = '<?php echo URL_SISINV ?>Equipo/ListarEquipo';
            }, 2000); 
        });
    }
  });
</script>