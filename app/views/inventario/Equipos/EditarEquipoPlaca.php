<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR REGIONAL-->
<div class="modal fade" id="Modelequipo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDITAR PLACA</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-md-12 pr-1">
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" name="idEquipo" value=" <?php echo $datos['idEquipo'] ?> " id="idEquipo" type="hidden" required><br>
                            <label class="" style="font-weight: bold;">
                                SELECCIONA LA SEDE:*
                            </label>
                            <select class="form-control" id="equipoSede">
                                <option>--SELECCIONAR--</option>
                                <?php foreach ($datos['ListarSede'] as $ListarSede) : ?>
                                    <option value="<?php echo $ListarSede->tbl_sede_ID ?>"><?php echo $ListarSede->tbl_sede_NOMBRE ?></option>
                                <?php endforeach; ?>
                            </select> <br>
                            <div class="form-group">
                                <label class="" style="font-weight: bold;">PLACA:*</label>
                                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" name="equipoPlaca" value="<?php echo $datos['equipoPlaca'] ?>" id="equipoPlaca" type="text" required><br>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-info btn-round col-md-12" id="EditarEquipo">ACTUALIZAR</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#Modelequipo").modal("show");
        $("#Modelequipo").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Equipo/ListarEquipo');
        });
        document.getElementById('EditarEquipo').addEventListener('click', function() {
            EditarEquipo()
        });

        function EditarEquipo() {
            var equipoSede = $('#equipoSede').val()
            var idEquipo = $('#idEquipo').val();
            var equipoPlaca = $('#equipoPlaca').val();
            var equipoCodigoExists = false;
           
            if (equipoPlaca == "" || equipoSede == "--SELECCIONAR--") {
                FillData();
            } else {
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Equipo/CompararEquipo', 
                    type: 'POST',
                    data: {
                        idEquipo
                    },
                }).done(function(response){
                    var data = JSON.parse(response);
                    if (data.length > 0){
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].tbl_sede_tbl_sede_ID == equipoSede && data[i].tbl_equipo_PLACA == equipoPlaca) {
                                Existe();
                                equipoCodigoExists = true;
                                break;
                            };
                        }
                    }
                    if (!equipoCodigoExists) {
                        $.ajax({
                            url: '<?php echo URL_SISINV ?>Equipo/EditarEquipo2',
                            type: 'POST',
                            data: {
                                idEquipo : idEquipo,
                                equipoSede: equipoSede,
                                equipoPlaca: equipoPlaca
                            }
                        }).done(function() {
                            Edit()
                            // function de tiempo
                            setTimeout(function() {
                                window.location.href = '<?php echo URL_SISINV ?>Equipo/ListarEquipo';
                            }, 2000);
                        }).fail(function() {
                            ErrorEdit()
                            // function de tiempo
                            setTimeout(function() {
                                window.location.href = '<?php echo URL_SISINV ?>Equipo/ListarEquipo';
                            }, 2000);
                        })
                    }
                })
            }
        }
    })
</script>