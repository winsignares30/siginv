<?php require_once "../app/views/template.php"; ?>
<!--MODAL EDITAR EQUIPO-->
<div class="modal fade" id="ModelEquipo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDITAR EQUIPOS</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="position-relative form-group col-md-12">
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" name="idEquipo" value=" <?php echo $datos['idEquipo'] ?> " id="idEquipo" type="hidden" required><br>
                            <label class="" style="font-weight: bold;">MODELO:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" name="equipoModelo" value=" <?php echo $datos['equipoModelo'] ?> " id="equipoModelo" type="text" required><br>
                            
                            <label class="" style="font-weight: bold;">CANTIDAD:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" name="equipoCantidad" value=" <?php echo $datos['equipoCantidad'] ?> " id="equipoCantidad" type="text" required><br>

                            <label class="" style="font-weight: bold;">CONSECUTIVO:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" name="equipoConsecutivo" value=" <?php echo $datos['equipoConsecutivo'] ?> " id="equipoConsecutivo" type="text" required><br>

                            <label class="" style="font-weight: bold;">DESCRIPCION:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" name="equipoDescripcion" value=" <?php echo $datos['equipoDescripcion'] ?> " id="equipoDescripcion" type="text" required><br>

                            <label class="" style="font-weight: bold;">DESCRIPCION ACTUAL:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" name="equipoDescripcionActual" value=" <?php echo $datos['equipoDescripcionActual'] ?> " id="equipoDescripcionActual" type="text" required><br>

                            <label class="" style="font-weight: bold;">TIPO:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" name="equipoTipo" value=" <?php echo $datos['equipoTipo'] ?> " id="equipoTipo" type="text" required><br>
                            
                            <label class="" style="font-weight: bold;">RUBRO PRESUPUESTAL:*</label>
                            <select class="form-control" id="rubroPresupuestal" name="rubroPresupuestal">
                                <option value="REGULAR">REGULAR</option>
                                <option value="ARTICULACION">ARTICULACION</option>
                                <option value="FIC">FIC</option>
                                <option value="DESPLAZADO">DESPLAZADO</option>
                            </select><br>
                            
                            <label class="" style="font-weight: bold;">VALOR DE INGRESO:*</label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" name="equipoValordeingreso" value=" <?php echo $datos['equipoValordeingreso'] ?> " id="equipoValordeingreso" type="text" required><br>
                            
                            <label class="" style="font-weight: bold;">FECHA DE ADQUISICION:*</label>
                            <div class="form-group row">
                                <div class="col-10">
                                    <input class="form-control" type="datetime" value="<?php echo $datos['equipoFechadeAdquision']; ?>" name="FECHA_INGRESO" id="equipoFechadeAdquision" id="example-date-input">
                                    <i class="fa fas-calendar "></i>
                                </div>
                            </div>
                            <button class="mb-2 mr-2 btn btn-info col-md-12" type="button" id="EditarEquipo">ACTUALIZAR</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#ModelEquipo").modal("show");
        $("#ModelEquipo").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV; ?>Equipo/ListarEquipo');
        });


        document.getElementById('EditarEquipo').addEventListener('click', function() {
            EditarEquipo()
        });

        function EditarEquipo() {
            var idEquipo = $('#idEquipo').val().trim();
            var equipoModelo = $('#equipoModelo').val().trim();
            var equipoCantidad = $('#equipoCantidad').val().trim();
            var equipoConsecutivo = $('#equipoConsecutivo').val().trim();
            var equipoDescripcion = $('#equipoDescripcion').val().trim();
            var equipoDescripcionActual = $('#equipoDescripcionActual').val().trim();
            var equipoTipo = $('#equipoTipo').val().trim();
            var rubroPresupuestal = $('#rubroPresupuestal').val().trim();
            var equipoValordeingreso = $('#equipoValordeingreso').val().trim();
            var equipoFechadeAdquision = $('#equipoFechadeAdquision').val();
            if(idEquipo === "" || equipoModelo ==="" || equipoCantidad === "" || equipoConsecutivo ==="" || equipoDescripcion ==="" || equipoDescripcionActual==="" || equipoTipo ==="" || rubroPresupuestal == "" || equipoValordeingreso ==="" || equipoFechadeAdquision === ""){
                FillData();
            }else{
                $.ajax({
                    url: '<?php echo URL_SISINV;?>Equipo/EditarEquipo',
                    type: 'POST',
                    data: {
                        idEquipo: idEquipo,
                        equipoModelo: equipoModelo,
                        equipoCantidad: equipoCantidad,
                        equipoConsecutivo: equipoConsecutivo,
                        equipoDescripcion: equipoDescripcion,
                        equipoDescripcionActual: equipoDescripcionActual,
                        equipoTipo: equipoTipo,
                        rubroPresupuestal : rubroPresupuestal,
                        equipoValordeingreso: equipoValordeingreso,
                        equipoFechadeAdquision: equipoFechadeAdquision
                    }
                }).done(function(){
                    Edit();
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Equipo/ListarEquipo';
                    }, 2000);
                }).fail(function(){
                    ErrorEdit();
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Equipo/ListarEquipo';
                    }, 2000); 
                });
            }
            
        }
    });
</script>