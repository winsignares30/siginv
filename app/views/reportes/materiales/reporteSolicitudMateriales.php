<?php require_once "../app/views/template.php";?>
<div id="contenedor_carga"><div id="carga"></div></div>
<div class="content">
    <div class="row"> 
        <div class="container-fluid">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="card-body">
                        
                         <div id="">
                            <h2>Reporte de Solicitud de Materiales</h2>
                        </div>
                        <table class="table table-bordered table-striped table-hover" id="reporteMaterialesSolicitadosEntreDiasTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Id Solicitud</th>
                                  <th>Codigo</th>
                                  <th>Nombre</th>
                                  <th>Descripcion</th>
                                  <th>Cantidad</th>
                                  <th>Fecha</th>
                                  <th>Tipo</th>
                                  <th>Persona</th>
                                  <th>Ficha</th>
                                  <th>Rubro</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $contador = 1;
                                foreach ($datos['reporteMaterialesSolicitadosEntreDias'] as $reporteMaterialesSolicitadosEntreDias) : ?>
                                  <tr>
                                    <th scope="row"><?php echo $contador++; ?></th>
                                    <td><?php echo $reporteMaterialesSolicitadosEntreDias->id_solicitud; ?></td>
                                    <td><?php echo $reporteMaterialesSolicitadosEntreDias->codigo; ?></td>
                                    <td><?php echo $reporteMaterialesSolicitadosEntreDias->nombre; ?></td>
                                    <td><?php echo $reporteMaterialesSolicitadosEntreDias->descripcion; ?></td>
                                    <td><?php echo $reporteMaterialesSolicitadosEntreDias->cantidad; ?></td>
                                    <td><?php echo $reporteMaterialesSolicitadosEntreDias->fecha; ?></td>
                                    <td><?php echo $reporteMaterialesSolicitadosEntreDias->tipo; ?></td>
                                    <td><?php echo $reporteMaterialesSolicitadosEntreDias->persona; ?></td>
                                    <td><?php echo $reporteMaterialesSolicitadosEntreDias->ficha; ?></td>
                                    <td><?php echo $reporteMaterialesSolicitadosEntreDias->rubro; ?></td>
                                  </tr>
                                <?php endforeach; ?>
                            </tbody>
                        
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.css">
<script src="https://cdn.jsdelivr.net/combine/npm/chart.js@2.9.4,npm/chart.js@2.9.4/dist/Chart.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>





<script type="text/javascript">
$(document).ready(function(){

    $('#reporteMaterialesSolicitadosEntreDiasTable').DataTable({        
        language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
			     },
			     "sProcessing":"Procesando...",
            },
        //para usar los botones   
        responsive: "true",
        dom: 'Bfrtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Exportar a Excel',
				className: 'btn btn-success'
			},
			{
				extend:    'pdfHtml5',
				text:      '<i class="fas fa-file-pdf"></i> ',
				titleAttr: 'Exportar a PDF',
				className: 'btn btn-danger'
			},
			{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},
		]	        
    });
    
});
    window.onload = function(){
		var contenedor = document.getElementById('contenedor_carga');
		contenedor.style.visibility = 'hidden';
		contenedor.style.opacity = '0'; 
	}
</script>

