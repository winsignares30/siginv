<?php require_once "../app/views/template.php"; ?>
<div class="content">
    <div class="row"> 
        <div class="container-fluid">
            <h2 class="mb-5">Modulo de Reportes</h2>
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5">
                
                <div class="col mb-4 col-12 col-sm-12 col-md-12 col-lg-6 col-xl-4">
                    <div class="card card-nav-tabs">
                      <div class="card-header card-header-warning" align="center" style="background-color: #F56400; color: white; font-size: 1.2rem;">
                       <b >Solicitud Herramientas</b>
                      </div>
                      <img class="card-img-top" style="padding: 9px 4px 4px 4px; height: 260px; margin-inline: auto; width: 220px;" src="<?php echo URL_SISINV?>images/reporte1.png" rel="nofollow" alt="" >
                      <div class="card-body" align="center">
                        <p class="card-text" style="padding: 10px; margin-bottom: 0;" >En éste apartado podrás hacer reportes de las solicitudes de <strong>herramientas</strong> que se hicieron.</p>
                        <a href="#" data-toggle="modal" data-target="#modal-reporte-herramientas-solicitadas" style="margin-left: 10px;" class="btn btn-primary btn-round">Consultar</a>
                      </div>
                    </div>
                </div>
                
                <div class="col mb-4 col-12 col-sm-12 col-md-12 col-lg-6 col-xl-4">
                    <div class="card card-nav-tabs">
                      <div class="card-header card-header-warning" align="center" style="background-color: #F56400; color: white; font-size: 1.2rem;">
                       <b >Solicitud Materiales</b>
                      </div>
                      <img class="card-img-top" style="padding: 9px 4px 4px 4px; height: 260px; margin-inline: auto; width: 220px;" src="<?php echo URL_SISINV?>images/reporte1.png" rel="nofollow" alt="">
                      <div class="card-body" align="center">
                        <p class="card-text" style="padding: 10px; margin-bottom: 0;" >En éste apartado podrás hacer reportes de las solicitudes de <strong>materiales</strong> que se hicieron.</p>
                        <a href="#" data-toggle="modal" data-target="#modal-reporte-materiales-solicitadas" style="margin-left: 10px;" class="btn btn-primary btn-round">Consultar</a>
                      </div>
                    </div>
                </div>
               
               <div class="col mb-4 col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4">
                    <div class="card card-nav-tabs">
                      <div class="card-header card-header-warning" align="center" style="background-color: #F56400; color: white; font-size: 1.2rem;">
                       <b >Solicitud Equipos</b>
                      </div>
                      <img class="card-img-top" style="padding: 9px 4px 4px 4px; height: 260px; margin-inline: auto; width: 220px;" src="<?php echo URL_SISINV?>images/reporte1.png" rel="nofollow" alt="">
                      <div class="card-body" align="center">
                        <p class="card-text" style="padding: 10px; margin-bottom: 0;" >En éste apartado podrás hacer reportes de las solicitudes de <strong>equipos</strong> que se hicieron.</p>
                        <a href="#" data-toggle="modal" data-target="#modal-reporte-equipos-solicitadas" style="margin-left: 10px;" class="btn btn-primary btn-round">Consultar</a>
                      </div>
                    </div>
                </div>
                
                <!--<div class="col mb-4 col-12 col-sm-6 col-md-6 col-lg-4 col-xl-3">
                    <div class="card card-nav-tabs">
                      <div class="card-header card-header-warning" align="center" style="background-color: #F56400; color: white; font-size: 1.2rem;">
                       <b >Devoluciones</b>
                      </div>
                      <img class="card-img-top" style="padding: 9px 4px 4px 4px; height: 260px; margin-inline: auto; width: 220px;" src="<?php echo URL_SISINV?>images/devoluciones.png" rel="nofollow" alt="">
                      <div class="card-body" align="center">
                        <p class="card-text" style="padding: 10px; margin-bottom: 0;" >En éste apartado podrás hacer reportes de las devoluciones de <strong>herramientas, materiales y equipos</strong> que se hicieron.</p>
                        <a href="#" style="margin-left: 10px;" class="btn btn-primary btn-round">Consultar</a>
                      </div>
                    </div>
                </div>-->
                
            </div>
        </div>
    </div>
</div>
<!-- MODAL REPORTE HERRAMIENTAS SOLICITADAS ENTRE DIAS-->
<div class="modal fade modal-reporte-herramientas-solicitadas" id="modal-reporte-herramientas-solicitadas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">HERRAMIENTAS</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="content">
                <div class="row"> 
                    <div class="container-fluid">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <div class="card-body">
                                    <div class="card-header">
                                        <button id="btn-request-by-instructor" style="float: right" type="submit" class="btn btn-success btn-round col-md-2" >Instructor</button>
                                        <button id="btn-request_tool" style="float: right" type="submit" class="btn btn-success btn-round col-md-2" >Go back</button>
                                    </div>
                                    <div id="div-tool_request">
                                        <h2>Reporte de Solicitud de Herramientas</h2>
                                    </div>
                
                                    <form id="form-tool_request" method="post" action="<?php echo URL_SISINV ?>Reporte/imprimirReporteHerramientasSolicitadasEntreDias">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="exampleInputEmail1">Seleccionar dia Inicial:</label>
                                                <div class="input-group mb-3">
                                                    <input type="date" class="form-control" id="fechaInicial" name="fechaInicial" value="">
                                                </div>
                                            </div>
                                                
                                            <div class="col-md-4">
                                                <label for="exampleInputPassword1">Seleccionar dia Final:</label>
                                                <div class="input-group mb-3">
                                                    <input type="date" class="form-control" id="fechaFinal" name="fechaFinal" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group mb-3 pt-4">
                                                    <button type="submit" class="btn btn-primary btn-round col-md-12" >Consultar</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </form>
                                    
                                    <div id="div-request-by-instructor">
                                        <h2>Reporte de Solicitud de Herramientas por instructor</h2>
                                    </div>
                
                                    <form method="post" action="<?php echo URL_SISINV ?>Reporte/herramientasPorInstructor" id="form-tool_request-by-instructor">
                                        <div class="row">
                                             <div class="col-md-3">
                                                <label for="exampleInputEmail1">Seleccionar Instructor</label>
                                                <div class="input-group mb-3">
                                                    <select id="select-id_instructor" name="select-id_instructor" class="form-control" aria-label=".form-select-sm example">
                                                        <option selected>Seleccionar...</option>
                                                        <?php foreach($datos['instructores'] as $instructor): ?>
                                                            <?php if($instructor): ?>
                                                                <option value="<?php echo $instructor->id; ?>"><?php echo $instructor->nombres.' '. $instructor->apellido1 .' '. $instructor->apellido2 ;?></option>
                                                            <?php else: ?>
                                                                <option value="0">-- NO SE ENCONTRARON DATOS --</option>
                                                            <?php endif;?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="exampleInputEmail1">Seleccionar dia Inicial:</label>
                                                <div class="input-group mb-3">
                                                    <input type="date" class="form-control" id="fechaInicial" name="fechaInicial" value="">
                                                </div>
                                            </div>
                                                
                                            <div class="col-md-3">
                                                <label for="exampleInputPassword1">Seleccionar dia Final:</label>
                                                <div class="input-group mb-3">
                                                    <input type="date" class="form-control" id="fechaFinal" name="fechaFinal" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="input-group mb-3 pt-4">
                                                    <button type="submit" class="btn btn-primary btn-round col-md-12" >Consultar</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
        </div>
    </div>
  </div>
</div>

<!-- MODAL REPORTE MATERIALES SOLICITADOS ENTRE DIAS-->
<div class="modal fade modal-reporte-materiales-solicitadas" id="modal-reporte-materiales-solicitadas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">MATERIALES</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="content">
                <div class="row"> 
                    <div class="container-fluid">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <div class="card-body">
                                    <div class="card-header">
                                        <button id="btn-request_material-by-instructor" style="float: right" type="submit" class="btn btn-success btn-round col-md-2" >Instructor</button>
                                        <button id="btn-request_material" style="float: right" type="submit" class="btn btn-success btn-round col-md-2" >Go back</button>
                                    </div>
                                    <div id="div-material_request">
                                        <h2>Reporte de Solicitud de Materiales</h2>
                                    </div>
                                    <form id="form-material_request" method="post" action="<?php echo URL_SISINV ?>Reporte/imprimirReporteMaterialesSolicitadosEntreDias">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="exampleInputEmail1">Seleccionar dia Inicial:</label>
                                                <div class="input-group mb-3">
                                                    <input type="date" class="form-control" id="fechaInicial" name="fechaInicial" value="">
                                                </div>
                                            </div>
                                                
                                            <div class="col-md-4">
                                                <label for="exampleInputPassword1">Seleccionar dia Final:</label>
                                                <div class="input-group mb-3">
                                                    <input type="date" class="form-control" id="fechaFinal" name="fechaFinal" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group mb-3 pt-4">
                                                    <button type="submit" class="btn btn-primary btn-round col-md-12" >Consultar</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </form>
                                    
                                    <div id="div-request_material-by-instructor">
                                        <h2>Reporte de Solicitud de Materiales por instructor</h2>
                                    </div>
                
                                    <form method="post" action="<?php echo URL_SISINV ?>Reporte/materialesPorInstructor" id="form-material_request-by-instructor">
                                        <div class="row">
                                             <div class="col-md-3">
                                                <label for="exampleInputEmail1">Seleccionar Instructor</label>
                                                <div class="input-group mb-3">
                                                    <select id="select-id_instructor" name="select-id_instructor" class="form-control" aria-label=".form-select-sm example">
                                                        <option selected>Seleccionar...</option>
                                                        <?php foreach($datos['instructores'] as $instructor): ?>
                                                            <?php if($instructor): ?>
                                                                <option value="<?php echo $instructor->id; ?>"><?php echo $instructor->nombres.' '. $instructor->apellido1 .' '. $instructor->apellido2 ;?></option>
                                                            <?php else: ?>
                                                                <option value="0">-- NO SE ENCONTRARON DATOS --</option>
                                                            <?php endif;?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="exampleInputEmail1">Seleccionar dia Inicial:</label>
                                                <div class="input-group mb-3">
                                                    <input type="date" class="form-control" id="fechaInicial" name="fechaInicial" value="">
                                                </div>
                                            </div>
                                                
                                            <div class="col-md-3">
                                                <label for="exampleInputPassword1">Seleccionar dia Final:</label>
                                                <div class="input-group mb-3">
                                                    <input type="date" class="form-control" id="fechaFinal" name="fechaFinal" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="input-group mb-3 pt-4">
                                                    <button type="submit" class="btn btn-primary btn-round col-md-12" >Consultar</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
        </div>
    </div>
  </div>
</div>

<!-- MODAL REPORTE EQUIPOS SOLICITADAS ENTRE DIAS-->
<div class="modal fade modal-reporte-equipos-solicitadas" id="modal-reporte-equipos-solicitadas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">EQUIPOS</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="content">
                <div class="row"> 
                    <div class="container-fluid">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <div class="card-body">
                                    <div class="card-header">
                                        <button id="btn-request_equipment-by-instructor" style="float: right" type="submit" class="btn btn-success btn-round col-md-2" >Instructor</button>
                                        <button id="btn-request_equipment" style="float: right" type="submit" class="btn btn-success btn-round col-md-2" >Go back</button>
                                    </div>
                                    <div  id="div-equipment_request">
                                        <h2>Reporte de Solicitud de Equipos</h2>
                                    </div>
                                    <form id="form-equipment_request" method="post" action="<?php echo URL_SISINV ?>Reporte/imprimirReporteEquiposSolicitadosEntreDias">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="exampleInputEmail1">Seleccionar dia Inicial:</label>
                                                <div class="input-group mb-3">
                                                    <input type="date" class="form-control" id="fechaInicial" name="fechaInicial" value="">
                                                </div>
                                            </div>
                                                
                                            <div class="col-md-4">
                                                <label for="exampleInputPassword1">Seleccionar dia Final:</label>
                                                <div class="input-group mb-3">
                                                    <input type="date" class="form-control" id="fechaFinal" name="fechaFinal" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group mb-3 pt-4">
                                                    <button type="submit" class="btn btn-primary btn-round col-md-12" >Consultar</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </form>
                                    
                                    <div id="div-request_equipment-by-instructor">
                                        <h2>Reporte de Solicitud de Equipos por instructor</h2>
                                    </div>
                
                                    <form method="post" action="<?php echo URL_SISINV ?>Reporte/equiposPorInstructor" id="form-equipment_request-by-instructor">
                                        <div class="row">
                                             <div class="col-md-3">
                                                <label for="exampleInputEmail1">Seleccionar Instructor</label>
                                                <div class="input-group mb-3">
                                                    <select id="select-id_instructor" name="select-id_instructor" class="form-control" aria-label=".form-select-sm example">
                                                        <option selected>Seleccionar...</option>
                                                        <?php foreach($datos['instructores'] as $instructor): ?>
                                                            <?php if($instructor): ?>
                                                                <option value="<?php echo $instructor->id; ?>"><?php echo $instructor->nombres.' '. $instructor->apellido1 .' '. $instructor->apellido2 ;?></option>
                                                            <?php else: ?>
                                                                <option value="0">-- NO SE ENCONTRARON DATOS --</option>
                                                            <?php endif;?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label for="exampleInputEmail1">Seleccionar dia Inicial:</label>
                                                <div class="input-group mb-3">
                                                    <input type="date" class="form-control" id="fechaInicial" name="fechaInicial" value="">
                                                </div>
                                            </div>
                                                
                                            <div class="col-md-3">
                                                <label for="exampleInputPassword1">Seleccionar dia Final:</label>
                                                <div class="input-group mb-3">
                                                    <input type="date" class="form-control" id="fechaFinal" name="fechaFinal" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="input-group mb-3 pt-4">
                                                    <button type="submit" class="btn btn-primary btn-round col-md-12" >Consultar</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
        </div>
    </div>
  </div>
</div>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.css">
<script src="https://cdn.jsdelivr.net/combine/npm/chart.js@2.9.4,npm/chart.js@2.9.4/dist/Chart.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>

<script type="text/javaScript">
    $(document).ready(function(){
        // herramientas
        $('#div-request-by-instructor').hide()
        $('#form-tool_request-by-instructor').hide()
        $('#btn-request_tool').hide()
        $('#btn-request-by-instructor').click(function(){
            $('#btn-request_tool').show()
            $('#div-request-by-instructor').show()
            $('#form-tool_request-by-instructor').show()
            $('#div-tool_request').hide()
            $('#form-tool_request').hide()
            $('#btn-request-by-instructor').hide()
            
        })
        $('#btn-request_tool').click(function(){
            $('#div-tool_request').show()
            $('#form-tool_request').show()
            $('#btn-request-by-instructor').show()
            $('#btn-request_tool').hide()
            $('#div-request-by-instructor').hide()
            $('#form-tool_request-by-instructor').hide()
        })
        // materiales
        $('#btn-request_material').hide()
        $('#form-material_request-by-instructor').hide()
        $('#div-request_material-by-instructor').hide()
        
        $('#btn-request_material-by-instructor').click(function(){
            $('#btn-request_material').show()
            $('#form-material_request-by-instructor').show()
            $('#div-request_material-by-instructor').show()
            
            $('#btn-request_material-by-instructor').hide()
            $('#div-material_request').hide()
            $('#form-material_request').hide()
        })
        
        $('#btn-request_material').click(function(){
            $('#btn-request_material-by-instructor').show()
            $('#div-material_request').show()
            $('#form-material_request').show()
            
            $('#btn-request_material').hide()
            $('#form-material_request-by-instructor').hide()
            $('#div-request_material-by-instructor').hide()
        })
        
        // equipos
        $('#btn-request_equipment').hide()
        $('#div-request_equipment-by-instructor').hide()
        $('#form-equipment_request-by-instructor').hide()
        
        $('#btn-request_equipment-by-instructor').click(function(){
            $('#btn-request_equipment').show()
            $('#div-request_equipment-by-instructor').show()
            $('#form-equipment_request-by-instructor').show()
            
            $('#btn-request_equipment-by-instructor').hide()
            $('#form-equipment_request').hide()
            $('#div-equipment_request').hide()
        })
        
        $('#btn-request_equipment').click(function(){
            $('#btn-request_equipment-by-instructor').show()
            $('#form-equipment_request').show()
            $('#div-equipment_request').show()
            
            $('#btn-request_equipment').hide()
            $('#div-request_equipment-by-instructor').hide()
            $('#form-equipment_request-by-instructor').hide()
        })
    })
</script>





























