<?php require_once "../app/views/template.php"; ?>
<!--<div id="contenedor_carga">
  <div id="carga"></div>
</div>-->

<div class="tab-content">
    <!-- HEADER -->
    <div class="tab-pane tabs-animation fade active show" id="tab-content-0" role="tabpanel">
        <div class=" main-card mb-4 p-3 card">
            <div class="row">
                
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 p-3 justify-content-center row">
                    <h5 class="align-self-end card-title text-center font-weight-bold text-primary"><img src="<?php echo URL_SISINV ?>images/logosena.png" style="width:40%;"><br>
                        SENA: METALMECANICA DE MALAMBO</h5>
                </div>
                
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 p-1 pt-3 row align-items-center">
                    <div class="col">
                        <div>
                            <p class="card-title text-center font-weight-bold text-primary">REGISTRO</p>
                            <h6 class="card-title text-center font-weight-bold text-primary">DEVOLUCIÓN DE HERRAMIENTAS
                            </h6>
                            <p class="card-title text-center font-weight-bold text-primary">AMBIENTE: SOFTWARE </p>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 p-3 row align-items-center text-center">
                    <div class="col">
                        <div>
                            <p class="card-title  font-weight-bold text-primary">FECHA DE DEVOLUCIÓN:
                                <script type="text/javascript">
                                    var d = new Date();
                                    document.write(d.getDay(), '/' + d.getMonth(), '/' + d.getFullYear(), '-' + d.getHours(), ':' + d.getMinutes(), '-' + d.getSeconds());
                                </script>
                            </p>

                            <p class="card-title  font-weight-bold text-primary">NOMBRE SOLICITANTE:
                                <?php echo $_SESSION['sesion_active']['nombre'] . " " .  $_SESSION['sesion_active']['p_apellido'] . " " .  $_SESSION['sesion_active']['s_apellido']; ?>
                            </p>
                            <input id="idPersona" type="hidden" value="<?php echo $_SESSION['sesion_active']['cod'] ?>">
                            <h6 class="card-title  font-weight-bold text-primary">JEFE INMEDIATO: ALDO SILVERA</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- FIN HEADER -->
    <br>
    <!-- BODY -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                
                <div class="col-md-11">
                    <br>
                    <h6 class="m-0 font-weight-bold text-primary">LISTADO DE HERAMIENTAS</h6>
                </div>
                
                <div class="col-md-1">
                    <?php if ( $_SESSION['sesion_active']['tipo_usuario'] == 'HERRAMENTERO') : ?>
                        <cite title="Agregar" id='BtnHerramienta'>
                            <center><a class="btn btn-none btn-icon-split" id="BtnHerramienta" data-toggle="modal" data-target="#AgregarHerramienta">
                                <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-cart-plus" viewBox="0 0 16 16">
                                  <path d="M9 5.5a.5.5 0 0 0-1 0V7H6.5a.5.5 0 0 0 0 1H8v1.5a.5.5 0 0 0 1 0V8h1.5a.5.5 0 0 0 0-1H9V5.5z"/>
                                  <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
                                </svg>
                            </a></center>
                        </cite>
                        
                        <cite title="Ag" id='BottonHerramienta'>

                        </cite>
                    <?php endif; ?>
                </div>
                
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <!-- tabla solicitud final -->
                <table class="table table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th >#</th> <!--scope="col"-->
                            <th >Código</th> 
                            <th >Nombre</th> 
                            <th >Descripción</th>
                            <th >Persona</th>
                            <th >Cantidad</th>
                            <th>Acción</th> <!--Nuevo-->
                        </tr>
                    </thead>
                    <tbody id="cards"> <!--items-->
                    </tbody>
                    <!--<tfoot>
                        <tr id="footer">
                            <th scope="row" colspan="5">Carrito de devoluciones vacío - comience hacer una solicitud!</th>
                        </tr>
                    </tfoot>-->
                </table>
                <template id="template-card">
                    <tr class="tr">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><button id= "bottonH" data-toggle="modal" data-target="#AgregarHerramienta" class="btn btn-success">Agregar</button><!-- <i class="fas fa-plus"></i> -->
                        </td>
                    </tr>
                </template>
                <!-- Parte del bloque principal
                <template id="template-footer">
                    <td>
                        <button class="btn btn-danger btn-sm" id="vaciar-carrito">
                            VACIAR TODO
                            <i class="far fa-trash-alt"></i>
                        </button>
                    </td>
                    <td>
                         <button id="enviar" class="btn btn-primary" type="submit" value="">
                             ENVIAR
                             <i class="fas fa-location-arrow"></i>
                         </button>
                    </td>
                </template>


                <template id="template-carrito">
                    <tr>
                        <th scope="row">id</th>
                        <td>Código</td>
                        <td>nombre</td>
                        <td>descripcion</td>
                        <td>cantidad</td>
                    </tr>
                </template>-->

            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid  -->
<!-- MODAL INSERTAR HERRAMIENTA -->
<div class="modal fade" id="AgregarHerramienta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">AGREGAR HERRAMIENTAS</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <div class="row">
                            <div class="col-md-11">
                                <br>
                                <h6 class="m-0 font-weight-bold text-primary">LISTADO DE HERAMIENTA</h6>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <!--<div class="container mt-5">
                            <h1>buscador con indexOf</h1>
                            <input type="text" id="formulario" class="form-control" placeholder="Filtrar">
                            <button class="btn btn-info mb-2" id="boton">Buscar</button>
                            <ul id="resultado">

                            </ul>
                        </div>-->
                        <table class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Código</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Descripción</th>
                                    <th scope="col">Persona</th>
                                    <th scope="col">Cantidad</th>
                                    <!--<th>Acción</th>-->
                                </tr>
                            </thead>
                            <tbody id="items"><!--cards-->

                            </tbody>
                            <!-- Campo Nuevo del Principal -->
                            <tfoot>
                                <tr id="footer">
                                    <th scope="row" colspan="6">Carrito de devoluciones vacío - comience hacer una solicitud!</th>
                                </tr>
                            </tfoot>

                        </table>
                        <!-- Campo Nuevo del Principal -->
                        <template id="template-footer">
                            <td>
                                <button class="btn btn-danger btn-sm" id="vaciar-carrito">
                                    VACIAR TODO
                                    <i class="far fa-trash-alt"></i>
                                </button>
                            </td>
                            <td>
                                <button id="enviar" class="btn btn-primary" type="submit" value="">
                                    ENVIAR
                                    <i class="fas fa-location-arrow"></i>
                                </button>
                            </td>
                        </template>

                        <template id="template-carrito">
                            <tr>
                                <th scope="row">id</th>
                                <td>Código</td>
                                <td>nombre</td>
                                <td>descripcion</td>
                                <td>persona</td>
                                <td>cantidad</td>
                            </tr>
                        </template>
                        <!--Bloque del modal 
                        <template id="template-card">
                            <tr class="tr">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><button class="btn btn-success"><i class="fas fa-plus"></i></button>
                                </td>
                            </tr>
                        </template>-->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
       
        let carrito = {}; //Objeto
        let toolList = []; //Array

        const tr = document.getElementById('tr')
        const cards = document.getElementById('cards')
        const items = document.getElementById('items')
        const footer = document.getElementById('footer')
        //const filtro = document.getElementById('filtro')
        //const templateFiltro = document.getElementById('template-filtrar')
        const templateCard = document.getElementById('template-card').content
        const templateCarrito = document.getElementById('template-carrito').content
        const templateFooter = document.getElementById('template-footer').content
        const fragment = document.createDocumentFragment()
        //console.log(items)
        //console.log(footer)
        //console.log(filtro)
        //console.log(templateFiltro)
        //console.log(templateCard)
        //console.log(templateCarrito)
        //console.log(templateFooter)
        //console.log(fragment)
        //console.log(cards)
        //console.log(tr)
        
        cards.addEventListener('click', e => {
            addCarrito(e)
        })
        items.addEventListener('click', e => {
            btnAccion(e)
        })
        const listarDev = () => {
            var idPersona = $('#idPersona').val()
            var tipo = 'herramienta'
            
            $.ajax({
                url: '<?php echo URL_SISINV ?>DevolucionHerramientas/devolverHerramienta',
                type: 'POST',
                data: {
                    idPersona: idPersona,
                    tipo: tipo
                }
            }).done((response) => {
                toolList = JSON.parse(response)
                pintarCards(toolList)
                //console.log(toolList)
            })
        }
        listarDev();
        $('#BtnHerramienta').hide();
        for(var h = 0; h < 1; h++){
            var AgreagarBtn = "";
            AgreagarBtn += '<center><a class="btn btn-none btn-icon-split" id="bottonHerramienta" data-toggle="modal" data-target="#AgregarHerramienta"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-cart-plus" viewBox="0 0 16 16"><path d="M9 5.5a.5.5 0 0 0-1 0V7H6.5a.5.5 0 0 0 0 1H8v1.5a.5.5 0 0 0 1 0V8h1.5a.5.5 0 0 0 0-1H9V5.5z"/><path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/></svg></a></center>';
            $("#BottonHerramienta").html(AgreagarBtn);
        }
        
        /*$('#BottonHerramienta').hide();
        $('#BtnHerramienta').click(function() {
            $('#BtnHerramienta').hide();
            $('#BottonHerramienta').show();
            var idPersona = $('#idPersona').val()
            var tipo = 'herramienta'
            
            $.ajax({
                url: '<?php echo URL_SISINV ?>DevolucionHerramientas/devolverHerramienta',
                type: 'POST',
                data: {
                    idPersona: idPersona,
                    tipo: tipo
                }
            }).done((response) => {
                toolList = JSON.parse(response)
                pintarCards(toolList)
                //Carga todos los datos de la tabla solicitudes en el pintarCards()
                //console.log(pintarCards(toolList))
            });

        });*/

        //Pintar herramientas

        const pintarCards = toolList => {
            let contador = 0
            toolList.forEach(herramienta => {
                contador = contador + 1
                templateCard.querySelectorAll('td')[0].textContent = contador
                templateCard.querySelectorAll('td')[1].textContent = herramienta.codigo
                templateCard.querySelectorAll('td')[2].textContent = herramienta.nombre
                templateCard.querySelectorAll('td')[3].textContent = herramienta.descripcion
                templateCard.querySelectorAll('td')[4].textContent = herramienta.persona
                templateCard.querySelectorAll('td')[5].textContent = herramienta.cantidad
                templateCard.querySelector('.btn-success').dataset.identificacion = herramienta.identificacion


                const clone = templateCard.cloneNode(true)
                fragment.appendChild(clone)
                //console.log(clone)
            })
            cards.appendChild(fragment)
        }
        //console.log(pintarCards(toolList))
        // agregar al arrito
        const addCarrito = e => {

            if (e.target.classList.contains('btn-success')) {
                let identificacion = e.target.dataset.identificacion

                let objeto = toolList.find(objeto => objeto.identificacion === identificacion)
                setCarrito(objeto)
            }
            e.stopPropagation()
        }
        //console.log(addCarrito(e))

        const setCarrito = objeto => {
            const herramienta = {
                id2: objeto.id,
                id: objeto.identificacion,
                codigo: objeto.codigo,
                nombre: objeto.nombre,
                descripcion: objeto.descripcion,
                persona: objeto.persona,
                cantidad: objeto.cantidad,
                tipo: 'herramienta'
            }
            carrito[herramienta.id] = {
                ...herramienta
            } // copia de carrito
            pintarCarrito()
            console.log(carrito[herramienta.id])
        }
        //console.log(setCarrito(objeto))

        // mostrar carrito
        const pintarCarrito = () => {

            items.innerHTML = "" // limpiar carrito. es decir que parta en 0
            let contador = 0
            Object.values(carrito).forEach(herramienta => {
                contador++

                templateCarrito.querySelector('th').textContent = contador
                templateCarrito.querySelectorAll('td')[0].textContent = herramienta.codigo
                templateCarrito.querySelectorAll('td')[1].textContent = herramienta.nombre
                templateCarrito.querySelectorAll('td')[2].textContent = herramienta.descripcion
                templateCarrito.querySelectorAll('td')[3].textContent = herramienta.persona
                templateCarrito.querySelectorAll('td')[4].textContent = herramienta.cantidad
                const clone = templateCarrito.cloneNode(true)
                fragment.appendChild(clone)
            })
            items.appendChild(fragment)
            pintarFooter()
            // local storage, la llave del carrito es carrito4
            sessionStorage.setItem('carrito4', JSON.stringify(carrito))

        }
        //console.log(pintarCarrito());

        const pintarFooter = () => {
            footer.innerHTML = "" // limpiar 
            if (Object.keys(carrito).length === 0) {
                footer.innerHTML = `
                    <th scope="row" colspan="5">Carrito de devoluciones vacío - comience hacer una solicitud!</th>
                    `
                return
            }

            const clone = templateFooter.cloneNode(true)
            fragment.appendChild(clone)
            footer.appendChild(fragment)

            // vaciar carrito 
            const btnVaciar = document.getElementById('vaciar-carrito')
            btnVaciar.addEventListener('click', () => {
                Swal.fire({
                    title: '¿Quieres vaciar todo?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, vaciar !'
                }).then((result) => {
                    if (result.isConfirmed) {
                        carrito = {}
                        pintarCarrito() // se ejecutanuevamente
                    }
                })

            })

            document.getElementById("enviar").addEventListener("click", function() {
                var good = false

                for (const property in carrito) {
                    
                    //console.log(`${prop}: ${carrito3[prop]['codigo']}`);
                    var id = `${carrito[property]['id']}`
                    var codigo = `${carrito[property]['codigo']}`
                    var nombre = `${carrito[property]['nombre']}`
                    var descripcion = `${carrito[property]['descripcion']}`
                    var idPersona = $('#idPersona').val()
                    var nombrePersona = $('#nombrePersona').val()
                    
                    // se trae la cantidad de la herramienta
                    $.ajax({
                        url: '<?php echo URL_SISINV ?>DevolucionHerramientas/CantidadHerramienta',
                        type: 'POST',
                        data: {
                            id
                        }
                    }).done(response => {
                        devolucionHerramienta(response)
                    })

                    function devolucionHerramienta(response) {
                        //console.log(response)
                        var id = `${carrito[property]['id']}`
                        var id2 = `${carrito[property]['id2']}`
                        var cantidad = `${carrito[property]['cantidad']}`
                        var tipo = `${carrito[property]['tipo']}`

                        const data = JSON.parse(response)
                        var cantidad2 = data['tbl_herramienta_CANTIDAD']
                        //console.log(cantidad2)
                        $.ajax({
                            url: '<?php echo URL_SISINV ?>DevolucionHerramientas/DevolucionHerramientas',
                            type: 'POST',
                            data: {
                                id: id,
                                id2: id2,
                                idPersona: idPersona,
                                cantidad: cantidad,
                                cantidad2: cantidad2,
                                tipo: tipo
                            }
                        }).done(function() {
                            sessionStorage.removeItem('carrito4')
                            good = true
                            setTimeout(function() {
                                window.location.href = '<?php echo URL_SISINV ?>DevolucionHerramientas/devherramienta';
                            }, 3000);

                        }).fail(function() {
                            sessionStorage.removeItem('carrito4')
                            good = false
                            setTimeout(function() {
                                window.location.href = '<?php echo URL_SISINV ?>DevolucionHerramientas/devherramienta';
                            }, 3000);
                        })
                    }


                }
                if (good = true) {

                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Devolución enviada exitosamente',
                        showConfirmButton: false,
                        timer: 3000
                    })

                } else {

                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'No se pudo enviar la devolución',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })

        }


        // obtener datos del localSotage
        if (sessionStorage.getItem('carrito4')) {
            carrito = JSON.parse(sessionStorage.getItem('carrito4'));
            pintarCarrito()
        }
        /*window.onload = function() {
        var contenedor = document.getElementById('contenedor_carga');
        contenedor.style.visibility = 'hidden';
        contenedor.style.opacity = '0';
        }*/
    })
</script>