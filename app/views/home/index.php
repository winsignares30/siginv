<?php require_once "../app/views/template.php"; ?>
<!--<div id="Cargando">Cargando...</div>-->

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800"></h1>
  <?php if($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR' || $_SESSION['sesion_active']['tipo_usuario'] == 'HERRAMENTERO') : ?>
    <a href="<?php echo URL_SISINV; ?>Reporte/ImprimirReportes" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
  <?php endif; ?>
</div>

<?php if($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR' || $_SESSION['sesion_active']['tipo_usuario'] == 'HERRAMENTERO') : ?>
<div class="row">
  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-4 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-1 px-2">
      <div class="card-body">
        <div class="row "> <!-- no-gutters align-items-center -->

          <div class="col-6 p-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase m-2">
              <a href="#" data-toggle="modal" data-target="#modal-herramientas-solicitadas">Solicitudes de Herramienta</a>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div id="totalSoicitudesHerramienta" class="h5 mb-0 m-3 font-weight-bold text-gray-800"></div>
              </div>
              
            </div>
          </div>

          <div class="col-6 p-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase m-1">Disponibles</div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div id="herramientasDisonibles" class="h5 mb-0 m-3 font-weight-bold text-gray-800"></div>
              </div>
            </div>
          </div>


          <div class="col-6 p-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase m-1">Total Herramientas</div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div id="totalHerramientas" class="h5 mb-0 m-3 font-weight-bold text-gray-800"></div>
              </div>
            </div>
          </div>
          
          <div class="col-6 ">
            <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR' || $_SESSION['sesion_active']['tipo_usuario'] == 'HERRAMENTERO') : ?>
              <cite title="Ver" id='VerHerramientas'>
                <center class="">
                    <a id="btnHerramienta" data-toggle="modal" data-target="#AgregarHerramienta">
                      <span class="icon text-white-50">
                        <i class="fas fa-plus">
                          <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="btn btn btn-icon-split" viewBox="0 0 16 16" id="btnHerramienta" data-toggle="modal" data-target="#AgregarHerramienta">
                            <path d="M1 0 0 1l2.2 3.081a1 1 0 0 0 .815.419h.07a1 1 0 0 1 .708.293l2.675 2.675-2.617 2.654A3.003 3.003 0 0 0 0 13a3 3 0 1 0 5.878-.851l2.654-2.617.968.968-.305.914a1 1 0 0 0 .242 1.023l3.356 3.356a1 1 0 0 0 1.414 0l1.586-1.586a1 1 0 0 0 0-1.414l-3.356-3.356a1 1 0 0 0-1.023-.242L10.5 9.5l-.96-.96 2.68-2.643A3.005 3.005 0 0 0 16 3c0-.269-.035-.53-.102-.777l-2.14 2.141L12 4l-.364-1.757L13.777.102a3 3 0 0 0-3.675 3.68L7.462 6.46 4.793 3.793a1 1 0 0 1-.293-.707v-.071a1 1 0 0 0-.419-.814L1 0zm9.646 10.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708zM3 11l.471.242.529.026.287.445.445.287.026.529L5 13l-.242.471-.026.529-.445.287-.287.445-.529.026L3 15l-.471-.242L2 14.732l-.287-.445L1.268 14l-.026-.529L1 13l.242-.471.026-.529.445-.287.287-.445.529-.026L3 11z" />
                          </svg>
                        </i>
                      </span>
                    </a>
                </center>
              </cite>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-4 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2 px-2">
      <div class="card-body">
        <div class="row "> 
        
          <div class="col-6">
            <div class="text-xs font-weight-bold text-success text-uppercase  m-2">
              <a href="#" data-toggle="modal" data-target="#modal-materiales-solicitados">Solicitudes de Materiales</a>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div id="totalSolicitudesMateriales" class="h5 mb-0 m-3 font-weight-bold text-gray-800"></div>
              </div>
            </div>
          </div>

          <div class="col-6 p-2">
            <div class="text-xs font-weight-bold text-success text-uppercase  m-1">Disponible</div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div id="materialesDisponibles" class="h5 mb-0 m-3 font-weight-bold text-gray-800"></div>
              </div>

            </div>
          </div>
          <div class="col-6 p-2">
            <div class="text-xs font-weight-bold text-success text-uppercase m-1">Total Materiales</div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div id="totalMateriales" class="h5 mb-0 m-3 font-weight-bold text-gray-800"></div>
              </div>
            </div>
          </div>
          <div class="col-6 p-2">
            <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR' || $_SESSION['sesion_active']['tipo_usuario'] == 'HERRAMENTERO') : ?>

              <cite title="Ver" id='VerMateriales'>
                <span class="icon text-white-50">
                  <i class="fas fa-plus">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="btn btn btn-icon-split" viewBox="0 0 16 16" id="btnMateriales" data-toggle="modal" data-target="#AgregarMateriales">
                      <path d="M4.58 1a1 1 0 0 0-.868.504l-3.428 6a1 1 0 0 0 0 .992l3.428 6A1 1 0 0 0 4.58 15h6.84a1 1 0 0 0 .868-.504l3.429-6a1 1 0 0 0 0-.992l-3.429-6A1 1 0 0 0 11.42 1H4.58zm5.018 9.696a3 3 0 1 1-3-5.196 3 3 0 0 1 3 5.196z" />
                    </svg>
                  </i>
                </span>
              </cite>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-4 col-md-12 mb-4">
    <div class="card border-left-info shadow h-100 py-2 px-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
            
          <div class="col-6 p-2">
            <div class="text-xs font-weight-bold text-info text-uppercase m-2"><a href="#" data-toggle="modal" data-target="#modal-equipos-solicitados">Solicitudes de equipos </a></div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div id="totalSolicitudesEquipos" class="h5 mb-0 m-3 font-weight-bold text-gray-800"></div>
              </div>
            </div>
          </div>

          <div class="col-6 p-2">
            <div class="text-xs font-weight-bold text-info text-uppercase m-1">Disponible</div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div id="equiposDisponibles" class="h5 mb-0 m-3 font-weight-bold text-gray-800"></div>
              </div>
            </div>
          </div>
          <div class="col-6 p-2">
            <div class="text-xs font-weight-bold text-info text-uppercase m-1">Total Equipos</div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div id="totalEquipos" class="h5 mb-0 m-3 font-weight-bold text-gray-800"></div>
              </div>
            </div>
          </div>
          <div class="col-6">
            <cite title="Ver" id='VerEquipos'>
              <!--  <a class="btn btn-primary btn-icon-split" id="" data-toggle="modal" data-target="#AgregarHerramienta2" xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-tools" viewBox="0 0 16 16>
                     <a class="btn btn-success btn-icon-split" id="btnHerramienta" data-toggle="modal" data-target="#AgregarHerramienta"> -->
              <span class="icon text-white-50">
                <i class="fas fa-plus">
                  <!--     <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="btn btn btn-icon-split" viewBox="0 0 16 16" id="btnEquipos" data-toggle="modal" data-target="#AgregarEquipos">
                                   <path d="M4.58 1a1 1 0 0 0-.868.504l-3.428 6a1 1 0 0 0 0 .992l3.428 6A1 1 0 0 0 4.58 15h6.84a1 1 0 0 0 .868-.504l3.429-6a1 1 0 0 0 0-.992l-3.429-6A1 1 0 0 0 11.42 1H4.58zm5.018 9.696a3 3 0 1 1-3-5.196 3 3 0 0 1 3 5.196z" />
                                </svg> -->
                </i>
              </span>
              <!-- </a> -->
            </cite>
            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-pc-display-horizontal" viewBox="0 0 16 16" id="btnEquipos" data-toggle="modal" data-target="#AgregarEquipos">
              <path d="M1.5 0A1.5 1.5 0 0 0 0 1.5v7A1.5 1.5 0 0 0 1.5 10H6v1H1a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1h-5v-1h4.5A1.5 1.5 0 0 0 16 8.5v-7A1.5 1.5 0 0 0 14.5 0h-13Zm0 1h13a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .5-.5ZM12 12.5a.5.5 0 1 1 1 0 .5.5 0 0 1-1 0Zm2 0a.5.5 0 1 1 1 0 .5.5 0 0 1-1 0ZM1.5 12h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1 0-1ZM1 14.25a.25.25 0 0 1 .25-.25h5.5a.25.25 0 1 1 0 .5h-5.5a.25.25 0 0 1-.25-.25Z" />
            </svg>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Pending Requests Card Example 
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-warning shadow h-100 py-2 px-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-warning text-uppercase mb-2">Pending Requests</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">18</div>
          </div>
          <div class="col-auto">
            <i class="fas fa-comments fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>-->
</div>

<!---INSTRUTOR-->
<?php elseif($_SESSION['sesion_active']['tipo_usuario'] == 'INSTRUCTOR') :?>
<div class="row">
     <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-4 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2 px-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">

          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-primary text-uppercase mb-2">
              <a>Solicitudes de Herramienta</a>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div id="div-mount-tools_requested-instructor" class="h5 mb-0 mr-3 font-weight-bold text-gray-800"></div>
              </div>
            </div>
          </div>
          <div class="col-auto">
            <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'INSTRUCTOR') : ?>
              <cite title="Ver" id=''>
                  <span class="icon text-white-50">
                    <i class="fas fa-plus">
                      <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="btn btn btn-icon-split" viewBox="0 0 16 16" id="" data-toggle="modal" data-target="#verHerramientasInstructor">
                        <path d="M1 0 0 1l2.2 3.081a1 1 0 0 0 .815.419h.07a1 1 0 0 1 .708.293l2.675 2.675-2.617 2.654A3.003 3.003 0 0 0 0 13a3 3 0 1 0 5.878-.851l2.654-2.617.968.968-.305.914a1 1 0 0 0 .242 1.023l3.356 3.356a1 1 0 0 0 1.414 0l1.586-1.586a1 1 0 0 0 0-1.414l-3.356-3.356a1 1 0 0 0-1.023-.242L10.5 9.5l-.96-.96 2.68-2.643A3.005 3.005 0 0 0 16 3c0-.269-.035-.53-.102-.777l-2.14 2.141L12 4l-.364-1.757L13.777.102a3 3 0 0 0-3.675 3.68L7.462 6.46 4.793 3.793a1 1 0 0 1-.293-.707v-.071a1 1 0 0 0-.419-.814L1 0zm9.646 10.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708zM3 11l.471.242.529.026.287.445.445.287.026.529L5 13l-.242.471-.026.529-.445.287-.287.445-.529.026L3 15l-.471-.242L2 14.732l-.287-.445L1.268 14l-.026-.529L1 13l.242-.471.026-.529.445-.287.287-.445.529-.026L3 11z" />
                      </svg>
                    </i>
                  </span>
              </cite>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-4 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2 px-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-success text-uppercase mb-2">
              <a>Solicitudes de Materiales</a>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div id="div-amount-materials_requested-instructor" class="h5 mb-0 mr-3 font-weight-bold text-gray-800"></div>
              </div>
            </div>
          </div>
          
          <div class="col-auto">
            <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'INSTRUCTOR') : ?>

              <cite title="Ver" id='VerMateriales'>
                <span class="icon text-white-50">
                  <i class="fas fa-plus">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="btn btn btn-icon-split" viewBox="0 0 16 16" id="" data-toggle="modal" data-target="#verMaterialesInstructor">
                      <path d="M4.58 1a1 1 0 0 0-.868.504l-3.428 6a1 1 0 0 0 0 .992l3.428 6A1 1 0 0 0 4.58 15h6.84a1 1 0 0 0 .868-.504l3.429-6a1 1 0 0 0 0-.992l-3.429-6A1 1 0 0 0 11.42 1H4.58zm5.018 9.696a3 3 0 1 1-3-5.196 3 3 0 0 1 3 5.196z" />
                    </svg>
                  </i>
                </span>
              </cite>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-4 col-md-12 mb-4">
    <div class="card border-left-info shadow h-100 py-2 px-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-info text-uppercase mb-2"><a>Solicitudes de equipos </a></div>
            <div class="row no-gutters align-items-center">
              <div class="col-auto">
                <div id="div-amount-equipments_requested-instructor" class="h5 mb-0 mr-3 font-weight-bold text-gray-800"></div>
              </div>
            </div>
          </div>
          <div class="col-auto">
            <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'INSTRUCTOR') : ?>
              <cite title="Ver" id='VerMateriales'>
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-pc-display-horizontal" viewBox="0 0 16 16" id="" data-toggle="modal" data-target="#verEquiposInstructor">
                        <path d="M1.5 0A1.5 1.5 0 0 0 0 1.5v7A1.5 1.5 0 0 0 1.5 10H6v1H1a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1h-5v-1h4.5A1.5 1.5 0 0 0 16 8.5v-7A1.5 1.5 0 0 0 14.5 0h-13Zm0 1h13a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .5-.5ZM12 12.5a.5.5 0 1 1 1 0 .5.5 0 0 1-1 0Zm2 0a.5.5 0 1 1 1 0 .5.5 0 0 1-1 0ZM1.5 12h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1 0-1ZM1 14.25a.25.25 0 0 1 .25-.25h5.5a.25.25 0 1 1 0 .5h-5.5a.25.25 0 0 1-.25-.25Z" />
                    </svg>
              </cite>
            <?php endif; ?>
          </div>
         
        </div>
      </div>
    </div>
  </div>

  <!-- Pending Requests Card Example 
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-warning shadow h-100 py-2 px-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="text-xs font-weight-bold text-warning text-uppercase mb-2">Pending Requests</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">18</div>
          </div>
          <div class="col-auto">
            <i class="fas fa-comments fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>-->
</div>
<!--FIN INSTRUCTOR -->
<?php endif;?>
<!-- Content Row -->




<!-- Content Row -->

<div class="row">
    <!-- Area Chart -->
      <div class="col-xl-8 col-lg-7">
        <div class="card shadow mb-4">
          <!-- Card Header - Dropdown -->
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Earnings Overview</h6>
            <div class="dropdown no-arrow">
              <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                <div class="dropdown-header">Dropdown Header:</div>
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </div>
          </div>
          <!-- Card Body --->
          <div class="card-body">
            <div class="chart-area">
                
                <!--<canvas id="myChart" width="400" height="135" class="chartjs-render-monitor"></canvas>-->
                        
                <canvas id="myAreaChart"></canvas>
              
            </div>
          </div>
        </div>
      </div>
  <!-- Area Chart --
  <div class="col-xl-8 col-lg-7">
    <div class="card shadow mb-4">
      <!-- Card Header - Dropdown --
      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Earnings Overview</h6>
        <div class="dropdown no-arrow">
          <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
            <div class="dropdown-header">Dropdown Header:</div>
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </div>
      </div>
      <!-- Card Body ---
      <div class="card-body" style="">
        <div class="chart-area" max-width="100%" max-height="90%">
            
            <!--<canvas id="myChart" width="400" height="135" class="chartjs-render-monitor"></canvas>--
            <canvas id="MyAreaChart" class="w-100" height="270px"></canvas><!--myAreaChart-->
            <!--<div class="row" style="border-radius: 50rem; padding: 2px; position: relative;">
                
                <div class="col-12">
                    <canvas id="myAreaChart" class="chart-area"></canvas>
                </div>
            </div>--
        </div>
      </div>
    </div>
  </div>

  <!-- Area Chart Herramientas 
  <div class="col-xl-8 col-lg-7">
    <div class="card shadow mb-4">
      <!-- Card Header - Dropdown -->
  <!--<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Herramientas</h6>
        <div class="dropdown no-arrow">
              <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                <div class="dropdown-header">Dropdown Header:</div>
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </div>
         </div>
      <!-- Card Body -->
  <!--<div class="card-body">
        <div class="chart-area">

          <canvas id="MyAreaChart" width="280" height="120"></canvas>

        </div>
      </div>
    </div>
  </div>-->

  <!-- Pie Chart -->
  <div class="col-xl-4 col-lg-5">
    <div class="card shadow mb-4">
      <!-- Card Header - Dropdown -->
      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Revenue Sources</h6>
        <div class="dropdown no-arrow">
          <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
            <div class="dropdown-header">Dropdown Header:</div>
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </div>
      </div>
      <!-- Card Body -->
      <div class="card-body">
        <div class="chart-pie pt-4 pb-2">
          <canvas id="myPieChart"></canvas>
        </div>
        <div class="mt-4 text-center small">
          <span class="mr-2">
            <i class="fas fa-circle text-primary"></i> Direct
          </span>
          <span class="mr-2">
            <i class="fas fa-circle text-success"></i> Social
          </span>
          <span class="mr-2">
            <i class="fas fa-circle text-info"></i> Referral
          </span>
        </div>
      </div>
    </div>
  </div>
</div>
<!--
<div class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="card ">
        <div class="card-header">
          <h5 class="card-title" style="color:black"><strong>Herramientas solicitadas</strong></h5>
        </div>
        <div class="card-body ">
          <canvas height="250" id="chartsAutoreporteAprendices" width="400"></canvas>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card ">
        <div class="card-header">
          <h5 class="card-title" style="color:black"><strong>Materiales solicitados</strong></h5>
        </div>
        <div class="card-body">
          <canvas height="250" id="chartsAutoreporteInstructor" width="400"></canvas>
        </div>
      </div>
    </div>
  </div>

</div>
<div class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="card ">
        <div class="card-header">
          <h5 class="card-title" style="color:black"><strong>Equipos solicitados</strong></h5>
        </div>
        <div class="card-body ">
          <canvas height="250" id="chartsAutoreporteAprendices" width="400"></canvas>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card ">
        <div class="card-header">
          <h5 class="card-title" style="color:black"><strong>Otros datos</strong></h5>
        </div>

        <div class="card-body">
          <canvas height="250" id="chartsAutoreporteInstructor" width="400"></canvas>
        </div>
      </div>
    </div>
  </div>

</div>-->

<!--CREANDO MODALES-->

<!-- MODAL LISTADO DE HERRAMIENTAS DISPONIBLES-->
<div class="modal fade" id="AgregarHerramienta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">HERRAMIENTAS</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class=" col-lg-12">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <div class="row">
                <div class="col-md-11">
                  <br>
                  <h6 class="m-0 font-weight-bold text-primary">LISTADO DE HERRAMIENTAS DISPONIBLES</h6>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" id="toolsAvailableTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Código</th>
                      <th>Nombre</th>
                      <th>Descripción</th>
                      <th>Stock</th>
                      <th>Fecha</th>
                      <th>Rubro</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $contador = 1;
                    foreach ($datos['toolsAvailiable'] as $toolAvailiable) : ?>
                      <tr>
                        <th scope="row"><?php echo $contador++; ?></th>
                        <td><?php echo $toolAvailiable->tbl_herramienta_CODIGO; ?></td>
                        <td><?php echo $toolAvailiable->tbl_herramienta_NOMBRE; ?></td>
                        <td><?php echo $toolAvailiable->tbl_herramienta_DESCRIPCION; ?></td>
                        <td><?php echo $toolAvailiable->tbl_herramienta_CANTIDAD; ?></td> <!--cantidad-->
                        <td><?php echo $toolAvailiable->tbl_herramienta_FECHA; ?></td>
                        <td><?php echo $toolAvailiable->tbl_herramienta_RUBRO; ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>

                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
      </div>
    </div>
  </div>
</div>

<!-- MODAL HERRAMIENTAS SOLICITADAS-->
<div class="modal fade modal-herramientas-solicitadas" id="modal-herramientas-solicitadas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">HERRAMIENTAS</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class=" col-lg-12">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <div class="row">
                <div class="col-md-11">
                  <br>
                  <h6 class="m-0 font-weight-bold text-primary">LISTADO DE HERRAMIENTAS SOLICITADAS</h6>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" id="toolsRequestedTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Id Solicitud</th>
                      <th>Código</th>
                      <th>Nombre</th>
                      <th>Descripción</th>
                      <th>Cantidad</th>
                      <th>Fecha</th>
                      <th>Tipo</th>
                      <th>Persona</th>
                      <th>Ficha</th>
                      <th>Rubro</th>
                      <th>Estado</th>
                      <th>Rechazo</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $contador = 1;
                    foreach ($datos['toolsRequested'] as $toolsRequested) : ?>
                      <tr>
                        <th scope="row"><?php echo $contador++; ?></th>
                        <td><?php echo $toolsRequested->id_solicitud; ?></td>
                        <td><?php echo $toolsRequested->codigo; ?></td>
                        <td><?php echo $toolsRequested->nombre; ?></td>
                        <td><?php echo $toolsRequested->descripcion; ?></td>
                        <td><?php echo $toolsRequested->cantidad; ?></td>
                        <td><?php echo $toolsRequested->fecha; ?></td>
                        <td><?php echo $toolsRequested->tipo; ?></td>
                        <td><?php echo $toolsRequested->persona; ?></td>
                        <td><?php echo $toolsRequested->ficha; ?></td>
                        <td><?php echo $toolsRequested->rubro; ?></td>
                        <?php if($toolsRequested->estado == 0):?>
                              <td><?php echo 'Rechazado'; ?></td>
                        <?php elseif($toolsRequested->estado == 2):?>
                              <td><?php echo 'Ingresado'; ?></td>
                        <?php elseif($toolsRequested->estado == 3):?>
                              <td><?php echo 'Entregado'; ?></td>
                        <?php elseif($toolsRequested->estado == 4):?>
                              <td><?php echo 'Devuelta'; ?></td>
                        <?php endif;?>
                        <!--<td><?php //echo $toolsRequested->motivoRechazo; ?></td>-->
                        <?php if($toolsRequested->motivoRechazo == ""):?>
                            <td><?php echo "No se ha rechazado"; ?></td>
                        <?php else:?>
                            <td><?php echo $toolsRequested->motivoRechazo; ?></td>
                        <?php endif;?>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>

                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
      </div>
    </div>
  </div>
</div>


<!-- MODAL LISTADO DE MATERIALES DISPONIBLES-->

<div class="modal fade" id="AgregarMateriales" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">MATERIALES</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class=" col-lg-12">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <div class="row">
                <div class="col-md-11">
                  <br>
                  <h6 class="m-0 font-weight-bold text-primary">LISTADO DE MATERIALES DISPONIBLES</h6>
                </div>
              </div>

            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" id="materialsAvailableTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Código UNSPSC</th>
                      <th>Descripción</th>
                      <th>Stock</th>
                      <th>Fecha</th>
                      <th>Rubro</th>
                      <th>Tipo</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $contador = 1;
                    foreach ($datos['materialsAvailable'] as $materialAvailable) : ?>
                      <tr>
                        <th scope="row"><?php echo $contador++; ?></th>
                        <td><?php echo $materialAvailable->tbl_materiales_UNSPSC; ?></td>
                        <td><?php echo $materialAvailable->tbl_materiales_DESCRIPCION; ?></td>
                        <td><?php echo $materialAvailable->tbl_materiales_CANTIDAD; ?></td> <!--cantidad-->
                        <td><?php echo $materialAvailable->tbl_materiales_FECHA; ?></td>
                        <td><?php echo $materialAvailable->tbl_materiales_RUBRO; ?></td>
                        <?php if($materialAvailable->tbl_materiales_TIPOMATERIAL == ""):?>
                            <td><?php echo "No tiene tipo"; ?></td>
                        <?php else:?>
                            <td><?php echo $materialAvailable->tbl_materiales_TIPOMATERIAL; ?></td>
                        <?php endif;?>
                        
                      </tr>
                    <?php endforeach; ?>

                  </tbody>

                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
      </div>
    </div>
  </div>
</div>
<!--MODAL MATERIALES SOLICITADOS-->
<div class="modal fade" id="modal-materiales-solicitados" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">MATERIALES</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class=" col-lg-12">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <div class="row">
                <div class="col-md-11">
                  <br>
                  <h6 class="m-0 font-weight-bold text-primary">LISTADO DE MATERIALES SOLICITADOS</h6>
                </div>
              </div>

            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" id="materialsRequestedTable" width="100%" cellspacing="0">
                  <thead>
                     <tr>
                      <th>#</th>
                      <th>Id Solicitud</th>
                      <th>Código</th>
                      <th>Nombre</th>
                      <th>Descripción</th>
                      <th>Cantidad</th>
                      <th>Fecha</th>
                      <th>Tipo</th>
                      <th>Persona</th>
                      <th>Ficha</th>
                      <th>Rubro</th>
                      <th>Estado</th>
                      <th>Rechazo</th>
                    </tr>
                  </thead>
                    <tbody>
                    <?php $contador = 1;
                    foreach ($datos['materialsRequested'] as $materialRequested) : ?>
                      <tr>
                        <th scope="row"><?php echo $contador++; ?></th>
                        <td><?php echo $materialRequested->id_solicitud; ?></td>
                        <td><?php echo $materialRequested->codigo; ?></td>
                        <td><?php echo $materialRequested->nombre; ?></td>
                        <td><?php echo $materialRequested->descripcion; ?></td>
                        <td><?php echo $materialRequested->cantidad; ?></td>
                        <td><?php echo $materialRequested->fecha; ?></td>
                        <td><?php echo $materialRequested->tipo; ?></td>
                        <td><?php echo $materialRequested->persona; ?></td>
                        <td><?php echo $materialRequested->ficha; ?></td>
                        <td><?php echo $materialRequested->rubro; ?></td>
                        <?php if($materialRequested->estado == 0):?>
                              <td><?php echo 'Rechazado'; ?></td>
                        <?php elseif($materialRequested->estado == 1):?>
                              <td><?php echo 'Ingresado'; ?></td>
                        <?php elseif($materialRequested->estado == 2):?>
                            <td><?php echo 'Aprobado'; ?></td>
                        <?php elseif($materialRequested->estado == 3):?>
                              <td><?php echo 'Entregado'; ?></td>
                        <?php elseif($materialRequested->estado == 4):?>
                              <td><?php echo 'Devuelta'; ?></td>
                        <?php endif;?>
                        <!--<td><?php //echo $materialRequested->motivoRechazo; ?></td>-->
                        <?php if($materialRequested->motivoRechazo == ""):?>
                            <td><?php echo "No se ha rechazado"; ?></td>
                        <?php else:?>
                            <td><?php echo $materialRequested->motivoRechazo; ?></td>
                        <?php endif;?>
                      </tr>
                    <?php endforeach; ?>

                  </tbody>

                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
      </div>
    </div>
  </div>
</div>

<!-- MODAL LISTADO DE EQUIPOS DISPONIBLES-->
<div class="modal fade" id="AgregarEquipos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">EQUIPOS</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <div class="row">
              <div class="col-md-11">
                <br>
                <h6 class="m-0 font-weight-bold text-primary">LISTADO DE EQUIPOS DISPONIBLES</h6>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-hover" id="equipmentsAvailableTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Placa</th>
                    <th>Stock</th>
                    <th>Descripción</th>
                    <th>Modelo</th>
                    <th>Valor</th>
                    <th>Tipo</th>
                    <th>Rubro</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $contador = 1;
                  foreach ($datos['equipmentsAvailable'] as $equipmentAvailable) : ?>
                    <tr>
                      <th scope="row"><?php echo $contador++; ?></th>
                      <td><?php echo $equipmentAvailable->tbl_equipo_PLACA; ?></td>
                      <td><?php echo $equipmentAvailable->tbl_equipo_CANTIDAD; ?></td> <!--cantidad-->
                      <td><?php echo $equipmentAvailable->tbl_equipo_DESCRIPCION; ?></td>
                      <td><?php echo $equipmentAvailable->tbl_equipo_MODELO; ?></td>
                      <td><?php echo $equipmentAvailable->tbl_equipo_VALOR_INGRESO; ?></td>
                      <td><?php echo $equipmentAvailable->tbl_equipo_TIPO; ?></td>
                      <td><?php echo $equipmentAvailable->tbl_equipo_RUBRO; ?></td>
                    </tr>
                  <?php endforeach; ?>

                </tbody>

              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
      </div>
    </div>
  </div>
</div>


<!-- MODAL EQUIPOS SOLICITADOS-->
<div class="modal fade" id="modal-equipos-solicitados" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">EQUIPOS</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <div class="row">
              <div class="col-md-11">
                <br>
                <h6 class="m-0 font-weight-bold text-primary">LISTADO DE EQUIPOS SOLICITADOS</h6>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" id="equipmentsRequestedTable" width="100%" cellspacing="0">
                  <thead>
                     <tr>
                      <th>#</th>
                      <th>Id Solicitud</th>
                      <th>Código</th>
                      <th>Nombre</th>
                      <th>Descripción</th>
                      <th>Cantidad</th>
                      <th>Fecha</th>
                      <th>Tipo</th>
                      <th>Persona</th>
                      <th>Ficha</th>
                      <th>Rubro</th>
                      <th>Estado</th>
                      <th>Rechazo</th>
                    </tr>
                  </thead>
                    <tbody>
                    <?php $contador = 1;
                    foreach ($datos['equipmentsRequested'] as $equipmentRequested) : ?>
                      <tr>
                        <th scope="row"><?php echo $contador++; ?></th>
                        <td><?php echo $equipmentRequested->id_solicitud; ?></td>
                        <td><?php echo $equipmentRequested->codigo; ?></td>
                        <td><?php echo $equipmentRequested->nombre; ?></td>
                        <td><?php echo $equipmentRequested->descripcion; ?></td>
                        <td><?php echo $equipmentRequested->cantidad; ?></td>
                        <td><?php echo $equipmentRequested->fecha; ?></td>
                        <td><?php echo $equipmentRequested->tipo; ?></td>
                        <td><?php echo $equipmentRequested->persona; ?></td>
                        <td><?php echo $equipmentRequested->ficha; ?></td>
                        <td><?php echo $equipmentRequested->rubro; ?></td>
                        <?php if($equipmentRequested->estado == 0):?>
                              <td><?php echo 'Rechazado'; ?></td>
                        <?php elseif($equipmentRequested->estado == 2):?>
                              <td><?php echo 'Ingresado'; ?></td>
                        <?php elseif($equipmentRequested->estado == 3):?>
                              <td><?php echo 'Entregado'; ?></td>
                        <?php elseif($equipmentRequested->estado == 4):?>
                              <td><?php echo 'Devuelta'; ?></td>
                        <?php endif;?>
                        <!--<td><?php //echo $equipmentRequested->motivoRechazo; ?></td>-->
                        <?php if($equipmentRequested->motivoRechazo == ""):?>
                            <td><?php echo "No se ha rechazado"; ?></td>
                        <?php else:?>
                            <td><?php echo $equipmentRequested->motivoRechazo; ?></td>
                        <?php endif;?>
                      </tr>
                    <?php endforeach; ?>

                  </tbody>

                </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
      </div>
    </div>
  </div>
</div>
<!--MODALES DE INSTRUCTOR-->
<!-- MODAL HERRAMIENTAS -->
<div class="modal fade" id="verHerramientasInstructor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Herramientas</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class=" col-lg-12">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <div class="row">
                <div class="col-md-11">
                  <br>
                  <h6 class="m-0 font-weight-bold text-primary"> Hola <?php echo $_SESSION['sesion_active']['nombre'] . " " .  $_SESSION['sesion_active']['p_apellido'] . " " .  $_SESSION['sesion_active']['s_apellido']; ?> Aquí podrás ver las herramientas solicitadas</h6>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" id="toolsRequestedInstructorTable" width="100%" cellspacing="0">
                  <thead>
                     <tr>
                      <th>#</th>
                      <th>Id Solicitud</th>
                      <th>Código</th>
                      <th>Nombre</th>
                      <th>Descripción</th>
                      <th>Cantidad</th>
                      <th>Fecha</th>
                      <th>Tipo</th>
                      <th>Persona</th>
                      <th>Ficha</th>
                      <th>Rubro</th>
                      <th>Estado</th>
                      <th>Rechazo</th>
                    </tr>
                  </thead>
                    <tbody>
                    <?php $contador = 1;
                    foreach ($datos['toolsRequestedInstructor'] as $toolRequestedInstructor) : ?>
                      <tr>
                        <th scope="row"><?php echo $contador++; ?></th>
                        <td><?php echo $toolRequestedInstructor->id_solicitud; ?></td>
                        <td><?php echo $toolRequestedInstructor->codigo; ?></td>
                        <td><?php echo $toolRequestedInstructor->nombre; ?></td>
                        <td><?php echo $toolRequestedInstructor->descripcion; ?></td>
                        <td><?php echo $toolRequestedInstructor->cantidad; ?></td>
                        <td><?php echo $toolRequestedInstructor->fecha; ?></td>
                        <td><?php echo $toolRequestedInstructor->tipo; ?></td>
                        <td><?php echo $toolRequestedInstructor->persona; ?></td>
                        <td><?php echo $toolRequestedInstructor->ficha; ?></td>
                        <td><?php echo $toolRequestedInstructor->rubro; ?></td>
                        <?php if($toolRequestedInstructor->estado == 0):?>
                              <td><?php echo 'Rechazado'; ?></td>
                        <?php elseif($toolRequestedInstructor->estado == 2):?>
                              <td><?php echo 'Ingresado'; ?></td>
                        <?php elseif($toolRequestedInstructor->estado == 3):?>
                              <td><?php echo 'Entregado'; ?></td>
                        <?php elseif($toolRequestedInstructor->estado == 4):?>
                              <td><?php echo 'Devuelta'; ?></td>
                        <?php endif;?>
                        <!--<td><?php //echo $toolRequestedInstructor->motivoRechazo; ?></td>-->
                        <?php if($toolRequestedInstructor->motivoRechazo == ""):?>
                            <td><?php echo "No se ha rechazado"; ?></td>
                        <?php else:?>
                            <td><?php echo $toolRequestedInstructor->motivoRechazo; ?></td>
                        <?php endif;?>
                      </tr>
                    <?php endforeach; ?>

                  </tbody>

                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
      </div>
    </div>
  </div>
</div>

<!-- MODAL MATERIALES -->
<div class="modal fade" id="verMaterialesInstructor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Materiales</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class=" col-lg-12">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <div class="row">
                <div class="col-md-11">
                  <br>
                  <h6 class="m-0 font-weight-bold text-primary"> Hola <?php echo $_SESSION['sesion_active']['nombre'] . " " .  $_SESSION['sesion_active']['p_apellido'] . " " .  $_SESSION['sesion_active']['s_apellido']; ?> Aquí podras ver los materiales solicitados</h6>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" id="materialsRequestedInstructorTable" width="100%" cellspacing="0">
                  <thead>
                     <tr>
                      <th>#</th>
                      <th>Id Solicitud</th>
                      <th>Código</th>
                      <th>Nombre</th>
                      <th>Descripción</th>
                      <th>Cantidad</th>
                      <th>Fecha</th>
                      <th>Tipo</th>
                      <th>Persona</th>
                      <th>Ficha</th>
                      <th>Rubro</th>
                      <th>Estado</th>
                      <th>Rechazo</th>
                    </tr>
                  </thead>
                    <tbody>
                    <?php $contador = 1;
                    foreach ($datos['materialsRequestedInstructor'] as $materialRequestedInstructor) : ?>
                      <tr>
                        <th scope="row"><?php echo $contador++; ?></th>
                        <td><?php echo $materialRequestedInstructor->id_solicitud; ?></td>
                        <td><?php echo $materialRequestedInstructor->codigo; ?></td>
                        <td><?php echo $materialRequestedInstructor->nombre; ?></td>
                        <td><?php echo $materialRequestedInstructor->descripcion; ?></td>
                        <td><?php echo $materialRequestedInstructor->cantidad; ?></td>
                        <td><?php echo $materialRequestedInstructor->fecha; ?></td>
                        <td><?php echo $materialRequestedInstructor->tipo; ?></td>
                        <td><?php echo $materialRequestedInstructor->persona; ?></td>
                        <td><?php echo $materialRequestedInstructor->ficha; ?></td>
                        <td><?php echo $materialRequestedInstructor->rubro; ?></td>
                        <?php if($materialRequestedInstructor->estado == 0):?>
                              <td><?php echo 'Rechazado'; ?></td>
                        <?php elseif($materialRequestedInstructor->estado == 1):?>
                              <td><?php echo 'Ingresado'; ?></td>
                        <?php elseif($materialRequestedInstructor->estado == 2):?>
                            <td><?php echo 'Aprobado'; ?></td>
                        <?php elseif($materialRequestedInstructor->estado == 3):?>
                              <td><?php echo 'Entregado'; ?></td>
                        <?php elseif($materialRequestedInstructor->estado == 4):?>
                              <td><?php echo 'Devuelta'; ?></td>
                        <?php endif;?>
                        <!--<td><?php //echo $materialRequestedInstructor->motivoRechazo; ?></td>-->
                        <?php if($materialRequestedInstructor->motivoRechazo == ""):?>
                            <td><?php echo "No se ha rechazado"; ?></td>
                        <?php else:?>
                            <td><?php echo $materialRequestedInstructor->motivoRechazo; ?></td>
                        <?php endif;?>
                      </tr>
                    <?php endforeach; ?>

                  </tbody>

                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
      </div>
    </div>
  </div>
</div>


<!-- MODAL EQUIPOS -->
<div class="modal fade" id="verEquiposInstructor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Equipos</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class=" col-lg-12">
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <div class="row">
                <div class="col-md-11">
                  <br>
                  <h6 class="m-0 font-weight-bold text-primary"> Hola <?php echo $_SESSION['sesion_active']['nombre'] . " " .  $_SESSION['sesion_active']['p_apellido'] . " " .  $_SESSION['sesion_active']['s_apellido']; ?> Aquí podras ver los equipos solicitados</h6>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" id="equipmentsRequestedInstructor" width="100%" cellspacing="0">
                  <thead>
                     <tr>
                      <th>#</th>
                      <th>Id Solicitud</th>
                      <th>Código</th>
                      <th>Nombre</th>
                      <th>Descripción</th>
                      <th>Cantidad</th>
                      <th>Fecha</th>
                      <th>Tipo</th>
                      <th>Persona</th>
                      <th>Ficha</th>
                      <th>Rubro</th>
                      <th>Estado</th>
                      <th>Rechazo</th>
                    </tr>
                  </thead>
                    <tbody>
                    <?php $contador = 1;
                    foreach ($datos['equipmentsRequestedInstructor'] as $equipmentsRequestedInstructor) : ?>
                      <tr>
                        <th scope="row"><?php echo $contador++; ?></th>
                        <td><?php echo $equipmentsRequestedInstructor->id_solicitud; ?></td>
                        <td><?php echo $equipmentsRequestedInstructor->codigo; ?></td>
                        <td><?php echo $equipmentsRequestedInstructor->nombre; ?></td>
                        <td><?php echo $equipmentsRequestedInstructor->descripcion; ?></td>
                        <td><?php echo $equipmentsRequestedInstructor->cantidad; ?></td>
                        <td><?php echo $equipmentsRequestedInstructor->fecha; ?></td>
                        <td><?php echo $equipmentsRequestedInstructor->tipo; ?></td>
                        <td><?php echo $equipmentsRequestedInstructor->persona; ?></td>
                        <td><?php echo $equipmentsRequestedInstructor->ficha; ?></td>
                        <td><?php echo $equipmentsRequestedInstructor->rubro; ?></td>
                        <?php if($equipmentsRequestedInstructor->estado == 0):?>
                              <td><?php echo 'Rechazado'; ?></td>
                        <?php elseif($equipmentsRequestedInstructor->estado == 2):?>
                              <td><?php echo 'Ingresado'; ?></td>
                        <?php elseif($equipmentsRequestedInstructor->estado == 3):?>
                              <td><?php echo 'Entregado'; ?></td>
                        <?php elseif($equipmentsRequestedInstructor->estado == 4):?>
                              <td><?php echo 'Devuelta'; ?></td>
                        <?php endif;?>
                        <!--<td><?php //echo $equipmentsRequestedInstructor->motivoRechazo; ?></td>-->
                        <?php if($equipmentsRequestedInstructor->motivoRechazo == ""):?>
                            <td><?php echo "No se ha rechazado"; ?></td>
                        <?php else:?>
                            <td><?php echo $equipmentsRequestedInstructor->motivoRechazo; ?></td>
                        <?php endif;?>
                      </tr>
                    <?php endforeach; ?>

                  </tbody>

                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
      </div>
    </div>
  </div>
</div>
<!--FIN MODALES DE INSTRUCTOR-->




<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.css">
<script src="//cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>
<script src="https://cdn.jsdelivr.net/combine/npm/chart.js@2.9.4,npm/chart.js@2.9.4/dist/Chart.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
     // Descargar archivos
    $('#toolsAvailableTable').DataTable({        
        language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
			     },
			     "sProcessing":"Procesando...",
            },
        //para usar los botones   
        responsive: "true",
        dom: 'Bfrtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Exportar a Excel',
				className: 'btn btn-success'
			},
			{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},
		]	        
    });
    
    
    $('#equipmentsAvailableTable').DataTable({        
        language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
			     },
			     "sProcessing":"Procesando...",
            },
        //para usar los botones   
        responsive: "true",
        dom: 'Bfrtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Exportar a Excel',
				className: 'btn btn-success'
			},
			
			{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},
		]	        
    });
    
    
    $('#toolsRequestedTable').DataTable({        
        language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
			     },
			     "sProcessing":"Procesando...",
            },
        //para usar los botones   
        responsive: "true",
        dom: 'Bfrtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Exportar a Excel',
				className: 'btn btn-success'
			},
			
			{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},
		]	        
    });
    $('#materialsRequestedTable').DataTable({        
        language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
			     },
			     "sProcessing":"Procesando...",
            },
        //para usar los botones   
        responsive: "true",
        dom: 'Bfrtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Exportar a Excel',
				className: 'btn btn-success'
			},

			{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},
		]	        
    });
    $('#materialsAvailableTable').DataTable({        
        language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
			     },
			     "sProcessing":"Procesando...",
            },
        //para usar los botones   
        responsive: "true",
        dom: 'Bfrtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Exportar a Excel',
				className: 'btn btn-success'
			},
			{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},
		]	        
    });
    $('#equipmentsRequestedTable').DataTable({        
        language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
			     },
			     "sProcessing":"Procesando...",
            },
        //para usar los botones   
        responsive: "true",
        dom: 'Bfrtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Exportar a Excel',
				className: 'btn btn-success'
			},
			{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},
		]	        
    });
    
    
    $('#toolsAvailableTable').DataTable();
    $('#toolsRequestedTable').DataTable()
    $('#materialsRequestedTable').DataTable()
    $('#materialsAvailableTable').DataTable()
    $('#equipmentsRequestedTable').DataTable()
    $('#equipmentsAvailableTable').DataTable();
    
    CantidadHerramientasSolicidatas()

    function CantidadHerramientasSolicidatas() {
      $.ajax({
        url: '<?php echo URL_SISINV ?>Herramienta/totalSolicitudesHerramientas',
        type: 'POST'
      }).done(response => {
        let data = JSON.parse(response)
        let totalSolicitudesHerramientas = data.length
        console.log(totalSolicitudesHerramientas)
        $('#totalSoicitudesHerramienta').append(totalSolicitudesHerramientas)

        // Cantidad herramientas disponibles en la tabla
        $.ajax({
          url: '<?php echo URL_SISINV ?>Herramienta/HerramientasDisponibles',
          type: 'POST'
        }).done(response => {
          var datos = JSON.parse(response)
          var disponible = []
          for (var i = 0; i < datos.length; i++) {
            disponible.push(datos[i].cantidad)
          }
          $('#herramientasDisonibles').append(disponible)
          if(disponible[0] === null){
            $('#herramientasDisonibles').append(0)
          }
        
          $.ajax({
            url: '<?php echo URL_SISINV ?>Herramienta/totalHerramientas',
            type: 'POST'
          }).done(function(response) {
            var data = JSON.parse(response)
      
            var totalHerramientas = []
            for (var i = 0; i < data.length; i++) {
              totalHerramientas.push(data[i].total)
            }
            
            $('#totalHerramientas').append(totalHerramientas)
            if(totalHerramientas[0] === null){
                $('#totalHerramientas').append(0)
            }
          })
        })

      })
    }


    CantidadMaterialesSolicitados()

    function CantidadMaterialesSolicitados() {
      $.ajax({
        url: '<?php echo URL_SISINV ?>Herramienta/totalSolicitudesMateriales',
        type: 'POST'
      }).done(response => {
        let data = JSON.parse(response)
        let totalSolicitudesMateriales = data.length
        $('#totalSolicitudesMateriales').html(totalSolicitudesMateriales)

        $.ajax({
          url: '<?php echo URL_SISINV ?>Herramienta/totalMateriales',
          type: 'POST'
        }).done(response => {
            var datos = JSON.parse(response)
            var totalMateriales = []
            for (var i = 0; i < datos.length; i++) {
                totalMateriales.push(datos[i].total)
            }
            $('#totalMateriales').append(totalMateriales)
            if(totalMateriales[0] === null){
                $('#totalMateriales').append(0)
            }
              
              // materiales disponibles
              
            $.ajax({
                url: '<?php echo URL_SISINV ?>Herramienta/MaterialesDisponibles',
                type: 'POST'
            }).done(response => {
                var materiales = JSON.parse(response)
                let materialesDisponibles =[]
                for(var i = 0; i < materiales.length; i++){
                    materialesDisponibles.push(materiales[i].cantidad)
                }
               
                $('#materialesDisponibles').append(materialesDisponibles)
                if(materialesDisponibles[0] === null){
                    $('#materialesDisponibles').append(0)
                }
          
            })
         
        })
      })


    }

    CantidadEquiposSolicitados()

    function CantidadEquiposSolicitados() {
      $.ajax({
        url: '<?php echo URL_SISINV ?>Herramienta/totalSolicitudesEquipos',
        type: 'POST'
      }).done(response => {
        let data = JSON.parse(response)
        let totalSolicitudesEquipos = data.length
        
        $('#totalSolicitudesEquipos').append(totalSolicitudesEquipos)
        
        // se treaen el total de equipos de la tabla equipos
        $.ajax({
        url: '<?php echo URL_SISINV ?>Herramienta/totalEquipos',
        type: 'POST'
        }).done(response => {
            let datos = JSON.parse(response)
            var totalEquipos = []
            for (var i = 0; i < datos.length; i++) {
                totalEquipos.push(datos[i].total)
            }
            $('#totalEquipos').append(totalEquipos)
            if (totalEquipos[0] === null) {
                $('#totalEquipos').append(0) 
            }
            
            // se treaen los equipos disponibles en la tabla equipo
            $.ajax({
                url: '<?php echo URL_SISINV ?>Herramienta/EquiposDisponibles',
                type: 'POST'
            }).done(response => {
                var equipos = JSON.parse(response)
                var disponibles = []
    
                for (var i = 0; i < equipos.length; i++) {
                  disponibles.push(equipos[i].cantidad)
                }
                
                $('#equiposDisponibles').html(disponibles)
    
                if (disponibles[0] === null) {
                  $('#equiposDisponibles').append(0) 
                }
    
            })
        })
      })

    }


    // Function para charts herramientas

    function CargarDatosGraficosLine() {

      $.ajax({
        url: '<?php echo URL_SISINV ?>Herramienta/TopHerramientasControllers',
        type: 'POST'
      }).done(function(response) {

        var cantidad = [];
        var nombre = [];
        var TopHerramientas = JSON.parse(response);
        for (var i = 0; i < TopHerramientas.length; i++) {
          cantidad.push(TopHerramientas[i].cantidad)
          nombre.push(TopHerramientas[i].nombre)
        }

        //Creamos una constante y almacemos el id escrito en el canvas dentro del div
        const chartsjs = document.getElementById('MyAreaChart');
        const myChart = new Chart(chartsjs, {
          type: 'line',
          data: {
            //Valores que representa cada barra
            labels: [nombre[0], nombre[1], nombre[2], nombre[3]],
            datasets: [{
              label: 'Top Usados',
              data: [cantidad[0], cantidad[1], cantidad[2], cantidad[3]],
              backgroundColor: [
                'rgba(51, 246, 255, 0.6)',
                'rgba(255, 51, 82, 0.2)',
                'rgba(255, 206, 86)',
                'rgba(75, 192, 192)'
              ],
              borderColor: [
                'rgba(51, 246, 255)'
              ],
              borderWidth: 1
            }]
          },
          options: {
            scales: {
              y: {
                beginAtZero: true
              }
            }
          }
        });
        //FIN 
        //START
        /*
        var TotalReporteAprendices = document.getElementById('chartsAutoreporteAprendices').getContext('2d');
        var myChart = new Chart(TotalReporteAprendices, {
            type: 'bar',
            data: {
                labels: ['TOTAL'],
                datasets: [
                    {
                        label: 'Autoreporte',
                        data: countdataAutoreporte,
                        backgroundColor: 'rgb(41, 129, 49)',
                        borderWidth: 2,
                        borderRadius: 5
                    },
                    {
                        label: 'Asistencia',
                        data: countdataAsistencia,
                        backgroundColor: 'rgb(39, 174, 96)',
                        borderWidth: 2,
                        borderRadius: 10
                    },
                    {
                        label: 'Tiempo Real',
                        data: countTiempoReal,
                        backgroundColor: 'rgb(213, 245, 227)',
                        borderWidth: 2,
                        borderRadius: 10
                    }
                ]
            },
            options:{
                responsive: true,
                barRoundness: 1,
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: "rgba(0, 0, 0, 0)",
                        },
                        barThickness : 50
                    }],
                    yAxes: [{
                        gridLines: {
                            color: "rgba(0, 0, 0, 0)",
                        },
                        ticks: {
                            beginAtZero: true,
                            min: 0,
                            fixedStepSize: 20
                        }
                    }]
                },
                animation: {
                    onComplete: () => {
                        delayed = true;
                    },
                    delay: (context) => {
                        let delay = 0;
                        if (context.type === 'data' && context.mode === 'default' && !delayed) {
                          delay = context.dataIndex * 300 + context.datasetIndex * 100;
                        }
                        return delay;
                    },
                }
            }
        });*/

        //Fin
      })
    }

    CargarDatosGraficosLine()
    
    
    // functions of the instructor
    
    $('#toolsRequestedInstructorTable').DataTable({        
        language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
			     },
			     "sProcessing":"Procesando...",
            },
        //para usar los botones   
        responsive: "true",
        dom: 'Bfrtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Exportar a Excel',
				className: 'btn btn-success'
			},
			
			{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},
		]	        
    });
    
    $('#materialsRequestedInstructorTable').DataTable({        
        language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
			     },
			     "sProcessing":"Procesando...",
            },
        //para usar los botones   
        responsive: "true",
        dom: 'Bfrtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Exportar a Excel',
				className: 'btn btn-success'
			},
		
			{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},
		]	        
    });
    
    $('#equipmentsRequestedInstructor').DataTable({        
        language: {
                "lengthMenu": "Mostrar _MENU_ registros",
                "zeroRecords": "No se encontraron resultados",
                "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast":"Último",
                    "sNext":"Siguiente",
                    "sPrevious": "Anterior"
			     },
			     "sProcessing":"Procesando...",
            },
        //para usar los botones   
        responsive: "true",
        dom: 'Bfrtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      '<i class="fas fa-file-excel"></i> ',
				titleAttr: 'Exportar a Excel',
				className: 'btn btn-success'
			},
			
			{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> ',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},
		]	        
    });
    
    getAmountToolsInstructor()
    
    function getAmountToolsInstructor(){
        
        let tools_requested_instructor = document.querySelector('#div-mount-tools_requested-instructor')
        fetch('<?php echo URL_SISINV ?>Home/getAmountToolsInstructor').then(function(response) {
            return response.json()
        }).then(data => {
            if (data[0] === null){
                tools_requested_instructor.innerHTML = `${0}`
            }else{
                tools_requested_instructor.innerHTML = `${data.length}`
            }
        })
        .catch(function(error){
            //console.log('Hubo un error en la consulta fetch:'+ error.message)
        })
    }
    
    
    const getAmountMaterialsInstructor = async () =>{
        let materials_requested_instructor = document.querySelector('#div-amount-materials_requested-instructor')
        try {
            const amountMaterialsInstructor = await fetch('<?php echo URL_SISINV ?>Home/getAmountMaterialsInstructor')
            const amountMaterial = await amountMaterialsInstructor.json()
            if(amountMaterial[0] === null){
                materials_requested_instructor.append(0)
            }else{
                materials_requested_instructor.innerHTML = `${amountMaterial.length}`
            }
            
        }catch (error){
            //console.log(error)
        }
    }
    getAmountMaterialsInstructor()
    
    const getAmountEquipmentsInstructor = async () => {
        let equipments_requested_instructor = document.querySelector('#div-amount-equipments_requested-instructor')
        try {
            let amountEquipmentsInstructor = await fetch('<?php echo URL_SISINV ?>Home/getAmountEquipmentsInstructor')
            let amountEquipment = await amountEquipmentsInstructor.json()
           
            if(amountEquipment[0] === null){
                 equipments_requested_instructor.append(0)
            }else{
                equipments_requested_instructor.append(amountEquipment.length)
            }
        }catch (error){
            //console.log(error)
        }
    }
    
    getAmountEquipmentsInstructor()

  })
</script>
<!--
<script>
const ctxj = document.getElementById('myChart').getContext('2d');
const myChart = new Chart(ctxj, {
    type: 'bar',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});
</script>-->