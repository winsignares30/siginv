<?php require_once "../app/views/template.php"; ?>


<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <h1 class="h3 mb-2 text-gray-800">Listado de Estante</h1>
            <p class="mb-4">
                Registre la cantidad de bodegas que esten asociados a los estantes.
            </p>
        </div>
    </div>
</div>
<div class="tab-content">
    <div class="tab-pane tabs-animation fade active show" id="tab-content-0" role="tabpanel">
        <div class="row">
            <div class="col-md-4">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">REGISTRAR ESTANTE</h5>
                        <label class="" style="font-weight: bold;">
                            SELECCIONA LA REGIONAL:*
                        </label>
                        <select class="form-control" id="EstanteRegional" name="EstanteRegional">
                            <option>--SELECCIONAR--</option>
                            <?php foreach ($datos['ListarRegional'] as $ListarRegional) : ?>
                                <?php if ($ListarRegional) : ?>
                                    <option value="<?php echo $ListarRegional->tbl_regional_ID; ?>"><?php echo $ListarRegional->tbl_regional_NOMBRE; ?></option>
                                <?php else : ?>
                                    <option value="0">-- NO SE ENCONTRARON DATOS --</option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select><br>

                        <label class="" style="font-weight: bold;">
                            SELECCIONA EL CENTRO:*
                        </label>
                        <select class="form-control" id="estanteCentro" name="estanteCentro">

                        </select><br>
                        <label class="" style="font-weight: bold;">
                            SELECCIONA LA SEDE:*
                        </label>
                        <select class="form-control" id="estanteSede" name="estanteSede">
                        </select><br>
                        <form>
                            <div class="row">
                                <div class="position-relative form-group col-md-12">

                                    <label class="" style="font-weight: bold;" id="labelEstante">
                                        SELECCIONA BODEGA:*
                                    </label>
                                    <select class="form-control" id="estanteBodega">
                                    </select><br>
                                    <label class="" style="font-weight: bold;">
                                        NUMERO DE ESTANTE:*
                                    </label>
                                    <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" id="estanteNumero" class="form-control" placeholder="1-14" type="number"><br>
                                    <label class="" style="font-weight: bold;">
                                        DESCRIPCION:*
                                    </label>
                                    <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="estanteDescripcion" placeholder="DESCRICION...." type="text">
                                    <p class="text-muted"><i> Los campos con * son obligatorios</i></p>
                                    <button class="mb-2 mr-2 btn btn-primary col-md-12" type="button" value="REGISTRAR" id="RegistrarEstante">
                                        REGISTRAR
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <div class="row">
                            <div class="col-md-11"><br>
                                <h6 class="m-0 font-weight-bold">ESTANTE</h6>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>REGIONAL</th>
                                        <th>CENTRO</th>
                                        <th>SEDE</th>
                                        <th>BODEGA</th>
                                        <th>NUMERO DE ESTANTE</th>
                                        <th>DESCRIPCION</th>
                                        <th>ACCIONES</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $contador = 1;
                                    foreach ($datos['ListarEstante'] as $ListarEstante) : ?>
                                      <tr>
                                          <th scope="row"> <?php echo $contador++; ?></th>
                                          <td id="gavetaRegional"> <?php echo $ListarEstante->tbl_regional_NOMBRE; ?></td>
                                          <td id="gavetaCentro"><?php echo $ListarEstante->tbl_centro_NOMBRE; ?></td>
                                          <td id="gavetaSede"><?php echo $ListarEstante->tbl_sede_NOMBRE; ?></td>
                                          <td id="gavetaBodega"><?php echo $ListarEstante->tbl_bodega_NOMBRE; ?></td>
                                           <td id="gavetaNuemeroEstante"><?php echo $ListarEstante->tbl_estante_NUMERO; ?></td>
                                          <td id="gavetaEstanteDescripcion"><?php echo $ListarEstante->tbl_estante_DESCRIPCION; ?></td>
                                            <?php  if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR') :
                                            ?>
                                            <td>
                                              <cite title="Editar">
                                                  <a class="btn btn-info btn-icon-split" href="<?php echo URL_SISINV; ?>Estante/ObtenerEstante/<?php echo $ListarEstante->tbl_estante_ID; ?>">
                                                      <span class="icon text-white-50">
                                                          <i class="fas fa-edit"></i>
                                                      </span>
                                                  </a>
                                              </cite>
                                              <cite title="Eliminar">
                                                  <a href="<?php echo URL_SISINV; ?>Estante/EliminarEstante/<?php echo $ListarEstante->tbl_estante_ID; ?>" class="btn btn-danger btn-icon-split">
                                                      <span class="icon text-white-50">
                                                          <i class="fas fa-trash"></i>
                                                      </span>
                                                  </a>
                                              </cite>
                                            </td>
                                            <?php  endif;
                                            ?>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#dataTable').DataTable()
        document.getElementById("RegistrarEstante").addEventListener('click', function() {
            RegistrarEstante();
        });
        
        /*Cambiar regional */
        $('#EstanteRegional').change(function() {
            var regionalID = $('#EstanteRegional').val();
            LoadCentros(regionalID)
        });
        if ($('#EstanteRegional').change(function() {
            }))

        function LoadCentros(regionalID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadCentros',
                type: 'POST',
                data: {
                    regionalID: regionalID
                }
            }).done(function(response) {
                var data = JSON.parse(response);
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='estanteCentro' value='" + data[i].tbl_centro_ID + "'>" + data[i].tbl_centro_NOMBRE + "</option>"
                    }
                    $('#estanteCentro').html(cadena);
                    var centroID = $('#estanteCentro').val();
                    LoadSedes(centroID);
                } else {
                    cadena = "<option value='0'>NO SE ENCONTRARON DATOS </option>";
                    $('#estanteCentro').html(cadena);
                    var centroID = $('#estanteCentro').val();
                    LoadSedes(centroID);
                }
            })
        }
        $('#estanteCentro').change(function() {
            var centroID = $('#estanteCentro').val();
            LoadSedes(centroID);
        })
        // Load sede
        function LoadSedes(centroID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadSedes',
                type: 'POST',
                data: {
                    centroID: centroID
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='estanteSede' value='" + data[i].tbl_sede_ID + "'>" + data[i].tbl_sede_NOMBRE + "</option>";
                    }
                    var centroID = $('#estanteCentro').val()
                    if (centroID == 0) {
                        $('#estanteSede').html('');
                    }
                    $('#estanteSede').html(cadena);
                    var sedeId = $('#estanteSede').val();
                    LoadBodegas(sedeId)
                } else {
                    cadena = "<option id='estanteSede' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#estanteSede').html(cadena); // <- significa poner los datos dentro del input
                    var sedeId = $('#estanteSede').val();
                    LoadBodegas(sedeId)
                }
            })
        }
        $('#estanteSede').change(function() {
            var sedeId = $('#estanteSede').val();
            LoadBodegas(sedeId);
        })
        
        // Load Bodegas
        function LoadBodegas(sedeId) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Estante/LoadBodegas',
                type: 'POST',
                data: {
                    sedeId: sedeId
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='estanteBodega' value='" + data[i].tbl_bodega_ID + "'>" + data[i].tbl_bodega_NOMBRE + "</option>";
                    }
                    var sedeId = $('#estanteSede').val()
                    if (sedeId == 0) {
                        $('#estanteBodega').html('');
                    }
                    $('#estanteBodega').html(cadena);
                } else {
                    cadena = "<option id='estanteBodega' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#estanteBodega').html(cadena); // <- significa poner los datos dentro del input
                }
            })
        }
        function RegistrarEstante() {
            var estanteNumero = $('#estanteNumero').val();
            var estanteDescripcion = $('#estanteDescripcion').val();
            var estanteBodega = $('#estanteBodega').val();
            var estanteExists = false;
             var EstanteRegional = $('#EstanteRegional').val();
            var estanteCentro = $('#estanteCentro').val();
            var estanteSede = $('#estanteSede').val();
            if (estanteNumero == "" || estanteDescripcion == "" || estanteBodega == "" || estanteSede == 0 || estanteCentro == 0 || EstanteRegional == "--SELECCIONAR--") {
                FillData();
            }else {
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Estante/RegistrarEstante',
                    type: 'POST',
                    data: {
                        estanteNumero: estanteNumero,
                        estanteDescripcion: estanteDescripcion,
                        estanteBodega: estanteBodega
                    }
                }).done(function() {
                    Success()
                    // function de tiempo
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Estante/ListarEstante';
                    }, 2000);
                }).fail(function() {
                    error()
                    // function de tiempo
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Estante/ListarEstante';
                    }, 2000);
                })
            }


        }
        window.onload = function() {
        var contenedor = document.getElementById('contenedor_carga');
        contenedor.style.visibility = 'hidden';
        contenedor.style.opacity = '0';
        }

    });
</script>