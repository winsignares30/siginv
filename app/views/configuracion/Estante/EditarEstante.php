<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR REGIONAL-->
<div class="modal fade" id="ModelEstante" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDITAR ESTANTE</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">


                <label class="" style="font-weight: bold;">
                    SELECCIONA LA REGIONAL:*
                </label>
                <select class="form-control" id="EstanteRegional" name="EstanteRegional">
                    <option>--SELECCIONAR--</option>
                    <?php foreach ($datos['ListarRegional'] as $ListarRegional) : ?>
                        <?php if ($ListarRegional) : ?>
                            <option value="<?php echo $ListarRegional->tbl_regional_ID; ?>"><?php echo $ListarRegional->tbl_regional_NOMBRE; ?></option>
                        <?php else : ?>
                            <option value="0">-- NO SE ENCONTRARON DATOS --</option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select><br>

                <label class="" style="font-weight: bold;">
                    SELECCIONA LA CENTRO:*
                </label>
                <select class="form-control" id="estanteCentro" name="estanteCentro">

                </select><br>
                <label class="" style="font-weight: bold;">
                    SELECCIONA LA SEDE:*
                </label>
                <select class="form-control" id="estanteSede" name="estanteSede">
                </select><br>
                <form>
                    <div class="row">
                        <div class="position-relative form-group col-md-12">
                            <label class="" style="font-weight: bold;" id="labelEstante">
                                SELECCIONA BODEGA:*
                            </label>
                            <select class="form-control" id="estanteBodega">
                            </select><br>
                            
                            <input type="hidden" id="idEstante" value="<?php echo $datos['idEstante'] ?>"><br>
                            <label class="" style="font-weight: bold;">
                                NUMERO DE ESTANTE:*
                            </label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" type="text" class="form-control" value="<?php echo $datos['estanteNumero'] ?>" id="estanteNumero"><br>
                            <label class="" style="font-weight: bold;">
                                DESCRIPCION:*
                            </label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" type="text" class="form-control" value="<?php echo $datos['estanteDescripcion'] ?>" id="estanteDescripcion"><br>
                            <button class="btn btn-info btn-round col-md-12" type="button" id="EditarEstante">

                                ACTUALIZAR
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#ModelEstante").modal("show");
        $("#ModelEstante").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Estante/ListarEstante');
        });

        document.getElementById("EditarEstante").addEventListener('click', function() {
            EditarEstante();
        });

        /*Cambiar regional */
        $('#EstanteRegional').change(function() {
            var regionalID = $('#EstanteRegional').val();
            LoadCentros(regionalID)
        });

        function LoadCentros(regionalID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadCentros',
                type: 'POST',
                data: {
                    regionalID: regionalID
                }
            }).done(function(response) {
                var data = JSON.parse(response);
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='estanteCentro' value='" + data[i].tbl_centro_ID + "'>" + data[i].tbl_centro_NOMBRE + "</option>"
                    }
                    $('#estanteCentro').html(cadena);
                    var centroID = $('#estanteCentro').val();
                    LoadSedes(centroID);
                } else {
                    cadena = "<option value='0'>NO SE ENCONTRARON DATOS </option>";
                    $('#estanteCentro').html(cadena);
                    var centroID = $('#estanteCentro').val();
                    LoadSedes(centroID);
                }
            })
        }
        $('#estanteCentro').change(function() {
            var centroID = $('#estanteCentro').val();
            LoadSedes(centroID);
        })
        // Load sede
        function LoadSedes(centroID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadSedes',
                type: 'POST',
                data: {
                    centroID: centroID
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='estanteSede' value='" + data[i].tbl_sede_ID + "'>" + data[i].tbl_sede_NOMBRE + "</option>";
                    }
                    var centroID = $('#estanteCentro').val()
                    if (centroID == 0) {
                        $('#estanteSede').html('');
                    }
                    $('#estanteSede').html(cadena);
                    var sedeId = $('#estanteSede').val();
                    LoadBodegas(sedeId)
                } else {
                    cadena = "<option id='estanteSede' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#estanteSede').html(cadena); // <- significa poner los datos dentro del input
                    var sedeId = $('#estanteSede').val();
                    LoadBodegas(sedeId)
                }
            })
        }
        $('#estanteSede').change(function() {
            var sedeId = $('#estanteSede').val();
            LoadBodegas(sedeId)
        })

        function LoadBodegas(sedeId) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Estante/LoadBodegas',
                type: 'POST',
                data: {
                    sedeId: sedeId
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='estanteBodega' value='" + data[i].tbl_bodega_ID + "'>" + data[i].tbl_bodega_NOMBRE + "</option>";
                    }
                    var sedeId = $('#estanteSede').val()
                    if (sedeId == 0) {
                        $('#estanteBodega').html('');
                    }
                    $('#estanteBodega').html(cadena);
                } else {
                    cadena = "<option id='estanteBodega' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#estanteBodega').html(cadena); // <- significa poner los datos dentro del input
                }
            })
        }

        function EditarEstante() {
            var idEstante = $('#idEstante').val();
            var estanteNumero = $('#estanteNumero').val();
            var estanteDescripcion = $('#estanteDescripcion').val();
            var estanteBodega = $('#estanteBodega').val();
            var EstanteRegional = $('#EstanteRegional').val();
            var estanteCentro = $('#estanteCentro').val();
            var estanteSede = $('#estanteSede').val();
            if (estanteNumero == "" || estanteDescripcion == "" || estanteBodega == "" || estanteSede == 0 || estanteCentro == 0 || EstanteRegional == "--SELECCIONAR--") {
                FillData();
            } else {
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Estante/EditarEstante',
                    type: 'POST',
                    data: {
                        idEstante: idEstante,
                        estanteNumero: estanteNumero,
                        estanteDescripcion: estanteDescripcion,
                        estanteBodega: estanteBodega
                    }
                }).done(function() {
                    Success();
                    // function de tiempo
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Estante/ListarEstante';
                    }, 2000);

                }).fail(function() {
                    error();
                    // function de tiempo
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Estante/ListarEstante';
                    }, 2000);
                })
            }
        }

    });
</script>