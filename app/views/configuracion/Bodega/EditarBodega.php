<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR REGIONAL-->
<div class="modal fade" id="ModelCentro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDITAR BODEGA</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#">
                    <div class="row">
                        <div class="col-md-12 pr-1">
                            <label class="" style="font-weight: bold;">
                                SELECCIONA LA REGIONAL:*
                            </label>
                            <select class="form-control" id="bodegaRegional">
                                <option>--SELECCIONAR--</option>
                                <?php foreach ($datos['ListarRegional'] as $ListarRegional) : ?>
                                    <option value="<?php echo $ListarRegional->tbl_regional_ID ?>"><?php echo $ListarRegional->tbl_regional_NOMBRE ?></option>
                                <?php endforeach; ?>
                            </select> <br>
                            <label class="" style="font-weight: bold;">
                                SELECCIONA EL CENTRO:*
                            </label>
                            <select class="form-control" id="bodegaCentro">
                            </select><br>
                            <label class="" style="font-weight: bold;">
                                SELECCIONA EL SEDE:*
                            </label>
                            <select class="form-control" id="bodegaSede">

                            </select>
                            <div class="form-group">
                                <label>NOMBRE BODEGA:</label>
                                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" type="text" class="form-control" value="<?php echo $datos['bodegaNombre'] ?>" id="bodegaNombre"><br>

                                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" type="hidden" class="form-control" value="<?php echo $datos['idBodega'] ?>" id="idBodega"><br> 
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-info btn-round col-md-12" type="button" id="EditarBodega">ACTUALIZAR</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#ModelCentro").modal("show");
        $("#ModelCentro").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Bodega/ListarBodega');
        });
        document.getElementById('EditarBodega').addEventListener('click', function() {
            EditarBodega()
        });
        /*Cambiar regional */
        $('#bodegaRegional').change(function() {
            var regionalID = $('#bodegaRegional').val();
            LoadCentros(regionalID)
        });
        function LoadCentros(regionalID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadCentros',
                type: 'POST',
                data: {
                    regionalID: regionalID
                }
            }).done(function(response) {
                var data = JSON.parse(response);
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='bodegaCentro' value='" + data[i].tbl_centro_ID + "'>" + data[i].tbl_centro_NOMBRE + "</option>"
                    }
                    $('#bodegaCentro').html(cadena);
                    var centroID = $('#bodegaCentro').val();
                    LoadSedes(centroID);
                } else {
                    cadena = "<option value='0'>NO SE ENCONTRARON DATOS </option>";
                    $('#bodegaCentro').html(cadena);
                    var centroID = $('#bodegaCentro').val();
                    LoadSedes(centroID);
                }
            })
        }
        $('#bodegaCentro').change(function() {
            var centroID = $('#bodegaCentro').val();
            LoadSedes(centroID);
        })

        function LoadSedes(centroID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadSedes',
                type: 'POST',
                data: {
                    centroID: centroID
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='bodegaSede' value='" + data[i].tbl_sede_ID + "'>" + data[i].tbl_sede_NOMBRE + "</option>";
                    }
                    var centroID = $('#ambienteCentro').val()
                    if (centroID == 0) {
                        $('#ambienteSede').html('');
                    }
                    $('#bodegaSede').html(cadena);
                    var sedeID = $('#bodegaSede').val();

                } else {
                    cadena = "<option id='bodegaSede' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#bodegaSede').html(cadena); // <- significa poner los datos dentro del input
                    var sedeID = $('#bodegaSede').val();
                }
            })
        }
        //edit ambiente
        function EditarBodega() {
            var idBodega = $('#idBodega').val();
            var bodegaRegional = $('#bodegaRegional').val();
            var bodegaCentro = $('#bodegaCentro').val();
            var bodegaSede = $('#bodegaSede').val();
            var bodegaNombre = $('#bodegaNombre').val();
            var bodegaExists = false;

            if (bodegaNombre == "" || bodegaCentro == 0 || bodegaSede == "" || bodegaRegional == "--SELECCIONAR--") {
                FillData()
            } else { 
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Bodega/EditarBodega',
                    type: 'POST',
                    data: {
                        idBodega: idBodega,
                        bodegaNombre: bodegaNombre,
                        bodegaSede: bodegaSede
                    }
                }).done(function() {
                    Edit()
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Bodega/ListarBodega';
                    }, 2000);

                }).fail(function() {
                    ErrorEdit()
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Bodega/ListarBodega';
                    }, 2000);

                })
            }
        }
    });
</script>