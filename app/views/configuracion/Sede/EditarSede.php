<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR REGIONAL-->
<div class="modal fade" id="ModelSede" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDITAR SEDE</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="col-md-12 pr-1">
                            <div class="form-group">
                                <input type="hidden" id="idSede" value="<?php echo $datos['idSede'] ?>">
                                <label>Nombre Sede:</label>
                                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" type="text" class="form-control" value="<?php echo $datos['sedeNombre'] ?>" id="sedeNombre" readonly="readonly"><br>
                                <label>Responsable:</label>
                                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" type="text" class="form-control" value="<?php echo $datos['sedeResponsable'] ?>" id="sedeResponsable"><br>
                                <label>Telefono:</label>
                                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" type="text" class="form-control" value="<?php echo $datos['sedeTelefono'] ?>" id="sedeTelefono"><br>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-info btn-round col-md-12" id="EditarSede">ACTUALIZAR</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/core/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#ModelSede").modal("show");
        $("#ModelSede").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Sede/ListarSede');
        });
        document.getElementById("EditarSede").addEventListener('click', function() {
            EditarSede()
        });
        function EditarSede() {
            var idSede = $('#idSede').val();
            var sedeNombre = $('#sedeNombre').val();
            var sedeResponsable = $('#sedeResponsable').val();
            var sedeTelefono = $('#sedeTelefono').val();
            var sedeExists = false
            if (sedeNombre == "" || sedeResponsable == "" || sedeTelefono == "") {
                FillData();
            } else {
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Sede/EditarSede',
                    type: 'POST',
                    data: {
                        idSede: idSede,
                        sedeNombre: sedeNombre,
                        sedeResponsable: sedeResponsable,
                        sedeTelefono: sedeTelefono
                    }
                }).done(function() {
                    Edit();
                    // funcion de tiempo
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Sede/ListarSede';
                    }, 3000);
                }).fail(function() {
                    Error2();
                    // funcion de tiempo
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Sede/ListarSede';
                    }, 3000);
                })
            }
        }
    })
</script>