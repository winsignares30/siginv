<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR REGIONAL-->
<div class="modal fade" id="ModelCentro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDITAR SEDE</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="" method="POST">
                    <div class="row">
                        <div class="col-md-12 pr-1">
                            <label class="" style="font-weight: bold;">
                                SELECCIONA EL CENTRO:*
                            </label>
                            <select class="form-control" id="sedeCentro">
                                <option>--SELECCIONAR--</option>
                                <?php foreach ($datos['ListarCentro'] as $ListarCentro) : ?>
                                    <option value="<?php echo $ListarCentro->tbl_centro_ID ?>"><?php echo $ListarCentro->tbl_centro_NOMBRE ?></option>
                                <?php endforeach; ?>
                            </select> <br>

                            <div class="form-group">
                                <input type="hidden" id="idSede" value="<?php echo $datos['idSede'] ?>">
                                <label>Nombre Sede:</label>
                                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" type="text" class="form-control" value="<?php echo $datos['sedeNombre'] ?>" id="sedeNombre"><br>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-info btn-round col-md-12" id="EditarSede">ACTUALIZAR</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#ModelCentro").modal("show");
        $("#ModelCentro").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Sede/ListarSede');
        });
        document.getElementById("EditarSede").addEventListener('click', function() {
            EditarSede()
        });
        function EditarSede() {
            var sedeNombre = $('#sedeNombre').val()
            var idSede = $('#idSede').val();
            var sedeCentro = $('#sedeCentro').val();
            var sedeExistes = false;
            if (sedeNombre == "" || sedeCentro == "--SELECCIONAR--") {
                FillData();
            } else {
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Sede/CompararSede',
                    type: 'POST',
                    data: {
                        sedeCentro: sedeCentro
                    }
                }).done(function(response) {
                    var data = JSON.parse(response)
                    for (i = 0; i < data.length; i++) {
                        if (data[i].tbl_centro_tbl_centro_ID == sedeCentro && data[i].tbl_sede_NOMBRE == sedeNombre) {
                            Existe();
                            sedeExistes = true;
                            break;
                        } 
                    }
                    if (!sedeExistes) {
                        $.ajax({
                            url: '<?php echo URL_SISINV ?>Sede/EditarSede2',
                            type: 'POST',
                            data: {
                                sedeCentro: sedeCentro,
                                idSede: idSede,
                                sedeNombre: sedeNombre
                            }
                        }).done(function() {
                            Edit();
                            // function de tiempo
                            setTimeout(function() {
                                window.location.href = '<?php echo URL_SISINV ?>Sede/ListarSede';
                            }, 2000);
                        }).fail(function() {
                            ErrorEdit()
                            // function de tiempo
                            setTimeout(function() {
                                window.location.href = '<?php echo URL_SISINV ?>Sede/ListarSede';
                            }, 2000);
                        })
                    }
                });
            }
        }
    })
</script>