<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR REGIONAL-->
<div class="modal fade" id="ModelCentro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDITAR AMBIENTE</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#">
                    <div class="row">
                        <div class="col-md-12 pr-1">
                            <div class="form-group">
                                <label>Nombre Ambiente:</label>
                                <input onkeyup="mayus(this);" type="text" class="form-control" value="<?php echo $datos['ambienteNombre'] ?>" id="ambienteNombre"><br>
                                <input onkeyup="mayus(this);" type="hidden" class="form-control" value="<?php echo $datos['idAmbiente'] ?>" id="idAmbiente"><br>
                            </div>
                            <label>Instructor:</label>
                            <select class="form-control" name="" id="ambienteInstructor">
                                <option>--SELECCIONAR--</option>
                                <?php foreach ($datos['ListarInstructor'] as $ListarInstructor) : ?>
                                    <?php if ($ListarInstructor) : ?>
                                        <option value="<?php echo $ListarInstructor->tbl_persona_ID; ?>"><?php echo $ListarInstructor->tbl_persona_NOMBRES. ' '. $ListarInstructor->tbl_persona_PRIMERAPELLIDO. ' '.$ListarInstructor->tbl_persona_SEGUNDOAPELLIDO; ?></option>
                                    <?php else : ?>
                                        <option value="0">-- NO SE ENCONTRARON DATOS --</option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select><br>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-info btn-round col-md-12" type="button" id="EditarAmbiente">ACTUALIZAR</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#ModelCentro").modal("show");
        $("#ModelCentro").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Ambiente/ListarAmbiente');
        });
        document.getElementById('EditarAmbiente').addEventListener('click', function() {
            EditarAmbiente()
        });
        
        //edit ambiente
        function EditarAmbiente() {
           var ambienteInstructor = $('#ambienteInstructor').val();
            
            var idAmbiente = $('#idAmbiente').val();
            var ambienteNombre = $('#ambienteNombre').val();
            
            if (ambienteNombre == "" || ambienteInstructor == "--SELECCIONAR--") {
                FillData()
            }else{
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Ambiente/EditarAmbiente',
                    type: 'POST',
                    data: {
                    idAmbiente: idAmbiente,
                    ambienteNombre: ambienteNombre,
                    ambienteInstructor: ambienteInstructor,
                    }
                }).done(function() {
                    Edit()
                    setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Ambiente/ListarAmbiente';
                    }, 2000);

                }).fail(function() {
                    ErrorEdit()
                   setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Ambiente/ListarAmbiente';
                    }, 2000);

                })
            }
            
        }
    })
</script>