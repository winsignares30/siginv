<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR REGIONAL-->
<div class="modal fade" id="DeleteGaveta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">ELIMINAR GAVETAS</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="" method="POST">
                    <div class="row">
                        <div class="col-md-12 pr-1">
                            <div class="form-group">
                                <label>NUMERO DE GAVETA:</label>
                                <input onkeyup="mayus(this);" type="text" class="form-control" readonly="readonly" value="<?php echo $datos['gavetaNumero'] ?>" id="gavetaNumero"><br>
                                <label>DESCRIPCION:*:</label>
                                <input onkeyup="mayus(this);" type="text" class="form-control" readonly="readonly" value="<?php echo $datos['gavetaDescripcion'] ?>" id="gavetaDescripcion"><br>
                                <input onkeyup="mayus(this);" type="hidden" class="form-control" readonly="readonly" value="<?php echo $datos['idGaveta'] ?>" id="idGaveta"><br>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-info btn-danger col-md-12" id="EliminarGaveta">ELIMINAR</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#DeleteGaveta").modal("show");
        $("#DeleteGaveta").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Gaveta/ListarGaveta');
        });

        document.getElementById("EliminarGaveta").addEventListener('click', function() {
            EliminarGaveta()
        });

        function EliminarGaveta() {
            var idGaveta = $('#idGaveta').val();
            $.ajax({
                url: '<?php echo URL_SISINV ?>Gaveta/IfGavetaExiste',
                type: 'POST',
                data: {
                    idGaveta: idGaveta
                }
            }).done(function(response) {
                console.log(response);
                var data = JSON.parse(response);
                if (data.length > 0) {
                    IfDataExist();
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Gaveta/ListarGaveta';
                    }, 2000);
                } else {
                    $.ajax({
                        url: '<?php echo URL_SISINV ?>Gaveta/DeleteGaveta',
                        type: 'POST',
                        data: {
                            idGaveta: idGaveta
                        }
                    }).done(function() {
                        Delete();
                        // function de tiempo
                        setTimeout(function() {
                            window.location.href = '<?php echo URL_SISINV ?>Gaveta/ListarGaveta';
                        }, 2000);

                    }).fail(function() {
                        ErrorDelete();
                        // function de tiempo
                        setTimeout(function() {
                            window.location.href = '<?php echo URL_SISINV ?>Gaveta/ListarGaveta';
                        }, 2000);
                    })
                }
            })

        }
    })
</script>