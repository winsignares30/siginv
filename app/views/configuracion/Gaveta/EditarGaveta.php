<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR REGIONAL-->
<div class="modal fade" id="Modelgaveta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDITAR GAVETAS</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <label class="" style="font-weight: bold;">
                    SELECCIONA LA REGIONAL:*
                </label>
                <select class="form-control" id="gavetaRegional" name="gavetaRegional">
                    <option>--SELECCIONAR--</option>
                    <?php foreach ($datos['ListarRegional'] as $ListarRegional) : ?>
                        <?php if ($ListarRegional) : ?>
                            <option value="<?php echo $ListarRegional->tbl_regional_ID; ?>"><?php echo $ListarRegional->tbl_regional_NOMBRE; ?></option>
                        <?php else : ?>
                            <option value="0">-- SELECCIONA LA REGIONAL--</option>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </select><br>
                <label class="" style="font-weight: bold;">
                    SELECCIONA LA CENTRO:*
                </label>
                <select class="form-control" id="gavetaCentro" name="gavetaCentro">

                </select><br>
                <label class="" style="font-weight: bold;">
                    SELECCIONA LA SEDE:*
                </label>
                <select class="form-control" id="gavetaSede" name="gavetaSede">

                </select><br>
                <form>
                    <div class="row">
                        <div class="position-relative form-group col-md-12">
                            <label class="" style="font-weight: bold;">
                                SELECCIONA BODEGA:*
                            </label>
                            <select class="form-control" id="gavetaBodega">
                                
                            </select><br>
                            <label class="" style="font-weight: bold;">
                                ESTANTE:*
                            </label>
                            <select class="form-control" id="gavetaEstante">
                            </select><br>
                            <input type="hidden" id="idGaveta" value="<?php echo $datos['idGaveta'] ?>"><br>
                            <label class="" style="font-weight: bold;">
                                NUMERO DE GAVETA:*
                            </label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" type="number" class="form-control" value="<?php echo $datos['gavetaNumero'] ?>" id="gavetaNumero"><br>
                            <label class="" style="font-weight: bold;">
                                DESCRIPCION GAVETA:*
                            </label>
                            <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" type="text" class="form-control" value="<?php echo $datos['gavetaDescripcion'] ?>" id="gavetaDescripcion"><br>
                            <button class="btn btn-info btn-round col-md-12" type="button" id="EditarGaveta">
                                ACTUALIZAR
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#Modelgaveta").modal("show");
        $("#Modelgaveta").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Gaveta/ListarGaveta');
        });

        document.getElementById("EditarGaveta").addEventListener('click', function() {
            EditarGaveta();
        });

        /*Cambiar regional */
        $('#gavetaRegional').change(function() {
            var regionalID = $('#gavetaRegional').val();
            LoadCentros(regionalID)
        });

        function LoadCentros(regionalID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadCentros',
                type: 'POST',
                data: {
                    regionalID: regionalID
                }
            }).done(function(response) {
                var data = JSON.parse(response);
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='gavetaCentro' value='" + data[i].tbl_centro_ID + "'>" + data[i].tbl_centro_NOMBRE + "</option>"
                    }
                    $('#gavetaCentro').html(cadena);
                    var centroID = $('#gavetaCentro').val();
                    LoadSedes(centroID);
                } else {
                    cadena = "<option value='0'>NO SE ENCONTRARON DATOS </option>";
                    $('#gavetaCentro').html(cadena);
                    var centroID = $('#gavetaCentro').val();
                    LoadSedes(centroID);
                }
            })
        }
        $('#gavetaCentro').change(function() {
            var centroID = $('#gavetaCentro').val();
            LoadSedes(centroID);
        })
        // Load sedes
        function LoadSedes(centroID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadSedes',
                type: 'POST',
                data: {
                    centroID: centroID
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='gavetaSede' value='" + data[i].tbl_sede_ID + "'>" + data[i].tbl_sede_NOMBRE + "</option>";
                    }
                    var centroID = $('#gavetaCentro').val()
                    if (centroID == 0) {
                        $('#gavetaSede').html('');
                    }
                    $('#gavetaSede').html(cadena);
                    var sedeId = $('#gavetaSede').val();
                    LoadBodegas(sedeId);
                } else {
                    cadena = "<option id='gavetaSede' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#gavetaSede').html(cadena); // <- significa poner los datos dentro del input
                    var sedeId = $('#gavetaSede').val();
                    LoadBodegas(sedeId);
                }
            })
        }
        $('#gavetaSede').change(function() {
            var sedeId = $('#gavetaSede').val();
            LoadBodegas(sedeId);
        })

        function LoadBodegas(sedeId) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Estante/LoadBodegas',
                type: 'POST',
                data: {
                    sedeId: sedeId
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='gavetaBodega' value='" + data[i].tbl_bodega_ID + "'>" + data[i].tbl_bodega_NOMBRE + "</option>";
                    }
                    var sedeId = $('#estanteSede').val()
                    if (sedeId == 0) {
                        $('#gavetaBodega').html('');
                    }
                    $('#gavetaBodega').html(cadena);
                    var bodegaID = $('#gavetaBodega').val();
                    LoadEstantes(bodegaID)
                } else {
                    cadena = "<option id='gavetaBodega' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#gavetaBodega').html(cadena); // <- significa poner los datos dentro del input
                    var bodegaID = $('#gavetaBodega').val();
                    LoadEstantes(bodegaID)
                }
            })
        }
        $('#gavetaBodega').change(function() {
            var bodegaID = $('#gavetaBodega').val();
            LoadEstantes(bodegaID)
        })
        // Load Estantes
        function LoadEstantes(bodegaID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Gaveta/LoadEstantes',
                type: 'POST',
                data: {
                    bodegaID: bodegaID
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='gavetaEstante' value='" + data[i].tbl_estante_ID + "'>" + data[i].tbl_estante_DESCRIPCION + "</option>";
                    }
                    var bodegaID = $('#gavetaBodega').val()
                    if (bodegaID == 0) {
                        $('#gavetaEstante').html('');
                    }
                    $('#gavetaEstante').html(cadena);
                    var gavetaEstante = $('#gavetaEstante').val()
                }else{
                    cadena = "<option id='gavetaEstante' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#gavetaEstante').html(cadena);
                }
            })
        }
        function EditarGaveta() {
            var idGaveta = $('#idGaveta').val();
            var gavetaRegional = $('#gavetaRegional').val();
            var gavetaCentro = $('#gavetaCentro').val();
            var gavetaSede = $('#gavetaSede').val();
            var gavetaEstante = $('#gavetaEstante').val();
            var entorno = $('#Entorno').val()
            var gavetaBodega = $('#gavetaBodega').val()
            var gavetaNumero = $('#gavetaNumero').val();
            var gavetaDescripcion = $('#gavetaDescripcion').val();
            if (gavetaRegional == "--SELECCIONAR--" || gavetaCentro == 0 || gavetaSede == 0 || gavetaBodega== 0 || gavetaEstante == 0 || gavetaNumero == "" || gavetaDescripcion == "") {
                FillData()
            } else {
                /*Pasamos a comparar el ambiente */
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Gaveta/EditarGaveta',
                    type: 'POST',
                    data: {
                        idGaveta: idGaveta,
                        gavetaEstante: gavetaEstante,
                        gavetaNumero: gavetaNumero,
                        gavetaDescripcion: gavetaDescripcion
                    }
                }).done(function() {
                    Success();
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Gaveta/ListarGaveta';
                    }, 2000);
                }).fail(function() {
                    error()
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Gaveta/ListarGaveta';
                    }, 2000);
                })
                
            }
        }
    });
</script>