<?php require_once "../app/views/template.php"; ?>
<!--<div id="contenedor_carga">
  <div id="carga"></div>
</div>-->

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <h1 class="h3 mb-2 text-gray-800">Listado de Gavetas</h1>
            <p class="mb-4">
                Registre la cantidad de Gavetas que esten asociados a cada uno de los Estantes.
            </p>
        </div>
    </div>
</div>
<div class="tab-content">
    <div class="tab-pane tabs-animation fade active show" id="tab-content-0" role="tabpanel">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-4">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">REGISTRAR GAVETAS</h5>
                        <label class="" style="font-weight: bold;">
                            SELECCIONA LA REGIONAL:*
                        </label>
                        <select class="form-control" id="gavetaRegional" name="gavetaRegional">
                            <option>--SELECCIONAR--</option>
                            <?php foreach ($datos['ListarReginal'] as $ListarReginal) : ?>
                                <?php if ($ListarReginal) : ?>
                                    <option value="<?php echo $ListarReginal->tbl_regional_ID; ?>"><?php echo $ListarReginal->tbl_regional_NOMBRE; ?></option>
                                <?php else : ?>
                                    <option value="0">-- SELECCIONA LA REGIONAL--</option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select><br>
                        <label class="" style="font-weight: bold;">
                            SELECCIONA LA CENTRO:*
                        </label>
                        <select class="form-control" id="gavetaCentro" name="gavetaCentro">

                        </select><br>
                        <label class="" style="font-weight: bold;">
                            SELECCIONA LA SEDE:*
                        </label>
                        <select class="form-control" id="gavetaSede" name="gavetaSede">

                        </select><br>
                        <form class="" action="<?php echo URL_SISINV; ?>Gaveta/RegistrarGaveta" method="POST">
                            <div class="row">
                                <div class="position-relative form-group col-md-12">
                                    <label class="" style="font-weight: bold;">
                                        SELECCIONA BODEGA:*
                                    </label>
                                    <select class="form-control" id="gavetaBodega">
                                    
                                    </select><br>
                                    <label class="" style="font-weight: bold;">
                                        ESTANTE:*
                                    </label>
                                    <select class="form-control" id="gavetaEstante">
                                    </select>
                                    <label class="" style="font-weight: bold;">
                                        NUMERO DE GAVETA:*
                                    </label>
                                    <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="gavetaNumero" name="gavetaNumero" placeholder="1-6" type="number" required>
                                    <br>
                                    <label class="" style="font-weight: bold;">
                                        DESCRIPCION:*
                                    </label>
                                    <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" id="gavetaDescripcion" placeholder="DESCRICION...." type="text"><br>
                                    <button class="mb-2 mr-2 btn btn-primary col-md-12" id="RegistrarGaveta" type="button" value="REGISTRAR">
                                        REGISTRAR
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-8">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <div class="row">
                            <div class="col-md-11">
                                <br>
                                <h6 class="m-0 font-weight-bold">GAVETAS</h6>
                            </div>
                            <div class="col-md-1">
                                <!-- <cite title="Agregar">
                                        <a  class="btn btn-success btn-icon-split" href="<?php echo URL_SISINV; ?>Gaveta/ListarGaveta">
                                            <span class="icon text-white-50">
                                            <i class="fas fa-plus"></i>
                                            </span>
                                        </a>
                                    </cite> -->
                            </div>
                        </div>
                    </div>
                    <div class="card-body" width="100%">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>NUMERO REGIONAL</th>
                                      <th>NUMERO CENTRO</th>
                                      <th>NUMERO SEDE</th>
                                      <th>NUMERO BODEGA</th>
                                      <th>ESTANTE DESCRIPCIÓN</th>
                                      <th>NUMERO DE ESTANTE</th>
                                      <th>NUMERO DE GAVETA</th>
                                      <th>DESCRIPCION</th>
                                      <th>ACCIONES</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php $contador = 1;
                                  foreach ($datos['ListarGavetas'] as $ListarGavetas) : ?>
                                      <tr>
                                          <th scope="row"> <?php echo $contador++; ?></th>
                                          <td id="gavetaRegional"> <?php echo $ListarGavetas->tbl_regional_NOMBRE; ?></td>
                                          <td id="gavetaCentro"><?php echo $ListarGavetas->tbl_centro_NOMBRE; ?></td>
                                          <td id="gavetaSede"><?php echo $ListarGavetas->tbl_sede_NOMBRE; ?></td>
                                          <td id="gavetaBodega"><?php echo $ListarGavetas->tbl_bodega_NOMBRE; ?></td>
                                          <td id="gavetaEstanteDescripcion"><?php echo $ListarGavetas->tbl_estante_DESCRIPCION; ?></td>
                                          <td id="gavetaNuemeroEstante"><?php echo $ListarGavetas->tbl_estante_NUMERO; ?></td>
                                          <td id="gavetaNumero"><?php echo $ListarGavetas->tbl_gaveta_NUMERO; ?></td>
                                          <td id="gavetaDescripcion"><?php echo $ListarGavetas->tbl_gaveta_DESCRIPCION; ?></td>
    
                                            <?php // if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR') :
                                            ?>
                                            <td>
                                              <cite title="Editar">
                                                  <a class="btn btn-info btn-icon-split" href="<?php echo URL_SISINV; ?>Gaveta/ObtenerGaveta/<?php echo $ListarGavetas->tbl_gaveta_ID; ?>">
                                                      <span class="icon text-white-50">
                                                          <i class="fas fa-edit"></i>
                                                      </span>
                                                  </a>
                                              </cite>
                                              <cite title="Eliminar">
                                                  <a href="<?php echo URL_SISINV; ?>Gaveta/EliminarGaveta/<?php echo $ListarGavetas->tbl_gaveta_ID; ?>" class="btn btn-danger btn-icon-split">
                                                      <span class="icon text-white-50">
                                                          <i class="fas fa-trash"></i>
                                                      </span>
                                                  </a>
                                              </cite>
                                            </td>
                                            <?php // endif;
                                            ?>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        document.getElementById("RegistrarGaveta").addEventListener('click', function() {
            RegistrarGaveta();
        });
        
        /*Cambiar regional */
        $('#gavetaRegional').change(function() {
            var regionalID = $('#gavetaRegional').val();
            LoadCentros(regionalID)
        });

        function LoadCentros(regionalID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadCentros',
                type: 'POST',
                data: {
                    regionalID: regionalID
                }
            }).done(function(response) {
                var data = JSON.parse(response);
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='gavetaCentro' value='" + data[i].tbl_centro_ID + "'>" + data[i].tbl_centro_NOMBRE + "</option>"
                    }
                    $('#gavetaCentro').html(cadena);
                    var centroID = $('#gavetaCentro').val();
                    LoadSedes(centroID);
                } else {
                    cadena = "<option value='0'>NO SE ENCONTRARON DATOS </option>";
                    $('#gavetaCentro').html(cadena);
                    var centroID = $('#gavetaCentro').val();
                    LoadSedes(centroID);
                }
            })
        }
        $('#gavetaCentro').change(function() {
            var centroID = $('#gavetaCentro').val();
            LoadSedes(centroID);
        })
        // Load sedes
        function LoadSedes(centroID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadSedes',
                type: 'POST',
                data: {
                    centroID: centroID
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='gavetaSede' value='" + data[i].tbl_sede_ID + "'>" + data[i].tbl_sede_NOMBRE + "</option>";
                    }
                    var centroID = $('#gavetaCentro').val()
                    if (centroID == 0) {
                        $('#gavetaSede').html('');
                    }
                    $('#gavetaSede').html(cadena);
                    var sedeId = $('#gavetaSede').val();
                    LoadBodegas(sedeId);
                } else {
                    cadena = "<option id='gavetaSede' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#gavetaSede').html(cadena); // <- significa poner los datos dentro del input
                    var sedeId = $('#gavetaSede').val();
                    LoadBodegas(sedeId);
                }
            })
        }
        $('#gavetaSede').change(function() {
            var sedeId = $('#gavetaSede').val();
            LoadBodegas(sedeId);
        })

        function LoadBodegas(sedeId) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Estante/LoadBodegas',
                type: 'POST',
                data: {
                    sedeId: sedeId
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='gavetaBodega' value='" + data[i].tbl_bodega_ID + "'>" + data[i].tbl_bodega_NOMBRE + "</option>";
                    }
                    var sedeId = $('#estanteSede').val()
                    if (sedeId == 0) {
                        $('#gavetaBodega').html('');
                    }
                    $('#gavetaBodega').html(cadena);
                    var bodegaID =  $('#gavetaBodega').val()
                    LoadEstantes(bodegaID);
                } else {
                    cadena = "<option id='gavetaBodega' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#gavetaBodega').html(cadena); // <- significa poner los datos dentro del input
                    var bodegaID =  $('#gavetaBodega').val()
                    LoadEstantes(bodegaID);
                }
            })
        }
        $('#gavetaBodega').change(function() {
            var bodegaID = $('#gavetaBodega').val();
            LoadEstantes(bodegaID)
        })

        // Load Estantes
        function LoadEstantes(bodegaID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Gaveta/LoadEstantes',
                type: 'POST',
                data: {
                    bodegaID: bodegaID
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='gavetaEstante' value='" + data[i].tbl_estante_ID + "'>" + data[i].tbl_estante_DESCRIPCION + "</option>";
                    }
                    var bodegaID = $('#gavetaBodega').val()
                    if (bodegaID == 0) {
                        $('#gavetaEstante').html('');
                    }
                    $('#gavetaEstante').html(cadena);

                }else{
                    cadena = "<option id='gavetaEstante' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#gavetaEstante').html(cadena);
                }
            })
        }

        function RegistrarGaveta() {
            var gavetaRegional = $('#gavetaRegional').val();
            var gavetaCentro = $('#gavetaCentro').val();
            var gavetaSede = $('#gavetaSede').val();
            var gavetaEstante = $('#gavetaEstante').val();
            var gavetaNumero = $('#gavetaNumero').val();
            var gavetaDescripcion = $('#gavetaDescripcion').val();
            var gavetaBodega = $('#gavetaBodega').val();
            var gavetaExists = false;

            if (gavetaRegional == "--SELECCIONAR--" || gavetaCentro == 0 || gavetaSede == 0 || gavetaBodega == 0 || gavetaEstante == ""  || gavetaNumero == "" || gavetaDescripcion == "") {
                FillData()
            } else {
                /*Pasamos a comparar el ambiente */
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Gaveta/CompararGaveta',
                    type: 'POST',
                    data: {
                        gavetaEstante: gavetaEstante
                    }
                }).done(function(response) {
                    console.log(response);
                    var data = JSON.parse(response);
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].tbl_amb_NOMBRE == gavetaDescripcion && data[i].tbl_estante_tbl_estante_ID == gavetaDescripcion) {
                                Existe();
                                gavetaExists = true;
                                break
                            }
                        }
                    }
                    /*si nunca encontro el ambiente  pasa a registrar*/
                    if (!gavetaExists) {
                        $.ajax({
                            url: '<?php echo URL_SISINV ?>Gaveta/RegistrarGaveta',
                            type: 'POST',
                            data: {
                                gavetaEstante: gavetaEstante,
                                gavetaNumero: gavetaNumero,
                                gavetaDescripcion: gavetaDescripcion
                            }
                        }).done(function() {
                            Success();
                            setTimeout(function() {
                                window.location.href = '<?php echo URL_SISINV ?>Gaveta/ListarGaveta';
                             }, 2000); 
                        }).fail(function() {
                            error()
                            setTimeout(function() {
                                window.location.href = '<?php echo URL_SISINV ?>Gaveta/ListarGaveta';
                            }, 2000);
                        });
                    }
                });
            }

            
        }
        window.onload = function() {
        var contenedor = document.getElementById('contenedor_carga');
        contenedor.style.visibility = 'hidden';
        contenedor.style.opacity = '0';
        }
    });
</script>