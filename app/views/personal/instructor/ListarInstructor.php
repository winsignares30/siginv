<?php require_once "../app/views/template.php"; ?>
    <!--<div id="contenedor_carga">
      <div id="carga"></div>
    </div>-->
    
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <h1 class="h3 mb-2 text-gray-800">Listado de Instructores</h1>
                <p class="mb-4">
                    Registre la cantidad de instructor que esten asociados a la sede.
                </p>
            </div>
        </div>
    </div>
    <div class="tab-content">
        <div class="tab-pane tabs-animation fade active show" id="tab-content-0" role="tabpanel">
            <div class="row">
                <div class="col-md-12">
                    <div class="card shadow mb-4 table-responsive">
                        <div class="card-header py-3">
                            <div class="row">
                                <div class="col-md-12"><br>
                                    <h6 class="m-0 font-weight-bold">TABLA DE INFORMACION CON INSTRUCTORES</h6>
                                </div>
                            </div>
                        </div>
                        <div class="card-body mb-12">
                            <table class="table table-bordered table-striped table-hover table-responsive{-sm|-md|-lg|-xl|-xxl}" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>REGIONAL</th>
                                        <th>CENTRO</th>
                                        <th>SEDE</th>
                                        <th>NOMBRES</th>
                                        <th>PRIMER APELLIDO</th>
                                        <th>SEGUNDO APELLIDO</th>
                                        <th>FECHA NACIMIENTO</th>
                                        <th>TIPO DE DOCUMENTO</th>
                                        <th>NUMERO DE DOCUMENTO</th>
                                        <th>TELEFONO</th>
                                        <th>CARGO</th>
                                        <th>DIRECCION DE CORREO</th>
                                        <th>DIRECCION</th>
                                        <th>TIPO CONTRATO</th>
                                        <th>ACCION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $contador = 1;
                                    foreach ($datos['ListarInstructores'] as $ListarInstructor) : ?>
                                        <tr>
                                            <th scope="row" ><?php echo $contador++; ?></th>
                                            <td><?php echo $ListarInstructor->tbl_regional_NOMBRE; ?></td>
                                            <td><?php echo $ListarInstructor->tbl_centro_NOMBRE; ?></td>
                                            <td><?php echo $ListarInstructor->tbl_sede_NOMBRE; ?></td>
                                            <td><?php echo $ListarInstructor->tbl_persona_NOMBRES; ?></td>
                                            <td><?php echo $ListarInstructor->tbl_persona_PRIMERAPELLIDO; ?></td>
                                            <td><?php echo $ListarInstructor->tbl_persona_SEGUNDOAPELLIDO; ?></td>
                                            <td><?php echo $ListarInstructor->tbl_persona_FECHANAC; ?></td>
                                            <td><?php echo $ListarInstructor->tbl_tipodocumento_tbl_tipodocumento_ID; ?></td>
                                            <td><?php echo $ListarInstructor->tbl_persona_NUMDOCUMENTO; ?></td>
                                            <td><?php echo $ListarInstructor->tbl_persona_TELEFONO; ?></td>
                                            <td><?php echo $ListarInstructor->tbl_cargo_TIPO; ?></td>
                                            <td><?php echo $ListarInstructor->tbl_persona_CORREO; ?></td>
                                            <td><?php echo $ListarInstructor->tbl_persona_DIRECCION; ?></td>
                                            <td><?php echo $ListarInstructor->tipo_contrato; ?></td>
                                            <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR') : ?>
                                                <td>
                                                    <cite title="Editar">
                                                        <a href="<?php echo URL_SISINV; ?>Persona/ObtenerInstructor/<?php echo $ListarInstructor->tbl_persona_ID ?>" class="btn btn-info btn-icon-split" id="EditarInstructor">
                                                            <span class="icon text-white-50">
                                                                <i class="fas fa-edit"></i>
                                                            </span>
                                                        </a>
                                                    </cite>
                                                    <cite title="Borrar">
                                                        <a href="<?php echo URL_SISINV; ?>Persona/EliminarInstructor/<?php echo $ListarInstructor->tbl_persona_ID; ?>" class="btn btn-danger btn-icon-split" id="EliminarInstructor">
                                                            <span class="icon text-white-50">
                                                                <i class="fas fa-trash"></i>
                                                            </span>
                                                        </a>
                                                    </cite>
                                                </td>
                                            <?php endif; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        document.getElementById("RegistrarInstructor").addEventListener('click', function() {
            RegistrarInstructor();
        });
        /*Cambiar regional */
        $('#instructorRegional').change(function() {
            var regionalID = $('#instructorRegional').val();
            LoadCentros(regionalID)
        });

        function LoadCentros(regionalID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadCentros',
                type: 'POST',
                data: {
                    regionalID: regionalID
                }
            }).done(function(response) {
                var data = JSON.parse(response);
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='instructorCentro' value='" + data[i].tbl_centro_ID + "'>" + data[i].tbl_centro_NOMBRE + "</option>"
                    }
                    $('#instructorCentro').html(cadena);
                    var centroID = $('#instructorCentro').val();
                    LoadSedes(centroID);
                } else {
                    cadena = "<option value='0'>NO SE ENCONTRARON DATOS </option>";
                    $('#instructorCentro').html(cadena);
                    var centroID = $('#instructorCentro').val();
                    LoadSedes(centroID);
                }
            })
        }
        $('#instructorCentro').change(function() {
            var centroID = $('#instructorCentro').val();
            LoadSedes(centroID);
        })

        function LoadSedes(centroID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadSedes',
                type: 'POST',
                data: {
                    centroID: centroID
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='instructorSedeID' value='" + data[i].tbl_sede_ID + "'>" + data[i].tbl_sede_NOMBRE + "</option>";
                    }
                    var centroID = $('#instructorCentro').val()
                    if (centroID == 0) {
                        $('#instructorSedeID').html('');
                    }
                    $('#instructorSedeID').html(cadena);
                    var sedeID = $('#instructorSedeID').val();
                } else {
                    cadena = "<option id='instructorSedeID' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#instructorSedeID').html(cadena); // <- significa poner los datos dentro del input
                    var sedeID = $('#instructorSedeID').val();

                }
            })
        }

        function RegistrarInstructor() {
            var instructorSedeID = $('#instructorSedeID').val();
            var instructorRegional = $('#instructorRegional').val();
            var instructorCentro = $('#instructorCentro').val();
            var instructorNombre = $('#instructorNombre').val();
            var instructorApellido = $('#instructorApellido').val();
            var instructorTipoDocumento = $('#instructorTipoDocumento').val();
            var instructorNumeroDocumento = $('#instructorNumeroDocumento').val();
            var instructorNumeroTelefono = $('#instructorNumeroTelefono').val();
            var instructorDirecionCorreo = $('#instructorDirecionCorreo').val();
            var instructorDirecion = $('#instructorDirecion').val();
            $.ajax({
                    url: '<?php echo URL_SISINV ?>Instructor/RegistrarInstructor',
                    type: 'POST',
                    data: {
                        instructorSedeID: instructorSedeID,
                        instructorNombre: instructorNombre,
                        instructorApellido: instructorApellido,
                        instructorTipoDocumento: instructorTipoDocumento,
                        instructorNumeroDocumento: instructorNumeroDocumento,
                        instructorNumeroTelefono: instructorNumeroTelefono,
                        instructorDirecionCorreo: instructorDirecionCorreo,
                        instructorDirecion: instructorDirecion
                    }
                }).done(function() {
                    Success();
                    // funcion de tiempo
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Instructor/ListarInstructor';
                    }, 3000);
                }).fail(function() {
                    error();
                    // funcion de tiempo
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Instructor/ListarInstructor';
                    }, 3000);
                })
            if (instructorRegional == "--SELECCIONAR--" || instructorSedeID == "" || instructorSedeID == "0" || instructorCentro =="" || instructorCentro == "0" || instructorNombre == "" || instructorApellido == "" || instructorTipoDocumento == "" || instructorNumeroDocumento == "" || instructorNumeroTelefono == "" || instructorDirecionCorreo == "" || instructorDirecion == "") {
                FillData()
            } else {
               
            }
        }
        /*window.onload = function() {
        var contenedor = document.getElementById('contenedor_carga');
        contenedor.style.visibility = 'hidden';
        contenedor.style.opacity = '0';
        }*/
    });
</script>
