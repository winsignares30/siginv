<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR ESTUDIANTES -->
<div class="modal fade" id="EditarInstructores" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Instructor</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold;">
                               Seleccione Regional:*
                            </label>
                            <div class="form-group">
                                <select class="form-control" name="" id="instructorRegional">
                                    <option>--SELECCIONAR--</option>
                                    <?php foreach ($datos['ListarRegional'] as $ListarRegional) : ?>
                                        <?php if ($ListarRegional) : ?>
                                            <option value="<?php echo $ListarRegional->tbl_regional_ID; ?>"><?php echo $ListarRegional->tbl_regional_NOMBRE; ?></option>
                                        <?php else : ?>
                                            <option value="0">-- NO SE ENCONTRARON DATOS --</option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select><br>
                                
                            </div>
                        </div> 
                        <?php foreach($datos['instructores'] as $instructor) :?>
                            <?php if($instructor):?>
                                <div class="col-md-6">
                                    <label style="font-weight: bold;">
                                       Seleccione Centro:*
                                    </label>
                                    <div class="form-group">
                                        <select class="form-control" id="instructorCentro">
        
                                        </select><br>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label style="font-weight: bold;">
                                       Seleccione Sede:*
                                    </label>
                                    <div class="form-group">
                                        <select class="form-control" id="instructorSede">
        
                                        </select><br>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nombres</label>
                                        <input type="hidden" id="instructorId" value="<?php echo $datos['instructorId'] ?>">
                                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" id="instructorNombre" type="text" class="form-control" value="<?php echo $instructor->tbl_persona_NOMBRES; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Primer Apellido</label>
                                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" id="instructorApellido1" type="text" class="form-control" value="<?php echo $instructor->tbl_persona_PRIMERAPELLIDO ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Segundo Apellido</label>
                                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" id="instructorApellido2" type="text" class="form-control" value="<?php echo $instructor->tbl_persona_SEGUNDOAPELLIDO ?>">
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label>Tipo documento:</label>
                                        <select id="instructorTipoDocumento" value="" class="form-select form-control" aria-label="Default select example">
                                            <option>--SELECCIONAR--</option>
                                            <option value="CEDULA">CÉDULA</option>
                                            <option value="CEDULA EXTRANJERA">CÉDULA EXTRANJERA</option>
                                            <option value="TARJETA IDENTIDAD">TARJETA IDENTIDAD</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Documento</label>
                                        <input id="instructorDocumento" value="<?php echo $instructor->tbl_persona_NUMDOCUMENTO ?>" type="number" pattern="[0-9]{8,10}" oninput="if(this.value.length > this.maxlength) this.value.slice(0, this.maxlength)" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label>Tipo Contrato:</label>
                                        <select id="instructorTipoContrato" value="" class="form-select form-control" aria-label="Default select example">
                                            <option>--SELECCIONAR--</option>
                                            <option value="PLANTA">PLANTA</option>
                                            <option value="CONTRATISTA">CONTRATISTA</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label>Cargo</label>
                                        <select id="instructorCargo" value="" class="form-select form-control" aria-label="Default select example">
                                            <option>--SELECCIONAR--</option>
                                            <option value="3">INSTRUCTOR</option>
                                            <option value="2">HERRAMENTERO</option>
                                            <option value="1">ADMINISTRADOR</option>
                                        </select>
                                    </div>
                                </div>
                    
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Fecha Nacimiento</label>
                                        <input id="instructorFechaNacimiento" type="date" class="form-control" value="<?php echo $instructor->tbl_persona_FECHANAC ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Celular</label>
                                        <input type="number" id="instructorCelular" value="<?php echo $instructor->tbl_persona_TELEFONO ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Correo</label>
                                        <input type="email" id="instructorCorreo" value="<?php echo $instructor->tbl_persona_CORREO ?>" pattern="[0-9]{8,10}" class="form-control">
                                    </div>
                                </div>
                                <!--<div class="col-md-12">
                                    <div class="form-group">
                                        <label>Contraseña</label>
                                        <input onkeyup="mayus(this);" id="instructorPassword" value="" type="text" class="form-control">
                                    </div>
                                </div>-->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Direccion</label>
                                        <input type="text" onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" id="instructorDireccion" value="<?php echo $instructor->tbl_persona_DIRECCION ?>" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <button type="button" id="EditarInstructor" class="btn btn-info btn-round col-md-12">Editar</button>
                                </div>
                            <?php endif;?>
                        <?php endforeach;?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#EditarInstructores").modal("show");
        $("#EditarInstructores").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Persona/ListarInstructor');
        });
        document.getElementById('EditarInstructor').addEventListener('click', function() {
            EditarInstructor()
            //EditarAdministradorUsuario()
        });
        
        /*Cambiar regional */
        $('#instructorRegional').change(function() {
            var regionalID = $('#instructorRegional').val();
            LoadCentros(regionalID);
        });

        function LoadCentros(regionalID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadCentros',
                type: 'POST',
                data: {
                    regionalID: regionalID
                }
            }).done(function(response) {
                var data = JSON.parse(response);
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='instructorCentro' value='" + data[i].tbl_centro_ID + "'>" + data[i].tbl_centro_NOMBRE + "</option>"
                    }
                    $('#instructorCentro').html(cadena);
                    var centroID = $('#instructorCentro').val();
                    LoadSedes(centroID);
                } else {
                    cadena = "<option value='0'>NO SE ENCONTRARON DATOS </option>";
                    $('#instructorCentro').html(cadena);
                    var centroID = $('#instructorCentro').val();
                    LoadSedes(centroID);
                }
            })
        }
        
        $('#instructorCentro').change(function() {
            var centroID = $('#instructorCentro').val();
            LoadSedes(centroID);
        })

        function LoadSedes(centroID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadSedes',
                type: 'POST',
                data: {
                    centroID: centroID
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='instructorSede' value='" + data[i].tbl_sede_ID + "'>" + data[i].tbl_sede_NOMBRE + "</option>";
                    }
                    var centroID = $('#instructorCentro').val()
                    if (centroID == 0) {
                        $('#instructorSede').html('');
                    }
                    $('#instructorSede').html(cadena);
                } else {
                    cadena = "<option id='instructorSede' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#instructorSede').html(cadena); // <- significa poner los datos dentro del input
                }
            })
        }
        
        //Editar Instructores
        
        //$('#EditarInstructor').click(function(){
        function EditarInstructor() {
            
            var instructorRegional = $('#instructorRegional').val();
            var instructorCentro = $('#instructorCentro').val();
            var instructorSede = $('#instructorSede').val();
            var instructorId = $('#instructorId').val().trim();
            var instructorNombre = $('#instructorNombre').val().trim();
            var instructorApellido1 = $('#instructorApellido1').val().trim();
            var instructorApellido2 = $('#instructorApellido2').val().trim();
            var instructorTipoDocumento = $('#instructorTipoDocumento').val().trim();
            var instructorDocumento = $('#instructorDocumento').val().trim();
            var instructorTipoContrato = $('#instructorTipoContrato').val().trim();
            var instructorCargo = $('#instructorCargo').val().trim();
            var instructorFechaNacimiento = $('#instructorFechaNacimiento').val().trim();
            var instructorCelular = $('#instructorCelular').val().trim();
            var instructorCorreo = $('#instructorCorreo').val().trim();
            var instructorDireccion = $('#instructorDireccion').val().trim();
            
            if(instructorRegional == "--SELECCIONAR--" || instructorSede == "" || instructorSede == "0" || instructorCentro =="" || instructorCentro == "0" || instructorId == "" || instructorDocumento == "" || instructorNombre == "" || instructorApellido1 == "" || instructorApellido2 == "" || instructorFechaNacimiento == "" ||  instructorCelular == "" || instructorCorreo == "" || instructorCargo == "" || instructorCargo == "--SELECCIONAR--" || instructorDireccion == "" || instructorTipoContrato == "" || instructorTipoContrato == "--SELECCIONAR--" || instructorTipoDocumento == "" || instructorTipoDocumento == "--SELECCIONAR--"){
                FillData()
            }else{
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Persona/EditarInstructor',
                    type: 'POST',
                    data: {
                        instructorRegional: instructorRegional,
                        instructorCentro: instructorCentro,
                        instructorSede: instructorSede,
                        instructorId: instructorId,
                        instructorNombre: instructorNombre,
                        instructorApellido1: instructorApellido1,
                        instructorApellido2: instructorApellido2,
                        instructorFechaNacimiento: instructorFechaNacimiento,
                        instructorDocumento: instructorDocumento,
                        instructorCelular: instructorCelular,
                        instructorCorreo: instructorCorreo,
                        instructorCargo: instructorCargo,
                        instructorDireccion: instructorDireccion,
                        instructorTipoContrato: instructorTipoContrato,
                        instructorTipoDocumento: instructorTipoDocumento
                    
                    }
                }).done(function() {
                    $.ajax({
                        url: '<?php echo URL_SISINV ?>Persona/EditarInstructorUsuario',
                        type: 'POST',
                        data: {
                            instructorRegional: instructorRegional,
                            instructorSede: instructorSede,
                            instructorCentro: instructorCentro,
                            instructorId: instructorId,
                            instructorCorreo: instructorCorreo
                        
                        }
                    }).done(function() {
                        Edit()
                        setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Persona/ListarInstructor';
                        }, 2000);
    
                    }).fail(function() {
                        /*ErrorEdit()
                       setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Persona/ListarInstructor';
                        }, 2000);*/
    
                    })
                    /*Edit()
                    setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Persona/ListarInstructor';
                    }, 2000);*/

                }).fail(function() {
                    ErrorEdit()
                   setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Persona/ListarInstructor';
                    }, 2000);

                })
                
            }
            
        }
        
        
       
        
    })
</script>