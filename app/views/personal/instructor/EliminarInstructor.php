<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR REGIONAL-->
<div class="modal fade" id="InstructorModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">ELIMINAR INSTRUCTOR</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="" method="POST">
                    <div class="row">
                        <?php foreach($datos['instructores'] as $instructores) : ?>
                            <?php if($instructores) :?>
                                <div class="col-md-12 pr-1">
                                    <input type="hidden" id="instructorId" value="<?php echo $datos['instructorId'] ?>">
                                    
                                    <label class="" style="font-weight: bold;">
                                        NOMBRES:*
                                    </label>
                                    <input onkeyup="mayus(this);" class="form-control" placeholder="NOMBRES" type="text" value="<?php echo $instructores->tbl_persona_NOMBRES ?>" id="instructorNombre" readonly="readonly"><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        APELLIDOS:*
                                    </label>
                                    <input onkeyup="mayus(this);" class="form-control" placeholder="APELLIDOS" type="text" value="<?php echo $instructores->tbl_persona_PRIMERAPELLIDO ?>" id="instructorApellido1" readonly="readonly"><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        NUMERO DE DOCUMENTO:*
                                    </label>
                                    <input onkeyup="mayus(this);" class="form-control" placeholder="NUMERO DE DOCUMENTO" type="text" value="<?php echo $instructores->tbl_persona_NUMDOCUMENTO ?>" id="instructorDocumento" readonly="readonly"><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        NUMERO DE TELEFONO:*
                                    </label>
                                    <input onkeyup="mayus(this);" class="form-control" placeholder="NUMERO DE TELEFONO" type="text" value="<?php echo $instructores->tbl_persona_TELEFONO ?>" id="instructorCelular" readonly="readonly"><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        DIRECION DE CORREO:*
                                    </label>
                                    <input onkeyup="mayus(this);" class="form-control" placeholder="DIRECCION DE CORREO" type="text" value="<?php echo $instructores->tbl_persona_CORREO ?>" id="instructorCorreo" readonly="readonly"><br>
                                    
                                </div>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-info btn-danger col-md-12" id="EliminarInstructor">ELIMINAR</button>
                                </div>
                            <?php endif;?>
                        <?php endforeach;?>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/core/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#InstructorModel").modal("show");
        $("#InstructorModel").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Persona/ListarInstructor');
        });
        document.getElementById("EliminarInstructor").addEventListener('click', function() {
            EliminarInstructor();
        });
        function EliminarInstructor() {
            var instructorId = $('#instructorId').val();
            $.ajax({
                url: '<?php echo URL_SISINV ?>Persona/DeleteInstructor',
                type: 'POST',
                data: {

                    instructorId: instructorId
                }
            }).done(function() {
                Delete();
                // function de tiempo
                setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Persona/ListarInstructor';
                }, 3000);
            }).fail(function() {
                errorDelete();
                // function de tiempo
                setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Persona/ListarInstructor';
                }, 3000);
            })
        }
    })
</script>