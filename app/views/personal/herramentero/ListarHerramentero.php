<?php require_once "../app/views/template.php"; ?>
<!--<div id="contenedor_carga">
  <div id="carga"></div>
</div>-->

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <h1 class="h3 mb-2 text-gray-800">Listado de Herramentero</h1>
            <p class="mb-4">
                Registre la cantidad de herramenteros que esten asociados a la sede.
            </p>
        </div>
    </div>
</div>
<div class="tab-content">
    <div class="tab-pane tabs-animation fade active show" id="tab-content-0" role="tabpanel">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow mb-4 table-responsive">
                    <div class="card-header py-3">
                        <div class="row">
                            <div class="col-md-12"><br>
                                <h6 class="m-0 font-weight-bold">TABLA DE INFORMACION DE HERRAMENTEROS</h6>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped table-hover " id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>REGIONAL</th>
                                    <th>CENTRO</th>
                                    <th>SEDE</th>
                                    <th>NOMBRES</th>
                                    <th>PRIMER APELLIDO</th>
                                    <th>SEGUNDO APELLIDO</th>
                                    <th>FECHA NACIMIENTO</th>
                                    <th>TIPO DE DOCUMENTO</th>
                                    <th>NUMERO DE DOCUMENTO</th>
                                    <th>TELEFONO</th>
                                    <th>CARGO</th>
                                    <th>DIRECCION DE CORREO</th>
                                    <th>DIRECCION</th>
                                    <th>TIPO CONTRATO</th>
                                    <th>ACCION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $contador = 1;
                                foreach ($datos['ListarHerramentero'] as $ListarHerramentero) : ?>
                                    <tr>
                                        <th scope="row" ><?php echo $contador++; ?></th>
                                        <td><?php echo $ListarHerramentero->tbl_regional_NOMBRE; ?></td>
                                        <td><?php echo $ListarHerramentero->tbl_centro_NOMBRE; ?></td>
                                        <td><?php echo $ListarHerramentero->tbl_sede_NOMBRE; ?></td>
                                        <td><?php echo $ListarHerramentero->tbl_persona_NOMBRES; ?></td>
                                        <td><?php echo $ListarHerramentero->tbl_persona_PRIMERAPELLIDO; ?></td>
                                        <td><?php echo $ListarHerramentero->tbl_persona_SEGUNDOAPELLIDO; ?></td>
                                        <td><?php echo $ListarHerramentero->tbl_persona_FECHANAC; ?></td>
                                        <td><?php echo $ListarHerramentero->tbl_tipodocumento_tbl_tipodocumento_ID; ?></td>
                                        <td><?php echo $ListarHerramentero->tbl_persona_NUMDOCUMENTO; ?></td>
                                        <td><?php echo $ListarHerramentero->tbl_persona_TELEFONO; ?></td>
                                        <td><?php echo $ListarHerramentero->tbl_cargo_TIPO; ?></td>
                                        <td><?php echo $ListarHerramentero->tbl_persona_CORREO; ?></td>
                                        <td><?php echo $ListarHerramentero->tbl_persona_DIRECCION; ?></td>
                                        <td><?php echo $ListarHerramentero->tipo_contrato; ?></td>
                                        <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR') : ?>
                                            <td>
                                                <cite title="Editar">
                                                    <a href="<?php echo URL_SISINV; ?>Persona/ObtenerHerramentero/<?php echo $ListarHerramentero->tbl_persona_ID ?>" class="btn btn-info btn-icon-split" id="EditarHerramentero">
                                                        <span class="icon text-white-50">
                                                            <i class="fas fa-edit"></i>
                                                        </span>
                                                    </a>
                                                </cite>
                                                <cite title="Borrar">
                                                    <a href="<?php echo URL_SISINV; ?>Persona/EliminarHerramentero/<?php echo $ListarHerramentero->tbl_persona_ID; ?>" class="btn btn-danger btn-icon-split" id="BorrarHerramentero">
                                                        <span class="icon text-white-50">
                                                            <i class="fas fa-trash"></i>
                                                        </span>
                                                    </a>
                                                </cite>
                                            </td>
                                        <?php endif; ?>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    /*$(document).ready(function() {

        document.getElementById("RegistrarHerramentero").addEventListener('click', function() {
            RegistrarHerramenteroPersona();
            RegistrarHerramenteroUsuario()
        });
        /*Cambiar regional 
        $('#administradorRegional').change(function() {
            var regionalID = $('#administradorRegional').val();
            LoadCentros(regionalID)
        });

        function LoadCentros(regionalID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadCentros',
                type: 'POST',
                data: {
                    regionalID: regionalID
                }
            }).done(function(response) {
                var data = JSON.parse(response);
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='administradorCentro' value='" + data[i].tbl_centro_ID + "'>" + data[i].tbl_centro_NOMBRE + "</option>"
                    }
                    $('#administradorCentro').html(cadena);
                    var centroID = $('#administradorCentro').val();
                    LoadSedes(centroID);
                } else {
                    cadena = "<option value='0'>NO SE ENCONTRARON DATOS </option>";
                    $('#administradorCentro').html(cadena);
                    var centroID = $('#administradorCentro').val();
                    LoadSedes(centroID);
                }
            })
        }
        $('#administradorCentro').change(function() {
            var centroID = $('#administradorCentro').val();
            LoadSedes(centroID);
        })

        function LoadSedes(centroID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadSedes',
                type: 'POST',
                data: {
                    centroID: centroID
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='administradoSedeID' value='" + data[i].tbl_sede_ID + "'>" + data[i].tbl_sede_NOMBRE + "</option>";
                    }
                    var centroID = $('#administradorCentro').val()
                    if (centroID == 0) {
                        $('#administradorSedeID').html('');
                    }
                    $('#administradorSedeID').html(cadena);
                    var sedeID = $('#administradorSedeID').val();
                } else {
                    cadena = "<option id='administradorSedeID' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#administradorSedeID').html(cadena); // <- significa poner los datos dentro del input
                    var sedeID = $('#administradorSedeID').val();

                }
            })
        }

        function RegistrarAdministradorPersona() {
            //var administradorSedeID = $('#administradorSedeID').val().trim();
            //var administradorRegional = $('#administradorRegional').val().trim();
            //var administradorCentro = $('#administradorCentro').val().trim();
            var administradorNombre = $('#administradorNombre').val().trim();
            var administradorApellido1 = $('#administradorApellido1').val().trim();
            var administradorApellido2 = $('#administradorApellido2').val().trim();
            var administradorFechadeNacimiento = $('#administradorFechadeNacimiento').val().trim();
            var administradorTipoDocumento = $('#administradorTipoDocumento').val().trim();
            var administradorNumeroDocumento = $('#administradorNumeroDocumento').val().trim();
            var administradorNumeroTelefono = $('#administradorNumeroTelefono').val().trim();
            var administradorCargo = $('#administradorCargo').val().trim();
            var administradorDireccionCorreo = $('#administradorDireccionCorreo').val().trim();
            //var administradorContrasenia = $('#administradorContrasenia').val();
            var administradorDireccion = $('#administradorDireccion').val().trim();
            //var administradorDireccion = $('#administrador').val();
            $.ajax({
                    url: '<?php echo URL_SISINV ?>Persona/RegistrarAdministradorPersona',
                    type: 'POST',
                    data: {
                        //administradorSedeID: administradorSedeID,
                        administradorNombre: administradorNombre,
                        administradorApellido1: administradorApellido1,
                        administradorApellido2: administradorApellido2,
                        administradorFechadeNacimiento: administradorFechadeNacimiento,
                        administradorTipoDocumento: administradorTipoDocumento,
                        administradorNumeroDocumento: administradorNumeroDocumento,
                        administradorNumeroTelefono: administradorNumeroTelefono,
                        administradorCargo: administradorCargo,
                        administradorDireccionCorreo: administradorDireccionCorreo,
                        //administradorContrasenia: administradorContrasenia,
                        administradorDireccion: administradorDireccion
                    }
                }).done(function() {
                    Success();
                    // funcion de tiempo
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Persona/ListarAdministrador';
                    }, 3000);
                }).fail(function() {
                    error();
                    // funcion de tiempo
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Persona/ListarAdministrador';
                    }, 3000);
                })
            if (administradorRegional == "--SELECCIONAR--" || administradorSedeID == "" || administradorSedeID == "0" || administradorCentro =="" || administradorCentro == "0" || administradorNombre == "" || administradorApellido1 == "" || administradorApellido2 == "" || administradorFechadeNacimiento == "" || administradorTipoDocumento == "" || administradorNumeroDocumento == "" || administradorNumeroTelefono == "" || administradorCargo == "" || administradorDireccionCorreo == "" || administradorContrasenia == "" || administradorDireccion == "") {
                FillData()
            } else {
               
            }
        }
        
        document.addEventListener("click", () => {
       
            const form = document.createElement("form");
            const input = document.createElement("input");
            input.type = "password";
            form.appendChild(input);
            document.body.appendChild(form);

        }, { "once": true });
        
        
        
        /*window.onload = function() {
        var contenedor = document.getElementById('contenedor_carga');
        contenedor.style.visibility = 'hidden';
        contenedor.style.opacity = '0';
        }
    });*/
</script>
