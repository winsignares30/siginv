<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR REGIONAL-->
<div class="modal fade" id="HerramenteroModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">ELIMINAR HERRAMENTERO</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="" method="POST">
                    <div class="row">
                        <?php foreach($datos['herramenteros'] as $herramentero) : ?>
                            <?php if($herramentero) :?>
                                <div class="col-md-12 pr-1">
                                    <input type="hidden" id="herramenteroId" value="<?php echo $datos['herramenteroId'] ?>">
                                    
                                    <label class="" style="font-weight: bold;">
                                        NOMBRES:*
                                    </label>
                                    <input onkeyup="mayus(this);" class="form-control" placeholder="NOMBRES" type="text" value="<?php echo $herramentero->tbl_persona_NOMBRES ?>" id="herramenteroNombre" readonly="readonly"><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        APELLIDOS:*
                                    </label>
                                    <input onkeyup="mayus(this);" class="form-control" placeholder="APELLIDOS" type="text" value="<?php echo $herramentero->tbl_persona_PRIMERAPELLIDO ?>" id="herramenteroApellido1" readonly="readonly"><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        NUMERO DE DOCUMENTO:*
                                    </label>
                                    <input onkeyup="mayus(this);" class="form-control" placeholder="NUMERO DE DOCUMENTO" type="text" value="<?php echo $herramentero->tbl_persona_NUMDOCUMENTO ?>" id="herramenteroDocumento" readonly="readonly"><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        NUMERO DE TELEFONO:*
                                    </label>
                                    <input onkeyup="mayus(this);" class="form-control" placeholder="NUMERO DE TELEFONO" type="text" value="<?php echo $herramentero->tbl_persona_TELEFONO ?>" id="herramenteroCelular" readonly="readonly"><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        DIRECION DE CORREO:*
                                    </label>
                                    <input onkeyup="mayus(this);" class="form-control" placeholder="DIRECCION DE CORREO" type="text" value="<?php echo $herramentero->tbl_persona_CORREO ?>" id="herramenteroCorreo" readonly="readonly"><br>
                                    
                                </div>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-info btn-danger col-md-12" id="EliminarHerramentero">ELIMINAR</button>
                                </div>
                            <?php endif;?>
                        <?php endforeach;?>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/core/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#HerramenteroModel").modal("show");
        $("#HerramenteroModel").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Persona/ListarHerramentero');
        });
        document.getElementById("EliminarHerramentero").addEventListener('click', function() {
            EliminarHerramentero();
        });
        function EliminarHerramentero() {
            var herramenteroId = $('#herramenteroId').val();
            $.ajax({
                url: '<?php echo URL_SISINV ?>Persona/DeleteHerramentero',
                type: 'POST',
                data: {

                    herramenteroId: herramenteroId
                }
            }).done(function() {
                Delete();
                // function de tiempo
                setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Persona/ListarHerramentero';
                }, 3000);
            }).fail(function() {
                errorDelete();
                // function de tiempo
                setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Persona/ListarHerramentero';
                }, 3000);
            })
        }
    })
</script>