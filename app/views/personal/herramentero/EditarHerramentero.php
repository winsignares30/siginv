<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR ESTUDIANTES -->
<div class="modal fade" id="EditarHerramenteros" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Herramentero</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            <label style="font-weight: bold;">
                               Seleccione Regional:*
                            </label>
                            <div class="form-group">
                                <select class="form-control" name="" id="herramenteroRegional">
                                    <option>--SELECCIONAR--</option>
                                    <?php foreach ($datos['ListarRegional'] as $ListarRegional) : ?>
                                        <?php if ($ListarRegional) : ?>
                                            <option value="<?php echo $ListarRegional->tbl_regional_ID; ?>"><?php echo $ListarRegional->tbl_regional_NOMBRE; ?></option>
                                        <?php else : ?>
                                            <option value="0">-- NO SE ENCONTRARON DATOS --</option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select><br>
                                
                            </div>
                        </div> 
                        <?php foreach($datos['herramenteros'] as $herramentero) :?>
                            <?php if($herramentero):?>
                                <div class="col-md-6">
                                    <label style="font-weight: bold;">
                                       Seleccione Centro:*
                                    </label>
                                    <div class="form-group">
                                        <select class="form-control" id="herramenteroCentro">
        
                                        </select><br>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label style="font-weight: bold;">
                                       Seleccione Sede:*
                                    </label>
                                    <div class="form-group">
                                        <select class="form-control" id="herramenteroSede">
        
                                        </select><br>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nombres</label>
                                        <input type="hidden" id="herramenteroId" value="<?php echo $datos['herramenteroId'] ?>">
                                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" id="herramenteroNombre" type="text" class="form-control" value="<?php echo $herramentero->tbl_persona_NOMBRES; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Primer Apellido</label>
                                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" id="herramenteroApellido1" type="text" class="form-control" value="<?php echo $herramentero->tbl_persona_PRIMERAPELLIDO ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Segundo Apellido</label>
                                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" id="herramenteroApellido2" type="text" class="form-control" value="<?php echo $herramentero->tbl_persona_SEGUNDOAPELLIDO ?>">
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label>Tipo documento:</label>
                                        <select id="herramenteroTipoDocumento" value="" class="form-select form-control" aria-label="Default select example">
                                            <option>--SELECCIONAR--</option>
                                            <option value="CEDULA">CÉDULA</option>
                                            <option value="CEDULA EXTRANJERA">CÉDULA EXTRANJERA</option>
                                            <option value="TARJETA IDENTIDAD">TARJETA IDENTIDAD</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Documento</label>
                                        <input id="herramenteroDocumento" value="<?php echo $herramentero->tbl_persona_NUMDOCUMENTO ?>" type="number" pattern="[0-9]{8,10}" oninput="if(this.value.length > this.maxlength) this.value.slice(0, this.maxlength)" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label>Tipo Contrato:</label>
                                        <select id="herramenteroTipoContrato" value="" class="form-select form-control" aria-label="Default select example">
                                            <option>--SELECCIONAR--</option>
                                            <option value="PLANTA">PLANTA</option>
                                            <option value="CONTRATISTA">CONTRATISTA</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <label>Cargo</label>
                                        <select id="herramenteroCargo" value="" class="form-select form-control" aria-label="Default select example">
                                            <option>--SELECCIONAR--</option>
                                            <option value="2">HERRAMENTERO</option>
                                            <option value="3">INSTRUCTOR</option>
                                            <option value="1">ADMINISTRADOR</option>
                                        </select>
                                    </div>
                                </div>
                    
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Fecha Nacimiento</label>
                                        <input id="herramenteroFechaNacimiento" type="date" class="form-control" value="<?php echo $herramentero->tbl_persona_FECHANAC ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Celular</label>
                                        <input type="number" id="herramenteroCelular" value="<?php echo $herramentero->tbl_persona_TELEFONO ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Correo</label>
                                        <input type="email" id="herramenteroCorreo" value="<?php echo $herramentero->tbl_persona_CORREO ?>" pattern="[0-9]{8,10}" class="form-control">
                                    </div>
                                </div>
                                <!--<div class="col-md-12">
                                    <div class="form-group">
                                        <label>Contraseña</label>
                                        <input onkeyup="mayus(this);" id="instructorPassword" value="" type="text" class="form-control">
                                    </div>
                                </div>-->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Direccion</label>
                                        <input type="text" onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" id="herramenteroDireccion" value="<?php echo $herramentero->tbl_persona_DIRECCION ?>" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <button type="button" id="EditarHerramentero" class="btn btn-info btn-round col-md-12">Editar</button>
                                </div>
                            <?php endif;?>
                        <?php endforeach;?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#EditarHerramenteros").modal("show");
        $("#EditarHerramenteros").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Persona/ListarHerramentero');
        });
        document.getElementById('EditarHerramentero').addEventListener('click', function() {
            EditarHerramentero()
            //EditarAdministradorUsuario()
        });
        
        /*Cambiar regional */
        $('#herramenteroRegional').change(function() {
            var regionalID = $('#herramenteroRegional').val();
            LoadCentros(regionalID);
        });

        function LoadCentros(regionalID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadCentros',
                type: 'POST',
                data: {
                    regionalID: regionalID
                }
            }).done(function(response) {
                var data = JSON.parse(response);
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='herramenteroCentro' value='" + data[i].tbl_centro_ID + "'>" + data[i].tbl_centro_NOMBRE + "</option>"
                    }
                    $('#herramenteroCentro').html(cadena);
                    var centroID = $('#herramenteroCentro').val();
                    LoadSedes(centroID);
                } else {
                    cadena = "<option value='0'>NO SE ENCONTRARON DATOS </option>";
                    $('#herramenteroCentro').html(cadena);
                    var centroID = $('#herramenteroCentro').val();
                    LoadSedes(centroID);
                }
            })
        }
        
        $('#herramenteroCentro').change(function() {
            var centroID = $('#herramenteroCentro').val();
            LoadSedes(centroID);
        })

        function LoadSedes(centroID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadSedes',
                type: 'POST',
                data: {
                    centroID: centroID
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='herramenteroSede' value='" + data[i].tbl_sede_ID + "'>" + data[i].tbl_sede_NOMBRE + "</option>";
                    }
                    var centroID = $('#herramenteroCentro').val()
                    if (centroID == 0) {
                        $('#herramenteroSede').html('');
                    }
                    $('#herramenteroSede').html(cadena);
                } else {
                    cadena = "<option id='herramenteroSede' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#herramenteroSede').html(cadena); // <- significa poner los datos dentro del input
                }
            })
        }
        
        //Editar Instructores
        
        //$('#EditarInstructor').click(function(){
        function EditarHerramentero() {
            // Funcion para editar Instructor
            var herramenteroRegional = $('#herramenteroRegional').val();
            var herramenteroCentro = $('#herramenteroCentro').val();
            var herramenteroSede = $('#herramenteroSede').val();
            var herramenteroId = $('#herramenteroId').val().trim();
            var herramenteroNombre = $('#herramenteroNombre').val().trim();
            var herramenteroApellido1 = $('#herramenteroApellido1').val().trim();
            var herramenteroApellido2 = $('#herramenteroApellido2').val().trim();
            var herramenteroTipoDocumento = $('#herramenteroTipoDocumento').val().trim();
            var herramenteroDocumento = $('#herramenteroDocumento').val().trim();
            var herramenteroTipoContrato = $('#herramenteroTipoContrato').val().trim();
            var herramenteroCargo = $('#herramenteroCargo').val().trim();
            var herramenteroFechaNacimiento = $('#herramenteroFechaNacimiento').val().trim();
            var herramenteroCelular = $('#herramenteroCelular').val().trim();
            var herramenteroCorreo = $('#herramenteroCorreo').val().trim();
            var herramenteroDireccion = $('#herramenteroDireccion').val().trim();
            
            if(herramenteroRegional == "--SELECCIONAR--" || herramenteroSede == "" || herramenteroSede == "0" || herramenteroCentro =="" || herramenteroCentro == "0" || herramenteroId == "" || herramenteroDocumento == "" || herramenteroNombre == "" || herramenteroApellido1 == "" || herramenteroApellido2 == "" || herramenteroFechaNacimiento == "" ||  herramenteroCelular == "" || herramenteroCorreo == "" || herramenteroCargo == "" || herramenteroCargo == "--SELECCIONAR--" || herramenteroDireccion == "" || herramenteroTipoContrato == "" || herramenteroTipoContrato == "--SELECCIONAR--" || herramenteroTipoDocumento == "" || herramenteroTipoDocumento == "--SELECCIONAR--"){
                FillData()
            }else{
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Persona/EditarHerramentero',
                    type: 'POST',
                    data: {
                        herramenteroRegional: herramenteroRegional,
                        herramenteroCentro: herramenteroCentro,
                        herramenteroSede: herramenteroSede,
                        herramenteroId: herramenteroId,
                        herramenteroNombre: herramenteroNombre,
                        herramenteroApellido1: herramenteroApellido1,
                        herramenteroApellido2: herramenteroApellido2,
                        herramenteroFechaNacimiento: herramenteroFechaNacimiento,
                        herramenteroDocumento: herramenteroDocumento,
                        herramenteroCelular: herramenteroCelular,
                        herramenteroCorreo: herramenteroCorreo,
                        herramenteroCargo: herramenteroCargo,
                        herramenteroDireccion: herramenteroDireccion,
                        herramenteroTipoContrato: herramenteroTipoContrato,
                        herramenteroTipoDocumento: herramenteroTipoDocumento
                    
                    }
                }).done(function() {
                    $.ajax({
                        url: '<?php echo URL_SISINV ?>Persona/EditarHerramenteroUsuario',
                        type: 'POST',
                        data: {
                            herramenteroRegional: herramenteroRegional,
                            herramenteroSede: herramenteroSede,
                            herramenteroCentro: herramenteroCentro,
                            herramenteroId: herramenteroId,
                            herramenteroCorreo: herramenteroCorreo
                        
                        }
                    }).done(function() {
                        Edit()
                        setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Persona/ListarHerramentero';
                        }, 2000);
    
                    }).fail(function() {
                        /*ErrorEdit()
                       setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Persona/ListarInstructor';
                        }, 2000);*/
    
                    })
                    /*Edit()
                    setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Persona/ListarInstructor';
                    }, 2000);*/

                }).fail(function() {
                    ErrorEdit()
                   setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Persona/ListarHerramentero';
                    }, 2000);

                })
                
            }
            
        }
        
        
       
        
    })
</script>