<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR REGIONAL-->
<div class="modal fade" id="ModelCentro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDITAR PERSONAL</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="" method="POST">
                    <div class="row">
                        
                        <div class="col-md-6">
                            <label style="font-weight: bold;">
                               Seleccione Regional:*
                            </label>
                            <div class="form-group">
                                <select class="form-control" name="" id="administradorRegional">
                                    <option>--SELECCIONAR--</option>
                                    <?php foreach ($datos['ListarRegional'] as $ListarRegional) : ?>
                                        <?php if ($ListarRegional) : ?>
                                            <option value="<?php echo $ListarRegional->tbl_regional_ID; ?>"><?php echo $ListarRegional->tbl_regional_NOMBRE; ?></option>
                                        <?php else : ?>
                                            <option value="0">-- NO SE ENCONTRARON DATOS --</option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select><br>
                                
                            </div>
                        </div>
                        
                        <?php foreach($datos['Administradores'] as $administrador) : ?>
                            <?php if($administrador) :?>
                            
                                <div class="col-md-6">
                                    <label style="font-weight: bold;">
                                       Seleccione Centro:*
                                    </label>
                                    <div class="form-group">
                                        <select class="form-control" id="administradorCentro">
        
                                        </select><br>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <label style="font-weight: bold;">
                                       Seleccione Sede:*
                                    </label>
                                    <div class="form-group">
                                        <select class="form-control" id="administradorSede">
        
                                        </select><br>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="hidden" id="idAdministrador" value="<?php echo $datos['idAdministrador'] ?>">
                                        <label class="" style="font-weight: bold;">
                                            NOMBRES:*
                                        </label>
                                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="NOMBRES" type="text" value="<?php echo $administrador->tbl_persona_NOMBRES ?>" id="administradorNombre"><br>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" style="font-weight: bold;">
                                            PRIMER APELLIDO:*
                                        </label>
                                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="PRIMER APELLIDO" type="text" value="<?php echo $administrador->tbl_persona_PRIMERAPELLIDO ?>" id="administradorApellido1"><br>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" style="font-weight: bold;">
                                            SEGUNDO APELLIDO:*
                                        </label>
                                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="SEGUNDO APELLIDO" type="text" value="<?php echo $administrador->tbl_persona_SEGUNDOAPELLIDO ?>" id="administradorApellido2"><br>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" style="font-weight: bold;">
                                            TIPO DE TIPO DE DOCUMENTO:*
                                        </label>
                                        <select onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="TIPO DE DOCUMENTO" type="text" id="administradorTipoDocumento" value="<?php echo $administrador->tbl_tipodocumento_tbl_tipodocumento_ID ?>"><br>
                                            <option>--SELECCIONAR--</option>
                                            <option value="CÉDULA">CÉDULA</option>
                                            <option value="CÉDULA EXTRANJERA">CÉDULA EXTRANJERA</option>
                                            <option value="TARJETA IDENTIDAD">TARJETA IDENTIDAD</option>
                                        </select><br>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" style="font-weight: bold;">
                                            NUMERO DE DOCUMENTO:*
                                        </label>
                                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="NUMERO DE DOCUMENTO" type="text" value="<?php echo $administrador->tbl_persona_NUMDOCUMENTO ?>" id="administradorNumeroDocumento" ><br>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="" style="font-weight: bold;">
                                            NUMERO DE TELEFONO:*
                                        </label>
                                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="NUMERO DE TELEFONO" type="text" value="<?php echo $administrador->tbl_persona_TELEFONO ?>" id="administradorNumeroTelefono"><br>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">  
                                        <label class="" style="font-weight: bold;">
                                            CARGO:*
                                        </label>
                                        <select onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="CARGO" type="text" id="administradorCargo" value="<?php echo $administrador->tbl_cargo_tbl_cargo_ID ?>"><br>
                                            <option>--SELECCIONAR--</option>
                                            <option value="1">ADMINISTRADOR</option>
                                            <option value="2">HERRAMENTERO</option>
                                            <option value="3">INSTRUCTOR</option>
                                        </select><br>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group"> 
                                        <label class="" style="font-weight: bold;">
                                            FECHA DE NACIMIENTO:*
                                        </label>
                                        <input class="form-control" type="date" value="<?php echo $administrador->tbl_persona_FECHANAC ?>" name="administradorFechadeNacimiento" id="administradorFechadeNacimiento" id="example-date-input">
                                        <i class="fa fas-calendar "></i>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group"> 
                                        <label>Tipo Contrato:</label>
                                        <select id="administradorTipoContrato" value="<?php echo $administrador->tipo_contrato ?>" class="form-select form-control" aria-label="Default select example">
                                            <option>--SELECCIONAR--</option>
                                            <option value="PLANTA">PLANTA</option>
                                            <option value="CONTRATISTA">CONTRATISTA</option>
                                        </select><br>
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="" style="font-weight: bold;">
                                            DIRECCION DE CORREO:*
                                        </label>
                                        <input class="form-control" placeholder="DIRECCION DE CORREO" type="text" value="<?php echo $administrador->tbl_persona_CORREO ?>" id="administradorDireccionCorreo"><br> <!-- onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()"  -->
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="" style="font-weight: bold;">
                                            DIRECCION:*
                                        </label>
                                        <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="DIRECCION" type="text" value="<?php echo $administrador->tbl_persona_DIRECCION ?>" id="administradorDireccion"><br>
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-info btn-round col-md-12" id="EditarAdministrador" type="button">ACTUALIZAR</button>
                                </div>
                            <?php endif;?>
                        <?php endforeach;?>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#ModelCentro").modal("show");
        $("#ModelCentro").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Persona/ListarAdministrador');
        });
        document.getElementById('EditarAdministrador').addEventListener('click', function() {
            EditarAdministrador()
            //EditarAdministradorUsuario()
        });
        
        /*Cambiar regional */
        $('#administradorRegional').change(function() {
            var regionalID = $('#administradorRegional').val();
            LoadCentros(regionalID)
        });

        function LoadCentros(regionalID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadCentros',
                type: 'POST',
                data: {
                    regionalID: regionalID
                }
            }).done(function(response) {
                var data = JSON.parse(response);
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='administradorCentro' value='" + data[i].tbl_centro_ID + "'>" + data[i].tbl_centro_NOMBRE + "</option>"
                    }
                    $('#administradorCentro').html(cadena);
                    var centroID = $('#administradorCentro').val();
                    LoadSedes(centroID);
                } else {
                    cadena = "<option value='0'>NO SE ENCONTRARON DATOS </option>";
                    $('#administradorCentro').html(cadena);
                    var centroID = $('#administradorCentro').val();
                    LoadSedes(centroID);
                }
            })
        }
        
        $('#administradorCentro').change(function() {
            var centroID = $('#administradorCentro').val();
            LoadSedes(centroID);
        })

        function LoadSedes(centroID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadSedes',
                type: 'POST',
                data: {
                    centroID: centroID
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='administradorSede' value='" + data[i].tbl_sede_ID + "'>" + data[i].tbl_sede_NOMBRE + "</option>";
                    }
                    var centroID = $('#administradorCentro').val()
                    if (centroID == 0) {
                        $('#administradorSede').html('');
                    }
                    $('#administradorSede').html(cadena);
                } else {
                    cadena = "<option id='administradorSede' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#administradorSede').html(cadena); // <- significa poner los datos dentro del input
                }
            })
        }
        
        //edit ambiente
        function EditarAdministrador() {
            var administradorRegional = $('#administradorRegional').val();
            var administradorSedeID = $('#administradorSedeID').val();
            var administradorCentro = $('#administradorCentro').val();
            var idAdministrador = $('#idAdministrador').val().trim();
            var administradorNumeroDocumento = $('#administradorNumeroDocumento').val().trim();
            var administradorNombre = $('#administradorNombre').val().trim();
            var administradorApellido1 = $('#administradorApellido1').val().trim();
            var administradorApellido2 = $('#administradorApellido2').val().trim();
            var administradorFechadeNacimiento = $('#administradorFechadeNacimiento').val().trim();
            var administradorNumeroTelefono = $('#administradorNumeroTelefono').val().trim();
            var administradorDireccionCorreo = $('#administradorDireccionCorreo').val().trim();
            var administradorCargo = $('#administradorCargo').val().trim();
            var administradorDireccion = $('#administradorDireccion').val().trim();
            var administradorTipoContrato = $('#administradorTipoContrato').val().trim();
            var administradorTipoDocumento = $('#administradorTipoDocumento').val().trim();
            
            //console.log(administradorSede)
            console.log(administradorDireccion)
            //console.log(tbl_cargo_tbl_cargo_ID)
            if(administradorRegional == "--SELECCIONAR--" || administradorSedeID == "" || administradorSedeID == "0" || administradorCentro =="" || administradorCentro == "0" || idAdministrador == "" || administradorNumeroDocumento == "" || administradorNombre == "" || administradorApellido1 == "" || administradorApellido2 == "" || administradorFechadeNacimiento == "" ||  administradorNumeroTelefono == "" || administradorDireccionCorreo == "" || administradorCargo == "" || administradorCargo == "--SELECCIONAR--" || administradorDireccion == "" || administradorTipoContrato == "" || administradorTipoContrato == "--SELECCIONAR--" || administradorTipoDocumento == "" || administradorTipoDocumento == "--SELECCIONAR--"){
                FillData()
            }else{
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Persona/EditarAdministrador',
                    type: 'POST',
                    data: {
                        administradorRegional: administradorRegional,
                        administradorSedeID: administradorSedeID,
                        administradorCentro: administradorCentro,
                        idAdministrador: idAdministrador,
                        administradorNombre: administradorNombre,
                        administradorApellido1: administradorApellido1,
                        administradorApellido2: administradorApellido2,
                        administradorFechadeNacimiento: administradorFechadeNacimiento,
                        administradorNumeroDocumento: administradorNumeroDocumento,
                        administradorNumeroTelefono: administradorNumeroTelefono,
                        administradorDireccionCorreo: administradorDireccionCorreo,
                        administradorCargo: administradorCargo,
                        administradorDireccion: administradorDireccion,
                        administradorTipoContrato: administradorTipoContrato,
                        administradorTipoDocumento: administradorTipoDocumento
                    
                    }
                }).done(function() {
                    $.ajax({
                        url: '<?php echo URL_SISINV ?>Persona/EditarAdministradorUsuario',
                        type: 'POST',
                        data: {
                            //administradorSedeID: administradorSedeID,
                            administradorRegional: administradorRegional,
                            administradorSedeID: administradorSedeID,
                            administradorCentro: administradorCentro,
                            idAdministrador: idAdministrador,
                            administradorDireccionCorreo: administradorDireccionCorreo,
                        
                        }
                    }).done(function() {
                        Edit()
                        setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Persona/ListarAdministrador';
                        }, 2000);
    
                    }).fail(function() {
                        ErrorEdit()
                       setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Persona/ListarAdministrador';
                        }, 2000);
    
                    })
                    Edit()
                    setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Persona/ListarAdministrador';
                    }, 2000);

                }).fail(function() {
                    ErrorEdit()
                   setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Persona/ListarAdministrador';
                    }, 2000);

                })
                
            }
            
        }
        
        //Edit Correo Usuario
        /*function EditarAdministradorUsuario() {
            
            //var administradorSedeID = $('#administradorSedeID').val()
            var administradorRegional = $('#administradorRegional').val();
            var administradorSedeID = $('#administradorSedeID').val();
            var administradorCentro = $('#administradorCentro').val();
            var idAdministrador = $('#idAdministrador').val().trim();
            var administradorDireccionCorreo = $('#administradorDireccionCorreo').val().trim();
            
            console.log(administradorDireccionCorreo)
            console.log(administradorDireccion)
            //console.log(tbl_cargo_tbl_cargo_ID)
            if(administradorRegional == "--SELECCIONAR--" || administradorSedeID == "" || administradorSedeID == "0" || administradorCentro =="" || administradorCentro == "0" || idAdministrador == "" || administradorDireccionCorreo == "" ){
                FillData()
            }else{
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Persona/EditarAdministradorUsuario',
                    type: 'POST',
                    data: {
                        //administradorSedeID: administradorSedeID,
                        administradorRegional: administradorRegional,
                        administradorSedeID: administradorSedeID,
                        administradorCentro: administradorCentro,
                        idAdministrador: idAdministrador,
                        administradorDireccionCorreo: administradorDireccionCorreo,
                    
                    }
                }).done(function() {
                    Edit()
                    setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Persona/ListarAdministrador';
                    }, 2000);

                }).fail(function() {
                    ErrorEdit()
                   setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Persona/ListarAdministrador';
                    }, 2000);

                })
            }
            
        }*/
        
    })
</script>


















