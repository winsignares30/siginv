<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR REGIONAL-->
<div class="modal fade" id="CentroModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">ELIMINAR ADMINISTRADOR</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <?php foreach($datos['Administradores'] as $administrador) : ?>
                            <?php if($administrador) :?>
                                <div class="col-md-12 pr-1">
                                    <div class="form-group">
                                        <input type="hidden" id="idAdministrador" value="<?php echo $datos['idAdministrador'] ?>">
                                            
                                            <label class="" style="font-weight: bold;">
                                                NOMBRES:*
                                            </label>
                                            <input onkeyup="mayus(this);" class="form-control" placeholder="NOMBRES" type="text" value="<?php echo $administrador->tbl_persona_NOMBRES ?>" id="administradorNombre" readonly="readonly"><br>
                                            
                                            <label class="" style="font-weight: bold;">
                                                PRIMER APELLIDO:*
                                            </label>
                                            <input onkeyup="mayus(this);" class="form-control" placeholder="APELLIDO" type="text" value="<?php echo $administrador->tbl_persona_PRIMERAPELLIDO ?>" id="administradorApellido1" readonly="readonly"><br>
                                            
                                            <label class="" style="font-weight: bold;">
                                                SEGUNDO APELLIDO:*
                                            </label>
                                            <input onkeyup="mayus(this);" class="form-control" placeholder="APELLIDO" type="text" value="<?php echo $administrador->tbl_persona_SEGUNDOAPELLIDO ?>" id="administradorApellido2" readonly="readonly"><br>
                                            
                                            <label class="" style="font-weight: bold;">
                                                NUMERO DE DOCUMENTO:*
                                            </label>
                                            <input onkeyup="mayus(this);" class="form-control" placeholder="NUMERO DE DOCUMENTO" type="text" value="<?php echo $administrador->tbl_persona_NUMDOCUMENTO ?>" id="administradorNumeroDocumento" readonly="readonly"><br>
                                            
                                            <label class="" style="font-weight: bold;">
                                                DIRECCION DE CORREO:*
                                            </label>
                                            <input onkeyup="mayus(this);" class="form-control" placeholder="DIRECCION DE CORREO" type="text" value="<?php echo $administrador->tbl_persona_CORREO ?>" id="administradorDireccionCorreo" readonly="readonly"><br>
                                            
                                            <label class="" style="font-weight: bold;">
                                                CARGO:*
                                            </label>
                                            <input onkeyup="mayus(this);" class="form-control" placeholder="CARGO" type="text" value="<?php echo $administrador->tbl_cargo_tbl_cargo_ID ?>" id="administradorCargo" readonly="readonly"><br>
                                            
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-info btn-danger col-md-12" type="button" name="" id="EliminarAdministrador">ELIMINAR</button>
                                </div>
                            <?php endif;?>
                        <?php endforeach;?>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#CentroModel").modal("show");
        $("#CentroModel").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Persona/ListarAdministrador');
        });
        document.getElementById('EliminarAdministrador').addEventListener('click', function() {
            EliminarAdministrador();
        });

        function EliminarAdministrador() {
            var idAdministrador = $('#idAdministrador').val()

            $.ajax({
                url: '<?php echo URL_SISINV ?>Persona/DeleteAdministrador',
                type: 'POST',
                data: {
                    idAdministrador: idAdministrador
                }
            }).done(function() {
                Delete()
                // function de tiempo
                setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Persona/ListarAdministrador';
                }, 2000);
            }).fail(function() {
                ErrorDelete()
                // function de tiempo
                setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Persona/ListarAdministrador';
                }, 2000);
            })
        }

    })
</script>