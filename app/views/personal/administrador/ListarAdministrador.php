<?php require_once "../app/views/template.php"; ?>
<!--<div id="contenedor_carga">
  <div id="carga"></div>
</div>-->

<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <h1 class="h3 mb-2 text-gray-800">Listado de Administrador</h1>
            <p class="mb-4">
                Registre la cantidad de administradores que esten asociados a la sede.
            </p>
        </div>
    </div>
</div>
<div class="tab-content">
    <div class="tab-pane tabs-animation fade active show" id="tab-content-0" role="tabpanel">
        <div class="row">
            <div class="col-md-4">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">REGISTRAR PERSONAL</h5>
                        <form>
                            <div class="row">
                                <div class="position-relative form-group col-md-12">
                                    
                                    <label class="" style="font-weight: bold;">
                                        SELECCIONA LA REGIONAL:*
                                    </label>
                                    <select class="form-control" name="" id="administradorRegional">
                                        <option>--SELECCIONAR--</option>
                                        <?php foreach ($datos['ListarRegional'] as $ListarRegional) : ?>
                                            <?php if ($ListarRegional) : ?>
                                                <option value="<?php echo $ListarRegional->tbl_regional_ID; ?>"><?php echo $ListarRegional->tbl_regional_NOMBRE; ?></option>
                                            <?php else : ?>
                                                <option value="0">-- NO SE ENCONTRARON DATOS --</option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        SELECCIONA LA CENTRO:*
                                    </label>
                                    <select class="form-control" id="administradorCentro">

                                    </select><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        NOMBRE DE LA SEDE:*
                                    </label>
                                    <select class="form-control" id="administradorSedeID">

                                    </select><br>

                                    <label class="" style="font-weight: bold;">
                                        NOMBRES:*
                                    </label>
                                    <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="NOMBRES" type="text" id="administradorNombre"><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        PRIMER APELLIDO:*
                                    </label>
                                    <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="PRIMER APELLIDO" type="text" id="administradorApellido1"><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        SEGUNDO APELLIDO:*
                                    </label>
                                    <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="SEGUNDO APELLIDO" type="text" id="administradorApellido2"><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        FECHA DE NACIMIENTO:*
                                    </label>
                                    <input class="form-control" type="date" value="yyyy-MM-dd" name="administradorFechadeNacimiento" id="administradorFechadeNacimiento" id="example-date-input">
                                    <i class="fa fas-calendar "></i><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        TIPO DE DOCUMENTO:*
                                    </label>
                                    <select onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="TIPO DE DOCUMENTO" type="text" id="administradorTipoDocumento"><br>
                                        <option>--SELECCIONAR--</option>
                                        <option value="CÉDULA">CÉDULA</option>
                                        <option value="CÉDULA EXTRANJERA">CÉDULA EXTRANJERA</option>
                                        <option value="TARJETA IDENTIDAD">TARJETA IDENTIDAD</option>
                                    </select><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        NUMERO DE DOCUMENTO:*
                                    </label>
                                    <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="NUMERO DE DOCUMENTO" type="number" id="administradorNumeroDocumento"><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        NUMERO DE TELEFONO:*
                                    </label>
                                    <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="NUMERO DE TELEFONO" type="number" id="administradorNumeroTelefono"><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        CARGO:*
                                    </label>
                                    <select onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="CARGO" type="text" id="administradorCargo"><br>
                                        <option>--SELECCIONAR--</option>
                                        <option value="1">ADMINISTRADOR</option>
                                        <option value="2">HERRAMENTERO</option>
                                        <option value="3">INSTRUCTOR</option>
                                    </select><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        DIRECCION DE CORREO:*
                                    </label>
                                    <input  class="form-control" placeholder="DIRECCION DE CORREO" type="text" id="administradorDireccionCorreo"><br><!-- onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" -->
                                    
                                    <label class="" style="font-weight: bold;">
                                        CONTRASEÑA:*
                                    </label>
                                    <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="CONTRASEÑA" type="password" id="administradorContrasenia"><br>
                                    
                                    <label class="" style="font-weight: bold;">
                                        DIRECCION:*
                                    </label>
                                    <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" class="form-control" placeholder="DIRECCION" type="text" id="administradorDireccion"><br>
                                    
                                    <label>Tipo Contrato:</label>
                                    <select id="administradorTipoContrato" value="" class="form-select form-control" aria-label="Default select example">
                                        <option>--SELECCIONAR--</option>
                                        <option value="PLANTA">PLANTA</option>
                                        <option value="CONTRATISTA">CONTRATISTA</option>
                                    </select><br>
                                    
                                    <p class="text-muted"><i> Los campos con * son obligatorios</i></p>
                                    
                                    <button class="mb-2 mr-2 btn btn-primary col-md-12" value="REGISTRAR" type="button" id="RegistrarAdministrador">
                                        REGISTRAR
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card shadow mb-4 table-responsive">
                    <div class="card-header py-3">
                        <div class="row">
                            <div class="col-md-11"><br>
                                <h6 class="m-0 font-weight-bold">TABLA DE INFORMACION</h6>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped table-hover " id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>REGIONAL</th>
                                    <th>CENTRO</th>
                                    <th>SEDE</th>
                                    <th>NOMBRES</th>
                                    <th>PRIMER APELLIDO</th>
                                    <th>SEGUNDO APELLIDO</th>
                                    <th>FECHA NACIMIENTO</th>
                                    <th>TIPO DE DOCUMENTO</th>
                                    <th>NUMERO DE DOCUMENTO</th>
                                    <th>TELEFONO</th>
                                    <th>CARGO</th>
                                    <th>DIRECCION DE CORREO</th>
                                    <th>DIRECCION</th>
                                    <th>TIPO CONTRATO</th>
                                    <th>ACCION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $contador = 1;
                                foreach ($datos['ListarAdministrador'] as $ListarAdministrador) : ?>
                                    <tr>
                                        <th scope="row" ><?php echo $contador++; ?></th>
                                        <td><?php echo $ListarAdministrador->tbl_regional_NOMBRE; ?></td>
                                        <td><?php echo $ListarAdministrador->tbl_centro_NOMBRE; ?></td>
                                        <td><?php echo $ListarAdministrador->tbl_sede_NOMBRE; ?></td>
                                        <td><?php echo $ListarAdministrador->tbl_persona_NOMBRES; ?></td>
                                        <td><?php echo $ListarAdministrador->tbl_persona_PRIMERAPELLIDO; ?></td>
                                        <td><?php echo $ListarAdministrador->tbl_persona_SEGUNDOAPELLIDO; ?></td>
                                        <td><?php echo $ListarAdministrador->tbl_persona_FECHANAC; ?></td>
                                        <td><?php echo $ListarAdministrador->tbl_tipodocumento_tbl_tipodocumento_ID; ?></td>
                                        <td><a href="<?php echo URL_SISINV; ?>Persona/EditarAdministradorNumero/<?php echo $ListarAdministrador->tbl_persona_ID ?>"><?php echo $ListarAdministrador->tbl_persona_NUMDOCUMENTO; ?></a></td>
                                        <td><?php echo $ListarAdministrador->tbl_persona_TELEFONO; ?></td>
                                        <td><?php echo $ListarAdministrador->tbl_cargo_TIPO; ?></td>
                                        <td><?php echo $ListarAdministrador->tbl_persona_CORREO; ?></td>
                                        <td><?php echo $ListarAdministrador->tbl_persona_DIRECCION; ?></td>
                                        <td><?php echo $ListarAdministrador->tipo_contrato; ?></td>
                                        <?php if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR') : ?>
                                            <td>
                                                <cite title="Editar">
                                                    <a href="<?php echo URL_SISINV; ?>Persona/ObtenerAdministrador/<?php echo $ListarAdministrador->tbl_persona_ID ?>" class="btn btn-info btn-icon-split" id="EditarAdministrador">
                                                        <span class="icon text-white-50">
                                                            <i class="fas fa-edit"></i>
                                                        </span>
                                                    </a>
                                                </cite>
                                                <cite title="Borrar">
                                                    <a href="<?php echo URL_SISINV; ?>Persona/EliminarAdministrador/<?php echo $ListarAdministrador->tbl_persona_ID; ?>" class="btn btn-danger btn-icon-split">
                                                        <span class="icon text-white-50">
                                                            <i class="fas fa-trash"></i>
                                                        </span>
                                                    </a>
                                                </cite>
                                            </td>
                                        <?php endif; ?>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        document.getElementById("RegistrarAdministrador").addEventListener('click', function() {
            RegistrarAdministradorPersona();
            RegistrarAdministradorUsuario();
            
        });
        /*Cambiar regional */
        $('#administradorRegional').change(function() {
            var regionalID = $('#administradorRegional').val();
            LoadCentros(regionalID)
        });

        function LoadCentros(regionalID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadCentros',
                type: 'POST',
                data: {
                    regionalID: regionalID
                }
            }).done(function(response) {
                var data = JSON.parse(response);
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='administradorCentro' value='" + data[i].tbl_centro_ID + "'>" + data[i].tbl_centro_NOMBRE + "</option>"
                    }
                    $('#administradorCentro').html(cadena);
                    var centroID = $('#administradorCentro').val();
                    LoadSedes(centroID);
                } else {
                    cadena = "<option value='0'>NO SE ENCONTRARON DATOS </option>";
                    $('#administradorCentro').html(cadena);
                    var centroID = $('#administradorCentro').val();
                    LoadSedes(centroID);
                }
            })
        }
        $('#administradorCentro').change(function() {
            var centroID = $('#administradorCentro').val();
            LoadSedes(centroID);
        })

        function LoadSedes(centroID) {
            $.ajax({
                url: '<?php echo URL_SISINV ?>Ambiente/LoadSedes',
                type: 'POST',
                data: {
                    centroID: centroID
                },
            }).done(function(resp) {
                var data = JSON.parse(resp)
                var cadena = "";
                if (data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        cadena += "<option id='administradoSedeID' value='" + data[i].tbl_sede_ID + "'>" + data[i].tbl_sede_NOMBRE + "</option>";
                    }
                    var centroID = $('#administradorCentro').val()
                    if (centroID == 0) {
                        $('#administradorSedeID').html('');
                    }
                    $('#administradorSedeID').html(cadena);
                    var sedeID = $('#administradorSedeID').val();
                } else {
                    cadena = "<option id='administradorSedeID' value='0'>NO SE ENCONTRARON DATOS</option>";
                    $('#administradorSedeID').html(cadena); // <- significa poner los datos dentro del input
                    var sedeID = $('#administradorSedeID').val();

                }
            })
        }
         
        function RegistrarAdministradorPersona() {
            var administradorRegional = $('#administradorRegional').val();
            var administradorCentro = $('#administradorCentro').val();
            var administradorSedeID = $('#administradorSedeID').val();
            var administradorNombre = $('#administradorNombre').val().trim();
            var administradorApellido1 = $('#administradorApellido1').val().trim();
            var administradorApellido2 = $('#administradorApellido2').val().trim();
            var administradorFechadeNacimiento = $('#administradorFechadeNacimiento').val().trim();
            var administradorTipoDocumento = $('#administradorTipoDocumento').val().trim();
            var administradorNumeroDocumento = $('#administradorNumeroDocumento').val().trim();
            var administradorNumeroTelefono = $('#administradorNumeroTelefono').val().trim();
            var administradorDireccionCorreo = $('#administradorDireccionCorreo').val().trim();
            var administradorContrasenia = $('#administradorContrasenia').val().trim();
            var administradorCargo = $('#administradorCargo').val().trim();
            var administradorDireccion = $('#administradorDireccion').val().trim();
            var administradorTipoContrato = $('#administradorTipoContrato').val().trim();
            console.log(administradorCargo);
            if (administradorRegional == "--SELECCIONAR--" || administradorSedeID == "" || administradorSedeID == "0" || administradorCentro =="" || administradorCentro == "0" || administradorNombre == "" || administradorApellido1 == "" || administradorApellido2 == "" || administradorFechadeNacimiento == "" || administradorTipoDocumento == "" || administradorTipoDocumento == "--SELECCIONAR--" || administradorNumeroDocumento == "" || administradorNumeroTelefono == "" || administradorDireccionCorreo == "" || administradorContrasenia == "" || administradorCargo == "" || administradorCargo == "--SELECCIONAR--" || administradorDireccion == "" || administradorTipoContrato == "" || administradorTipoContrato == "--SELECCIONAR--" ) {
                FillData()
            } else {
                $.ajax({
                url: '<?php echo URL_SISINV ?>Persona/RegistrarAdministradorPersona',
                type: 'POST',
                data: {
                    administradorNombre: administradorNombre,
                    administradorApellido1: administradorApellido1,
                    administradorApellido2: administradorApellido2,
                    administradorFechadeNacimiento: administradorFechadeNacimiento,
                    administradorTipoDocumento: administradorTipoDocumento,
                    administradorNumeroDocumento: administradorNumeroDocumento,
                    administradorNumeroTelefono: administradorNumeroTelefono,
                    administradorDireccionCorreo: administradorDireccionCorreo,
                    administradorCargo: administradorCargo,
                    administradorDireccion: administradorDireccion,
                    administradorTipoContrato: administradorTipoContrato
                }
            }).done(function() {
                $.ajax({
                url:'<?php echo URL_SISINV ?>Persona/contadorRegistros',
                type: 'POST'
                }).done(response => {
                    var data = JSON.parse(response)
                    let cantidadRegistros = ""
                    data.forEach(element =>{
                        cantidadRegistros = (element['tbl_persona_ID'])
                    })
                    cantidadRegistros = parseInt(cantidadRegistros)
                    console.log(cantidadRegistros);
                    $.ajax({
                        url: '<?php echo URL_SISINV ?>Persona/RegistrarAdministradorUsuario',
                        type: 'POST',
                        data: {
                            administradorSedeID: administradorSedeID,
                            cantidadRegistros: cantidadRegistros,
                            administradorDireccionCorreo: administradorDireccionCorreo,
                            administradorContrasenia: administradorContrasenia
                            
                        }
                    }).done(function() {
                        Success();
                        
                        setTimeout(function() {
                            window.location.href = '<?php echo URL_SISINV ?>Persona/ListarAdministrador';
                        }, 3000);
                    }).fail(function() {
                        error();
                        //funcion de tiempo
                        setTimeout(function() {
                            window.location.href = '<?php echo URL_SISINV ?>Persona/ListarAdministrador';
                        }, 3000);
                    })
                    /*if (administradorRegional == "--SELECCIONAR--" || administradorSedeID == "" || administradorSedeID == "0" || administradorCentro =="" || administradorCentro == "0" || administradorDireccionCorreo == "" || administradorContrasenia == "" || cantidadRegistros == "") {
                        FillData()
                    } else {
                       
                    }*/
                    
                
                })
                    /*Success();
                    funcion de tiempo
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Persona/ListarAdministrador';
                    }, 3000);*/
            }).fail(function() {
                error();
                setTimeout(function() {
                    window.location.href = '<?php echo URL_SISINV ?>Persona/ListarAdministrador';
                }, 3000);
            })//Fail Function
            }
            
        }//Funcion Fin
        
        /*Registro User y Password
        function RegistrarAdministradorUsuario() {
            var administradorSedeID = $('#administradorSedeID').val().trim();
            var administradorDireccionCorreo = $('#administradorDireccionCorreo').val().trim();
            var administradorContrasenia = $('#administradorContrasenia').val().trim();
            
            $.ajax({
                url:'<?php echo URL_SISINV ?>Persona/contadorRegistros',
                type: 'POST'
            }).done(response => {
                var data = JSON.parse(response)
                let cantidadRegistros = ""
                data.forEach(element =>{
                    cantidadRegistros = (element['tbl_persona_ID'])
                })
                cantidadRegistros = parseInt(cantidadRegistros)
                console.log(cantidadRegistros);
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Persona/RegistrarAdministradorUsuario',
                    type: 'POST',
                    data: {
                        administradorSedeID: administradorSedeID,
                        cantidadRegistros: cantidadRegistros,
                        administradorDireccionCorreo: administradorDireccionCorreo,
                        administradorContrasenia: administradorContrasenia
                        
                    }
                }).done(function() {
                    Success();
                    
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Persona/ListarAdministrador';
                    }, 3000);
                }).fail(function() {
                    error();
                    //funcion de tiempo
                    setTimeout(function() {
                        window.location.href = '<?php echo URL_SISINV ?>Persona/ListarAdministrador';
                    }, 3000);
                })
                /*if (administradorRegional == "--SELECCIONAR--" || administradorSedeID == "" || administradorSedeID == "0" || administradorCentro =="" || administradorCentro == "0" || administradorDireccionCorreo == "" || administradorContrasenia == "" || cantidadRegistros == "") {
                    FillData()
                } else {
                   
                }
                
            
            })
           
        }*/
        
        document.addEventListener("click", () => {
       
            const form = document.createElement("form");
            const input = document.createElement("input");
            input.type = "password";
            form.appendChild(input);
            document.body.appendChild(form);

        }, { "once": true });
        
        
        
        /*window.onload = function() {
        var contenedor = document.getElementById('contenedor_carga');
        contenedor.style.visibility = 'hidden';
        contenedor.style.opacity = '0';
        }*/
    });
</script>
