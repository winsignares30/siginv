<?php require_once "../app/views/template.php"; ?>
<!-- MODAL EDITAR REGIONAL-->
<div class="modal fade" id="ModelCentro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDITAR INSTRUCTOR</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="" method="POST">
                    <div class="row">
                        <div class="col-md-12 pr-1">
                            <label class="" style="font-weight: bold;">
                                SELECCIONA LA SEDE:*
                            </label>
                            <select class="form-control" id="instructorSede">
                                <option>--SELECCIONAR--</option>
                                <?php foreach ($datos['ListarSede'] as $ListarSede) : ?>
                                    <option value="<?php echo $ListarSede->tbl_sede_ID ?>"><?php echo $ListarSede->tbl_sede_NOMBRE ?></option>
                                <?php endforeach; ?>
                            </select> <br>

                            <div class="form-group">
                                <input type="hidden" id="idInstructor" value="<?php echo $datos['idInstructor'] ?>">
                                <label>Numero de Documento:</label>
                                <input onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" type="text" class="form-control" value="<?php echo $datos['instructorNumeroDocumento'] ?>" id="instructorNumeroDocumento"><br>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-info btn-round col-md-12" id="Editarinstructor">ACTUALIZAR</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL_SISINV ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#ModelCentro").modal("show");
        $("#ModelCentro").on('hidden.bs.modal', function() {
            window.location.replace('<?php echo URL_SISINV ?>Instructor/ListarInstructor');
        });
        document.getElementById("Editarinstructor").addEventListener('click', function() {
            Editarinstructor()
        });

        function Editarinstructor() {
            var instructorSedeID = $('#instructorSede').val()
            var idInstructor = $('#idInstructor').val();
            var instructorNumeroDocumento = $('#instructorNumeroDocumento').val();
            var InstructorExists = false;
            if (instructorNumeroDocumento == "" || instructorSedeID == "--SELECCIONAR--") {
                FillData();
            } else {
                $.ajax({
                    url: '<?php echo URL_SISINV ?>Instructor/ComprobarInstructor',
                    type: 'POST',
                    data: {
                        idInstructor: idInstructor
                    }
                }).done(function(response) {
                    var data = JSON.parse(response)
                    for (i = 0; i < data.length; i++) {
                        if (data[i].tbl_instructor_NUMDECUMENTO == instructorNumeroDocumento && data[i].tbl_sede_tbl_sede_ID == instructorSedeID) {
                            Existe()
                        } else {
                            $.ajax({
                                url: '<?php echo URL_SISINV ?>Instructor/EditarInstructor2',
                                type: 'POST',
                                data: {
                                    instructorSedeID: instructorSedeID,
                                    idInstructor: idInstructor,
                                    instructorNumeroDocumento: instructorNumeroDocumento
                                }
                            }).done(function() {
                                Edit();
                                // function de tiempo
                                setTimeout(function() {
                                    window.location.href = '<?php echo URL_SISINV ?>Instructor/ListarInstructor';
                                }, 2000);
                            }).fail(function() {
                                ErrorEdit()
                                // function de tiempo
                                setTimeout(function() {
                                    window.location.href = '<?php echo URL_SISINV ?>Instructor/ListarInstructor';
                                }, 2000);
                            })
                        }
                    }
                })
            }
        }
    })
</script>


























