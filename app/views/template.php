<?php

$template = new Template();
class Template
{
  public function __construct()
  {
    
?>

    <!DOCTYPE html>
    <html lang="es">

    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <style type="text/css">
        .sinborde {
          border: 0;
          box-shadow: none;
          outline: 0;
        }
      </style>

      
<title><?php echo  NAME_SISINV; ?></title>
      <!-- Custom fonts for this template-->
      <link href="<?php echo URL_SISINV; ?>MATERIAL_THEME/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
      <!-- Custom styles for this template-->
      <link href="<?php echo URL_SISINV; ?>css/custom.css" rel="stylesheet">
      <link href="<?php echo URL_SISINV; ?>css/carga.css" rel="stylesheet">
      <link href="<?php echo URL_SISINV; ?>css/style.css" rel="stylesheet">
      <link rel="shortcut icon" href="<?php echo URL_SISINV; ?>images/apoyo-tecnico.png">
      <!-- <link rel="shortcut icon" href="<?php echo URL_SISINV; ?>css/datatables-net/datatables.css"> -->
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css"/>
      <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css"/> -->
      <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css"> -->
      <!-- Conexion a Font Awesome Me para icono de print -->
      <script src="https://kit.fontawesome.com/75539c0174.js" crossorigin="anonymous"></script>
    </head>

    <body id="page-top">

      <!-- Page Wrapper -->
      <div id="wrapper">

            <!-- Sidebar -->

        <!--VALIDACION DEL MENU DINAMICO CON PRIVILEGIOS-->
        <?php
        if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR') {
          require 'menus/Administrador.php';
        } elseif ($_SESSION['sesion_active']['tipo_usuario'] == 'HERRAMENTERO') {
          require 'menus/Herramentero.php';
        } elseif ($_SESSION['sesion_active']['tipo_usuario'] == 'COORDINADOR') {
          require 'menus/Coordinador.php';
        } elseif ($_SESSION['sesion_active']['tipo_usuario'] == 'INSTRUCTOR') {
          require 'menus/Instructor.php';
        } elseif ($_SESSION['sesion_active']['tipo_usuario'] == 'APRENDIZ') {
          require 'menus/Aprendiz.php';
        }
        ?>
        <!--FIN SIDEBAR DINAMICO-->

        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

          <!-- Main Content -->
          <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

              <!-- Sidebar Toggle (Topbar) -->
              <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
              </button>

              <!-- Topbar Search -->
              <div class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                <!--<div class="input-group">
                  <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-primary" type="button">
                      <i class="fas fa-search fa-sm"></i>
                    </button>
                  </div>
                </div>-->
                <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
              </div>

              <!-- Topbar Navbar -->
              <ul class="navbar-nav ml-auto">

                <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                <li class="nav-item dropdown no-arrow d-sm-none">
                  <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-search fa-fw"></i>
                  </a>
                  <!-- Dropdown - Messages -->
                  <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                    <form class="form-inline mr-auto w-100 navbar-search">
                      <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                          <button class="btn btn-primary" type="button">
                            <i class="fas fa-search fa-sm"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </li>

                <!-- Nav notificaiones de solicitudes -->
                    <input type="hidden" id="validarTipoUsuario" value="<?php echo $_SESSION['sesion_active']['tipo_usuario']?>">
                    <li id="li-notification-admin" class="nav-item dropdown no-arrow mx-1">
                      <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            
                      </a>
                      <!-- Dropdown - Alerts -->
                      <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown"  style="width:200px; height:300px; overflow: scroll;">
                        <h6 class="dropdown-header">
                          Notificaciones
                        </h6>
                        <div id="divPersonName" >
                             
                        </div>
                       
                        <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
                      </div>
                    </li>
               
                
                
                    <li id="li-notification-herramentro" class="nav-item dropdown no-arrow mx-1">
                      <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            
                      </a>
                      <!-- Dropdown - Alerts -->
                      <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown"  style="width:300px; height:400px; overflow: scroll;">
                        <h6 class="dropdown-header">
                          Notificaciones del herramentero
                        </h6>
                        <div id="divPersonName2" >
                             
                        </div>
                       
                        <a class="dropdown-item text-center small text-gray-500" data-toggle="modal" data-target="#exampleModalCenter">Show All Alerts</a>
                      </div>
                    </li>
               
                <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <table class="table table-bordered table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th>Cantidad</th>
                                    <th>Fecha</th>
                                    <th>Estado</th>
                                    <th>Rechazo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $contador = 1;
                                foreach ($datos['HerramientasSolicitadas'] as $HerramientasSolicitadas) : ?>
                                    <tr>
                                        <th scope="row"><?php echo $contador++; ?></th>
                                        <td><?php echo $HerramientasSolicitadas->codigo; ?></td>
                                        <td><?php echo $HerramientasSolicitadas->nombre; ?></td>
                                        <td><?php echo $HerramientasSolicitadas->descripcion; ?></td>
                                        <td><?php echo $HerramientasSolicitadas->cantidad; ?></td>
                                        <td><?php echo $HerramientasSolicitadas->fecha; ?></td>
                                        
                                        <?php if($HerramientasSolicitadas->estado == 0): ?>
                                            <td><?php echo "Rechazado"; ?></td>
                                        <?php elseif($HerramientasSolicitadas->estado == 2): ?>
                                            <td><?php echo "Ingreso";?></td>
                                        <?php elseif($HerramientasSolicitadas->estado == 3): ?>
                                            <td><?php echo "Entregado"?></td>
                                        <?php else : ?>
                                            <td><?php echo "Devuelta" ?></td>
                                        <?php endif ?>
                                        <td><?php echo $HerramientasSolicitadas->motivoRechazo; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- Nav Item - Messages 
                <li class="nav-item dropdown no-arrow mx-1">
                  <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-envelope fa-fw"></i>
                    
                    <span class="badge badge-danger badge-counter">7</span>
                  </a>

                  <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                    <h6 class="dropdown-header">
                      Message Center
                    </h6>
                    <a class="dropdown-item d-flex align-items-center" href="#">
                      <div class="dropdown-list-image mr-3">
                        <img class="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="">
                        <div class="status-indicator bg-success"></div>
                      </div>
                      <div class="font-weight-bold">
                        <div class="text-truncate">Hi there! I am wondering if you can help me with a problem I've been having.</div>
                        <div class="small text-gray-500">Emily Fowler · 58m</div>
                      </div>
                    </a>
                    <a class="dropdown-item d-flex align-items-center" href="#">
                      <div class="dropdown-list-image mr-3">
                        <img class="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="">
                        <div class="status-indicator"></div>
                      </div>
                      <div>
                        <div class="text-truncate">I have the photos that you ordered last month, how would you like them sent to you?</div>
                        <div class="small text-gray-500">Jae Chun · 1d</div>
                      </div>
                    </a>
                    <a class="dropdown-item d-flex align-items-center" href="#">
                      <div class="dropdown-list-image mr-3">
                        <img class="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="">
                        <div class="status-indicator bg-warning"></div>
                      </div>
                      <div>
                        <div class="text-truncate">Last month's report looks great, I am very happy with the progress so far, keep up the good work!</div>
                        <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                      </div>
                    </a>
                    <a class="dropdown-item d-flex align-items-center" href="#">
                      <div class="dropdown-list-image mr-3">
                        <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="">
                        <div class="status-indicator bg-success"></div>
                      </div>
                      <div>
                        <div class="text-truncate">Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</div>
                        <div class="small text-gray-500">Chicken the Dog · 2w</div>
                      </div>
                    </a>
                    <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
                  </div>
                </li> -->

                <div class="topbar-divider d-none d-sm-block"></div>
                
                <!-- Condicion sesion imagen -->
                    <?php
                    if ($_SESSION['sesion_active']['tipo_usuario'] == 'ADMINISTRADOR') {
                    //Nav Item - User Information
                    ?>
                        <li class="nav-item dropdown no-arrow">
                          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                              <strong>
                                <?php echo $_SESSION['sesion_active']['nombre'] . " " .  $_SESSION['sesion_active']['p_apellido'] . " " .  $_SESSION['sesion_active']['s_apellido']; ?>
                              </strong>
                              <div class="widget-subheading"><?php echo $_SESSION['sesion_active']['tipo_usuario']; ?></div>
                            </span>
                            <img class="img-profile rounded-circle" src="<?php echo URL_SISINV; ?>images/administrador.png">
                          </a>
                          <!-- Dropdown - User Information -->
                          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                              Perfil
                            </a>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#CambioContrasenia">
                              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                              Cambiar Contraseña
                            </a>
                            <a class="dropdown-item" href="<?php echo URL_SISINV; ?>Persona/ListarAdministrador">
                              <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                              Registrar Usuarios
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                              Cerrar Sesión
                            </a>
                          </div>
                        </li>
                    <?php
                    } elseif ($_SESSION['sesion_active']['tipo_usuario'] == 'HERRAMENTERO') {
                    ?>
                        <li class="nav-item dropdown no-arrow">
                          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                              <strong>
                                <?php echo $_SESSION['sesion_active']['nombre'] . " " .  $_SESSION['sesion_active']['p_apellido'] . " " .  $_SESSION['sesion_active']['s_apellido']; ?>
                              </strong>
                              <div class="widget-subheading"><?php echo $_SESSION['sesion_active']['tipo_usuario']; ?></div>
                            </span>
                            <img class="img-profile rounded-circle" src="<?php echo URL_SISINV; ?>images/herramentero.png">
                          </a>
                          <!-- Dropdown - User Information -->
                          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                              Perfil
                            </a>
                            <a class="dropdown-item" href="#">
                              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                              Configuracion
                            </a>
                             <a class="dropdown-item" href="#" data-toggle="modal" data-target="#CambioContrasenia">
                              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                              Cambiar Contraseña
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                              Cerrar Sesión
                            </a>
                          </div>
                        </li>
                    <?
                    } elseif ($_SESSION['sesion_active']['tipo_usuario'] == 'COORDINADOR') {
                    ?>
                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                              <strong>
                                <?php echo $_SESSION['sesion_active']['nombre'] . " " .  $_SESSION['sesion_active']['p_apellido'] . " " .  $_SESSION['sesion_active']['s_apellido']; ?>
                              </strong>
                              <div class="widget-subheading"><?php echo $_SESSION['sesion_active']['tipo_usuario']; ?></div>
                            </span>
                            <img class="img-profile rounded-circle" src="<?php echo URL_SISINV; ?>images/administrador.png">
                          </a>
                          <!-- Dropdown - User Information -->
                          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                              Perfil
                            </a>
                            <a class="dropdown-item" href="#">
                              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                              Configuracion
                            </a>
                             <a class="dropdown-item" href="#" data-toggle="modal" data-target="#CambioContrasenia">
                              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                              Cambiar Contraseña
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                              Cerrar Sesión
                            </a>
                          </div>
                        </li>
                    <?php
                    } elseif ($_SESSION['sesion_active']['tipo_usuario'] == 'INSTRUCTOR') {
                    ?>
                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                              <strong>
                                <?php echo $_SESSION['sesion_active']['nombre'] . " " .  $_SESSION['sesion_active']['p_apellido'] . " " .  $_SESSION['sesion_active']['s_apellido']; ?>
                              </strong>
                              <div class="widget-subheading"><?php echo $_SESSION['sesion_active']['tipo_usuario']; ?></div>
                            </span>
                            <img class="img-profile rounded-circle" src="<?php echo URL_SISINV; ?>images/instructor.png">
                          </a>
                          <!-- Dropdown - User Information -->
                          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                              Perfil
                            </a>
                            <a class="dropdown-item" href="#">
                              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                              Configuracion
                            </a>
                             <a class="dropdown-item" href="#" data-toggle="modal" data-target="#CambioContrasenia">
                              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                              Cambiar Contraseña
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                              Cerrar Sesión
                            </a>
                          </div>
                        </li>
                    <?php
                    } elseif ($_SESSION['sesion_active']['tipo_usuario'] == 'APRENDIZ') {
                    ?>
                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                              <strong>
                                <?php echo $_SESSION['sesion_active']['nombre'] . " " .  $_SESSION['sesion_active']['p_apellido'] . " " .  $_SESSION['sesion_active']['s_apellido']; ?>
                              </strong>
                              <div class="widget-subheading"><?php echo $_SESSION['sesion_active']['tipo_usuario']; ?></div>
                            </span>
                            <img class="img-profile rounded-circle" src="<?php echo URL_SISINV; ?>images/administrador.png">
                          </a>
                          <!-- Dropdown - User Information -->
                          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                              Perfil
                            </a>
                            <a class="dropdown-item" href="#">
                              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                              Configuracion
                            </a>
                             <a class="dropdown-item" href="#" data-toggle="modal" data-target="#CambioContrasenia">
                              <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                              Cambiar Contraseña
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                              Cerrar Sesión
                            </a>
                          </div>
                        </li>
                    <?php
                    }
                ?>
                

              </ul>

            </nav>
            <!-- End of Topbar -->

            <div class="container-fluid">
            <?php
          }



          public function __destruct()
          {
            ?>

            </div>
            <!-- /.container-fluid -->

          </div>
          <!-- End of Main Content -->

          <!-- Footer -->
          <footer class="sticky-footer bg-white">
            <div class="container my-auto">
              <div class="copyright text-center my-auto">
                <span>Copyright &copy; SALA DE PROYECTOS ADSI-26 - SENA/ATLANTICO 2020</span>
              </div>
            </div>
          </footer>
          <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

      </div>
      <!-- End of Page Wrapper -->

      <!-- Scroll to Top Button-->
      <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
      </a>

      <!-- Logout Modal-->
      <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Estas seguro que quieres cerrar sesión?</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <h6>Seleciona "Cerrar Sesión" si quieres finalizar sesion.</h6>
            </div>
            <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
              <a class="btn btn-primary" id="logout" href="<?php echo URL_SISINV; ?>Login/Logout">Cerrar Sesión</a>
            </div>
          </div>
        </div>
      </div>
      
    
    <!-- MODAL VER MATERIALES SOLICITADAS PARA SER APROBADAS POR ADMINISTRADOR -->
    <div class="modal fade modal-request" id="modal-request" data-backdrop="static" data-keyboard="false"  tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content table-responsive">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">SOLICITUD</h5>
                </div>
                <div class="modal-body">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <div class="row">
                                <div class="col-md-11">
                                    <br>
                                    <h6 class="m-0 font-weight-bold text-primary">LISTADO DE ELEMENTOS DE LA SOLICITUD</h6>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Código</th>
                                            <th>Nombre</th>
                                            <th>Descripción</th>
                                            <th>Cantidad</th>
                                            <th>Fecha</th>
                                            <th>Tipo</th>
                                            <th>Persona</th>
                                            <th>Ficha</th>
                                            <th>Rubro</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody-request">
                                    </tbody>
                                </table>
                            </div>
                            
    
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" onclick="clearRequest()" data-dismiss="modal">Cancelar</button>
                    <button id="btn-accept-request" class="btn btn-primary" type="button">Aceptar</button>
                    <button id="btn-reject-admin"  class="btn btn-danger" type="button" data-toggle="modal" data-target="#ModalRechazo">Rechazar</button>
                </div>
            </div>
        </div>
    </div>
    
     <!-- MODAL VER HERRAMIENTAS APROBADAS- HERRAMENTERO-->
    <div class="modal fade modal-request2" id="modal-request2" data-backdrop="static" data-keyboard="false"  tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">SOLICITUD</h5>
                </div>
                <div class="modal-body">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <div class="row">
                                <div class="col-md-11">
                                    <br>
                                    <h6 class="m-0 font-weight-bold text-primary">LISTADO DE ELEMENTOS DE LA SOLICITUD</h6>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Código</th>
                                            <th>Nombre</th>
                                            <th>Descripción</th>
                                            <th>Cantidad</th>
                                            <th>Fecha</th>
                                            <th>Tipo</th>
                                            <th>Persona</th>
                                            <th>Ficha</th>
                                            <th>Rubro</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody-request2">
                                    </tbody>
                                </table>
    
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-12">
                            <button class="btn btn-secondary" type="button" onclick="clearRequest()" data-dismiss="modal">Cancelar</button>
                            <button id="btn-deliver-request" class="btn btn-primary" type="button">Entregar</button>
                            <button id="btn-reject-request"  class="btn btn-danger" type="button" data-toggle="modal" data-target="#ModalRechazo">Rechazar</button>
                        </div>
                        <!--<div class="col-12">
                            <div class="collapse static" id="collapseExample">
                              <div class="card card-body">
                                
                              </div>
                            </div>
                        </div>-->
                    </div>
                    
                </div>
                
                
            </div>
        </div>
    </div>
    
    <!-- MODAL DESCRIPCION RECHAZAR -->
    <div class="modal fade mt-3" id="ModalRechazo" tabindex="-2" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Motivo de Rechazo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <input class="form-control input-lg" placeholder="Motivo de rechazo" type="text" id="rejectModal" required>    
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger " id="rechazo">Enviar</button>
                    <button type="button" class="btn btn-secondary" id="close" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    
    
    <!-- MODAL CAMBIO DE CONTRASEÑA POR PEDIDO -->
     <div class="modal fade" id="CambioContrasenia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">CAMBIAR CONTRASEÑA</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" type="reset">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                
                        <div id="div-password">
                            <div class="form-group">
                               <label>Nueva contraseña</label>
                               <input class="form-control input-lg" placeholder="Ingresa la nueva contraseña" type="password" id="password1" required>
                            </div>

                            <div class="form-group">
                              <label>Repetir nueva contraseña</label>
                              <input class="form-control input-lg" placeholder="Repite la nueva contraseña" type="password"  id="password2" required>
                            </div>

                            <p class="mb-4" style="color: #FF0000" id = "campo"></p>
                            <button class="btn btn-primary mt-2" id="BtnCambioContraseniaActual" type="button">Cambiar contraseña</button>
                        </div>
                  
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
        
     <!-- Bootstrap core JavaScript-->
     <script src="<?php echo URL_SISINV; ?>MATERIAL_THEME/vendor/jquery/jquery.min.js"></script>
      <script src="<?php echo URL_SISINV; ?>MATERIAL_THEME/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- Core plugin JavaScript-->
      <script src="<?php echo URL_SISINV; ?>MATERIAL_THEME/vendor/jquery-easing/jquery.easing.min.js"></script>
      <!-- Latest compiled and minified JavaScript -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
      <!-- Custom scripts for all pages-->
      <script src="<?php echo URL_SISINV; ?>MATERIAL_THEME/js/sb-admin-2.min.js"></script>
      <!-- SweetAlert2 -->
      <link rel="stylesheet" href="<?php echo URL_SISINV; ?>public/plugins/sweetalert2.min.css">
      <script src="<?php echo URL_SISINV; ?>public/plugins/sweetalert2.all.min.js"></script>
      <!-- Optional: include a polyfill for ES6 Promises for IE11 -->
      <script src="//cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>
      <!-- Page level plugins -->
      <script src="<?php echo URL_SISINV; ?>MATERIAL_THEME/vendor/chart.js/Chart.min.js"></script>
      <script src="<?php echo URL_SISINV; ?>public/MATERIAL_THEME/vendor/bootstrap/js/alertas.js"></script>
      <script src="<?php echo URL_SISINV ?>js/alerts.js"></script>
      <!-- Page level custom scripts -->
      <script src="<?php echo URL_SISINV; ?>MATERIAL_THEME/js/demo/chart-area-demo.js"></script>
      <script src="<?php echo URL_SISINV; ?>MATERIAL_THEME/js/demo/chart-pie-demo.js"></script>
      <!--<script src="<?php echo URL_SISINV; ?>MATERIAL_THEME/vendor/datatables/jquery.dataTables.min.js"></script>
      <script src="<?php echo URL_SISINV; ?>MATERIAL_THEME/vendor/datatables/dataTables.bootstrap4.min.js"></script>
       Page level custom scripts 
      <script src="<?php echo URL_SISINV; ?>MATERIAL_THEME/js/demo/datatables-demo.js"></script>-->
      <script src="<?php echo URL_SISINV; ?>js/datatables-net/datatables.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.colVis.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
      <script type="text/javascript" src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
      <script type="text/javascript">
            $('#BtnCambioContraseniaActual').click(function(){
                CambioContraseniaActual()
            })
            
            function CambioContraseniaActual(){
                var password1 = $('#password1').val().trim();
                var password2 = $('#password2').val().trim();
                if(password1 === "" || password2 ===""){
                    FillData()
                }else if (password1 === password2){
                    $.ajax({
                         url: '<?php echo URL_SISINV ?>Login/CambioContraseniaActual',
                         type: 'POST',
                         data: {
                            password : password1
                         }
                    }).done(function(){
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Cambio de contraseña exitoso',
                            ShowConfirmButton: false,
                            timer: 3000
                        })   
                        setTimeout(function() {
                            window.location.href = '<?php echo URL_SISINV ?>Home/index';
                        }, 3000);
                        //location.reload() Recargar pagina
                        }).fail(response =>{
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Hubo un error',
                            ShowConfirmButton: false,
                            timer: 3000
                        })   
                        setTimeout(function() {
                            window.location.href = '<?php echo URL_SISINV ?>Home/index';
                        }, 3000);
                     })
                }else{
                    Swal.fire({
                         position: 'center',
                         icon: 'warning',
                         title: 'Las contraseñas no coinciden',
                         ShowConfirmButton: false,
                         timer: 2000
                    })
                }
           }
      </script>
    </body>

    </html>
    
    <script type="text/javascript">
        $(document).ready(function(){
            let tipoUsuario = $("#validarTipoUsuario").val()
            if(tipoUsuario === 'ADMINISTRADOR'){
                $('#li-notification-herramentro').hide()
                $('#btn-deliver-request').hide()
                $('#li-notification-admin').show()
                $('#btn-accept-request').show()
                $('#btn-reject-admin').show()
            
                
            }else if(tipoUsuario === 'HERRAMENTERO'){
                $('#li-notification-admin').hide()
                $('#btn-accept-request').hide()
                $('#btn-reject-request').show()
                $('#li-notification-herramentro').show()
                 $('#btn-deliver-request').show()
            }else{
                $('#li-notification-admin').hide()
                $('#li-notification-herramentro').hide()
            }
            
            function mayus(e) {
                e.value = e.value.toUpperCase();
            }
            
            const showRequest = () =>{
                $.ajax({
                url:'<?php echo URL_SISINV; ?>/Login/ShowRequest',
                type:'POST'
                }).done(response =>{
                    var data = JSON.parse(response)
                    var divPersonName = document.getElementById('divPersonName')
                    
                    var hash = {}
                    idRequestTransformed = data.filter(function(current) {
                        var exists = !hash[current.id_solicitud]
                        hash[current.id_solicitud] = true
                        return exists
                    })
                    
                    let numberNotification = idRequestTransformed.length
                    // numero de la notificacion
                    document.getElementById('alertsDropdown').innerHTML = `
                         <i class="fas fa-bell fa-fw fa-4x"></i>
                        <span id="span-tonification" style="font-size: 22px;" class="badge badge-danger badge-counter">${numberNotification}+</span>
                    `
                    // ingresar las notificaciones
                    if(idRequestTransformed.length === 0){
                         divPersonName.innerHTML += `
                         <p class="pt-4 pl-3">No hay Notificaciones de solicitud</p>
                     `
                    }
                    idRequestTransformed.forEach(notificactions =>{
                        divPersonName.innerHTML += `
                        <a id="${notificactions.id_solicitud}" class="click dropdown-item d-flex align-items-center" onclick="getIdRequest('${notificactions.id_solicitud}')">
                            <div class="mr-3">
                                <div class="icon-circle bg-primary">
                                  <i class="fas fa-file-alt text-white"></i>
                                </div>
                            </div>
                            <div >
                                <div class="small text-gray-500">Abril, 2022</div>
                                <span class="font-weight-bold">${notificactions.persona} ha hecho una solicitud de ${notificactions.tipo}</span>
                            </div>
                        </a>
                        `
                    })
                })
            }
            showRequest()
    
        })
            // Aqui se muestran en la tabla
            function getIdRequest(id){
            $('#modal-request').modal('show')
            $('#modal-request').modal({backdrop: 'static', keyboard: true})
            
            $.ajax({
                url:'<?php echo URL_SISINV; ?>/Login/getRequest',
                data: {id: id},
                type: 'POST'
            }).done(response =>{
                var data = JSON.parse(response)
                paintRequest(data)
                
            })
            
            const paintRequest = data => {
                const tbodyRequest = document.getElementById('tbody-request')
                let counter = 0
                data.forEach(request =>{
                    counter= counter + 1
                    tbodyRequest.innerHTML += `
                        <tr>
                            <th>${counter}</th>
                            <td>${request.codigo}</td>
                            <td>${request.nombre}</td>
                            <td>${request.descripcion}</td>
                            <td>${request.cantidad}</td>
                            <td>${request.fecha}</td>
                            <td>${request.tipo}</td>
                            <td>${request.persona}</td>
                            <td>${request.ficha}</td>
                            <td>${request.rubro}</td>
                            
                        </tr>
                    `
                })
                accepptRequest(data)
                rejectRequestAdmin(data)
                
            }
        }
        
             // aceppt request admin
            const accepptRequest = data =>{
                
               let wasSent = true
                $("#btn-accept-request").click(function(){
                    data.forEach(request =>{
                        let identificacion = request.identificacion
                        let tipo = request.tipo
                        let cantidad= request.cantidad
                        let codigo = request.codigo
                        let idRequest = request.id_solicitud
                    
                        $.ajax({
                            url: '<?php echo URL_SISINV; ?>/Login/cantidades',
                            type: 'POST',
                            data:{
                                id: identificacion,
                                tipo: tipo
                            }
                        }).done(response =>{
                            // se trae la cantidad para enviarla como stock y luego restarla
                            let stock = JSON.parse(response)
                            
                            stock.forEach(element =>{
                               let cantidades = element.cantidad
                               $.ajax({
                               url: '<?php echo URL_SISINV; ?>/Login/sendRequest',
                               type: 'POST',
                               data:{
                                   identificacion: identificacion,
                                   stock: cantidades,
                                   tipo: tipo,
                                   cantidad:cantidad,
                                   codigo: codigo,
                                   idRequest: idRequest
                                }
                               }).done(response =>{
                                   wasSent = true
                               }).fail(response =>{
                                   Swal.fire({
                                        position: 'center',
                                        icon: 'error',
                                        title: 'No se pudo enviar la solicitud',
                                        showConfirmButton: false,
                                        timer: 6000
                                    })
                                    setTimeout(function() {
                                        window.location.href = '<?php echo URL_SISINV ?>Home/index';
                                    }, 3000);
                                })
                            })
                            
                        })
                    })
                    
                   if(wasSent === true){
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Solicitud aceptada',
                            showConfirmButton: false,
                            timer: 3000
                        })
                        setTimeout(function() {
                            window.location.href = '<?php echo URL_SISINV ?>Home/index';
                        }, 3000);
                    }
                })
                
            }
            
            
            // funciones del herramentero
            
            const getRequestApproved = () =>{
                $.ajax({
                    url: '<?php echo URL_SISINV; ?>/Login/getRequestApproved',
                    type: 'POST',
                }).done(response =>{
                    let data = JSON.parse(response)
                    var divPersonName2 = document.getElementById('divPersonName2')
                    var hash = {}
                    idRequestTransformed2 = data.filter(function(current) {
                        var exists = !hash[current.id_solicitud]
                        hash[current.id_solicitud] = true
                        return exists
                    })
                    
                    let numberNotification2 = idRequestTransformed2.length
                    // numero de la notificacion
                 
                        document.getElementById('alertsDropdown2').innerHTML = `
                             <i class="fas fa-bell fa-fw fa-4x"></i>
                            <span id="span-tonification" style="font-size: 22px;" class="badge badge-danger badge-counter">${numberNotification2}+</span>
                        `
                    
                    
                    // ingresar las notificaciones
                    if(idRequestTransformed2.length === 0){
                         divPersonName2.innerHTML += `
                         <p class="pt-4 pl-3">No hay Notificaciones de solicitud</p>
                     `
                    }
                    idRequestTransformed2.forEach(notificaction =>{
                        divPersonName2.innerHTML += `
                        <a id="${notificaction.id_solicitud}" class="dropdown-item d-flex align-items-center" onclick="getIdRequest2('${notificaction.id_solicitud}')">
                            <div class="mr-3">
                                <div class="icon-circle bg-primary">
                                  <i class="fas fa-file-alt text-white"></i>
                                </div>
                            </div>
                            <div >
                                <div class="small text-gray-500">December 12, 2019</div>
                                <span class="font-weight-bold">${notificaction.persona} ha hecho una solicitud de ${notificaction.tipo}</span>
                            </div>
                        </a>
                        `
                       
                    })
                })
            }
            getRequestApproved()
       
        
        function getIdRequest2(id){
            $('#modal-request2').modal('show')
            $('#modal-request2').modal({backdrop: 'static', keyboard: true})
            
            $.ajax({
                url:'<?php echo URL_SISINV; ?>/Login/getRequest',
                data: {id: id},
                type: 'POST'
            }).done(response =>{
                var data = JSON.parse(response)
                paintRequest(data)
                
            })
            
            const paintRequest = data => {
                const tbodyRequest = document.getElementById('tbody-request2')
                let counter = 0
                data.forEach(request =>{
                    counter= counter + 1
                    tbodyRequest.innerHTML += `
                        <tr>
                            <th>${counter}</th>
                            <td>${request.codigo}</td>
                            <td>${request.nombre}</td>
                            <td>${request.descripcion}</td>
                            <td>${request.cantidad}</td>
                            <td>${request.fecha}</td>
                            <td>${request.tipo}</td>
                            <td>${request.persona}</td>
                            <td>${request.ficha}</td>
                            <td>${request.rubro}</td>
                        </tr>
                    `
                })
                deliverRequest(data)
                rejectRequest(data)
            }
        }
            const deliverRequest = data =>{
                
                let wasSent = true
                $("#btn-deliver-request").click(function(){
                    data.forEach(request =>{
                        let identificacion = request.identificacion
                        let tipo = request.tipo
                        let cantidad= request.cantidad
                        let codigo = request.codigo
                        let idRequest = request.id_solicitud
                        
                        // se consulta la cantidad de dependiendo del tipo
                        $.ajax({
                            url: '<?php echo URL_SISINV; ?>/Login/cantidades',
                            type: 'POST',
                            data:{
                                id: identificacion,
                                tipo: tipo
                            }
                        }).done(response =>{
                            // se trae la cantidad para enviarla como stock y luego restarla
                            let stock = JSON.parse(response)
                            
                            stock.forEach(element =>{
                               let cantidades = element.cantidad
                               $.ajax({
                               url: '<?php echo URL_SISINV; ?>/Login/deliverRequestHerramentero',
                               type: 'POST',
                               data:{
                                   identificacion: identificacion,
                                   stock: cantidades,
                                   tipo: tipo,
                                   cantidad:cantidad,
                                   codigo: codigo,
                                   idRequest: idRequest
                                }
                               }).done(response =>{
                                   wasSent = true
                               }).fail(response =>{
                                   Swal.fire({
                                        position: 'center',
                                        icon: 'error',
                                        title: 'No se pudo enviar la solicitud',
                                        showConfirmButton: false,
                                        timer: 6000
                                    })
                                    setTimeout(function() {
                                        window.location.href = '<?php echo URL_SISINV ?>Home/index';
                                    }, 3000);
                                })
                            })
                            
                        })
                    })
                    
                   if(wasSent === true){
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Solicitud aceptada',
                            showConfirmButton: false,
                            timer: 3000
                        })
                        setTimeout(function() {
                            window.location.href = '<?php echo URL_SISINV ?>Home/index';
                        }, 3000);
                    }
                })
                
            }
            
            // REJECT REQUEST ADMIN
            const rejectRequestAdmin = data =>{
                $("#btn-reject-admin").click(function(){
                    $("#modal-request").hide();
                    
                    $("#ModalRechazo").show();
                    
                    /*$("#rejectModal").keyup(function(event) {
                        data.forEach(request =>{
                            let rejectModal = $('#rejectModal').val().trim();
                            let id_solicitud = request.id_solicitud;
                            if (rejectModal == "" || id_solicitud == "" ){
                                    FillData();
                            } else if (event.keyCode === 13) {
                                
                                $.ajax({
                                    $("#rechazo").click();
                                }).done(function(){
                                    
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        title: 'Solicitud rechazada',
                                        showConfirmButton: false,
                                        timer: 3000
                                    })
                                    setTimeout(function() {
                                    window.location.href = '<?php echo URL_SISINV ?>Home/index';
                                    }, 3000);
                                })
                            }
                        })
                    });*/
                    /*$("#").click(function(let wasSent = true){*/
                    $("#rechazo").click(function(){ /*#btn-reject-request*/
                        var rejectModal = $('#rejectModal').val().trim();
                        
                        data.forEach(request =>{
                            let rejectModal = $('#rejectModal').val().trim();
                            let id_solicitud = request.id_solicitud;
                            
                            if (rejectModal == "" || id_solicitud == "" ){
                                FillData();
                            } else {
                                $.ajax({
                                    url: '<?php echo URL_SISINV; ?>/Login/Rechazarsolicitud',
                                    type: 'POST',
                                    data:{
                                        id_solicitud : id_solicitud,
                                        rejectModal: rejectModal,
                                        estado: 4
                                        
                                    }
                                }).done(function(){
                                    
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        title: 'Solicitud rechazada',
                                        showConfirmButton: false,
                                        timer: 3000
                                    })
                                    setTimeout(function() {
                                    window.location.href = '<?php echo URL_SISINV ?>Home/index';
                                    }, 3000);
                                    
                                })
                            }
                        })
                        
                    })
                    $("#close").click(function(){
                        $("#modal-request").show();
                    });
                })
            }
            // REJECT REQUEST HERRAMENTERO
            const rejectRequest = data =>{
                $("#btn-reject-request").click(function(){
                    $("#modal-request2").hide();
                    $("#ModalRechazo").show();
                    $("#rejectModal").keyup(function(event) {
                        if (event.keyCode === 13) {
                            $("#rechazo").click();
                        }
                    });
                    /*$("#").click(function(let wasSent = true){*/
                    $("#rechazo").click(function(){ /*#btn-reject-request*/
                        var rejectModal = $('#rejectModal').val().trim();
                        
                        data.forEach(request =>{
                            let rejectModal = $('#rejectModal').val().trim();
                            let id_solicitud = request.id_solicitud
                            if (rejectModal == "" || id_solicitud == "" ){
                                FillData();
                            } else {
                                $.ajax({
                                    url: '<?php echo URL_SISINV; ?>/Login/Rechazarsolicitud',
                                    type: 'POST',
                                    data:{
                                        id_solicitud : id_solicitud,
                                        rejectModal: rejectModal,
                                        estado: 4
                                        
                                    }
                                }).done(function(){
                                    
                                    Swal.fire({
                                        position: 'center',
                                        icon: 'success',
                                        title: 'Solicitud rechazada',
                                        showConfirmButton: false,
                                        timer: 3000
                                    })
                                    setTimeout(function() {
                                    window.location.href = '<?php echo URL_SISINV ?>Home/index';
                                    }, 3000);
                                    
                                })
                            }
                        })
                        
                    })
                    $("#close").click(function(){
                        $("#modal-request2").show();
                    });
                })
            }
            function clearRequest(){
              location.reload();
            }
            
            // funcion para eliminar el session storage
            $('#logout').click(function(){
                if(sessionStorage.getItem('carrito') || sessionStorage.getItem('carrito2') || sessionStorage.getItem('carrito3') || sessionStorage.getItem('carrito4') || sessionStorage.getItem('carrito5')){
                    // solicitudes
                    sessionStorage.removeItem('carrito')
                    sessionStorage.removeItem('carrito2')
                    sessionStorage.removeItem('carrito3')
                    // devoliciones
                    sessionStorage.removeItem('carrito4')
                    sessionStorage.removeItem('carrito5')
                }
            })
            
            
        
    </script>
<?php
          }
        }
?>












