<?php 

	Class Herramienta Extends Controlador{

		public function __construct(){
		    if(!isset($_SESSION['sesion_active'])):
		        header('location:' . URL_SISINV . 'Login/Logout');
		    endif;
		    
			$this->HerramientaModel = $this->modelo('HerramientaModel');
			$this->RegionalModel = $this->modelo('RegionalModel');
		}

		public function ListarHerramienta(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    
			$ListarRegional = $this->RegionalModel->ListarRegional();
			$ListarHerramienta = $this->HerramientaModel->ListarHerramienta($idSede);
			$datos=[
				'ListarHerramienta' => $ListarHerramienta,
				'ListarReginal' => $ListarRegional
			];
			$this->vista('inventario/herramientas/ListarHerramienta', $datos);
		}
		
		// registrar las herramientas
		public function RegistrarHerramienta(){
		    $datos = [
		        'rubroPresupuestal' => trim($_POST['rubroPresupuestal']),
		        'herramientaGaveta' => trim($_POST['herramientaGaveta']),
		        'herramientaDescripcion' => trim($_POST['herramientaDescripcion']),
		        'herramientaCantidad'=> trim($_POST['herramientaCantidad']),
		        'herramientaCodigo'=> trim($_POST['herramientaCodigo']),
		        'herramientaNombre'=> trim($_POST['herramientaNombre'])
		    ];
		    $this->HerramientaModel->RegistrarHerramienta($datos);       
		}
		
		public function ObtenerHerramienta($idHerramienta) {
		    // Get data
		    $result = $this->HerramientaModel->ObtenerHerramienta($idHerramienta);
		    $datos = [
		        'idHerramienta' => $idHerramienta,
		        'herraminetaDescripcion' => $result->tbl_herramienta_DESCRIPCION,
		        'herramientaFechadeAdquision' => $result->tbl_herramienta_FECHA,
		        'rubroPresupuestal' => $result->tbl_herramienta_RUBRO,
		        'herramientaCodigo' => $result->tbl_herramienta_CODIGO
		    ];
		    // sent data to the views
		    $this->vista('inventario/herramientas/EditarHerramienta', $datos);
		}
		
		public function EditarHerramientaNombre($idHerramienta) {
		    // Get data
		    $ListarGaveta = $this->HerramientaModel->ObtenerGaveta($idHerramienta);
		    $herramienta = $this->HerramientaModel->ObtenerHerramienta($idHerramienta);
		    $datos = [
		        'idHerramienta' => $idHerramienta,
		        'ListarGaveta' => $ListarGaveta,
		        'herraminetaDescripcion' => $herramienta->tbl_herramienta_DESCRIPCION
		    ];
		    // sent data to the views
		    $this->vista('inventario/herramientas/EditarHerramientaNombre', $datos);
		}

		public function EliminarHerramienta($idHerramienta){
			$herramienta = $this->HerramientaModel->ObtenerHerramienta($idHerramienta);
			$datos = [
				'idHerramienta' => $idHerramienta,
				'herraminetaDescripcion' => $herramienta->tbl_herramienta_DESCRIPCION,
				'herramientaCantidad' => $herramienta->tbl_herramienta_CANTIDAD
			];
			$this->vista('inventario/herramientas/EliminarHerramienta', $datos);
		}

		public function DeleteHerramienta(){
			$idHerramienta = trim($_POST['idHerramienta']);
			$idSede = $_SESSION['sesion_active']['idSede'];
			$this->HerramientaModel->DeleteHerramienta($idHerramienta, $idSede);
		}
		
		public function CompararHerramienta(){
		    $herramientaGaveta = trim($_POST['herramientaGaveta']);
		    $result = $this->HerramientaModel->CompararHerramienta($herramientaGaveta);
		    echo json_encode($result);
		}
		
		public function CompararHerramienta2(){
		    $herramientaCodigo = trim($_POST['herramientaCodigo']);
		    $result = $this->HerramientaModel->CompararHerramienta2($herramientaCodigo);
		    echo json_encode($result);
		}
		
		public function EditarHerramienta() {
		    $datos = [
		        'idHerramienta' => trim($_POST['idHerramienta']),
		        'herramientaCantidad' => trim($_POST['herramientaCantidad']),
		        'herramientaFechadeAdquision' => trim($_POST['herramientaFechadeAdquision']),
		        'rubroPresupuestal' => trim($_POST['rubroPresupuestal']),
		         'herramientaCodigo' => trim($_POST['herramientaCodigo'])
		    ];
		    $this->HerramientaModel->EditarHerramienta($datos);
		}
		
		public function EditarHerramienta2() {
		    $datos = [
		        'herramientaGaveta' => trim($_POST['herramientaGaveta']),
		        'idHerramienta' => trim($_POST['idHerramienta']),
		        'herraminetaDescripcion' => trim($_POST['herraminetaDescripcion'])
		    ];
		    $this->HerramientaModel->EditarHerramienta2($datos);
		}

        // Load gavetas 
	    public function LoadGavetas(){
	        $idEstante = $_POST['estanteID'];
	        $result = $this->HerramientaModel->LoadGavetas($idEstante);
	        echo json_encode($result);
	    }
	    
	    public function totalSolicitudesHerramientas(){
	        $idSede =  $_SESSION['sesion_active']['idSede'];
	        $result = $this->HerramientaModel->totalSolicitudesHerramientas($idSede);
	        echo json_encode($result);
	    }
	    
	    public function totalSolicitudesMateriales(){
	        $idSede =  $_SESSION['sesion_active']['idSede'];
	        $result = $this->HerramientaModel->totalSolicitudesMateriales($idSede);
	        echo json_encode($result);
	    }
	    
	    public function totalSolicitudesEquipos(){
	        $idSede =  $_SESSION['sesion_active']['idSede'];
	        $result = $this->HerramientaModel->totalSolicitudesEquipos($idSede);
	        echo json_encode($result);
	    }

		public function totalHerramientas(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
			$result = $this->HerramientaModel->totalHerramientas($idSede);
			echo json_encode($result);
		}

		public function totalMateriales(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
			$result = $this->HerramientaModel->totalMateriales($idSede);
			echo json_encode($result);
		}

		public function totalEquipos(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
			$result = $this->HerramientaModel->totalEquipos($idSede);
			echo json_encode($result);
		}

		public function HerramientasDisponibles(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
			$result = $this->HerramientaModel->HerramientasDisponibles($idSede);
			echo json_encode($result);
		}
		
		public function MaterialesDisponibles(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
			$result = $this->HerramientaModel->MaterialesDisponibles($idSede);
			echo json_encode($result);
		}
		
		public function EquiposDisponibles(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
			$result = $this->HerramientaModel->EquiposDisponibles($idSede);
			echo json_encode($result);
		}
		
		//Herramientas TOP 4
		public function TopHerramientasControllers()
		{
			$herramientas = $this->HerramientaModel->TopHerramientas();
			echo json_encode($herramientas);
		}
	}
?>