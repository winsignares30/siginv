<?php

	Class Estante Extends Controlador{

		public function __construct(){
		    if(!isset($_SESSION['sesion_active'])):
                header('location:' . URL_SISINV . 'Login/Logout');
            endif;
            
			$this->EstanteModel = $this->modelo('EstanteModel');
			$this->RegionalModel = $this->modelo('RegionalModel');
		}

		public function ListarEstante(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    
			$ListarEstante = $this->EstanteModel->ListarEstante($idSede);
			$ListarRegional = $this->RegionalModel->ListarRegional();
			$datos = [
				'ListarEstante' => $ListarEstante,
				'ListarRegional' => $ListarRegional
			];

			$this->vista('configuracion/Estante/ListarEstante', $datos);
		}

		public function RegistrarEstante(){
			$datos = [
				'estanteBodega' => trim($_POST['estanteBodega']),
				'estanteNumero' => trim($_POST['estanteNumero']),
				'estanteDescripcion' => trim($_POST['estanteDescripcion']),
			];
			$this->EstanteModel->RegistrarEstante($datos);
		}

		public function ObtenerEstante($idEstante) {
			$ListarRegional = $this->RegionalModel->ListarRegional();
			$result = $this->EstanteModel->ObtenerEstante($idEstante);
			$datos = [
				'idEstante' => $idEstante,
				'ListarRegional' => $ListarRegional,
				'ListarCentro' => $ListarCentro,
				'ListarSede' => $ListarSede,
				'estanteDescripcion' => $result->tbl_estante_DESCRIPCION,
				'estanteNumero' => $result->tbl_estante_NUMERO
			];
			/*Send data to the view editarAmbiente */
			$this->vista('configuracion/Estante/EditarEstante', $datos);
		}

		public function EditarEstante() {
			$datos = [
				'idEstante' =>$_POST['idEstante'],
				'estanteBodega' =>$_POST['estanteBodega'],
				'estanteNumero' =>$_POST['estanteNumero'],
				'estanteDescripcion' =>$_POST['estanteDescripcion']
			];
			$this->EstanteModel->EditarEstante($datos);
		}

		//first, we have to get the data
		public function EliminarEstante($idEstante){
			$result = $this->EstanteModel->ObtenerEstante($idEstante);
			$datos = [
				'idEstante' => $idEstante,
				'estanteNumero' => $result->tbl_estante_NUMERO,
				'estanteDescripcion' => $result->tbl_estante_DESCRIPCION
			];
			$this->vista('configuracion/Estante/EliminarEstante', $datos);
		}
		//afterward delete
		public function DeleteEstante(){
			$idEstante = trim($_POST['idEstante']);
			//Se agrega la session active de la sede y sus parametros en el call -> $this
			$idSede = $_SESSION['sesion_active']['idSede'];
			$this->EstanteModel->DeleteEstante($idEstante, $idSede);
		}

		public function LoadBodegas() {
			$idSede =  trim($_POST['sedeId']);
			$result = $this->EstanteModel->LoadBodegas($idSede);
			echo json_encode($result);
		}

		// verificar si existe el centro y el subdirector al editar
		public function CompararEstante(){
			$estanteBodega = trim($_POST['estanteBodega']);
			$result = $this->EstanteModel->CompararEstante($estanteBodega);
			echo json_encode($result);  // devuelve objeto json
		}
		
		public function IfDataExist(){
		    $idEstante = trim($_POST['idEstante']);
			$result = $this->EstanteModel->IfDataExist($idEstante);
			echo json_encode($result);
		}
    }
?>

