<?php

    Class DevolucionEquipos Extends Controlador{
        
        public function __construct(){
            if(!isset($_SESSION['sesion_active'])):
                header('location:' . URL_SISINV . 'Login/Logout');
            endif; 
            $this->EquipoModel = $this->modelo('EquipoModel');
            $this->HerramientaModel = $this->modelo('HerramientaModel');
        }
        
        
        public function devEquipos(){
            
            $datos = [];
            $this->vista('devolucion/equipos/devEquipos', $datos);
            
        }

        public function CantidadEquipos(){
            $id = $_POST['id'];
            $idSede = $_SESSION['sesion_active']['idSede'];
            $result = $this->EquipoModel->CantidadEquipos($id, $idSede);
            echo json_encode($result);
        }
        
        // funcion que solicita las herramientas en estado entregado
        public function devolverHerramienta(){
            $idSede = $_SESSION['sesion_active']['idSede'];
            
            $datos = [
                'idPersona' => $_POST['idPersona'],
                'tipo' => $_POST['tipo']
            ];
            $result = $this->HerramientaModel->devolverHerramienta($datos,$idSede);
            echo json_encode($result);
            
        }

        public function DevolucionEquipos(){
            $datos = [
                'id' => trim($_POST['id']),
                'id2' => trim($_POST['id2']),
                'idPersona' =>trim($_POST['idPersona']),
                'cantidad' => trim($_POST['cantidad']),
                'cantidad2' => trim($_POST['cantidad2']),
                'tipo' => trim($_POST['tipo']),
            ];
            $this->HerramientaModel->DevoHerramientas($datos);
            $this->HerramientaModel->DevolucionHerramientas($datos);
        }
        
    }