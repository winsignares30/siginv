<?php 
	class Centro Extends Controlador{

		public function __construct(){
		    if(!isset($_SESSION['sesion_active'])):
                header('location:' . URL_SISINV . 'Login/Logout');
            endif;
            
			$this->CentroModel = $this-> modelo('CentroModel');
			$this->RegionalModel = $this->modelo('RegionalModel');
		}
		
		public function ListarCentro(){
			$ListarRegional = $this->RegionalModel->ListarRegional();
			$ListarCentro = $this->CentroModel->ListarCentro();
			$datos = [
				'ListarCentro' => $ListarCentro,
				'ListarRegional' => $ListarRegional
			];
			$this->vista('configuracion/Centro/ListarCentro' , $datos);
		}	

		public function RegistrarCentro(){
			$datos = [
				'centroNombre' => trim($_POST['centroNombre']),
				'centroTelefono' => trim($_POST['centroTelefono']),
				'centroSubdirector' => trim($_POST['centroSubdirector']),
				'centroRegional' => trim($_POST['centroRegional'])
			];
			$this->CentroModel->RegistrarCentro($datos);
		}
		public function EditarCentro() {
			$datos = [
				'idCentro' => trim($_POST['idCentro']),
				'centroNombre' => trim($_POST['centroNombre']),
				'centroTelefono' => trim($_POST['centroTelefono']),
				'centroSubdirector' => trim($_POST['centroSubdirector']),
			];
			$this->CentroModel->EditarCentro($datos);
		}
		public function EditarCentro2() {
			$datos = [
				'idCentro' => trim($_POST['idCentro']),
				'centroNombre' => trim($_POST['centroNombre']),
				'centroRegional' => trim($_POST['centroRegional'])
			];
			$this->CentroModel->EditarCentro2($datos);
		}

		public function ObtenerCentroId($idCentro){
			$result = $this->CentroModel->ObtenerCentroId($idCentro);
			$datos = [
				'idCentro' => $idCentro,
				'centroNombre' => $result->tbl_centro_NOMBRE,
				'centroTelefono' => $result->tbl_centro_TELEFONO,
				'centroSubdirector' => $result->tbl_centro_SUBDIRECTOR
			];
			$this->vista('configuracion/Centro/EditarCentro',$datos);
		}
		public function ObtenerCentroNombre($idCentro){
			$ListarRegional = $this->RegionalModel->ListarRegional();
			$result = $this->CentroModel->ObtenerCentroId($idCentro);
			$datos = [
				'idCentro' => $idCentro,
				'centroNombre' => $result->tbl_centro_NOMBRE,
				'ListarRegional' => $ListarRegional
			];
			$this->vista('configuracion/Centro/EditarCentroNombre',$datos);
		}
		//eliminar 
		public function EliminarCentro($idCentro){
			$result = $this->CentroModel->ObtenerCentroID($idCentro);
			$datos = [
				'idCentro' => $idCentro,
				'centroNombre' => $result->tbl_centro_NOMBRE,
				'centroTelefono' => $result->tbl_centro_TELEFONO,
				'centroSubdirector' => $result->tbl_centro_SUBDIRECTOR
			];
			// obtenido los vista del modelo vista 
			$this->vista('configuracion/Centro/EliminarCentro',$datos);
			
		}

		public function DeleteCentro(){
			$this->CentroModel->EliminarCentro($_POST['idCentro']);
		}

		// verificar si existe el centro y el subdirector al editar
		public function CompararCentro(){
			$datos = [
				'centroRegional' => trim($_POST['centroRegional']),
			];
			$result = $this->CentroModel->CompararCentro($datos);
			echo json_encode($result);  // devuelve objeto json
		}

		public function loadCentros(){
			$idRegional = $_POST['regionalID'];
			//instancia al modelo 
			$result = $this->CentroModel->loadCentros($idRegional);
			echo json_encode($result);
		}
		
		public function IfDataExist() {
			$idCentro = trim($_POST['idCentro']);
			$result = $this->CentroModel->IfDataExist($idCentro);
			echo json_encode($result);
		}
	}
?>