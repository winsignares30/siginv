<?php 
	class Ambiente Extends Controlador{

		public function __construct(){
		    if(!isset($_SESSION['sesion_active'])):
                header('location:' . URL_SISINV . 'Login/Logout');
            endif;
            
			$this->AmbienteModel = $this->modelo('AmbienteModel');
			$this->RegionalModel = $this->modelo('RegionalModel');
		}
		// list ambiente
		public function ListarAmbiente(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
		  
			$ListarAmbiente = $this->AmbienteModel->ListarAmbiente($idSede);
			$ListarRegional = $this->RegionalModel->ListarRegional();
			$datos = [
				'ListarAmbientes' => $ListarAmbiente,
				'ListarRegional'=> $ListarRegional
			];
			$this->vista('configuracion/Ambiente/ListarAmbiente', $datos);
		}

		/* Funcion to register ambiente */
		public function RegistrarAmbiente(){
			$datos = [
				'ambienteSede' => trim($_POST['ambienteSede']),
				'ambienteNombre' => trim($_POST['ambienteNombre']),
				'ambienteInstructor' => trim($_POST['ambienteInstructor'])
			];
			$this->AmbienteModel->RegistrarAmbiente($datos);
		}
		
		/*Function to edit */
		public function ObtenerAmbiente($idAmbiente) {
		    $idSede = $_SESSION['sesion_active']['idSede'];
			$ListarInstructor = $this->AmbienteModel->LoadInstructores($idSede);
			$result = $this->AmbienteModel->ObtenerAmbiente($idAmbiente, $idSede);
			$datos = [
				'idAmbiente' => $idAmbiente,
				'ambienteNombre' => $result->tbl_amb_NOMBRE,
				'ambienteInstructor' => $result->tbl_instructor_NOMBRE,
				'ListarInstructor' => $ListarInstructor
			];
			/*Send data to the view editarAmbiente */
			$this->vista('configuracion/Ambiente/EditarAmbiente', $datos);
		}

		public function EditarAmbiente() {
			$datos = [
			    'idSede' => $_SESSION['sesion_active']['idSede'],
				'idAmbiente' =>$_POST['idAmbiente'],
				'ambienteNombre' =>$_POST['ambienteNombre'],
				'ambienteInstructor' =>$_POST['ambienteInstructor']
			];
			$this->AmbienteModel->EditarAmbiente($datos);
		}


		//first, we have to get the data 
		public function EliminarAmbiente($idAmbiente){
		    $idSede = $_SESSION['sesion_active']['idSede'];
			$result = $this->AmbienteModel->ObtenerAmbiente($idAmbiente, $idSede);
			
			$datos = [
				'idAmbiente' => $idAmbiente,
				'ambienteNombre' => $result->tbl_amb_NOMBRE
			];
			$this->vista('configuracion/Ambiente/EliminarAmbiente', $datos);
		}
		//afterward delete
		public function DeleteAmbiente(){
			$idAmbiente = $_POST['idAmbiente'];
			$idSede = $_SESSION['sesion_active']['idSede'];
			$result = $this->AmbienteModel->DeleteAmbiente($idAmbiente, $idSede);
			return $result;
		}

		// Load centros
		public function LoadCentros() {
			$idRegional = $_POST['regionalID'];
			$result = $this->AmbienteModel->LoadCentros($idRegional);
			echo json_encode($result);
		}

		// Load sede
		public function LoadSedes() {
			$idCentro = $_POST['centroID'];
			$result = $this->AmbienteModel->LoadSedes($idCentro);
			echo json_encode($result);
		}

		public function LoadInstructores() {
			$idSede = trim($_POST['sedeID']);
			$result = $this->AmbienteModel->LoadInstructores($idSede);
			echo json_encode($result);
		}

	}			

