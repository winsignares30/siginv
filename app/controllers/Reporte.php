<?php 
    class Reporte extends Controlador{
        
        public function __construct(){
            if(!isset($_SESSION['sesion_active'])):
		        header('location:' . URL_SISINV . 'Login/Logout');
		    endif;
		    $this->ReporteModel = $this->modelo('ReporteModel');
           
        }
        
        public function ImprimirReportes(){
            $idSede = $_SESSION['sesion_active']['idSede'];
            $result = $this->ReporteModel->getInstructors($idSede);
            $datos = [
                'instructores' => $result
            ];
            
            $this->vista('reportes/ImprimirReportes', $datos);
        }
        
        public function imprimirReporteHerramientasSolicitadasEntreDias(){
            $idSede = $_SESSION['sesion_active']['idSede'];
            $fechaInicial = $_POST['fechaInicial'];
            $fechaFinal = $_POST['fechaFinal'];
            $result = $this->ReporteModel->imprimirReporteHerramientasSolicitadasEntreDias($fechaInicial, $fechaFinal, $idSede);
            $datos = [
                'reporteHerramientasSolicitadasEntreDias' => $result
            ];
             $this->vista('reportes/herramientas/reporteSolicitudHerramientas', $datos);
        }
        
        public function imprimirReporteMaterialesSolicitadosEntreDias(){
            $idSede = $_SESSION['sesion_active']['idSede'];
            $fechaInicial = $_POST['fechaInicial'];
            $fechaFinal = $_POST['fechaFinal'];
            $result = $this->ReporteModel->imprimirReporteMaterialesSolicitadosEntreDias($fechaInicial, $fechaFinal, $idSede);
            $datos = [
                'reporteMaterialesSolicitadosEntreDias' => $result
            ];
             $this->vista('reportes/materiales/reporteSolicitudMateriales', $datos);
        }
        
        public function imprimirReporteEquiposSolicitadosEntreDias(){
            $idSede = $_SESSION['sesion_active']['idSede'];
            $fechaInicial = $_POST['fechaInicial'];
            $fechaFinal = $_POST['fechaFinal'];
            $result = $this->ReporteModel->imprimirReporteEquiposSolicitadosEntreDias($fechaInicial, $fechaFinal, $idSede);
            $datos = [
                'reporteEquiposSolicitadosEntreDias' => $result
            ];
             $this->vista('reportes/equipos/reporteSolicitudEquipos', $datos);
        }
        
        public function herramientasPorInstructor(){
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $idSede = $_SESSION['sesion_active']['idSede'];
                $idInstructor = $_POST['select-id_instructor'];
                $fechaInicial = $_POST['fechaInicial'];
                $fechaFinal = $_POST['fechaFinal'];
                
                $result = $this->ReporteModel->reporteSolicitudHerramientaInstructor($fechaInicial, $fechaFinal, $idSede, $idInstructor);
                $datos = [
                  'herramientasPorInstructores' => $result  
                ];
                $this->vista('reportes/herramientas/reporteSolicitudHerramientaInstructor', $datos);
            }
            
        }
        public function materialesPorInstructor(){
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $idSede = $_SESSION['sesion_active']['idSede'];
                $idInstructor = $_POST['select-id_instructor'];
                $fechaInicial = $_POST['fechaInicial'];
                $fechaFinal = $_POST['fechaFinal'];
                
                $result = $this->ReporteModel->reporteSolicitudMaterialesInstructor($fechaInicial, $fechaFinal, $idSede, $idInstructor);
                $datos = [
                  'materialesPorInstructores' => $result  
                ];
                $this->vista('reportes/materiales/reporteSolicitudMaterialesInstructor', $datos);
            }
            
        }
        
        public function equiposPorInstructor(){
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $idSede = $_SESSION['sesion_active']['idSede'];
                $idInstructor = $_POST['select-id_instructor'];
                $fechaInicial = $_POST['fechaInicial'];
                $fechaFinal = $_POST['fechaFinal'];
                
                $result = $this->ReporteModel->reporteSolicitudEquiposInstructor($fechaInicial, $fechaFinal, $idSede, $idInstructor);
                $datos = [
                  'equiposPorInstructores' => $result  
                ];
                $this->vista('reportes/equipos/reporteSolicitudEquiposInstructor', $datos);
            }
            
        }
        
        
        
        
        
        
        

    }
?>