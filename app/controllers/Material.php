<?php 

	Class Material Extends Controlador{

		public function __construct(){
		    if(!isset($_SESSION['sesion_active'])):
                header('location:' . URL_SISINV . 'Login/Logout');
            endif;
			
		    $this->MaterialModel = $this->modelo('MaterialModel');
			$this->RegionalModel = $this->modelo('RegionalModel');
		}

		public function ListarMaterial(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
            $ListarProgramas = $this->MaterialModel->LoadProgramas($idSede); //Dentro de un objeto instanciamos el modelo y su clase incluyendo var si cuenta con ella
		    $ListarMateriales = $this->MaterialModel->ListarMaterial($idSede);
		    $ListarRegional = $this->RegionalModel->ListarRegional();
		    $datos = [
		        'ListarProgramas' => $ListarProgramas, //En un array asignamos la llave y su respectivo valor
		        'ListarMateriales' => $ListarMateriales,
		        'ListarReginal' => $ListarRegional
		    ];
			$this->vista('inventario/materiales/ListarMaterial', $datos);
		}

		public function RegistrarMaterial(){
			$datos = [
			    'materialSede' => trim($_POST['materialSede']),
				'materialCodigo' => trim($_POST['materialCodigo']),
				'materialUnspsc' => trim($_POST['materialUnspsc']),
				'materialDescripcion' => trim($_POST['materialDescripcion']),
				'materialPrograma' => trim($_POST['materialPrograma']),
				'materialCantidad' => trim($_POST['materialCantidad']),
				'materialTipo' => trim($_POST['materialTipo']),
				'materialUnidadMedida' => trim($_POST['materialUnidadMedida']),
				'rubroPresupuestal' => trim($_POST['rubroPresupuestal']),
				'materialDestino' => trim($_POST['materialDestino'])
			];
			$this->MaterialModel->RegistrarMaterial($datos);
		}
		
		public function ObtenerMaterial($idMaterial) {
	    // Get data
		    $result = $this->MaterialModel->ObtenerMaterial($idMaterial);
		    $datos = [
		        'idMaterial' => $idMaterial,
		        'materialCodigo'=> $result->tbl_materiales_CODIGOSENA,
		        'materialUnspsc' => $result->tbl_materiales_UNSPSC,
		        'materialDescripcion'=> $result->tbl_materiales_DESCRIPCION,
		        'materialPrograma' => $result->tbl_materiales_PROGRAMAFORMACION,
		        'materialCantidad' => $result->tbl_materiales_CANTIDAD,
		        'materialTipo' => $result->tbl_materiales_TIPOMATERIAL,
		        'materialUnidadMedida' => $result->tbl_materiales_UNIDADMEDIDA,
		        'rubroPresupuestal' => $result->tbl_materiales_RUBRO,
		        'materialDestino' => $result->tbl_materiales_DESTINO,
		        'materialFecha' => $result->tbl_materiales_FECHA
		    ];
		    // sent data to the views
		    $this->vista('inventario/materiales/EditarMaterial', $datos);
		}
		
		public function EditarMaterial(){
			$datos = [
			    'idSede' =>$_SESSION['sesion_active']['idSede'],
			    'idMaterial' => trim($_POST['idMaterial']),
				'materialUnspsc' => trim($_POST['materialUnspsc']),
				'materialDescripcion' => trim($_POST['materialDescripcion']),
				'materialPrograma' => trim($_POST['materialPrograma']),
				'materialCantidad' => trim($_POST['materialCantidad']),
				'materialTipo' => trim($_POST['materialTipo']),
				'materialUnidadMedida' => trim($_POST['materialUnidadMedida']),
				'rubroPresupuestal' => trim($_POST['rubroPresupuestal']),
				'materialDestino' => trim($_POST['materialDestino']),
				'materialFecha' => trim($_POST['materialFecha'])
			];
			$this->MaterialModel->EditarMaterial($datos);
		}
		public function EditarMaterial2(){
			$datos = [
			    'idMaterial' => trim($_POST['idMaterial']),
				'materialSede' => trim($_POST['materialSede']),
				'materialCodigo' => trim($_POST['materialCodigo'])
			];
			$this->MaterialModel->EditarMaterial2($datos);
		}
		
		public function EditarMaterialCodigo($idMaterial){
		    $result = $this->MaterialModel->ObtenerMaterial($idMaterial);
		    $ListarSede = $this->MaterialModel->ObtenerSede($idMaterial);
		    $datos = [
		        'idMaterial' => $idMaterial,
		        'ListarSede' => $ListarSede,
		        'materialCodigo' => $result->tbl_materiales_CODIGOSENA
		    ];
		    
		    $this->vista('inventario/materiales/EditarMaterialCodigo', $datos);
		}
		
		
		public function CompararMaterial(){
		    $materialSede = trim($_POST['materialSede']);
		    $result = $this->MaterialModel->CompararMaterial($materialSede);
		    echo json_encode($result);
		}
		
		public function CompararMateriales($materialCodigo){
		    $materialCodigo = trim($_POST['materialCodigo']);
		    $result = $this->MaterialModel->CompararMateriales($materialCodigo);
		    echo json_encode($result);
		}

		
		public function EliminarMaterial($idMaterial){
		   $result = $this->MaterialModel->ObtenerMaterial2($idMaterial);
		   $datos = [
		       'idMaterial' => $idMaterial,
		       'materialCodigo' =>$result->tbl_materiales_CODIGOSENA,
		       'materialDescripcion' =>$result->tbl_materiales_DESCRIPCION
		   ];
		   $this->vista('inventario/materiales/EliminarMaterial', $datos);
		}
		
        public function DeleteMaterial(){
            $idSede = $_SESSION['sesion_active']['idSede'];
            $idMaterial = trim($_POST['idMaterial']);
            $this->MaterialModel->EliminarMaterial($idMaterial, $idSede);
        }
	}
?>