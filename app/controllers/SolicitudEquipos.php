<?php

    Class SolicitudEquipos Extends Controlador{
        
        public function __construct(){
            
            if(!isset($_SESSION['sesion_active'])):
                header('location:' . URL_SISINV . 'Login/Logout');
            endif;
            
            $this->EquipoModel = $this->modelo('EquipoModel');
            $this->HerramientaModel = $this->modelo('HerramientaModel');
        }
        
        function generarCodigo(){
            $key = date('m/d/Y g:ia');
            return $key;
        }
        
        public function SolicitarEquipos(){
            $idPersona =  $_SESSION['sesion_active']['cod'];
            $idSede =  $_SESSION['sesion_active']['idSede'];
            $LoadFichas = $this->HerramientaModel->LoadFichas($idSede);
            $result = $this->EquipoModel->MostrarEquiposSolicitados($idSede, $idPersona);
            
            $datos = [
                'LoadFichas' => $LoadFichas,
                'EquiposSolicitados' => $result
            ];
           
            $this->vista('solicitud/equipos/SolicitarEquipos', $datos);
            
        }
        
        public function SolicitarEquipos2(){
            $idSede = $_SESSION['sesion_active']['idSede'];
            $result = $this->EquipoModel->SolicitarEquipos($idSede);
            echo json_encode($result);
        }
        
        public function EnviarEquipos(){
          
            $id_solicitud = $_POST['id_solicitud'];
            //dia-mes-año-hora-minutos-segundos
            //echo date("d-m-Y H:i:s");
            $estado = 2;
            $idPersona =  $_SESSION['sesion_active']['cod'];
            $idSede =  $_SESSION['sesion_active']['idSede'];
            $datos = [
                'id_solicitud' => $id_solicitud,
                'id' => trim($_POST['id']),
                'ficha' => trim($_POST['ficha']),
                'rubro' => trim($_POST['rubro']),
                'codigo' => trim($_POST['codigo']),
                'nombre' => trim($_POST['nombre']),
                'cantidad' => trim($_POST['cantidad']),
                'descripcion' => trim($_POST['descripcion']),
                'stock' => trim($_POST['stock']),
                'tipo' => trim($_POST['tipo']),
                'idSede' => $idSede,
                'idPersona' => $idPersona,
                'nombrePersona' => trim($_POST['nombrePersona']),
                'estado' => $estado
            ];
            
            $this->EquipoModel->EnviarEquipos($datos);
            
        }
        public function verificarCantidad(){
            $datos = [
                'id' => trim($_POST['id']),
                'tipo'=> trim($_POST['tipo'])
            ];
            $result = $this->EquipoModel->verificarCantidad($datos);
            echo json_encode($result);
        }
    }
?>