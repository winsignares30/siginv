<?php 
	class Instructor Extends Controlador{

		public function __construct(){
		    if(!isset($_SESSION['sesion_active'])):
		        header('location:' . URL_SISINV . 'Login/Logout');
		    endif;
		    
			$this->SedeModel = $this->modelo('SedeModel');
			$this->InstructorModel = $this->modelo('InstructorModel');
			$this->RegionalModel = $this->modelo('RegionalModel');
		}

		public function ListarInstructor(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    
			$ListarInstructor = $this->InstructorModel->ListarInstructor($idSede);
			$ListarRegional = $this->RegionalModel->ListarRegional();
			$ListarSede = $this->SedeModel->ListarSede();
			$datos = [
				'ListarInstructor' => $ListarInstructor,
				'ListarSedes' => $ListarSede,
				'ListarRegional' => $ListarRegional
			];
			$this->vista('personal/Instructor/ListarInstructor', $datos);
		}

		public function RegistrarInstructor(){
			$datos = [
				'instructorSedeID' => trim($_POST['instructorSedeID']),
				'instructorNombre' => trim($_POST['instructorNombre']),
				'instructorApellido' => trim($_POST['instructorApellido']),
				'instructorTipoDocumento' => trim($_POST['instructorTipoDocumento']),
				'instructorNumeroDocumento' => trim($_POST['instructorNumeroDocumento']),
				'instructorNumeroTelefono' => trim($_POST['instructorNumeroTelefono']),
				'instructorDirecionCorreo' => trim($_POST['instructorDirecionCorreo']),
				'instructorDirecion' => trim($_POST['instructorDirecion']),
			];
			$this->InstructorModel->RegistrarInstructor($datos);
		}
		
		public function ObtenerInstructorId($idInstructor){
			$ListarSede = $this->InstructorModel->ListarSede($idInstructor);
			$result = $this->InstructorModel->ObtenerInstructorId($idInstructor);
			$datos = [
				'idInstructor' => $idInstructor,
				'ListarSede' => $ListarSede,
				'instructorNombre' => $result->tbl_instructor_NOMBRES,
				'instructorApellido' => $result->tbl_instructor_APELLIDOS,
				'instructorTipoDocumento' => $result->tbl_instructor_TIPODOCUMENTO,
				'instructorNumeroDocumento' => $result->tbl_instructor_NUMDECUMENTO,
				'instructorNumeroTelefono' => $result->tbl_instructor_TELEFONO,
				'instructorDirecionCorreo' => $result->tbl_instructor_CORREO,
				'instructorDirecion' => $result->tbl_instructor_DIRECION
			];
			$this->vista('personal/Instructor/EditarInstructor', $datos);
		}

		public function EditarInstructor(){
			$datos = [
				'idInstructor' => trim($_POST['idInstructor']),
				'instructorNombre' => trim($_POST['instructorNombre']),
				'instructorApellido' => trim($_POST['instructorApellido']) ,
				'instructorTipoDocumento' => trim($_POST['instructorTipoDocumento']),
				'instructorNumeroDocumento' => trim($_POST['instructorNumeroDocumento']),
				'instructorNumeroTelefono' => trim($_POST['instructorNumeroTelefono']),
				'instructorDirecionCorreo' => trim($_POST['instructorDirecionCorreo']),
				'instructorDirecion' => trim($_POST['instructorDirecion'])
			];
			$this->InstructorModel->EditarInstructor($datos);
		}
		
		public function EditarInstructor2(){
			$datos = [
				'idInstructor' => trim($_POST['idInstructor']),
				'instructorSedeID' => trim($_POST['instructorSedeID']) ,
				'instructorNumeroDocumento' => trim($_POST['instructorNumeroDocumento'])
			];
			$this->InstructorModel->EditarInstructor2($datos);
		}

		public function EliminarInstructor($idInstructor){
			$result = $this->InstructorModel ->ObtenerInstructorId($idInstructor);
			$datos = [
				'idInstructor' => $idInstructor,
				'instructorNombre' => $result->tbl_instructor_NOMBRES,
				'instructorApellido' => $result->tbl_instructor_APELLIDOS,
				'instructorTipoDocumento' => $result->tbl_instructor_TIPODOCUMENTO,
				'instructorNumeroDocumento' => $result->tbl_instructor_NUMDECUMENTO,
				'instructorNumeroTelefono' => $result->tbl_instructor_TELEFONO,
				'instructorDirecionCorreo' => $result->tbl_instructor_CORREO,
				'instructorDirecion' => $result->tbl_instructor_DIRECION
			];
			$this->vista('personal/Instructor/EliminarInstructor' ,$datos);
		}

		public function DeleteInstructor(){
		    $idInstructor = trim($_POST['idInstructor']);
		    $idSede = $_SESSION['sesion_active']['idSede'];
			$this->InstructorModel->EliminarInstructor($idInstructor, $idSede);
		}

		public function CompararInstructor(){
			$result = $this->InstructorModel->CompararInstructor($_POST['instructorNombre']);
			echo json_encode($result);
		}
		
		// Comprobar si existe el documento dle instructor
		public function ComprobarInstructor(){
		    $idInstructor = trim($_POST['idInstructor']);
		    $result = $this->InstructorModel->ComprobarInstructor($idInstructor);
		    echo json_encode($result);
		}
		
		// se obtiene datos del instructor
		public function EditarInstructorNumero($idInstructor){
			$ListarSede = $this->InstructorModel->ListarSede($idInstructor);
			$result = $this->InstructorModel->ObtenerInstructorId($idInstructor);
			$datos = [
				'idInstructor' => $idInstructor,
				'ListarSede' => $ListarSede,
				'instructorNumeroDocumento' => $result->tbl_instructor_NUMDECUMENTO,
			];
			$this->vista('personal/Instructor/EditarInstructorNumero', $datos);
		}
	}
?>