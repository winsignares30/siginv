<?php 
	class Administrador Extends Controlador{

		public function __construct(){
		    if(!isset($_SESSION['sesion_active'])):
		        header('location:' . URL_SISINV . 'Login/Logout');
		    endif;
		    
			$this->SedeModel = $this->modelo('SedeModel');
			$this->AdministradorModel = $this->modelo('AdministradorModel'); //InstructorModel
			$this->RegionalModel = $this->modelo('RegionalModel');
		}

		public function ListarAdministrador(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    
			$ListarAdministrador = $this->AdministradorModel->ListarAdministrador($idSede);
			$ListarRegional = $this->RegionalModel->ListarRegional();
			$ListarSede = $this->SedeModel->ListarSede();
			$datos = [
				'ListarAdministrador' => $ListarAdministrador,
				'ListarSedes' => $ListarSede,
				'ListarRegional' => $ListarRegional
			];
			$this->vista('personal/Administrador/ListarAdministrador', $datos);
		}

		public function RegistrarAdministrador(){
			$datos = [
				'administradorSedeID' => trim($_POST['administradorSedeID']),
				'administradorNombre' => trim($_POST['administradorNombre']),
				'administradorApellido' => trim($_POST['administradorApellido']),
				'administradorTipoDocumento' => trim($_POST['administradorTipoDocumento']),
				'administradorNumeroDocumento' => trim($_POST['administradorNumeroDocumento']),
				'administradorNumeroTelefono' => trim($_POST['administradorNumeroTelefono']),
				'administradorDirecionCorreo' => trim($_POST['administradorDirecionCorreo']),
				'administradorDirecion' => trim($_POST['administradorDirecion']),
			];
			$this->AdministradorModel->RegistrarAdministrador($datos);
		}
		
		public function ObtenerAdministradorId($idAdministrador){
			$ListarSede = $this->AdministradorModel->ListarSede($idAdministrador);
			$result = $this->AdministradorModel->ObtenerAdministradorId($idAdministrador);
			$datos = [
				'idAdministrador' => $idAdministrador,
				'ListarSede' => $ListarSede,
				'administradorNombre' => $result->tbl_persona_NOMBRES,
				'administradorApellido' => $result->tbl_persona_PRIMERAPELLIDO,
				'administradorTipoDocumento' => $result->tbl_tipodocumento_tbl_tipodocumento_ID,
				'administradorNumeroDocumento' => $result->tbl_persona_NUMDOCUMENTO,
				'administradorNumeroTelefono' => $result->tbl_persona_TELEFONO,
				'administradorDirecionCorreo' => $result->tbl_persona_CORREO,
				'administradorDirecion' => $result->tbl_persona_DIRECCION
			];
			$this->vista('personal/Administrador/EditarAdministrador', $datos);
		}

		public function EditarAdministrador(){
			$datos = [
				'idAdministrador' => trim($_POST['idAdministrador']),
				'administradorNombre' => trim($_POST['administradorNombre']),
				'administradorApellido' => trim($_POST['administradorApellido']) ,
				'administradorTipoDocumento' => trim($_POST['administradorTipoDocumento']),
				'administradorNumeroDocumento' => trim($_POST['administradorNumeroDocumento']),
				'administradorNumeroTelefono' => trim($_POST['administradorNumeroTelefono']),
				'administradorDirecionCorreo' => trim($_POST['administradorDirecionCorreo']),
				'administradorDirecion' => trim($_POST['administradorDirecion'])
			];
			$this->AdministradorModel->EditarAdministrador($datos);
		}
		
		public function EditarAdministrador2(){
			$datos = [
				'idAdministrador' => trim($_POST['idAdministrador']),
				'administradorSedeID' => trim($_POST['administradorSedeID']) ,
				'administradorNumeroDocumento' => trim($_POST['administradorNumeroDocumento'])
			];
			$this->AdministradorModel->EditarAdministrador2($datos);
		}

		public function EliminarAdministrador($idInstructor){
			$result = $this->AdministradorModel ->ObtenerAdministradorId($idInstructor);
			$datos = [
				'idAdministrador' => $idAdministrador,
				'administradorNombre' => $result->tbl_persona_NOMBRES,
				'administradorApellido' => $result->tbl_persona_PRIMERAPELLIDO,
				'administradorTipoDocumento' => $result->tbl_tipodocumento_tbl_tipodocumento_ID,
				'administradorNumeroDocumento' => $result->tbl_persona_NUMDOCUMENTO,
				'administradorNumeroTelefono' => $result->tbl_persona_TELEFONO,
				'administradorDirecionCorreo' => $result->tbl_persona_CORREO,
				'administradorDirecion' => $result->tbl_persona_DIRECCION
			];
			$this->vista('personal/Administrador/EliminarAdministrador' ,$datos);
		}

		public function DeleteInstructor(){
		    $idAdministrador = trim($_POST['idAdministrador']);
		    $idSede = $_SESSION['sesion_active']['idSede'];
			$this->AdministradorModel->EliminarAdministrador($idAdministrador, $idSede);
		}

		public function CompararAdministrador(){
			$result = $this->AdministradorModel->CompararAdministrador($_POST['administradorNombre']);
			echo json_encode($result);
		}
		
		// Comprobar si existe el documento del Administrador
		public function ComprobarAdministrador(){
		    $idAdministrador = trim($_POST['idAdministrador']);
		    $result = $this->AdministradorModel->ComprobarInstructor($idAdministrador);
		    echo json_encode($result);
		}
		
		// se obtiene datos del instructor
		public function EditarAdministradorNumero($idAdministrador){
			$ListarSede = $this->AdministradorModel->ListarSede($idAdministrador);
			$result = $this->AdministradorModel->ObtenerAdministradorId($idAdministrador);
			$datos = [
				'idAdministrador' => $idAdministrador,
				'ListarSede' => $ListarSede,
				'administradorNumeroDocumento' => $result->tbl_persona_NUMDOCUMENTO,
			];
			$this->vista('personal/Administrador/EditarAdministradorNumero', $datos);
		}
	}
?>