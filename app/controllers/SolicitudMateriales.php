<?php

    Class SolicitudMateriales Extends Controlador{
        
        public function __construct(){
            if(!isset($_SESSION['sesion_active'])):
                header('location:' . URL_SISINV . 'Login/Logout');
            endif; 
            
            $this->MaterialModel = $this->modelo('MaterialModel');
            $this->HerramientaModel = $this->modelo('HerramientaModel');
        }
        
        function generarCodigo(){
            $key = date('m/d/Y g:ia');
            return $key;
        }
        
        public function SolicitarMateriales(){
            $idPersona =  $_SESSION['sesion_active']['cod'];
            $idSede =  $_SESSION['sesion_active']['idSede'];
            $LoadFichas = $this->HerramientaModel->LoadFichas($idSede);
            $result2 = $this->MaterialModel->MostrarMaterialesSolicitados($idPersona, $idSede);
            $datos = [
                'LoadFichas' => $LoadFichas,
                'MaterialesSolicitados' => $result2
            ];
            $this->vista('solicitud/materiales/SolicitarMateriales', $datos);
        }
        
        // Funcion que lleva los datos en la vista para el carrito de solictudes
        public function SolicitarMateriales2(){
            $idSede =  $_SESSION['sesion_active']['idSede'];
            $result = $this->MaterialModel->SolicitarMateriales($idSede);
            echo json_encode($result);
        }
    }
?>