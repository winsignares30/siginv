<?php 

	Class Persona Extends Controlador{
		
		public function __construct(){
		    if(!isset($_SESSION['sesion_active'])):
		        header('location:' . URL_SISINV . 'Login/Logout');
		    endif;
		    
		    $this->PersonaModel = $this->modelo('PersonaModel');
			$this->RegionalModel = $this->modelo('RegionalModel');
		}
		// Listar Administradores
		public function ListarAdministrador(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    
		    $ListarAdministrador = $this->PersonaModel->ListarAdministrador($idSede);
			$ListarRegional = $this->RegionalModel->ListarRegional();
		    $datos = [
		        'ListarAdministrador' => $ListarAdministrador,
				'ListarRegional' => $ListarRegional
		    ];
		    $this->vista('personal/administrador/ListarAdministrador', $datos);
		}
	
		
		public function contadorRegistros(){
		    $resultContador = $this->PersonaModel->contadorRegistros();
		    echo json_encode($resultContador);
		}
        // Registrar Administradores
		public function RegistrarAdministradorPersona(){
			$datos = [
			    'administradorNumeroDocumento' => trim($_POST['administradorNumeroDocumento']),
				'administradorNombre' => trim($_POST['administradorNombre']),
				'administradorApellido1' => trim($_POST['administradorApellido1']),
				'administradorApellido2' => trim($_POST['administradorApellido2']),
				'administradorFechadeNacimiento' => trim($_POST['administradorFechadeNacimiento']),
				'administradorNumeroTelefono' => trim($_POST['administradorNumeroTelefono']),
				'administradorDireccionCorreo' => trim($_POST['administradorDireccionCorreo']),
				'administradorCargo' => trim($_POST['administradorCargo']),
				'administradorDireccion' => trim($_POST['administradorDireccion']),
				'administradorTipoContrato' => trim($_POST['administradorTipoContrato']),
				'administradorTipoDocumento' => trim($_POST['administradorTipoDocumento'])
				
			];
			$this->PersonaModel->RegistrarAdministradorPersona($datos);
		}
		
		public function RegistrarAdministradorUsuario(){
		    $cantidadRegistros = $_POST['cantidadRegistros'];
			$datos = [
			    
				'administradorSedeID' => trim($_POST['administradorSedeID']),
				'administradorDireccionCorreo' => trim($_POST['administradorDireccionCorreo']),
				'administradorContrasenia' => trim($_POST['administradorContrasenia']),
				'cantidadRegistros' => $cantidadRegistros
			];
			$this->PersonaModel->RegistrarAdministradorUsuario($datos);
			
		}
		// Obtener Administradores
		public function ObtenerAdministrador($idAdministrador){
		    $ListarRegional = $this->RegionalModel->ListarRegional();
			$result = $this->PersonaModel->ObtenerAdministradorId($idAdministrador);
			$datos = [
				'idAdministrador' => $idAdministrador,
				'ListarRegional' => $ListarRegional,
				'Administradores' => $result,
				'administradorNumeroDocumento' => $result->tbl_persona_NUMDOCUMENTO,
				'administradorNombre' => $result->tbl_persona_NOMBRES,
				'administradorApellido1' => $result->tbl_persona_PRIMERAPELLIDO,
				'administradorApellido2' => $result->tbl_persona_SEGUNDOAPELLIDO,
				'administradorFechadeNacimiento' => $result->tbl_persona_FECHANAC,
				'administradorNumeroTelefono' => $result->tbl_persona_TELEFONO,
				'administradorDireccionCorreo' => $result->tbl_persona_CORREO,
				'administradorCargo' => $result->tbl_cargo_tbl_cargo_ID,
				'administradorDireccion' => $result->tbl_persona_DIRECCION,
				'administradorTipoContrato' => $result->tipo_contrato,
				'administradorTipoDocumento' => $result->tbl_tipodocumento_tbl_tipodocumento_ID
			];
			$this->vista('personal/administrador/EditarAdministrador', $datos);
		}

		public function EditarAdministrador(){
			$datos = [
			    
				'idAdministrador' => trim($_POST['idAdministrador']),
				'administradorNumeroDocumento' => trim($_POST['administradorNumeroDocumento']),
				'administradorNombre' => trim($_POST['administradorNombre']),
				'administradorApellido1' => trim($_POST['administradorApellido1']),
				'administradorApellido2' => trim($_POST['administradorApellido2']),
				'administradorFechadeNacimiento' => trim($_POST['administradorFechadeNacimiento']),
				'administradorNumeroTelefono' => trim($_POST['administradorNumeroTelefono']),
				'administradorDireccionCorreo' => trim($_POST['administradorDireccionCorreo']),
				'administradorCargo' => trim($_POST['administradorCargo']),
				'administradorDireccion' => trim($_POST['administradorDireccion']),
				'administradorTipoContrato' => trim($_POST['administradorTipoContrato']),
				'administradorTipoDocumento' => trim($_POST['administradorTipoDocumento'])
			];
			$this->PersonaModel->EditarAdministrador($datos);
		}
		
		public function EditarAdministradorUsuario(){
			$datos = [
			    'administradorSedeID' => trim($_POST['administradorSedeID']),
				'idAdministrador' => trim($_POST['idAdministrador']),
				'administradorDireccionCorreo' => trim($_POST['administradorDireccionCorreo'])
			];
			$this->PersonaModel->EditarAdministradorUsuario($datos);
		}
		
		public function EditarAdministrador2(){
			$datos = [
				'idAdministrador' => trim($_POST['idAdministrador']),
				'administradorSedeID' => trim($_POST['administradorSedeID']) ,
				'administradorNumeroDocumento' => trim($_POST['administradorNumeroDocumento'])
			];
			$this->PersonaModel->EditarAdministrador2($datos);
		}

		public function EliminarAdministrador($idAdministrador){
			$result = $this->PersonaModel->ObtenerAdministradorId($idAdministrador);
			$datos = [
			    'Administradores' => $result,
				'idAdministrador' => $idAdministrador,
				'administradorNumeroDocumento' => $result->tbl_persona_NUMDOCUMENTO,
				'administradorNombre' => $result->tbl_persona_NOMBRES,
				'administradorApellido1' => $result->tbl_persona_PRIMERAPELLIDO,
				'administradorApellido2' => $result->tbl_persona_SEGUNDOAPELLIDO,
				'administradorDireccionCorreo' => $result->tbl_persona_CORREO,
				'administradorCargo' => $result->tbl_cargo_tbl_cargo_ID
			];
			$this->vista('personal/administrador/EliminarAdministrador', $datos);
		}

		public function DeleteAdministrador(){
		    $idAdministrador = trim($_POST['idAdministrador']);
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    //$idUsuario = $_SESSION['sesion_active']['idUsuario'];
			$this->PersonaModel->EliminarAdministrador($idAdministrador, $idSede);
		}

		public function CompararAdministrador(){
			$result = $this->PersonaModel->CompararAdministrador($_POST['administradorNombre']);
			echo json_encode($result);
		}
		
		// Comprobar si existe el documento del Administrador
		public function ComprobarAdministrador(){
		    $idAdministrador = trim($_POST['idAdministrador']);
		    $result = $this->PersonaModel->ComprobarAdministrador($idAdministrador);
		    echo json_encode($result);
		}
		
		// se obtiene datos del instructor
		public function EditarAdministradorNumero($idAdministrador){
			$ListarSede = $this->PersonaModel->ListarSede($idAdministrador);
			$result = $this->PersonaModel->ObtenerAdministradorId($idAdministrador);
			$datos = [
				'idAdministrador' => $idAdministrador,
				'ListarSede' => $ListarSede,
				'administradorNumeroDocumento' => $result->tbl_persona_NUMDOCUMENTO,
			];
			$this->vista('personal/administrador/EditarAdministradorNumero', $datos);
		}
		
		
		//------------INSTRUCTORES------------
		
		// Listar Instructores
		public function ListarInstructor(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    $result = $this->PersonaModel->ListarInstructor($idSede);
		    $datos = [
		        'ListarInstructores' => $result
		    ];
		    $this->vista('personal/instructor/ListarInstructor', $datos);
		}
		// Obtener Instructores
		public function ObtenerInstructor($instructorId){
			$ListarRegional = $this->RegionalModel->ListarRegional();
			$result = $this->PersonaModel->ObtenerInstructor($instructorId);
			$datos = [
				'instructorId' => $instructorId,
				'ListarRegional' => $ListarRegional,
				'instructores' => $result
			];
			$this->vista('personal/instructor/EditarInstructor', $datos);
		}
		//Editar Instructores
		public function EditarInstructor(){
			$datos = [
			    
				'instructorId' => trim($_POST['instructorId']),
				'instructorDocumento' => trim($_POST['instructorDocumento']),
				'instructorNombre' => trim($_POST['instructorNombre']),
				'instructorApellido1' => trim($_POST['instructorApellido1']),
				'instructorApellido2' => trim($_POST['instructorApellido2']),
				'instructorFechaNacimiento' => trim($_POST['instructorFechaNacimiento']),
				'instructorCelular' => trim($_POST['instructorCelular']),
				'instructorCorreo' => trim($_POST['instructorCorreo']),
				'instructorCargo' => trim($_POST['instructorCargo']),
				'instructorDireccion' => trim($_POST['instructorDireccion']),
				'instructorTipoContrato' => trim($_POST['instructorTipoContrato']),
				'instructorTipoDocumento' => trim($_POST['instructorTipoDocumento'])
			];
			$this->PersonaModel->EditarInstructor($datos);
		}
		
		// Editar Instructores Usuario
		public function EditarInstructorUsuario(){
			$datos = [
			    'instructorSedeID' => trim($_POST['instructorSedeID']),
				'instructorId' => trim($_POST['instructorId']),
				'instructorCorreo' => trim($_POST['instructorCorreo'])
			];
			$this->PersonaModel->EditarInstructorUsuario($datos);
		}
		// Eliminar Instructores
		public function EliminarInstructor($instructorId){
		    $result = $this->PersonaModel->ObtenerInstructor($instructorId);
		    $datos = [
		        'instructores' => $result,
		        'instructorId' => $instructorId,
		        'instructorNombre' => $result->tbl_persona_NOMBRES,
		        'instructorApellido1' => $result->tbl_persona_PRIMERAPELLIDO,
		        'instructorDocumento' => $result->tbl_persona_NUMDOCUMENTO,
		        'instructorCelular' => $result->tbl_persona_TELEFONO,
		        'instructorCorreo' => $result->tbl_persona_CORREO
		    ];
		    $this->vista('personal/instructor/EliminarInstructor', $datos);
		}

		public function DeleteInstructor(){
		    $instructorId = trim($_POST['instructorId']);
		    $idSede = $_SESSION['sesion_active']['idSede'];
	
			$this->PersonaModel->EliminarInstructor($instructorId, $idSede);
		}
		
		
		//------------ HERRAMENTERO ------------
		
		// Listar Herramentero
		public function ListarHerramentero(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    $result = $this->PersonaModel->ListarHerramentero($idSede);
		    $datos = [
		        'ListarHerramentero' => $result
		    ];
		    $this->vista('personal/herramentero/ListarHerramentero', $datos);
		}
		// Obtener Herramentero
		public function ObtenerHerramentero($herramenteroId){
			$ListarRegional = $this->RegionalModel->ListarRegional();
			$result = $this->PersonaModel->ObtenerHerramentero($herramenteroId);
			$datos = [
				'herramenteroId' => $herramenteroId,
				'ListarRegional' => $ListarRegional,
				'herramenteros' => $result //Directo al ForEach de editar
			];
			$this->vista('personal/herramentero/EditarHerramentero', $datos);
		}
		//Editar Herramentero
		public function EditarHerramentero(){
			$datos = [
			    
				'herramenteroId' => trim($_POST['herramenteroId']),
				'herramenteroDocumento' => trim($_POST['herramenteroDocumento']),
				'herramenteroNombre' => trim($_POST['herramenteroNombre']),
				'herramenteroApellido1' => trim($_POST['herramenteroApellido1']),
				'herramenteroApellido2' => trim($_POST['herramenteroApellido2']),
				'herramenteroFechaNacimiento' => trim($_POST['herramenteroFechaNacimiento']),
				'herramenteroCelular' => trim($_POST['herramenteroCelular']),
				'herramenteroCorreo' => trim($_POST['herramenteroCorreo']),
				'herramenteroCargo' => trim($_POST['herramenteroCargo']),
				'herramenteroDireccion' => trim($_POST['herramenteroDireccion']),
				'herramenteroTipoContrato' => trim($_POST['herramenteroTipoContrato']),
				'herramenteroTipoDocumento' => trim($_POST['herramenteroTipoDocumento'])
			];
			$this->PersonaModel->EditarHerramentero($datos);
		}
		
		// Editar Herramentero Usuario
		public function EditarHerramenteroUsuario(){
			$datos = [
			    'herramenteroSedeID' => trim($_POST['herramenteroSedeID']),
				'herramenteroId' => trim($_POST['herramenteroId']),
				'herramenteroCorreo' => trim($_POST['herramenteroCorreo'])
			];
			$this->PersonaModel->EditarHerramenteroUsuario($datos);
		}
		// Eliminar Herramentero
		public function EliminarHerramentero($herramenteroId){
		    $result = $this->PersonaModel->ObtenerHerramentero($herramenteroId);
		    $datos = [
		        'herramenteros' => $result,
		        'herramenteroId' => $herramenteroId,
		        'herramenteroNombre' => $result->tbl_persona_NOMBRES,
		        'herramenteroApellido1' => $result->tbl_persona_PRIMERAPELLIDO,
		        'herramenteroDocumento' => $result->tbl_persona_NUMDOCUMENTO,
		        'herramenteroCelular' => $result->tbl_persona_TELEFONO,
		        'herramenteroCorreo' => $result->tbl_persona_CORREO
		    ];
		    $this->vista('personal/herramentero/EliminarHerramentero', $datos);
		}

		public function DeleteHerramentero(){
		    $herramenteroId = trim($_POST['herramenteroId']);
		    $idSede = $_SESSION['sesion_active']['idSede'];
	
			$this->PersonaModel->EliminarHerramentero($herramenteroId, $idSede);
		}

	}

?>