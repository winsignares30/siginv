<?php
    
    class Equipo extends Controlador{

        public function __construct(){
            if(!isset($_SESSION['sesion_active'])):
                header('location:' . URL_SISINV . 'Login/Logout');
            endif;
            
			$this->EquipoModel = $this->modelo('EquipoModel');
			$this->RegionalModel = $this->modelo('RegionalModel');
			$this->CentroModel = $this->modelo('CentroModel');
			$this->SedeModel = $this->modelo('SedeModel');
        }
        
        public function ListarEquipo(){
            $idSede = $_SESSION['sesion_active']['idSede'];
        	$ListarEquipos = $this->EquipoModel->ListarEquipos($idSede);
        	$ListarRegional = $this->RegionalModel->ListarRegional();
			$datos = [
			    'ListarEquipos' => $ListarEquipos,
				'ListarRegional' => $ListarRegional
			];
			$this->vista('inventario/Equipos/ListarEquipo', $datos);
		}
		
		public function RegistrarEquipo(){
            $datos = [
                'equipoSede' => trim($_POST['equipoSede']),
                'equipoModelo' => trim($_POST['equipoModelo']),
                'equipoCantidad' => trim($_POST['equipoCantidad']),
                'equipoConsecutivo' => trim($_POST['equipoConsecutivo']),
                'equipoDescripcion' => trim($_POST['equipoDescripcion']),
                'equipoDescripcionActual' => trim($_POST['equipoDescripcionActual']),
                'equipoTipo' => trim($_POST['equipoTipo']),
                'rubroPresupuestal' => trim($_POST['rubroPresupuestal']),
                'equipoPlaca' => trim($_POST['equipoPlaca']),
                'equipoValordeingreso' => trim($_POST['equipoValordeingreso']),
                'rubroPresupuestal' => trim($_POST['rubroPresupuestal'])
            ];
            $this->EquipoModel->RegistrarEquipo($datos);
        }
        
        public function ObtenerEquipo($idEquipo) {
            // Get data
            $result = $this->EquipoModel->ObtenerEquipo($idEquipo);
    		$datos = [
    		    'idEquipo' => $idEquipo,
    		    'equipoModelo'=> $result->tbl_equipo_MODELO,
    		    'equipoCantidad'=> $result->tbl_equipo_CANTIDAD,
    		    'equipoConsecutivo' => $result->tbl_equipo_CONSECUTIVO,
    		    'equipoDescripcion'=> $result->tbl_equipo_DESCRIPCION,
    		    'equipoDescripcionActual' => $result->tbl_equipo_DESCRIPCION_ACTUAL,
    		    'equipoTipo' => $result->tbl_equipo_TIPO,
    		    'rubroPresupuestal' => $result->tbl_equipo_RUBRO,
    		    'equipoFechadeAdquision' => $result->tbl_equipo_FECHA_ADQUISICION,
    		    'equipoValordeingreso' => $result->tbl_equipo_VALOR_INGRESO,
    		];
    		// sent data to the views
		    $this->vista('inventario/Equipos/EditarEquipo', $datos);
        }
        
        public function EditarEquipo(){
			$datos = [
			    'idEquipo' => trim($_POST['idEquipo']),
				'equipoModelo' => trim($_POST['equipoModelo']),
				'equipoCantidad' => trim($_POST['equipoCantidad']),
				'equipoConsecutivo' => trim($_POST['equipoConsecutivo']),
				'equipoDescripcion' => trim($_POST['equipoDescripcion']),
				'equipoDescripcionActual' => trim($_POST['equipoDescripcionActual']),
				'equipoTipo' => trim($_POST['equipoTipo']),
				'rubroPresupuestal' => trim($_POST['rubroPresupuestal']),
				'equipoValordeingreso' => trim($_POST['equipoValordeingreso']),
				'equipoFechadeAdquision' => trim($_POST['equipoFechadeAdquision']),
			];
			$this->EquipoModel->EditarEquipo($datos);
		}
		public function EditarEquipo2(){
			$datos = [
			    'idEquipo' => trim($_POST['idEquipo']),
				'equipoSede' => trim($_POST['equipoSede']),
				'equipoPlaca' => trim($_POST['equipoPlaca'])
			];
			$this->EquipoModel->EditarEquipo2($datos);
		}
		
		
		public function EditarEquipo3(){
			$datos = [
			    'idEquipo' => trim($_POST['idEquipo']),
				'equipoSede' => trim($_POST['equipoSede']),
				'equipoSerial' => trim($_POST['equipoSerial'])
			];
			$this->EquipoModel->EditarEquipo3($datos);
		}
		
		public function EditarEquipoPlaca($idEquipo){
		    $result = $this->EquipoModel->ObtenerEquipo($idEquipo);
		    $ListarSede = $this->EquipoModel->ObtenerSede($idEquipo);
		    $datos = [
		        'idEquipo' => $idEquipo,
		        'ListarSede' => $ListarSede,
		        'equipoPlaca' => $result->tbl_equipo_PLACA
		    ];
		    
		    $this->vista('inventario/Equipos/EditarEquipoPlaca', $datos);
		}
		
		public function CompararEquipo(){
		    $idEquipo = trim($_POST['idEquipo']);
		    $result = $this->EquipoModel->CompararEquipo($idEquipo);
		    echo json_encode($result);
		}
		
		
		
		public function EliminarEquipo($idEquipo){
		   $result = $this->EquipoModel->ObtenerEquipo($idEquipo);
		   $datos = [
		        'idEquipo' => $idEquipo,
		        'equipoModelo' => $result->tbl_equipo_MODELO,
		        'equipoCantidad' => $result->tbl_equipo_CANTIDAD,
    		    'equipoConsecutivo' => $result->tbl_equipo_CONSECUTIVO,
    		    'equipoDescripcion' => $result->tbl_equipo_DESCRIPCION,
    		    'equipoDescripcionActual' => $result->tbl_equipo_DESCRIPCION_ACTUAL,
    		    'equipoTipo' => $result->tbl_equipo_TIPO,
    		    'equipoPlaca' => $result->tbl_equipo_PLACA,
    		    'equipoSerial' => $result->tbl_equipo_SERIAL,
    		    'equipoFechadeAdquision' => $result->tbl_equipo_FECHA_ADQUISICION,
    		    'equipoValordeingreso' => $result->tbl_equipo_VALOR_INGRESO,
    		    'rubroPresupuestal' => $result->tbl_equipo_RUBRO
		       
		   ];
		    $this->vista('inventario/Equipos/EliminarEquipo', $datos);
		}
		
        public function DeleteEquipo(){
            $idEquipo = trim($_POST['idEquipo']);
            $this->EquipoModel->EliminarEquipo($idEquipo);
        }
		
	}
?>