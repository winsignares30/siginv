<?php

	class Gaveta Extends Controlador{
		public function __construct()
		{
		    if (!isset($_SESSION['sesion_active'])):
		        header('location:' . URL_SISINV . 'Login/Logout');
		    endif;
            
			$this->EstanteModel = $this->modelo('EstanteModel');
			$this->GavetaModel = $this->modelo('GavetaModel');
			$this->BodegaModel = $this->modelo('BodegaModel');
			$this->AmbienteModel = $this->modelo('AmbienteModel');
			$this->CentroModel = $this-> modelo('CentroModel');
			$this->RegionalModel = $this->modelo('RegionalModel');
			$this->SedeModel = $this->modelo('SedeModel');
		}

		public function ListarGaveta(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
		   
			$ListarRegional = $this->RegionalModel->ListarRegional();
			$ListarGavetas = $this->GavetaModel->ListarGavetas($idSede);
			//var_dump($ListarGavetas);
			$datos = [
				'ListarGavetas' => $ListarGavetas,
				'ListarReginal' => $ListarRegional
			];
			$this->vista('configuracion/Gaveta/ListarGaveta', $datos);
		}
		public function RegistrarGaveta(){
			$datos = [
				'gavetaEstante' => trim($_POST['gavetaEstante']),
				'gavetaNumero' => trim($_POST['gavetaNumero']),
				'gavetaDescripcion' =>trim($_POST['gavetaDescripcion']),
			];
			$this->GavetaModel->RegistrarGaveta($datos);

		}
		public function ObtenerGaveta($idGaveta){
		    $result = $this->GavetaModel->ObtenerGaveta($idGaveta);
			$ListarRegional = $this->RegionalModel->ListarRegional();
			//$ListarCentro = $this->CentroModel->ListarCentro();
			//$ListarSede = $this->SedeModel->ListarSede();
			//$ListarEstante = $this->EstanteModel->ListarEstante();
			//$ListarBodega = $this->BodegaModel->ListarBodega();
			
			$datos = [
				'idGaveta' => $idGaveta,
				'ListarRegional' => $ListarRegional,
				//'ListarCentro' => $ListarCentro,
				//'ListarSede' => $ListarSede,
				'ListarEstante' => $ListarEstante,
				//'ListarBodega' => $ListarBodega,
				'gavetaDescripcion' => $result->tbl_gaveta_DESCRIPCION,
				'gavetaNumero' => $result->tbl_gaveta_NUMERO
			];
			$this->vista('configuracion/Gaveta/EditarGaveta', $datos);
		}

		public function EditarGaveta(){
			$datos = [
				'idGaveta' => trim($_POST['idGaveta']),
				'gavetaEstante' => trim($_POST['gavetaEstante']),
				'gavetaNumero' => trim($_POST['gavetaNumero']),
				'gavetaDescripcion' => trim($_POST['gavetaDescripcion'])
			];
			$this->GavetaModel->EditarGaveta($datos);
    	}
    	
		public function EliminarGaveta($idGaveta) {
			$result = $this->GavetaModel->ObtenerGaveta($idGaveta);
			$datos = [
				'idGaveta' => $idGaveta,
				'gavetaDescripcion' => $result->tbl_gaveta_DESCRIPCION,
				'gavetaNumero' => $result->tbl_gaveta_NUMERO
			];
			$this->vista('configuracion/Gaveta/EliminarGaveta', $datos);
		}
		
		public function CompararGaveta(){
			$gavetaEstante = trim($_POST['gavetaEstante']);
			$result = $this->GavetaModel->CompararGaveta($gavetaEstante);
			echo json_encode($result);  // devuelve objeto json
		}
		
		public function IfGavetaExiste(){
		    $idGaveta = trim($_POST['idGaveta']);
		    $result = $this->GavetaModel->IfGavetaExiste($idGaveta);
		    echo json_encode($result);
		}

		public function DeleteGaveta() {
			$idGaveta = trim($_POST['idGaveta']);
			$idSede = $_SESSION['sesion_active']['idSede'];
			$this->GavetaModel->DeleteGaveta($idGaveta, $idSede);
		
		}

		public function LoadEstantes() {
			$idBodega = trim($_POST['bodegaID']);
			$result = $this->GavetaModel->LoadEstantes($idBodega);
			echo json_encode($result);
		}
	}
 ?>
