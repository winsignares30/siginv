<?php 

	Class Bodega Extends Controlador{
		public function __construct(){
		    if(!isset($_SESSION['sesion_active'])):
		        header('location:' . URL_SISINV . 'Login/Logout');
		    endif;
		    
			$this->BodegaModel = $this->modelo('BodegaModel');
			$this->RegionalModel = $this->modelo('RegionalModel');
		}

		public function ListarBodega(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    
			$ListarRegional = $this->RegionalModel->ListarRegional();
			$ListarBodega = $this ->BodegaModel->ListarBodega($idSede);
			$datos = [
				'ListarReginal' => $ListarRegional,
				'ListarBodega' => $ListarBodega
			];
			$this->vista('configuracion/Bodega/ListarBodega', $datos);
		}
		public function RegistrarBodega(){
			$datos = [
				'bodegaSede' => trim($_POST['bodegaSede']),
				'bodegaNombre' => trim($_POST['bodegaNombre'])
			];
			$this->BodegaModel->RegistrarBodega($datos);
		}
		public function EliminarBodega($idBodega) {
			$result = $this->BodegaModel->ObtenerBodega($idBodega);
			$datos = [
				'bodegaNombre' => $result->tbl_bodega_NOMBRE,
				'idBodega' => $idBodega
			];
			$this->vista('configuracion/Bodega/EliminarBodega', $datos);
		}
		public function DeleteBodega() {
		    $idSede = $_SESSION['sesion_active']['idSede'];
			$idBodega = trim($_POST['idBodega']);
			$this->BodegaModel->DeleteBodega($idBodega, $idSede);
		}
		
		public function ObtenerBodega($idBodega) {
		    
			$result = $this->BodegaModel->ObtenerBodega($idBodega);
			$ListarRegional = $this->RegionalModel->ListarRegional();
			$datos = [
				'idBodega' => $idBodega,
				'bodegaNombre' => $result->tbl_bodega_NOMBRE,
				'ListarRegional' => $ListarRegional
			];
			$this->vista('configuracion/Bodega/EditarBodega', $datos);
		}
		
		
		public function EditarBodega() {
			$datos = [
				'bodegaSede' => trim($_POST['bodegaSede']),
				'bodegaNombre' => trim($_POST['bodegaNombre']),
				'idBodega' => trim($_POST['idBodega']),
			];
			$this->BodegaModel->EditarBodega($datos);
		}
		
		public function CompararBodega() {
			$bodegaSede = trim($_POST['bodegaSede']);
			$result = $this->BodegaModel->CompararBodega($bodegaSede);
			echo json_encode($result);
		}
		
		public function IfDataExist(){
		    $idBodega = trim($_POST['idBodega']);
			$result = $this->BodegaModel->IfDataExist($idBodega);
			echo json_encode($result);
		}
	}

?>