<?php

    Class DevolucionHerramientas Extends Controlador{
        
        public function __construct(){
            if(!isset($_SESSION['sesion_active'])):
                header('location:' . URL_SISINV . 'Login/Logout');
            endif; 
            $this->HerramientaModel = $this->modelo('HerramientaModel');

        }
        
        public function devherramienta(){
            
            $datos = [ ];
            
            $this->vista('devolucion/herramienta/devherramienta', $datos);
        }
        
        // funcion que solicita las herramientas en estado entregado
        public function devolverHerramienta(){
            $idSede = $_SESSION['sesion_active']['idSede'];
            
            $datos = [
                'idPersona' => $_POST['idPersona'],
                'tipo' => $_POST['tipo']
            ];
            $result = $this->HerramientaModel->devolverHerramienta($datos,$idSede);
            echo json_encode($result);
            
        }
        
        public function DevolucionHerramientas(){
            $datos = [
                'id' => trim($_POST['id']),
                'id2' => trim($_POST['id2']),
                'idPersona' =>trim($_POST['idPersona']),
                'cantidad' => trim($_POST['cantidad']),
                'cantidad2' => trim($_POST['cantidad2']),
                'tipo' => trim($_POST['tipo']),
            ];
            $this->HerramientaModel->DevoHerramientas($datos);
            $this->HerramientaModel->DevolucionHerramientas($datos);
        }
        
        public function CantidadHerramienta(){
            $id = $_POST['id'];
            $idSede = $_SESSION['sesion_active']['idSede'];
            $result = $this->HerramientaModel->CantidadHerramienta($id, $idSede);
            echo json_encode($result);
        }
        
        public function getRequest(){
		    $idRequest = $_POST['id'];
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    $result = $this->HerramientaModel->getRequest($idRequest, $idSede);
		    echo json_encode($result);
		}
        
        /* 
        public function devherramienta(){
            //$ListarRegional = $this->RegionalModel->ListarRegional();
            //$DevolucionesUsuarios = $this->HerramientaModel->devolucionesUsuario($codigo);
            
            //$idSede = $_SESSION['sesion_active']['idSede'];
            //$ListarDevHerramienta = $this->HerramientaModel->devolverHerramienta($idSede);
            
            
            $datos = [
                //'ListarDevHerramienta' => $ListarDevHerramienta
                //'DevHerramientaUser' => $DevHerramientaUser,
                //'codigo' => $_POST['codigo']
            ];
            
            $this->vista('devolucion/herramienta/devherramienta', $datos);
        }
        
        Obtener Administradores
		public function ObtenerDevHerramienta($id_solicitud){
			$DevHerramientaUser = $this->HerramientaModel->DevHerramientaUser($id_solicitud);
			$datos = [
				
				'DevHerramientaUser' => $DevHerramientaUser,
				'id_solicitud' => $id_solicitud
				
			];
			$this->vista('devolucion/herramienta/devherramientaEditar', $datos);
		}
		//Editar Devolucion Usuario
		public function EditarDevolucionUs(){
			$datos = [
			    'id_solicitud' => trim($_POST['id_solicitud'])
			];
			$this->HerramientaModel->EditarDevUsuario($datos);
		}*/
    }
?>