<?php defined('BASEPATH') or exit('No se permite acceso directo');

	class Login extends Controlador {
		
		public function __construct(){
			$this->LoginModel = $this->modelo("LoginModel");
		}

		public function index(){
			$datos = [];
			$this->vista('login/index', $datos);
		}

		public function SignIn(){
			if (!isset($_SESSION['sesion_active'])):
				if ($_SERVER['REQUEST_METHOD'] == 'POST'): 
					$username = trim($_POST['USERNAME']);
					$password = trim($_POST['PASSWORD']);
					$numeroComprobacion = $_POST['numeroComprobacion'];
					$resulset = $this->LoginModel->Autenticacion($username);
					
					$idSede = $resulset->idSede;
					$idPersona = $resulset->IDPERSONA;
        		    $toolsRequested = $this->LoginModel->getToolsRequested($idSede);
        		    $toolsAvailiable = $this->LoginModel->getToolsAvailable($idSede);
        		    $materialsRequested = $this->LoginModel->getMaterialsRequested($idSede);
        		    $materialsAvailable = $this->LoginModel->getMaterialsAvailable($idSede);
        		    $equipmentsRequested = $this->LoginModel->getEquipmentsRequested($idSede);
        		    $equipmentsAvailable = $this->LoginModel->getEquipmentsAvailable($idSede);
        		    $toolsRequestedInstructor = $this->LoginModel->getToolsRequestedInstructor($idPersona, $idSede);
        		    $materialsRequestedInstructor = $this->LoginModel->getMaterialsRequestedInstructor($idPersona, $idSede);
        		    $equipmentsRequestedInstructor = $this->LoginModel->getEquipmentsRequestedInstructor($idPersona, $idSede);
        		    
					if (isset($resulset)):
						if($resulset->PASSWORD == $password):
							$datos = [
							    'toolsRequestedInstructor' => $toolsRequestedInstructor,
                		        'materialsRequestedInstructor' => $materialsRequestedInstructor,
                		        'equipmentsRequestedInstructor' => $equipmentsRequestedInstructor,
							    'toolsRequested' => $toolsRequested,
                		        'toolsAvailiable' =>$toolsAvailiable,
                		        'materialsRequested' => $materialsRequested,
                		        'materialsAvailable' => $materialsAvailable,
                		        'equipmentsRequested' => $equipmentsRequested,
                		        'equipmentsAvailable' => $equipmentsAvailable,
							    'idSede' => $resulset->idSede,
								'cod' => $resulset->IDPERSONA,
								'username' => $resulset->Tbl_usuario_USERNAME,
								'tipo_usuario' => $resulset->CARGO,
								'nombre' => $resulset->Tbl_persona_NOMBRES,
								'fecha_nacimiento' => $resulset->Tbl_persona_FECHANAC,
								'numero_documento' => $resulset->Tbl_persona_NUMDOCUMENTO,
								'p_apellido' => $resulset->Tbl_persona_PRIMERAPELLIDO,
								's_apellido' => $resulset->Tbl_persona_SEGUNDOAPELLIDO,
								'correo' => $resulset->Tbl_persona_CORREO,
								'tipo_documento' => $resulset->TIPO_DOCUMENTO,
								'estado' => $resulset->Tbl_usuario_ESTADO
							];
							$_SESSION['sesion_active'] = $datos;
							$tipoPersona = $_SESSION['sesion_active']['tipo_usuario'];
							
							
							if($numeroComprobacion == 1 and $tipoPersona == 'ADMINISTRADOR'):
							    $this->vista('home/index',$_SESSION['sesion_active']);
							elseif($numeroComprobacion == 2 and $tipoPersona == 'HERRAMENTERO'):
							    $this->vista('home/index',$_SESSION['sesion_active']);
							elseif($numeroComprobacion == 3 and $tipoPersona == 'INSTRUCTOR'):
							    $this->vista('home/index',$_SESSION['sesion_active']);
							else:
							    unset($_SESSION['sesion_active']);
							    $this->vista('login/index', ['message' => "ERROR DEBES INGRESAR COMO $tipoPersona."]);
							endif;
						
						else:
							$this->vista('login/index', ['message' => 'LA CONTRASEÑA QUE INGRESO ES INCORRECTA']);
						endif;
					endif;
				endif;	
			else:
				$this->vista('home/index');
			endif;					
		}
		
		public function consultarCorreo(){
		    $email = $_POST['email'];
		    $result = $this->LoginModel->consultarCorreo($email);
		    echo json_encode($result);
		}
		
		public function CambioContrasenia(){
		    $datos = [
		        'password1' => $_POST['password1'],
		        'email' => $_POST['email']
		    ];
		    $this->LoginModel->CambioContrasenia($datos);
		}
		
		public function CambioContraseniaActual(){
		    $datos = [
		        'idPersona' => $_SESSION['sesion_active']['cod'],
		        'password' => $_POST['password']
		    ];
		    $this->LoginModel->CambioContraseniaActual($datos);
		}
		
		// funcion para traer las cantidades de herramientas, equipos o materiales
		public function cantidades(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    $datos = [
		        'idSede' => $idSede,
		        'id' => $_POST['id'],
		        'tipo' =>$_POST['tipo']
		    ];
		    $result = $this->LoginModel->cantidades($datos);
		    echo json_encode($result);
		}
		
		public function getRequestApproved(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    $requestAproved = $this->LoginModel->getRequestApproved($idSede);
		    echo json_encode($requestAproved);
		}
		
		public function sendRequest(){
		    $datos = [
		        'tipo' => $_POST['tipo'],
		        'idRequest' => $_POST['idRequest'],
		        'codigo' => $_POST['codigo'],
		        'stock' => $_POST['stock'],
		        'cantidad' => $_POST['cantidad'],
		        'identificacion' => $_POST['identificacion'],
		        'idSede' => $_SESSION['sesion_active']['idSede'],
		    ];
		    $result = $this->LoginModel->sendRequest($datos);
		}
		
		public function Rechazarsolicitud(){
		    $datos = [
		      'id_solicitud' => $_POST['id_solicitud'],
		      'rejectModal' => $_POST['rejectModal'],
		      'estado'=> $_POST['estado']
		    ];
		    $result = $this->LoginModel->Rechazarsolicitud($datos);
		}
		
		public function deliverRequestHerramentero(){
		    $datos = [
		        'tipo' => $_POST['tipo'],
		        'idRequest' => $_POST['idRequest'],
		        'codigo' => $_POST['codigo'],
		        'stock' => $_POST['stock'],
		        'cantidad' => $_POST['cantidad'],
		        'identificacion' => $_POST['identificacion'],
		        'idSede' => $_SESSION['sesion_active']['idSede'],
		    ];
		    $result = $this->LoginModel->deliverRequestHerramentero($datos);
		}
		
		public function deliverRequest(){
		    $idRequest = $_POST['idRequest'];
		    $codigo = $_POST['codigo'];
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    $result = $this->LoginModel->deliverRequest($idRequest, $codigo, $idSede);
		}
		
		public function getRequest(){
		    $idRequest = $_POST['id'];
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    $result = $this->LoginModel->getRequest($idRequest, $idSede);
		    echo json_encode($result);
		}

		public function Logout(){
			unset($_SESSION['sesion_active']);
			$this->vista('login/index');
		}
		
		public function ShowRequest(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    $result = $this->LoginModel->ShowRequest($idSede);
		    echo json_encode($result);
		}
		
	    
	
	}