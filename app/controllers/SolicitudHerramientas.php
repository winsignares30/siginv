<?php

    Class SolicitudHerramientas Extends Controlador{
        
        public function __construct(){
            if(!isset($_SESSION['sesion_active'])):
                header('location:' . URL_SISINV . 'Login/Logout');
            endif; 
            
            $this->HerramientaModel = $this->modelo('HerramientaModel');
			$this->RegionalModel = $this->modelo('RegionalModel');
        }
        
        function generarCodigo(){
            $key = date('m/d/Y g:ia');
            return $key;
        }
        
        public function SolicitarHerramientas(){
            
            $idPersona =  $_SESSION['sesion_active']['cod'];
            $idSede =  $_SESSION['sesion_active']['idSede'];
            $LoadFichas = $this->HerramientaModel->LoadFichas($idSede);
            $result2 = $this->HerramientaModel->MostrarHerramientasSolicitadas($idPersona, $idSede);
            $datos= [
                'HerramientasSolicitadas' => $result2,
                'LoadFichas' => $LoadFichas
            ];
            
            $this->vista('solicitud/herramienta/SolicitarHerramienta', $datos);
            
        }
        public function SolicitarHerramientas2(){
            $idSede =  $_SESSION['sesion_active']['idSede'];
            $result = $this->HerramientaModel->SolicitarHerramientas($idSede);
            echo json_encode($result);
        }
        
        public function AgregarHerramienta(){
            $idHerramienta = trim($_POST['idHerramienta']);
            $result = $this->HerramientaModel->AgregarHerramienta($idHerramienta);
            echo json_encode($result);
        }
    }
?>