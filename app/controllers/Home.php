<?php 

	class Home extends Controlador{

		public function __construct(){
		    if(!isset($_SESSION['sesion_active'])):
		        header('location:'. URL_SISINV . 'Login/Logout');
		    endif;
		    $this->LoginModel = $this->modelo('LoginModel');
		}

		public function index(){ 
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    $toolsRequested = $this->LoginModel->getToolsRequested($idSede);
		    $toolsAvailiable = $this->LoginModel->getToolsAvailable($idSede);
		    $materialsRequested = $this->LoginModel->getMaterialsRequested($idSede);
		    $materialsAvailable = $this->LoginModel->getMaterialsAvailable($idSede);
		    $equipmentsRequested = $this->LoginModel->getEquipmentsRequested($idSede);
		    $equipmentsAvailable = $this->LoginModel->getEquipmentsAvailable($idSede);
		    
		    // instructores
		    $idPersona = $_SESSION['sesion_active']['cod'];
		    $toolsRequestedInstructor = $this->LoginModel->getToolsRequestedInstructor($idPersona, $idSede);
		    $materialsRequestedInstructor = $this->LoginModel->getMaterialsRequestedInstructor($idPersona, $idSede);
		    $equipmentsRequestedInstructor = $this->LoginModel->getEquipmentsRequestedInstructor($idPersona, $idSede);
		    $datos = [
		        'toolsRequestedInstructor' => $toolsRequestedInstructor,
		        'materialsRequestedInstructor' => $materialsRequestedInstructor,
		        'equipmentsRequestedInstructor' => $equipmentsRequestedInstructor,
		        'toolsRequested' => $toolsRequested,
		        'toolsAvailiable' =>$toolsAvailiable,
		        'materialsRequested' => $materialsRequested,
		        'materialsAvailable' => $materialsAvailable,
		        'equipmentsRequested' => $equipmentsRequested,
		        'equipmentsAvailable' => $equipmentsAvailable
		    ];
		    
		    
			if(isset($_SESSION['sesion_active'])):		
				$this->vista('home/index', $datos);
			else:
				$this->vista('login/index', $datos);
			endif;
		}
		
		public function getAmountToolsInstructor(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    $idPersona = $_SESSION['sesion_active']['cod'];
		    $result = $this->LoginModel->getAmountToolsInstructor($idPersona, $idSede);
		   echo json_encode($result);
		}
		
		public function getAmountMaterialsInstructor(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    $idPersona = $_SESSION['sesion_active']['cod'];
		    $result = $this->LoginModel->getAmountMaterialsInstructor($idPersona, $idSede);
		   echo json_encode($result);
		}
		
		public function getAmountEquipmentsInstructor(){
		    $idSede = $_SESSION['sesion_active']['idSede'];
		    $idPersona = $_SESSION['sesion_active']['cod'];
		    $result = $this->LoginModel->getAmountEquipmentsInstructor($idPersona, $idSede);
		   echo json_encode($result);
		}

	}
?>